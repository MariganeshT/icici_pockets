import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/validator.dart';

class CustomRadioTextField extends StatefulWidget {
  final String hintText;
  final bool obscureText;
  final TextEditingController controller;
  final Widget? suffixWidget;
  final bool isEnable;
  final bool isReadOnly;
  final Function? onTapped;
  final Widget? prefixWidget;
  final TextInputType keyBoardType;
  final int? maximumWordCount;
  final Color titleColor;
  final Color? borderColor;
  final Color textColor;
  final bool isHighlighted;
  final Color highlightColor;
  final FocusNode? focusNode;
  final Color? focusTextColor;
  final String? descriptionText;
  List<String> validationRules = [];
  Function? onChange;

  // ignore: avoid_unused_constructor_parameters

  CustomRadioTextField(

      // ignore: invalid_required_positional_param
      @required this.hintText,
      // ignore: invalid_required_positional_param
      @required this.controller,
      {this.obscureText = false,
      this.suffixWidget,
      this.prefixWidget,
      this.isEnable = true,
      this.onTapped,
      this.isReadOnly = false,
      this.maximumWordCount,
      this.titleColor = ColorResource.color222222,
      this.textColor = ColorResource.color222222,
      this.borderColor = ColorResource.colorb9b9bf,
      this.isHighlighted = false,
      this.highlightColor = ColorResource.colorf5f5f7,
      this.focusNode,
      this.focusTextColor,
      this.keyBoardType = TextInputType.name,
      this.descriptionText,
      this.onChange,
      this.validationRules = const []});

  @override
  _CustomRadioTextFieldState createState() => _CustomRadioTextFieldState();
}

class _CustomRadioTextFieldState extends State<CustomRadioTextField> {
  @override
  void initState() {
    if (widget.focusNode != null) {
      widget.focusNode!.addListener(() {
        // setState(() {
        //   // FocusScope.of(context).requestFocus(widget.focusNode);
        // });
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      textInputAction: TextInputAction.done,

      validator: (String? value) {
        if (widget.validationRules.isNotEmpty) {
          final ValidationState validationStatus =
              Validator.validate(value ?? '', rules: widget.validationRules);
          if (!validationStatus.status) {
            return validationStatus.error;
          }
        }
        return null;
      },

      onEditingComplete: () {
        setState(() {});
        FocusScope.of(context).requestFocus(FocusNode());
      },

      onFieldSubmitted: (String text) {
        setState(() {});
        FocusScope.of(context).requestFocus(FocusNode());
      },

      onTap: () {
        if (widget.onChange != null) {
          widget.onChange!(widget.controller.text);
        }
        setState(() {});
        if (widget.onTapped != null) {
          widget.onTapped!();
        }
      },
      onChanged: (String q) {
        // if (widget.onChange != null) widget.onChange!(q);
        setState(() {});
      },
      inputFormatters: [
        // if (widget.maximumWordCount != null)
        //   LengthLimitingTextInputFormatter(widget.maximumWordCount),
      ],

      autocorrect: false,
      enableSuggestions: false,
      obscureText: widget.obscureText,
      controller: widget.controller,
      readOnly: widget.isReadOnly,
      enabled: widget.isEnable,
      keyboardType: widget.keyBoardType,
      // maxLines: 1,

      focusNode: widget.focusNode,
      style: TextStyle(
          color: (widget.focusNode != null && widget.focusNode!.hasFocus)
              ? widget.focusTextColor
              : widget.textColor,
          fontFamily: 'Mulish-Regular',
          fontSize: 18),
      decoration: InputDecoration(
          prefix: Text.rich(
            TextSpan(children: <InlineSpan>[
              WidgetSpan(
                child: Padding(
                  child: Icon(
                    ((widget.focusNode != null && widget.focusNode!.hasFocus) ||
                            widget.controller.text.isNotEmpty)
                        ? Icons.radio_button_checked_rounded
                        : Icons.radio_button_off,
                    color: ColorResource.color203718,
                    size: 24,
                  ),
                  padding: const EdgeInsets.only(right: 10.0),
                ),
              ),
            ]),
          ),
          fillColor: ColorResource.colorFFFFFF,
          filled: true,
          labelText: widget.hintText,
          isDense: false,
          counterText: widget.descriptionText,
          errorMaxLines: 1,
          counterStyle: const TextStyle(
              color: ColorResource.color222222,
              fontFamily: 'Mulish-Regular',
              fontWeight: FontWeight.normal,
              fontStyle: FontStyle.normal,
              fontSize: 12),
          labelStyle: TextStyle(
              color: (widget.focusNode != null && widget.focusNode!.hasFocus)
                  ? ColorResource.colorF58220
                  : ColorResource.color8d9091,
              fontFamily: 'Mulish-Regular',
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.w600,
              fontSize: 14),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide(color: ColorResource.colorF58220)),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide(color: ColorResource.colorEFEFEF)),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide(color: ColorResource.colorEFEFEF)),
          disabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide(color: ColorResource.colorEFEFEF)),
          errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: const BorderSide(color: Colors.red))),
    );
  }
}
