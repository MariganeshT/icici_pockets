
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:flutter/rendering.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:icici/utils/app_utils.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:path_provider/path_provider.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:share/share.dart';

import 'customText.dart';
import 'custom_button.dart';

class DisplayQR extends StatefulWidget {
  final String qrText;
  final GestureTapCallback? onTap;
  DisplayQR(
    this.qrText, 
    {
      this.onTap,
    }
    );

  @override
  _DisplayQRState createState() => _DisplayQRState();
}

class _DisplayQRState extends State<DisplayQR> {
  GlobalKey _globalKey = new GlobalKey();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

   var capturedBytes;

   Future<void> _shareQRcode() async {
   final RenderRepaintBoundary boundary =
        _globalKey.currentContext!.findRenderObject() as RenderRepaintBoundary;
   final ui.Image image = await boundary.toImage();
    final ByteData? byteData = 
    await image.toByteData(format: ui.ImageByteFormat.png);
    setState(() {
      capturedBytes = byteData!.buffer.asUint8List();
    });
      final Directory tempDir = await getTemporaryDirectory();
      final File file = await File('${tempDir.path}/pocketsQR.png').create();
      await file.writeAsBytes(capturedBytes);
      // widget.onTap;
      await Share.shareFiles([file.path],
                                  text: widget.qrText,subject: widget.qrText,);

  }

  _saveQRcode() async {
     final RenderRepaintBoundary boundary =
        _globalKey.currentContext!.findRenderObject() as RenderRepaintBoundary;
    final ui.Image image = await boundary.toImage();
    final ByteData? byteData = 
    await image.toByteData(format: ui.ImageByteFormat.png);
    setState(() {
      capturedBytes = byteData!.buffer.asUint8List();
    });
     await ImageGallerySaver.saveImage(
           capturedBytes,
           quality: 97,
           name: 'PocketsQR');
  }

  @override
  Widget build(BuildContext context) {
    return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
            return WillPopScope(
              onWillPop: () async => false,
              child: Container(
                padding: MediaQuery.of(context).viewInsets,
                child: Container(
                  decoration: const BoxDecoration(
                    color: ColorResource.colorFFFFFF,
                    borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(32.0),
                    topRight: Radius.circular(32.0)),
                    ),
                  
                  child: Stack(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 14, top: 12),
                        child: GestureDetector(
                          onTap: (){
                            Navigator.pop(context);
                          },
                          child: Image.asset(
                              ImageResource.close)
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(16, 17, 16, 35),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            CustomText(StringResource.qrCode,
                                        style: Theme.of(context).textTheme.headline5),
                              const SizedBox(height: 35,),
                              Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 90),
                                child: CustomText(
                                  StringResource.qrDescription,
                                textAlign: TextAlign.center,
                                          style: 
                                          Theme.of(context).textTheme.subtitle1!.copyWith(
                                            color: ColorResource.color787878)),
                              ),
                              const SizedBox(height: 18,),
                              RepaintBoundary(
                                key: _globalKey,
                                child: QrImage(
                                  backgroundColor: Colors.white,
                                        foregroundColor: Colors.black,
                                  data: widget.qrText,
                                  size: 210,
                                 
                                ),
                              ),
                              const SizedBox(height: 30,),
                              SizedBox(
                                width: 135,
                                child: CustomButton(StringResource.share,
                                fontSize: 14,
                                textColor: ColorResource.colorFF781F,
                                isLeading: true,
                                buttonBackgroundColor: ColorResource.colorFFFFFF,
                                trailingWidget: Image.asset(
                                  ImageResource.sharee, ),
                                  // onTap:widget.onTap,
                                onTap: (){
                                  _shareQRcode();
                                },
                                ),
                              ),
                              // if (capturedBytes != null) Image.memory(capturedBytes),
                              // const SizedBox(height: 30,),
                              // GestureDetector(
                              //   child: CustomText(
                              //      StringResource.saveToPhotos,
                              //       textAlign: TextAlign.center,
                              //             style: 
                              //             Theme.of(context)
                              //             .textTheme.subtitle1!
                              //             .copyWith(fontWeight: FontWeight.w700,
                              //             color: ColorResource.colorFF781F,),
                              //           ),
                              //           onTap: (){
                              //             _saveQRcode();
                              //             AppUtils.showToast(
                              //               StringResource.photoSaved, 
                              //               gravity: ToastGravity.BOTTOM);
                              //           },
                              // ),
                              const SizedBox(height: 45,),
                              Padding(
                                padding: const EdgeInsets
                                .symmetric(horizontal: 30),
                                child: CustomText(
                                 StringResource.qrNote,
                                textAlign: TextAlign.center,
                                          style: 
                                          Theme.of(context)
                                          .textTheme.subtitle2),
                              ),
                              
                          ],
                        ),
                      ),
                    ],
                  ),),),
            );
              }
            );
  }
}
