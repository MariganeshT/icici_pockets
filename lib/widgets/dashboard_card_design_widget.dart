import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:icici/router.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';

class DashBoardCardDesign extends StatefulWidget {
  const DashBoardCardDesign({Key? key}) : super(key: key);

  @override
  _DashBoardCardDesignState createState() => _DashBoardCardDesignState();
}

class _DashBoardCardDesignState extends State<DashBoardCardDesign> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,

      // margin: EdgeInsets.only(top: 67),
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: [
          Wrap(
            // direction: Axis.horizontal,
            children: [
              Container(
                margin: const EdgeInsets.only(
                  left: 12,
                ),
                decoration: BoxDecoration(
                  gradient: new LinearGradient(
                    begin: Alignment.centerLeft,
                    colors: [
                       ColorResource.colorffe4d0,
                       ColorResource.colorfff2e8,
                    ],
                    end: Alignment.centerRight,
                  ),
                 
                  borderRadius: BorderRadius.circular(24),
                  // image: DecorationImage(
                  //   image: AssetImage(ImageResource.wallet_background),
                  //   fit: BoxFit.cover,
                  // ),
                ),
                child: Stack(
                  children: [
                    Image.asset(
                      ImageResource.dashboardScreencardbg1,
                      fit: BoxFit.contain,
                    ),
                    Container(
                      height: 290,
                      width: 250,
                      padding: const EdgeInsets.all(15),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CustomText(StringResource.pocketWallet,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1!
                                  .copyWith(
                                      color: ColorResource.color4F0437,
                                      fontWeight: FontWeight.w600)),
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 2,
                            ),
                            child: CustomText(
                              '4333333337766',
                              color: ColorResource.color4F0437,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 32),
                            child: CustomText(
                              StringResource.walletBalance,
                              // lineHeight: 1.8,
                              color: ColorResource.color787878,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 6),
                            child: CustomText(
                              '₹0.00',
                              fontSize: 24,
                              fontWeight: FontWeight.w800,
                              color: ColorResource.color222222,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 30),
                            child: Row(
                              children: [
                                Container(
                                  height: 47,
                                  width: 123,
                                  child: ElevatedButton(
                                    onPressed: () {},
                                    child: CustomText(
                                      StringResource.addFunds,
                                      fontWeight: FontWeight.bold,
                                      color: ColorResource.colorFFFFFF,
                                    ),
                                    style: ElevatedButton.styleFrom(
                                        primary: ColorResource.colorFF781F),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 10),
                                  child: GestureDetector(
                                    child: CustomText(
                                      'Statement',
                                      fontWeight: FontWeight.w600,
                                      isUnderLine: true,
                                    ),
                                    onTap: ()  {
                                       Navigator.pushNamed(context,
                                          AppRoutes.statementScreen);
                                    },
                                  ),
                                )
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 16, top: 12),
                            child: CustomText(
                              StringResource.upiId +
                                  ' ' +
                                  '643233333357@pockets',
                              fontWeight: FontWeight.w600,
                              color: ColorResource.color222222,
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 12),
                height: 290,
                width: 250,
                decoration: BoxDecoration(
                    color: ColorResource.color060145,
                    borderRadius: BorderRadius.circular(24)),
                child: Column(
                  children: [
                    Stack(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(24),
                          child: Container(
                            height: 290,
                            width: 250,
                            child: Image.asset(
                              ImageResource.dashboardScreencardbg2,
                            ),
                            alignment: Alignment.bottomRight,
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.all(15),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                height: 55,
                                width: 130,
                                child: CustomText(
                                  StringResource.linkYourIciciaccount,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: ColorResource.colorFFFFFF,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 8,
                                ),
                                child: CustomText(
                                  StringResource.linkIciciQuotes,
                                  color: ColorResource.colorFFFFFF,
                                ),
                              ),
                              Container(
                                height: 47,
                                width: 123,
                                margin: const EdgeInsets.only(top: 84),
                                child: ElevatedButton(
                                  onPressed: () async {
                                    await Navigator.pushNamed(context,
                                        AppRoutes.authenticateAccountScreen);
                                  },
                                  child: CustomText(
                                    StringResource.linkAccount,
                                    fontWeight: FontWeight.bold,
                                    color: ColorResource.colorFFFFFF,
                                  ),
                                  style: ElevatedButton.styleFrom(
                                      primary: ColorResource.colorED2020),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 12),
                height: 290,
                width: 250,
                decoration: BoxDecoration(
                    color: ColorResource.color460037,
                    borderRadius: BorderRadius.circular(24)),
                child: Column(
                  children: [
                    Stack(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 16, top: 14),
                              child: CustomText(
                                StringResource.applyPhysicalCard,
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: ColorResource.colorFFFFFF,
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.only(
                                  left: 16, top: 18, right: 16),
                              padding: const EdgeInsets.all(15),
                              decoration: BoxDecoration(
                                color: ColorResource.colorED2020,
                                borderRadius: BorderRadius.circular(16),
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      CustomText(
                                        'Name',
                                        color: ColorResource.colorFFFFFF,
                                      ),
                                      Image.asset(
                                          ImageResource.dashboardVisaImage), //
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 14),
                                    child: CustomText(
                                      '3333  1245  6666  4321',
                                      color: ColorResource.colorFFFFFF,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 16, bottom: 10),
                                    child: Row(
                                      children: [
                                        Column(
                                          children: [
                                            CustomText(
                                              StringResource.validFrom,
                                              fontSize: 8,
                                              fontWeight: FontWeight.w500,
                                              color: ColorResource.colorFFFFFF
                                                  .withOpacity(0.6),
                                            ),
                                            CustomText(
                                              '01/15',
                                              color: ColorResource.colorFFFFFF,
                                            ),
                                          ],
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 27),
                                          child: Column(
                                            children: [
                                              CustomText(
                                                StringResource.validTo,
                                                fontSize: 8,
                                                fontWeight: FontWeight.w500,
                                                color: ColorResource.colorFFFFFF
                                                    .withOpacity(0.6),
                                              ),
                                              CustomText(
                                                '01/21',
                                                color:
                                                    ColorResource.colorFFFFFF,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              height: 47,
                              width: 123,
                              margin: const EdgeInsets.only(top: 20, left: 16,bottom: 2),
                              child: ElevatedButton(
                                onPressed: () {
                                  Navigator.pushNamed(context,
                                      AppRoutes.requestPhysicalCardScreen);
                                },
                                child: CustomText(
                                  StringResource.applyNow,
                                  fontWeight: FontWeight.bold,
                                  color: ColorResource.colorFFFFFF,
                                ),
                                style: ElevatedButton.styleFrom(
                                    primary: ColorResource.colorFF781F),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 12, right: 12),
                height: 290,
                width: 250,
                decoration: BoxDecoration(
                    color: ColorResource.color060145,
                    borderRadius: BorderRadius.circular(24)),
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Stack(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(24),
                            child: Container(
                              height: 290,
                              width: 250,
                              child: Image.asset(
                                ImageResource.dashboardScreencardbg4,
                                fit: BoxFit.fitHeight,
                              ),
                              alignment: Alignment.bottomRight,
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.all(15),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  height: 60,
                                  width: 85,
                                  child: CustomText(
                                    'Earn CashBack',
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: ColorResource.colorFFFFFF,
                                  ),
                                ),
                                // CustomText(
                                //   'CashBack',
                                //   fontSize: 20,
                                //   fontWeight: FontWeight.bold,
                                //   color: ColorResource.colorFFFFFF,
                                // ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    top: 20,
                                  ),
                                  child: CustomText(
                                    'upto',
                                    color: ColorResource.colorFFFFFF,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 4),
                                  child: CustomText(
                                    '₹1000.00',
                                    fontSize: 24,
                                    fontWeight: FontWeight.w900,
                                    color: ColorResource.colorFFFFFF,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 8),
                                  child: CustomText(
                                    'Per Referral*',
                                    fontWeight: FontWeight.w600,
                                    color: ColorResource.colorFFFFFF,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 6),
                                  child: CustomText(
                                    '*Terms and Conditions apply ',
                                    fontSize: 8,
                                    color: ColorResource.colorFFFFFF,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 20),
                                  child: Container(
                                    height: 47,
                                    width: 123,
                                    child: ElevatedButton(
                                      onPressed: () {
                                        Navigator.pushNamed(context,
                                            AppRoutes.referAndEarn);
                                      },
                                      child: Center(
                                        child: CustomText(
                                          StringResource.refferal,
                                          fontWeight: FontWeight.bold,
                                          color: ColorResource.colorFFFFFF,
                                        ),
                                      ),
                                      style: ElevatedButton.styleFrom(
                                          primary: ColorResource.colorED2020),

                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
