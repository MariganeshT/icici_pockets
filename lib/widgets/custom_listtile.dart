import 'package:flutter/material.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';

import 'customText.dart';

class CustomListTile extends StatefulWidget {
  final Widget leadingImage;
  final String title;
  final String? subtitle;
  final Widget? trailing;
  final GestureTapCallback? onTap;

  CustomListTile(
    this.leadingImage,
    this.title, {
    this.subtitle,
    this.trailing,
    this.onTap,
  });

  @override
  _CustomListTileState createState() => _CustomListTileState();
}

class _CustomListTileState extends State<CustomListTile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 65,
      child: ListTile(
          onTap: () {
            widget.onTap!();
          },
          contentPadding: EdgeInsets.all(10),
          isThreeLine: true,
          minVerticalPadding: 18,
          leading: SizedBox(width: 24, height: 24, child: widget.leadingImage),
          title: Transform(
            transform: Matrix4.translationValues(-18, 0.0, 0.0),
            child: CustomText(
              widget.title,
              style: Theme.of(context)
                  .textTheme
                  .subtitle1!
                  .copyWith(color: ColorResource.color222222),
            ),
          ),
          subtitle: widget.subtitle != null
              ? Transform(
                  transform: Matrix4.translationValues(-18, 0.0, 0.0),
                  child: Padding(
                    padding: const EdgeInsets.only(top: 0),
                    child: CustomText(
                      widget.subtitle!,
                      style: Theme.of(context)
                          .textTheme
                          .subtitle1!
                          .copyWith(color: ColorResource.color787878),
                    ),
                  ),
                )
              : Container(),
          trailing: Image.asset(
            ImageResource.arrow,
          )),
    );
  }
}
