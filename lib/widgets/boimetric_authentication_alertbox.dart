import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/font.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_button.dart';

class FingerprintAlertDialogue extends StatefulWidget {
  FingerprintAlertDialogue(this.onAcceptTapped, this.onNotAllowTapped);
  Function onAcceptTapped;
  Function onNotAllowTapped;
  // FingerprintAlertDialogue(this.onProceedTapped);
  GlobalKey<FormFieldState<String>> searchFormKey =
      GlobalKey<FormFieldState<String>>();
  @override
  _FingerprintAlertDialogueState createState() =>
      _FingerprintAlertDialogueState();
}

class _FingerprintAlertDialogueState extends State<FingerprintAlertDialogue> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(),
    );
  }

  Widget contentBox() {
    return Stack(
      children: <Widget>[
        Container(
          padding: const EdgeInsets.only(left: 8, right: 8, top: 10),
          margin: const EdgeInsets.only(top: 16),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
          ),
          child: Padding(
            padding: const EdgeInsets.only(left: 16, right: 16, top: 10),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                CustomText(
                  StringResource.doYouWantAllowFaceid,
                  textAlign: TextAlign.center,
                  font: Font.sourceSansProRegular,
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  color: Colors.black,
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 24, top: 10),
                  child: CustomText(
                    StringResource.loginInstantly,
                    textAlign: TextAlign.center,
                    color: ColorResource.color787878,
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                      child: CustomButton(
                        StringResource.dontAllow,
                        textColor: ColorResource.color0082FB,
                        buttonBackgroundColor: Colors.white,
                        borderColor: Colors.white,
                        onTap: () {
                          widget.onNotAllowTapped();
                        },
                      ),
                    ),
                    Expanded(
                      child: CustomButton(
                        StringResource.ok,
                        buttonBackgroundColor: Colors.white,
                        textColor: ColorResource.color0082FB,
                        borderColor: Colors.white,
                        onTap: () {
                          widget.onAcceptTapped();
                        },
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
