import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/validator.dart';

class CustomTextField extends StatefulWidget {
  final String hintText;
  final bool obscureText;
  final TextEditingController controller;
  final Widget? suffixWidget;
  final bool isEnable;
  final bool isReadOnly;
  final Function? onTapped;
  final Widget? prefixWidget;
  final TextInputType keyBoardType;
  final int? maximumWordCount;
  final Color titleColor;
  final Color? borderColor;
  final Color textColor;
  final bool isHighlighted;
  final Color highlightColor;
  final FocusNode? focusNode;
  final Color? focusTextColor;
  final String? descriptionText;
  final List<TextInputFormatter>? inputformaters;
  List<String> validationRules = [];
  Function? oncomplete;
  final Function? onEditing;
  final Function(bool)? validatorCallBack;

  // ignore: avoid_unused_constructor_parameters

  CustomTextField(

      // ignore: invalid_required_positional_param
      @required this.hintText,
      // ignore: invalid_required_positional_param
      @required this.controller,
      {this.obscureText = false,
      this.suffixWidget,
      this.prefixWidget,
      this.isEnable = true,
      this.onTapped,
      this.isReadOnly = false,
      this.maximumWordCount,
      this.titleColor = ColorResource.color222222,
      this.textColor = ColorResource.color222222,
      this.borderColor = ColorResource.colorb9b9bf,
      this.isHighlighted = false,
      this.highlightColor = ColorResource.colorf5f5f7,
      this.focusNode,
      this.focusTextColor,
      this.keyBoardType = TextInputType.name,
      this.descriptionText,
      this.oncomplete,
      this.validatorCallBack,
      this.onEditing,
      this.inputformaters,
      this.validationRules = const []});

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  @override
  void initState() {
    // if (widget.focusNode != null) {
    //   widget.focusNode!.addListener(() {
    //     setState(() {
    //       FocusScope.of(context).requestFocus(widget.focusNode);
    //     });
    //   });
    // }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      textInputAction: TextInputAction.done,

      validator: (String? value) {
        if (widget.validationRules.isNotEmpty) {
          final ValidationState validationStatus =
              Validator.validate(value ?? '', rules: widget.validationRules);
          widget.validatorCallBack!(validationStatus.status);
          if (!validationStatus.status) {
            return validationStatus.error;
          }
        }
        return null;
      },

      onEditingComplete: () {
        setState(() {});
        FocusScope.of(context).requestFocus(FocusNode());
        if (widget.onEditing != null) {
          widget.onEditing!();
        }
      },

      onFieldSubmitted: (text) {
        setState(() {});
        FocusScope.of(context).requestFocus(FocusNode());
        if (widget.onEditing != null) {
          widget.onEditing!();
        }
      },

      onTap: () {
        setState(() {});
        if (widget.onTapped != null) {
          widget.onTapped!();
        }
      },
      onChanged: (q) {
        setState(() {});
      },
      // ignore: prefer_const_literals_to_create_immutables
      // inputFormatters: [
      //   // if (widget.maximumWordCount != null)
      //   //   LengthLimitingTextInputFormatter(widget.maximumWordCount),
      // ],
      inputFormatters: widget.inputformaters,

      autocorrect: false,
      enableSuggestions: false,
      obscureText: widget.obscureText,
      controller: widget.controller,
      readOnly: widget.isReadOnly,
      enabled: widget.isEnable,
      keyboardType: widget.keyBoardType,
      // maxLines: 1,

      focusNode: widget.focusNode,
      style: Theme.of(context).textTheme.bodyText1!.copyWith(
          color: (widget.focusNode != null && widget.focusNode!.hasFocus)
              ? widget.focusTextColor
              : widget.textColor),

      decoration: InputDecoration(
          fillColor: ColorResource.colorFFFFFF,
          filled: true,
          labelText: widget.hintText,
          isDense: false,
          counterText: widget.descriptionText,
          errorMaxLines: 1,
          errorStyle: Theme.of(context)
              .textTheme
              .subtitle1!
              .copyWith(color: Colors.red),
          counterStyle: const TextStyle(
              color: ColorResource.color222222,
              fontFamily: 'Mulish-Regular',
              fontWeight: FontWeight.normal,
              fontStyle: FontStyle.normal,
              fontSize: 12),
          // errorText: validatePassword(widget.controller.text.trim()),
          labelStyle: Theme.of(context).textTheme.subtitle1!.copyWith(
              color: (widget.focusNode != null && widget.focusNode!.hasFocus)
                  ? ColorResource.colorF58220
                  : ColorResource.color8d9091),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide(color: ColorResource.colorF58220)),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide(color: ColorResource.colorEFEFEF)),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide(color: ColorResource.colorEFEFEF)),
          disabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide(color: ColorResource.colorEFEFEF)),
          errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: const BorderSide(color: Colors.red))),
    );
  }

  String? validatePassword(String value) {
    if (widget.validationRules.isNotEmpty) {
      final ValidationState validationStatus =
          Validator.validate(value, rules: widget.validationRules);
      if (!validationStatus.status) {
        return validationStatus.error;
      }
    }
    return null;
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
