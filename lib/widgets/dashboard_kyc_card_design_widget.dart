import 'package:flutter/material.dart';
import 'package:icici/languages/app_locale_constant.dart';
import 'package:icici/widgets/widget_utils.dart';

class DashboardKycCardDesign extends StatefulWidget {
  const DashboardKycCardDesign({Key? key}) : super(key: key);

  @override
  _DashboardKycCardDesignState createState() => _DashboardKycCardDesignState();
}

class _DashboardKycCardDesignState extends State<DashboardKycCardDesign> {
  Locale? _locale;
  PageController _controller = PageController(
    initialPage: 0,
  );

  @override
  void didChangeDependencies() {
    getLocale().then((Locale locale) {
      setState(() {
        _locale = locale;
      });
    });
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    /* return WidgetUtils.shopAndEarn(context, 'Shop & Earn',
        '15% OFF on your bill', 'Use Promo Code: SHOP15');*/
    /*  return WidgetUtils.KYCTrack(context, 'Complete your KYC',
        'Lorem ipsum dolor consectetur adipiscing elit', 'Complete');*/
    return WidgetUtils.KYCTrack(context, 'Track KYC Status',
        'Lorem ipsum dolor consectetur adipiscing elit', 'Track');
  }
}
