import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:icici/model/all_services_child_model.dart';
import 'package:icici/model/notification_model.dart';
import 'package:icici/model/statement_model.dart';
import 'package:icici/router.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';

import 'customText.dart';

class WidgetUtils {
  static Widget indicatorViewWidget(int selectedIndex) {
    return Flexible(
      child: Container(
        width: 70,
        height: 70,
        alignment: Alignment.center,
        child: WidgetUtils?.buildIndicatorView(
            4,
            ColorResource.colorFF781F,
            ColorResource.colorFFFFFF.withOpacity(0.5),
            ColorResource.colorFF781F,
            selectedIndex),
      ),
    );
  }

  static Widget buildIndicatorView(int size, Color activeColor,
      Color inActiveColor, Color borderColor, int selectedIndex) {
    return ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: size,
        itemBuilder: (BuildContext context, int index) {
          return _indicator(index == selectedIndex ? true : false, size,
              selectedIndex, index, activeColor, inActiveColor, borderColor);
        });
  }

  static Widget _indicator(bool isActive, int totalLength, int selectedindex,
      int index, Color activeColor, Color inActiveColor, Color borderColor) {
    return Container(
      height: 10,
      child: AnimatedContainer(
        duration: const Duration(milliseconds: 150),
        margin: const EdgeInsets.symmetric(horizontal: 2.0),
        height: isActive ? 16 : 8.0,
        width: isActive ? 16 : 8.0,
        decoration: BoxDecoration(
          border: index <= selectedindex
              ? Border.all(color: borderColor, width: 4.0)
              : Border.all(color: Colors.transparent),
          shape: BoxShape.circle,
          color: isActive
              ? Colors.white
              : index <= selectedindex
                  ? activeColor
                  : inActiveColor,
        ),
      ),
    );
  }

  static Widget buildPageViewIndicator(int size, Color activeColor,
      Color inActiveColor, Color borderColor, int selectedIndex) {
    return ListView.builder(
        shrinkWrap: true,
        primary: true,
        scrollDirection: Axis.horizontal,
        itemCount: size,
        itemBuilder: (BuildContext context, int index) {
          return pageViewIndicator(index == selectedIndex ? true : false,
              inActiveColor, borderColor);
        });
  }

  static Widget pageViewIndicator(
      bool isActive, Color inActiveColor, Color borderColor) {
    return Align(
      child: AnimatedContainer(
        duration: const Duration(milliseconds: 150),
        margin: const EdgeInsets.symmetric(horizontal: 4.0),
        width: isActive ? 30 : 8.0,
        height: isActive ? 5 : 8,
        decoration: BoxDecoration(
          borderRadius:
              isActive ? BorderRadius.circular(10) : BorderRadius.circular(30),
          color: isActive ? Colors.white : inActiveColor,
        ),
      ),
    );
  }

  static Widget KYCTrack(
      BuildContext context, String title, String subTitle, String buttonName) {
    return Container(
      alignment: Alignment.center,
      margin: const EdgeInsets.all(10),
      padding: const EdgeInsets.all(18),
      height: 119,
      decoration: BoxDecoration(
          color: ColorResource.color460037,
          borderRadius: BorderRadius.circular(20)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CustomText(title,
                    style: Theme.of(context).textTheme.subtitle1!.copyWith(
                        color: ColorResource.colorFFFFFF,
                        fontWeight: FontWeight.w900)),
                const SizedBox(
                  height: 10,
                ),
                CustomText(subTitle,
                    style: Theme.of(context).textTheme.subtitle1!.copyWith(
                        color: ColorResource.colorFFFFFF,
                        fontWeight: FontWeight.w400)),
              ],
            ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: const EdgeInsets.only(right: 16.0),
                  child: ElevatedButton(
                    onPressed: () async {
                      await Navigator.pushNamed(
                          context, AppRoutes.trackShipmentScreen);
                    },
                    child: CustomText(buttonName,
                        style: Theme.of(context).textTheme.subtitle1!.copyWith(
                            color: ColorResource.colorFFFFFF,
                            fontWeight: FontWeight.w700)),
                    style: ElevatedButton.styleFrom(
                        primary: ColorResource.colorFF781F),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  static Widget shopAndEarn(
      BuildContext context, String adsName, String title, String subTitle,
      {String? buttonName = ''}) {
    return Container(
      alignment: Alignment.center,
      margin: const EdgeInsets.all(10),
      padding: const EdgeInsets.all(10),
      height: 119,
      decoration: BoxDecoration(
          color: ColorResource.color060145,
          borderRadius: BorderRadius.circular(20)),
      child: Row(
        children: [
          Container(
            child: Image(
                width: 100,
                fit: BoxFit.fitWidth,
                alignment: Alignment.center,
                image: AssetImage(ImageResource.offer_image)),
          ),
          SizedBox(
            width: 15,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CustomText(
                adsName,
                style: Theme.of(context).textTheme.subtitle1!.copyWith(
                    color: ColorResource.colorFFFFFF,
                    fontWeight: FontWeight.w700),
              ),
              const SizedBox(
                height: 5,
              ),
              CustomText(
                title,
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                    color: ColorResource.colorFFFFFF,
                    fontWeight: FontWeight.w800),
              ),
              const SizedBox(
                height: 5,
              ),
              CustomText(
                subTitle,
                style: Theme.of(context).textTheme.subtitle1!.copyWith(
                    color: ColorResource.colorFFFFFF,
                    fontWeight: FontWeight.w300),
              ),
            ],
          ),
          Visibility(
            visible: buttonName != '' ? true : false,
            child: Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    onPressed: () {},
                    child: CustomText(buttonName!,
                        style: Theme.of(context).textTheme.subtitle1!.copyWith(
                            color: ColorResource.colorFFFFFF,
                            fontWeight: FontWeight.bold)),
                    style: ElevatedButton.styleFrom(
                        primary: ColorResource.colorFF781F),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget statementListWidget(
      StatementModel statementModel, BuildContext buildContext) {
    return ListTile(
      leading: Container(
          alignment: Alignment.center,
          height: 40,
          width: 40,
          child: statementModel.imageAddress == null
              ? Container(
                  height: 40,
                  width: 40,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                    // borderRadius: BorderRadius.circular(10),
                    color: ColorResource.colorFF781F,
                  ),
                  child: CustomText(
                    getInitials(statementModel.name).toString(),
                    style: Theme.of(buildContext).textTheme.bodyText1!.copyWith(
                        color: ColorResource.colorFFFFFF,
                        fontWeight: FontWeight.w600),
                  ),
                )
              : Container(
                  width: 40,
                  height: 40,
                  child: CircleAvatar(
                    maxRadius: 1,
                    backgroundImage: NetworkImage(statementModel.imageAddress!),
                    backgroundColor: ColorResource.color787878,
                  ))
          /* Image.network(
                statementModel.imageAddress!,

                loadingBuilder: (BuildContext context, Widget child,
                    ImageChunkEvent? loadingProgress) {
                  return Image.asset(
                    ImageResource.airtelPNG,
                    width: 40,
                    height: 48,
                  );
                },
              ),*/
          ),
      trailing: CustomText(
        '''
${statementModel.debitORCredit == 1 ? '+' : '-'} ₹${statementModel.amount}''',
        style: Theme.of(buildContext).textTheme.subtitle1!.copyWith(
            color: statementModel.debitORCredit == 1
                ? ColorResource.color11AB04
                : ColorResource.colorF92538,
            fontWeight: FontWeight.w600),
      ),
      title: CustomText(
        statementModel.name,
        style: Theme.of(buildContext).textTheme.subtitle1!.copyWith(
            color: ColorResource.color222222, fontWeight: FontWeight.w700),
      ),
      subtitle: CustomText(
        statementModel.dateTime,
        style: Theme.of(buildContext).textTheme.subtitle2!.copyWith(
            color: ColorResource.color787878, fontWeight: FontWeight.w400),
      ),
    );
  }

  Widget notificationListWidget(
      NotificationModel notificationModel, BuildContext buildContext) {
    return ListTile(
      leading: Container(
          alignment: Alignment.center,
          height: 40,
          width: 40,
          child: notificationModel.imageAddress == null
              ? Container(
                  height: 40,
                  width: 40,
                  alignment: Alignment.center,
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                    color: ColorResource.colorFF781F,
                  ),
                  child: CustomText(
                    getInitials(notificationModel.name).toString(),
                    style: Theme.of(buildContext).textTheme.bodyText1!.copyWith(
                        color: ColorResource.colorFFFFFF,
                        fontWeight: FontWeight.w600),
                  ),
                )
              : const SizedBox(
                  width: 40,
                  height: 40,
                  child: CircleAvatar(
                    maxRadius: 1,
                    backgroundImage: NetworkImage(
                        /*notificationModel.imageAddress!*/
                        'https://cdn.arstechnica.net/wp-content/uploads/2016/02/5718897981_10faa45ac3_b-640x624.jpg'),
                    backgroundColor: ColorResource.color787878,
                  ))),
      title: CustomText(
        notificationModel.name!,
        style: Theme.of(buildContext).textTheme.subtitle1!.copyWith(
            color: ColorResource.color222222, fontWeight: FontWeight.w700),
      ),
      subtitle: Container(
        margin: const EdgeInsets.only(top: 6),
        child: CustomText(
          notificationModel.message!,
          style: Theme.of(buildContext).textTheme.subtitle2!.copyWith(
              color: ColorResource.color787878, fontWeight: FontWeight.w400),
        ),
      ),
      contentPadding: EdgeInsets.zero,
    );
  }

  String getInitials(name) {
    final List<String> names = name.split(' ');
    String initials = '';
    int numWords = 2;
    if (names.length != 0 && names.length != 1) {
      if (numWords < names.length) {
        numWords = names.length;
      }
      for (int i = 0; i < numWords; i++) {
        initials += '${names[i][0]}';
      }
    } else {
      initials = name[0];
    }
    return initials.toUpperCase();
  }

  Widget dateTextField(BuildContext buildContext, String hint,
      TextEditingController controller) {
    return TextField(
      enabled: false,
      controller: controller,
      style: Theme.of(buildContext)
          .textTheme
          .bodyText1!
          .copyWith(color: ColorResource.color222222),
      decoration: InputDecoration(
        suffixIcon: Container(
          child: Image(
              width: 24,
              height: 24,
              image: AssetImage(ImageResource.date_icon)),
        ),
        fillColor: ColorResource.colorFFFFFF,
        filled: true,
        labelText: hint,
        isDense: false,
        errorMaxLines: 1,
        labelStyle: Theme.of(buildContext)
            .textTheme
            .subtitle1!
            .copyWith(color: ColorResource.color222222),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide: BorderSide(color: ColorResource.colorEFEFEF),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide: BorderSide(color: ColorResource.colorEFEFEF),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide: BorderSide(color: ColorResource.colorEFEFEF),
        ),
      ),
    );
  }

  static Widget gridChild(
      BuildContext buildContext, AllServiceChildModel allServiceChildModel) {
    return SizedBox(
      child: Column(
        children: [
          Container(
            height: 15,
            alignment: Alignment.center,
            child: Visibility(
              visible: allServiceChildModel.title != null ? true : false,
              child: Container(
                margin: const EdgeInsets.only(left: 24, right: 24, bottom: 3.0),
                padding: const EdgeInsets.all(3),
                child: CustomText(
                  // Languages.of(buildContext)!.KYCRequired,
                  allServiceChildModel.title.toString(),
                  style: Theme.of(buildContext).textTheme.caption!.copyWith(
                      color: ColorResource.color222222,
                      fontWeight: FontWeight.w700),
                  maxLines: 2,
                  textAlign: TextAlign.center,
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: ColorResource.colorF9BE25,
                ),
              ),
            ),
          ),
          SizedBox(
              child: Image.asset(
            allServiceChildModel.icon != null
                ? allServiceChildModel.icon!
                : ImageResource.offer_image,
            width: 32,
            height: 32,
          )),
          const SizedBox(
            height: 5,
          ),
          SizedBox(
            child: CustomText(
              allServiceChildModel.name!,
              style: Theme.of(buildContext).textTheme.subtitle2!.copyWith(
                  color: ColorResource.color222222,
                  fontWeight: FontWeight.w400),
              textAlign: TextAlign.center,
            ),
          ),
        ],
      ),
    );
  }

  static shuffleNumberKeyboardWidget(BuildContext context,
      TextEditingController? textEditingController, List<int> items) {
    return MediaQuery.removePadding(
      context: context,
      removeTop: true,
      removeBottom: true,
      removeRight: true,
      removeLeft: true,
      child: SizedBox(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            GridView.builder(
              itemCount: items.length,
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                childAspectRatio: (2),
              ),
              itemBuilder: (
                BuildContext context,
                int index,
              ) {
                return GestureDetector(
                  onTap: () {},
                  child: index == items.length - 3
                      ? Container()
                      : GestureDetector(
                          onTap: () {
                            if (index == items.length - 1) {
                              if (textEditingController!.text.isNotEmpty) {
                                if (textEditingController.text.isNotEmpty) {
                                  textEditingController.text =
                                      textEditingController.text.substring(
                                          0,
                                          textEditingController.text.length -
                                              1);
                                }
                              }
                            } else {
                              textEditingController!.text =
                                  textEditingController.text +
                                      items[index].toString();
                            }
                          },
                          child: Container(
                            alignment: Alignment.center,
                            margin: const EdgeInsets.all(2),
                            decoration: const BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                            ),
                            child: index == items.length - 1
                                ? const SizedBox(
                                    child: Icon(
                                      Icons.backspace,
                                      color: ColorResource.color222222,
                                      size: 24.0,
                                    ),
                                  )
                                : CustomText(items[index].toString(),
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline6!
                                        .copyWith(
                                            color: ColorResource.color222222,
                                            fontWeight: FontWeight.w600)),
                          ),
                        ),
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
