import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:flutter/material.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:share/share.dart';

class DownloadStatementPdf extends StatefulWidget {
  const DownloadStatementPdf({Key? key}) : super(key: key);

  @override
  _DownloadStatementPdfState createState() => _DownloadStatementPdfState();
}

class _DownloadStatementPdfState extends State<DownloadStatementPdf> {
  bool _isLoading = true;
  late PDFDocument document;

  @override
  void initState() {
    super.initState();
    loadDocument();
  }

  loadDocument() async {
    document = await PDFDocument.fromURL(
        'https://www.icicibank.com/managed-assets/docs/form-center/cc_auto.pdf');

    setState(() => _isLoading = false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorResource.color641653,
        leading: GestureDetector(
          child: Image.asset(ImageResource.cancel),
          onTap: () {
            Navigator.pop(context);
          },
        ),
        centerTitle: false,
        title: Text(Languages.of(context)!.pocketStatement),
        actions: [
          GestureDetector(
            child: Image.asset(ImageResource.share),
            onTap: () {
              Share.share(
                  'https://www.icicibank.com/managed-assets/docs/form-center/cc_auto.pdf');
              // Share.shareFiles();
            },

          ),

        ],
      ),
      body: Container(
        margin: EdgeInsets.only(left: 24,right: 24),
        child: Center(
          child: _isLoading
              ? Center(child: CircularProgressIndicator())
              : PDFViewer(
                  document: document,
                  zoomSteps: 1,
                ),
        ),
      ),
    );
  }
}
