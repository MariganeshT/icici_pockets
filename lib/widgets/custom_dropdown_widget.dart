import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';

import 'customText.dart';
class CustomDropDownWidget extends StatefulWidget {
  String? CardNumber1;
  String? CardNumber2;
  String? CardNumber3;
  CustomDropDownWidget(this.CardNumber1,this.CardNumber2, this.CardNumber3);
  @override
  _CustomDropDownWidgetState createState() => _CustomDropDownWidgetState();
}

class _CustomDropDownWidgetState extends State<CustomDropDownWidget> {
  int? selectedValue =1;


  @override
  Widget build(BuildContext context) {
    return

      Container(

        child:
        DropdownButton(
          value: selectedValue,
          iconEnabledColor: ColorResource.colorFF781F,
          iconDisabledColor: ColorResource.color222222,
          iconSize: 24,
          icon: Image.asset(ImageResource.downArrow),
          items: [
            DropdownMenuItem(
              child: CustomText(widget.CardNumber1!,
                fontSize: 24,
                color: ColorResource.colore222222,
                fontWeight: FontWeight.w700,
              ),
              value: 1,
            ),
            DropdownMenuItem(
              child: CustomText(widget.CardNumber2!,
                fontSize: 24,
                color: ColorResource.colore222222,
                fontWeight: FontWeight.w700,
              ),
              value: 2,
            ),
            DropdownMenuItem(
                child: CustomText(widget.CardNumber3!,
                  fontSize: 24,
                  color: ColorResource.colore222222,
                  fontWeight: FontWeight.w700,
                ),
                value: 3
            ),
          ],
          onChanged: (value) {
            setState(() {
              selectedValue = value! as int?;
            });
          },


        ),

      );
  }}
//

