class LoginSetupModel {
  final String iconName;
  final String titleName;
  final String setupNumber;
  LoginSetupModel(this.iconName, this.titleName, this.setupNumber);
}
