import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/model/address_model.dart';
import 'package:icici/screens/FAQs_screen/bloc/faqs_bloc.dart';
import 'package:icici/screens/FAQs_screen/faq_screen.dart';
import 'package:icici/screens/account_detail_screen/account_details_screen.dart';
import 'package:icici/screens/account_detail_screen/bloc/accountdetailbloc_bloc.dart';
import 'package:icici/screens/accounts_screen/accounts_screen.dart';
import 'package:icici/screens/accounts_screen/bloc/accounts_screen_bloc.dart';
import 'package:icici/screens/all_service_screen/all_service_screen.dart';
import 'package:icici/screens/all_service_screen/bloc/service_bloc.dart';
import 'package:icici/screens/authenticate_account_screen/authenticate_account_screen.dart';
import 'package:icici/screens/authenticate_account_screen/bloc/authenticateaccountscreen_bloc.dart';
import 'package:icici/screens/change_passcode_screen/bloc/changepasscode_bloc.dart';
import 'package:icici/screens/change_passcode_screen/change_passcode_screen.dart';
import 'package:icici/screens/confirm_passcode_screen/bloc/confirmpasscode_bloc.dart';
import 'package:icici/screens/confirm_passcode_screen/confirm_passcode_screen.dart';
import 'package:icici/screens/custom_otp_screen/bloc/custom_otp_bloc.dart';
import 'package:icici/screens/custom_otp_screen/custom_otp_screen.dart';
import 'package:icici/screens/dashboard_screen/bloc/dashboardscreen_bloc.dart';
import 'package:icici/screens/dashboard_screen/dashboard_screen.dart';
import 'package:icici/screens/expression_pockets_screen/bloc/expression_pockets_bloc.dart';
import 'package:icici/screens/expression_pockets_screen/expression_pockets_screen.dart';
import 'package:icici/screens/home_tab_screen/bloc/home_tab_bloc.dart';
import 'package:icici/screens/home_tab_screen/home_tab_screen.dart';
import 'package:icici/screens/landing_page/bloc/landing_bloc.dart';
import 'package:icici/screens/login_screen/bloc/login_bloc.dart';
import 'package:icici/screens/manage_notification/bloc/managenotification_bloc.dart';
import 'package:icici/screens/manage_notification/manage_notification.dart';
import 'package:icici/screens/my_referals/bloc/myreferal_bloc.dart';
import 'package:icici/screens/my_referals/my_referal_screen.dart';
import 'package:icici/screens/notification_screen/notification_bloc.dart';
import 'package:icici/screens/notification_screen/notification_event.dart';
import 'package:icici/screens/notification_screen/notification_screen.dart';
import 'package:icici/screens/order_status_screen/bloc/order_status_bloc.dart';
import 'package:icici/screens/order_status_screen/order_status_screen.dart';
import 'package:icici/screens/orderstatus_expression_screen/bloc/order_expression_bloc.dart';
import 'package:icici/screens/orderstatus_expression_screen/orderstatus_expression_screen.dart';
import 'package:icici/screens/otp_verification_screen/bloc/otp_verification_bloc.dart';
import 'package:icici/screens/otp_verification_screen/otp_verification_screen.dart';
import 'package:icici/screens/payment_status_screen/bloc/paymentstatusscreen_bloc.dart';
import 'package:icici/screens/payment_status_screen/payment_status_screen.dart';
import 'package:icici/screens/personal_detail_screen/bloc/personal_details_bloc.dart';
import 'package:icici/screens/personal_detail_screen/personal_detail_screen.dart';
import 'package:icici/screens/place_order_screen/bloc/place_order_bloc.dart';
import 'package:icici/screens/place_order_screen/place_order_screen.dart';
import 'package:icici/screens/placeorder_expression_screen/bloc/place_expression_bloc.dart';
import 'package:icici/screens/placeorder_expression_screen/placeorder_expression_screen.dart';
import 'package:icici/screens/profile_screen/profile_screen.dart';
import 'package:icici/screens/refer&earn_screen/bloc/referandearn_bloc.dart';
import 'package:icici/screens/refer&earn_screen/refer&earn_screen.dart';
import 'package:icici/screens/regular_pockets_card_screen/regular_pockets_card_screen.dart';
import 'package:icici/screens/request_physical_card_screen/bloc/request_physical_card_bloc.dart';
import 'package:icici/screens/request_physical_card_screen/request_physical_card_screen.dart';
import 'package:icici/screens/returning_user_screen/bloc/returning_user_bloc.dart';
import 'package:icici/screens/returning_user_screen/returning_user_screen.dart';
import 'package:icici/screens/set_digit_pin_screen/bloc/set_digit_pin_bloc.dart';
import 'package:icici/screens/set_digit_pin_screen/set_digit_pin_screen.dart';
import 'package:icici/screens/set_mpin_screen/bloc/set_mpin_bloc.dart';
import 'package:icici/screens/set_mpin_screen/set_mpin_screen.dart';
import 'package:icici/screens/set_pin_screen/bloc/set_pin_bloc.dart';
import 'package:icici/screens/set_pin_screen/set_pin.dart';
import 'package:icici/screens/set_transaction_limit_screen/bloc/set_transaction_limit_screen_bloc.dart';
import 'package:icici/screens/set_transaction_limit_screen/set_transaction_limit_screen.dart';
import 'package:icici/screens/settings_main_screen/bloc/settings_main_screen_bloc.dart';
import 'package:icici/screens/settings_main_screen/settings_main_screen.dart';
import 'package:icici/screens/statement_details/statement_details_bloc.dart';
import 'package:icici/screens/statement_details/statement_details_event.dart';
import 'package:icici/screens/statement_details/statement_details_screen.dart';
import 'package:icici/screens/statement_screen/statement_screen.dart';
import 'package:icici/screens/statement_screen/statement_screen_bloc.dart';
import 'package:icici/screens/support_screen/bloc/supportscreen_bloc.dart';
import 'package:icici/screens/support_screen/support_screen.dart';
import 'package:icici/screens/terms_conditions_screen/bloc/terms_conditions_bloc.dart';
import 'package:icici/screens/terms_conditions_screen/terms_condition_screen.dart';
import 'package:icici/screens/track_physical_card_screen/track_physical_card_screen.dart';
import 'package:icici/screens/track_service_request_screen/bloc/track_service_request_bloc.dart';
import 'package:icici/screens/track_service_request_screen/track_service_request_screen.dart';
import 'package:icici/screens/track_shipment_screen/track_shipment_bloc.dart';
import 'package:icici/screens/track_shipment_screen/track_shipment_event.dart';
import 'package:icici/screens/track_shipment_screen/track_shipment_screen.dart';
import 'package:icici/screens/verify_identity_screen/bloc/verify_identity_bloc.dart';
import 'package:icici/screens/verify_identity_screen/verify_identity_screen.dart';
import 'package:icici/screens/wallet_success_screen/bloc/wallet_create_success_bloc.dart';
import 'package:icici/screens/wallet_success_screen/wallet_create_success.dart';
import 'package:icici/screens/web_view_screen/bloc/webviewscreen_bloc.dart';
import 'package:icici/screens/web_view_screen/web_view_screen.dart';
import 'package:icici/screens/welcome_home_screen/bloc/welcome_home_bloc.dart';
import 'package:icici/screens/welcome_home_screen/welcome_home_screen.dart';
import 'package:icici/screens/welcome_screen/welcome_bloc.dart';
import 'package:icici/screens/welcome_screen/welcome_event.dart';
import 'package:icici/screens/welcome_screen/welcome_screen.dart';

import 'authentication/bloc/authentication_bloc.dart';
import 'model/web_view_model.dart';
import 'screens/accounts_screen/accounts_screen.dart';
import 'screens/landing_page/landing_screen.dart';
import 'screens/login_screen/login_screen.dart';
import 'screens/profile_screen/bloc/profile_bloc.dart';
import 'screens/regular_pockets_card_screen/bloc/regular_pockets_card_bloc.dart';
import 'screens/splash_screen/splash_screen.dart';
import 'screens/track_physical_card_screen/bloc/track_physical_card_bloc.dart';
import 'screens/wallet_closure_screen/bloc/wallet_closure_bloc.dart';
import 'screens/wallet_closure_screen/wallet_closure_screen.dart';

class AppRoutes {
  static const String splashScreen = 'splash_screen';
  static const String homeTabScreen = 'home_tab_screen';
  static const String loginScreen = 'login_screen';
  static const String landingScreen = 'landing_screen';
  static const String otpVerificationScreen = 'otp_verification_screen';
  static const String personalDetailsScreen = 'personal_detail_screen';
  static const String verifyIdentityScreen = 'verify_identity_screen';
  static const String welcomeScreen = 'welcome_screen';
  static const String setPinScreen = 'Set_pin_screen';
  static const String walletSuccessScreen = 'wallet_success_screen';
  static const String returningUserScreen = 'returning_user_screen';
  static const String settingsMainScreen = 'settings_main_screen';
  static const String profileScreen = 'profile_screen';
  static const String dashboardScreen = 'dashboard_screen';
  static const String supportScreen = 'support_screen';
  static const String faqScreen = 'faq_screen';
  static const String referAndEarn = 'refer_and_earn_screen';
  static const String myReferal = 'my_referal_screen';
  static const String manageNotificationScreen = 'manage_notification_screen';
  static const String accountsScreen = 'accounts_screen';
  static const String setTransactionLimitScreen =
      'set_transaction_limit_screen';
  static const String accountDetails = 'account_detail_screen';
  static const String statementScreen = 'statement_screen';
  static const String statementDetailsScreen = 'statement_details_screen';
  static const String authenticateAccountScreen = 'authenticate_account_screen';
  static const String paymentStatusScreen = 'payment_status_screen';
  static const String customOtpVerificationScreen =
      'custom_otp_verification_screen';
  static const String walletClosureScreen = 'wallet_closure_screen';
  static const String allServicesScreen = 'all_service_screen';
  static const String notificationScreen = 'notification_screen';
  static const String trackShipmentScreen = 'track_shipment_screen';
  static const String trackPhysicalCardScreen = 'track_physical_card_screen';
  static const String trackServiceRequest = 'track_service_request_screen';
  static const String setMpinScreen = 'set_mpin_screen';
  static const String changePasscodeScreen = 'change_passcode_screen';
  static const String confirmPasscodeScreen = 'confirm_passcode_screen';
  static const String expressionPocketsScreen = 'expression_pockets_screen';
  static const String regularPocketsCardScreen = 'regular_pockets_card_screen';
  static const String orderStatusScreen = 'order_status_screen';
  static const String placeOrderScreen = 'place_order_screen';
  static const String requestPhysicalCardScreen =
      'request_physical_card_screen';
  static const String setDigitPinScreen = 'set_digit_pin_screen';
  static const String placeOrderExpressionScreen =
      'placeorder_expression_screen';
  static const String orderSatusExpressionScreen =
      'orderstatus_expression_screen';
  static const String termsConditionsScreen = 'terms_conditions_screen';
  static const String webViewScreen = 'web_view_screen';
  static const String welcomeHomeScreen = 'welcome_home_screen';
}

Route<dynamic> getRoute(RouteSettings settings) {
  switch (settings.name) {
    case AppRoutes.splashScreen:
      return _buildSplashScreen();
    case AppRoutes.homeTabScreen:
      return _buildHomeTabScreen();
    case AppRoutes.loginScreen:
      return _buildLoginScreen();
    case AppRoutes.landingScreen:
      return _buildLandingScreen();
    case AppRoutes.otpVerificationScreen:
      return _buildOtpVerificationScreen(settings);
    case AppRoutes.personalDetailsScreen:
      return _buildPersonalDetailScreen();
    case AppRoutes.verifyIdentityScreen:
      return _buildVerifyIdentityScreen();
    case AppRoutes.welcomeScreen:
      return _buildWelcomeScreen();
    case AppRoutes.setPinScreen:
      return _buildSetPinScreen(settings);
    case AppRoutes.walletSuccessScreen:
      return _buildWalletSuccessScreen();
    case AppRoutes.returningUserScreen:
      return _buildReturningUserScreen();
    case AppRoutes.settingsMainScreen:
      return _buildSettingsMainScreen();
    case AppRoutes.profileScreen:
      return _buildProfileScreen();
    case AppRoutes.dashboardScreen:
      return _buildDashBoardScreen();
    case AppRoutes.supportScreen:
      return _buildSupportScreen();
    case AppRoutes.faqScreen:
      return _buildFaqScreen();
    case AppRoutes.referAndEarn:
      return _buildReferAndEarnScreen();
    case AppRoutes.myReferal:
      return _buildMyReferalScreen();
    case AppRoutes.manageNotificationScreen:
      return _buildManageNotificationScreen();
    case AppRoutes.accountsScreen:
      return _buildAccountsScreen();
    case AppRoutes.setTransactionLimitScreen:
      return _buildSetTransactionLimitScreen();
    case AppRoutes.accountDetails:
      return _buildAccountDetailScreen();
    case AppRoutes.statementScreen:
      return _buildStatementScreen();
    case AppRoutes.statementDetailsScreen:
      return _buildStatementDetailsScreen();
    case AppRoutes.authenticateAccountScreen:
      return _buildAuthenticateAccountScreen();
    case AppRoutes.paymentStatusScreen:
      return _buildPaymentStatusScreen();

    case AppRoutes.allServicesScreen:
      return _buildAllServicesScreen();
    case AppRoutes.notificationScreen:
      return _buildNotificationScreen();
    case AppRoutes.trackShipmentScreen:
      return _buildTrackShipmentScreen();
    case AppRoutes.customOtpVerificationScreen:
      return _buildCustomOtpVeriricationScreen(settings);
    case AppRoutes.walletClosureScreen:
      return _buildWalletClosureScreen();
    case AppRoutes.setMpinScreen:
      return _buildSetMpinScreen(settings);
    case AppRoutes.trackPhysicalCardScreen:
      return _buildTrackPhysicalCardScreen();
    case AppRoutes.trackServiceRequest:
      return _buildTrackServiceRequestScreen();
    case AppRoutes.trackShipmentScreen:
      return _buildTrackShipmentScreen();
    case AppRoutes.changePasscodeScreen:
      return _buildChangePasscodeScreen();
    case AppRoutes.confirmPasscodeScreen:
      return _buildConfirmPasscodeScreen();

    case AppRoutes.expressionPocketsScreen:
      return _buildExpressionPocketsScreen();
    case AppRoutes.requestPhysicalCardScreen:
      return _buildRequestPhysicalCardScreen();
    case AppRoutes.regularPocketsCardScreen:
      return _buildRegularPocketsCardScreen();
    case AppRoutes.orderStatusScreen:
      return _buildOrderStatusScreen();
    case AppRoutes.placeOrderScreen:
      return _buildPlaceOrderScreen(settings);
    case AppRoutes.placeOrderExpressionScreen:
      return _buildPlaceOrderExpressionScreen();
    case AppRoutes.orderSatusExpressionScreen:
      return _buildOrderStatusExpressionScreen();
    case AppRoutes.setDigitPinScreen:
      return _buildSetDigitPinScreen();
    case AppRoutes.termsConditionsScreen:
      return _buildTermsConditionsScreen();
    case AppRoutes.webViewScreen:
      return _buildWebViewScreen(settings);
    case AppRoutes.welcomeHomeScreen:
      return _buildWelcomeHomeScreen();
  }
  return _buildSplashScreen();
}

Route<dynamic> _buildCustomOtpVeriricationScreen(RouteSettings settings) {
  return MaterialPageRoute(
      builder: (context) => addAuthBloc(
          context, PageBuilder.buildCustomOtpVerificationScreen(settings)));
}

Route<dynamic> _buildWalletClosureScreen() {
  return MaterialPageRoute(
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildWalletClosureScreen()),
  );
}

Route<dynamic> _buildOrderStatusScreen() {
  return MaterialPageRoute(
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildOrderStatusScreen()),
  );
}

Route<dynamic> _buildProfileScreen() {
  return MaterialPageRoute(
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildProfileScreen()),
  );
}

Route<dynamic> _buildWalletSuccessScreen() {
  return MaterialPageRoute(
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildWalletSuccessScreen()),
  );
}

Route<dynamic> _buildSetPinScreen(RouteSettings settings) {
  return MaterialPageRoute(
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildSetPinScreen(settings)),
  );
}

Route<dynamic> _buildSplashScreen() {
  return MaterialPageRoute(
    builder: (context) => addAuthBloc(context, PageBuilder.buildSplashScreen()),
  );
}

Route<dynamic> _buildHomeTabScreen() {
  return MaterialPageRoute(
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildHomeTabScreen()),
  );
}

Route<dynamic> _buildLoginScreen() {
  return MaterialPageRoute(
    builder: (context) => addAuthBloc(context, PageBuilder.buildLoginScreen()),
  );
}

Route<dynamic> _buildLandingScreen() {
  return MaterialPageRoute(
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildLandingScreen()),
  );
}

Route<dynamic> _buildOtpVerificationScreen(RouteSettings settings) {
  return MaterialPageRoute(
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildOtpVerificationScreen(settings)),
  );
}

Route<dynamic> _buildPersonalDetailScreen() {
  return MaterialPageRoute(
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildPersonalDetailScreen()),
  );
}

Route<dynamic> _buildVerifyIdentityScreen() {
  return MaterialPageRoute(
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildVerifyIdentityScreen()),
  );
}

Route<dynamic> _buildWelcomeScreen() {
  return MaterialPageRoute(
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildWelcomeScreen()),
  );
}

Route<dynamic> _buildReturningUserScreen() {
  return MaterialPageRoute(
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildReturningUserScreen()),
  );
}

Route<dynamic> _buildSettingsMainScreen() {
  return MaterialPageRoute(
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildSettingsMainScreen()),
  );
}

Route<dynamic> _buildDashBoardScreen() {
  return MaterialPageRoute(
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildDashBoardScreen()),
  );
}

Route<dynamic> _buildSupportScreen() {
  return MaterialPageRoute(
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildSupportScreen()),
  );
}

Route<dynamic> _buildFaqScreen() {
  return MaterialPageRoute(
    builder: (context) => addAuthBloc(context, PageBuilder.buildFaqScreen()),
  );
}

Route<dynamic> _buildReferAndEarnScreen() {
  return MaterialPageRoute(
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildReferAndEarnScreen()),
  );
}

Route<dynamic> _buildMyReferalScreen() {
  return MaterialPageRoute(
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildMyReferalScreen()),
  );
}

Route<dynamic> _buildManageNotificationScreen() {
  return MaterialPageRoute(
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildManageNotificationScreen()),
  );
}

Route<dynamic> _buildStatementScreen() {
  return MaterialPageRoute(
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildStatementScreen()),
  );
}

Route<dynamic> _buildStatementDetailsScreen() {
  return MaterialPageRoute(
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildStatementDetailsScreen()),
  );
}

Route<dynamic> _buildAllServicesScreen() {
  return MaterialPageRoute(
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildAllServicesScreen()),
  );
}

Route<dynamic> _buildAccountDetailScreen() {
  return MaterialPageRoute(
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildAccountDetailScreen()),
  );
}

Route<dynamic> _buildAccountsScreen() {
  return MaterialPageRoute(
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildAccountsScreen()),
  );
}

Route<dynamic> _buildSetTransactionLimitScreen() {
  return MaterialPageRoute(
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildSetTransactionLimitScreen()),
  );
}

Route<dynamic> _buildAuthenticateAccountScreen() {
  return MaterialPageRoute(
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildAuthenticateAccountScreen()),
  );
}

Route<dynamic> _buildPaymentStatusScreen() {
  return MaterialPageRoute(
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildPaymentStatusScreen()),
  );
}

Route<dynamic> _buildNotificationScreen() {
  return MaterialPageRoute(
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildNotificationScreen()),
  );
}

Route<dynamic> _buildTrackShipmentScreen() {
  return MaterialPageRoute(
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildTrackShipmentScreen()),
  );
}

Route<dynamic> _buildSetMpinScreen(RouteSettings settings) {
  return MaterialPageRoute(
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildSetMpinScreen(settings)),
  );
}

Route<dynamic> _buildTrackPhysicalCardScreen() {
  return MaterialPageRoute(
      builder: (context) =>
          addAuthBloc(context, PageBuilder.buildTrackPhysicalCardScreen()));
}

Route<dynamic> _buildTrackServiceRequestScreen() {
  return MaterialPageRoute(
      builder: (context) =>
          addAuthBloc(context, PageBuilder.buildTrackServiceRequestScreen()));
}

Route<dynamic> _buildRequestPhysicalCardScreen() {
  return MaterialPageRoute(
      builder: (context) =>
          addAuthBloc(context, PageBuilder.buildRequestPhysicalCardScreen()));
}

Route<dynamic> _buildRegularPocketsCardScreen() {
  return MaterialPageRoute(
      builder: (context) =>
          addAuthBloc(context, PageBuilder.buildRegularPocketsCardScreen()));
}

Route<dynamic> _buildPlaceOrderScreen(RouteSettings settings) {
  return MaterialPageRoute(
      builder: (context) =>
          addAuthBloc(context, PageBuilder.buildPlaceOrderScreen(settings)));
}

Route<dynamic> _buildChangePasscodeScreen() {
  return MaterialPageRoute(
      builder: (context) =>
          addAuthBloc(context, PageBuilder.buildChangePasscodeScreen()));
}

Route<dynamic> _buildConfirmPasscodeScreen() {
  return MaterialPageRoute(
      builder: (context) =>
          addAuthBloc(context, PageBuilder.buildConfirmPasscodeScreen()));
}

Route<dynamic> _buildExpressionPocketsScreen() {
  return MaterialPageRoute(
      builder: (context) =>
          addAuthBloc(context, PageBuilder.buildExpressionPocketsScreen()));
}

Route<dynamic> _buildPlaceOrderExpressionScreen() {
  return MaterialPageRoute(
      builder: (context) =>
          addAuthBloc(context, PageBuilder.buildPlaceOrderExpressionScreen()));
}

Route<dynamic> _buildOrderStatusExpressionScreen() {
  return MaterialPageRoute(
      builder: (context) =>
          addAuthBloc(context, PageBuilder.buildOrderStatusExpressionScreen()));
}

Route<dynamic> _buildSetDigitPinScreen() {
  return MaterialPageRoute(
      builder: (context) =>
          addAuthBloc(context, PageBuilder.buildSetDigitPinScreen()));
}

Route<dynamic> _buildTermsConditionsScreen() {
  return MaterialPageRoute(
      builder: (context) =>
          addAuthBloc(context, PageBuilder.buildTermsConditionsScreen()));
}

Route<dynamic> _buildWebViewScreen(RouteSettings settings) {
  return MaterialPageRoute(
      builder: (context) =>
          addAuthBloc(context, PageBuilder.buildWebViewScreen(settings)));
}

Route<dynamic> _buildWelcomeHomeScreen() {
  return MaterialPageRoute(
      builder: (context) =>
          addAuthBloc(context, PageBuilder.buildWelcomeHomeScreen()));
}

// Route<dynamic> _buildSetMpinScreen() {
//   return MaterialPageRoute(
//     builder: (context) =>
//         addAuthBloc(context, PageBuilder.buildSetMpinScreen()),
//   );
// }

class PageBuilder {
  // Prefer (✔️) - SplashScreen Screen
  static Widget buildSplashScreen() {
    return BlocProvider(
      create: (BuildContext context) =>
          BlocProvider.of<AuthenticationBloc>(context),
      child: SplashScreen(),
    );
  }

  // Prefer (✔️) - SplashScreen Screen
  static Widget buildSetPinScreen(RouteSettings settings) {
    bool isForgotMpin = false;
    if (settings.arguments != null) {
      isForgotMpin = settings.arguments as bool;
    }
    bool isTerms = false;
    if (settings.arguments != null) {
      isTerms = settings.arguments as bool;
    }

    return BlocProvider(
      create: (BuildContext context) =>
          BlocProvider.of<SetPinBloc>(context)..add(SetPinInitialEvent()),
      child: SetPinScreen(isForgotMpinFlow: isForgotMpin, isTerms: isTerms),
    );
  }

  static Widget buildWalletSuccessScreen() {
    return BlocProvider(
      create: (BuildContext context) =>
          BlocProvider.of<WalletSuccessBloc>(context)
            ..add(WalletSuccessInitialEvent()),
      child: WalletSuccessScreen(),
    );
  }

  static Widget buildHomeTabScreen() {
    return BlocProvider(
      create: (BuildContext context) =>
          BlocProvider.of<HomeTabBloc>(context)..add(HomeTabInitialEvent()),
      child: HomeTabScreen(),
    );
  }

  static Widget buildLoginScreen() {
    return BlocProvider(
      create: (BuildContext context) =>
          BlocProvider.of<LoginBloc>(context)..add(LoginInitialEvent()),
      child: LoginScreen(),
    );
  }

  static Widget buildPaymentStatusScreen() {
    return BlocProvider(
      create: (BuildContext context) => PaymentstatusscreenBloc()
        ..add(PaymentStatusScreenIntialEvent(context)),
      child: PaymentStatusScreen(),
    );
  }

  static Widget buildLandingScreen() {
    return BlocProvider(
      create: (BuildContext context) =>
          LandingBloc()..add(LandingInitialEvent()),
      child: LandingScreen(),
    );
  }

  static Widget buildOtpVerificationScreen(RouteSettings settings) {
    OTPVerificationModel model = settings.arguments as OTPVerificationModel;

    return BlocProvider(
      create: (BuildContext context) =>
          OtpVerificationBloc()..add(OtpVerificationInitialEvent()),
      child: OtpVerificationScreen(
          isForgotMpinFlow: model.isForgotMpinFlow,
          isTerms: model.isTerms,
          navigateScreen: model.navigateScreen),
    );
  }

  static Widget buildPersonalDetailScreen() {
    return BlocProvider(
      create: (BuildContext context) =>
          PersonalDetailsBloc()..add(PersonalDetailsInitialEvent()),
      child: PersonalDetailScreen(),
    );
  }

  static Widget buildVerifyIdentityScreen() {
    return BlocProvider(
      create: (BuildContext context) =>
          VerifyIdentityBloc()..add(VerifyIdentityInitialEvent()),
      child: VerifyIdentityScreen(),
    );
  }

  static Widget buildWelcomeScreen() {
    return BlocProvider(
      create: (BuildContext context) => WelcomeBloc()..add(LoadingEvent()),
      child: WelcomeScreen(),
    );
  }

  static Widget buildReturningUserScreen() {
    return BlocProvider(
      create: (BuildContext context) =>
          ReturningUserBloc()..add(ReturningUserInitialEvent()),
      child: ReturningUserScreen(),
    );
  }

  static Widget buildSettingsMainScreen() {
    return BlocProvider(
      create: (context) {
        final AuthenticationBloc authBloc =
            BlocProvider.of<AuthenticationBloc>(context);

        return SettingsMainScreenBloc(authBloc)
          ..add(SettingsMainScreenInitialEvent());
      },
      child: SettingsMainScreen(),
    );
  }

  static Widget buildProfileScreen() {
    return BlocProvider(
        create: (BuildContext context) =>
            ProfileBloc()..add(ProfileInitialEvent()),
        child: ProfileScreen());
  }

  static Widget buildAccountsScreen() {
    return BlocProvider(
      create: (BuildContext context) => AccountsScreenBloc()
        ..add(AccountsScreenInitialEvent(context: context)),
      child: const AccountsScreen(),
    );
  }

  static Widget buildSetTransactionLimitScreen() {
    return BlocProvider(
      create: (BuildContext context) => SetTransactionLimitScreenBloc()
        ..add(SetTransactionLimitScreenInitialEvent()),
      child: SetTransactionLimit(),
    );
  }

  static Widget buildCustomOtpVerificationScreen(RouteSettings settings) {
    String otpAddress = settings.arguments as String;
    return BlocProvider(
        create: (BuildContext context) =>
            CustomOtpBloc(otpAddress)..add(CustomOtpVerificationInitialEvent()),
        child: CustomOtpScreen());
  }

  static Widget buildDashBoardScreen() {
    // ignore: always_specify_types
    return BlocProvider(
      // ignore: lines_longer_than_80_chars
      create: (BuildContext context) =>
          DashboardscreenBloc()..add(DashboardInitialEvent(context: context)),
      child: const DashBoardScreen(),
    );
  }

  static Widget buildSupportScreen() {
    // ignore: always_specify_types
    return BlocProvider(
      // ignore: lines_longer_than_80_chars
      create: (BuildContext context) =>
          SupportscreenBloc()..add(SupportscreenInitialEvent()),
      child: SupportScreen(),
    );
  }

  static Widget buildFaqScreen() {
    // ignore: always_specify_types
    return BlocProvider(
      create: (BuildContext context) => FaqsBloc()..add(FaqsInitialEvent()),
      child: FaqScreen(),
    );
  }

  static Widget buildReferAndEarnScreen() {
    return BlocProvider(
      create: (BuildContext context) =>
          ReferandearnBloc()..add(ReferandearnInitialEvent()),
      child: ReferAndEarn(),
    );
  }

  static Widget buildMyReferalScreen() {
    return BlocProvider(
      create: (BuildContext context) =>
          MyreferalBloc()..add(MyreferalInitialEvent()),
      child: MyReferalScreen(),
    );
  }

  static Widget buildManageNotificationScreen() {
    return BlocProvider(
      create: (BuildContext context) =>
          ManagenotificationBloc()..add(ManagenotificationInitialEvent()),
      child: ManageNotificationScreen(),
    );
  }

  static Widget buildStatementScreen() {
    return BlocProvider(
      create: (BuildContext context) =>
          StatementScreenBloc()..add(StatementInitialEvent(context: context)),
      child: const StatementScreen(),
    );
  }

  static Widget buildStatementDetailsScreen() {
    return BlocProvider(
      create: (BuildContext context) => StatementDetailsBloc()
        ..add(StatementDetailsInitialEvent(context: context)),
      child: const StatementDetailsScreen(),
    );
  }

  static Widget buildAllServicesScreen() {
    return BlocProvider(
      create: (BuildContext context) =>
          ServiceBloc()..add(ServiceInitailEvent(context: context)),
      child: const AllServiceScreen(),
    );
  }

  static Widget buildAccountDetailScreen() {
    return BlocProvider(
        create: (BuildContext context) =>
            AccountDetailBloc()..add(AccountDetailIntialEvent()),
        child: AccountDetailScreen());
  }

  static Widget buildAuthenticateAccountScreen() {
    return BlocProvider(
        create: (BuildContext context) => AuthenticateAccountScreenBloc()
          ..add(AuthenticateAccountScreenIntialEvent()),
        child: AuthenticateAccountScreen());
  }

  static Widget buildWalletClosureScreen() {
    return BlocProvider(
        create: (BuildContext context) =>
            WalletClosureBloc()..add(WalletClosureInitialEvent()),
        child: WalletClosureScreen());
  }

  static Widget buildNotificationScreen() {
    return BlocProvider(
        create: (BuildContext context) =>
            NotificationBloc()..add(NotificationInitialEvent(context: context)),
        child: const NotificationScreen());
  }

  static Widget buildTrackShipmentScreen() {
    return BlocProvider(
        create: (BuildContext context) => TrackShipmentBloc()
          ..add(TrackShipmentInitialEvent(context: context)),
        child: const TrackShipmentScreen());
  }

  static Widget buildSetMpinScreen(RouteSettings settings) {
    String argumentsvalue = '';
    if (settings.arguments != null) {
      argumentsvalue = settings.arguments as String;
    }

    return BlocProvider(
      create: (BuildContext context) =>
          SetMpinBloc()..add(SetMpinInitialEvent()),
      child: SetMpinScreen(navigateScreen: argumentsvalue),
    );
  }

  static Widget buildTrackServiceRequestScreen() {
    return BlocProvider(
        create: (BuildContext context) =>
            TrackServiceRequestBloc()..add(TrackServiceRequestInitialEvent()),
        child: const TrackServiceRequestScreen());
  }

  static Widget buildTrackPhysicalCardScreen() {
    return BlocProvider(
        create: (BuildContext context) =>
            TrackPhysicalCardBloc()..add(TrackPhysicalCardInitialEvent()),
        child: const TrackPhysicalCardScreen());
  }

  static Widget buildChangePasscodeScreen() {
    return BlocProvider(
        create: (BuildContext context) =>
            ChangepasscodeBloc()..add(ChangepasscodeInitialEvent()),
        child: ChangePasscodeScreen());
  }

  static Widget buildConfirmPasscodeScreen() {
    return BlocProvider(
        create: (BuildContext context) =>
            ConfirmpasscodeBloc()..add(ConfirmpasscodeInitialEvent()),
        child: ConformPasscodeScreen());
  }

// static Widget buildSetMpinScreen() {
//   return BlocProvider(
//     create: (BuildContext context) =>
//         SetMpinBloc()..add(SetMpinInitialEvent()),
//     child: SetMpinScreen(),
//   );
// }
  static Widget buildExpressionPocketsScreen() {
    return BlocProvider(
      create: (BuildContext context) =>
          ExpressionPocketsBloc()..add(ExpressionPocketsInitialEvent()),
      child: ExpressionPockets(),
    );
  }

  static Widget buildRequestPhysicalCardScreen() {
    return BlocProvider(
        create: (BuildContext context) =>
            RequestPhysicalCardBloc()..add(RequestPhysicalCardInitialEvent()),
        child: RequestPhysicalCardScreen());
  }

  static Widget buildRegularPocketsCardScreen() {
    return BlocProvider(
        create: (BuildContext context) =>
            RegularPocketsCardBloc()..add(RegularPocketsCardInitialEvent()),
        child: RegularPocketsCardScreen());
  }

  static Widget buildPlaceOrderScreen(RouteSettings settings) {
    AddressModel address;
    address = settings.arguments as AddressModel;
    return BlocProvider(
        create: (BuildContext context) =>
            PlaceOrderBloc()..add(PlaceOrderInitialEvent()),
        child: PlaceOrderScreen(address));
  }

  static Widget buildOrderStatusScreen() {
    return BlocProvider(
        create: (BuildContext context) =>
            OrderStatusBloc()..add(OrderStatusInitialEvent()),
        child: OrderStatusScreen());
  }

  static Widget buildPlaceOrderExpressionScreen() {
    return BlocProvider(
        create: (BuildContext context) =>
            PlaceExpressionBloc()..add(PlaceExpressionInitialEvent()),
        child: PlaceExpression());
  }

  static Widget buildOrderStatusExpressionScreen() {
    return BlocProvider(
        create: (BuildContext context) =>
            OrderExpressionBloc()..add(OrderExpressionIniialEvent()),
        child: OrderExpression());
  }

  static Widget buildSetDigitPinScreen() {
    return BlocProvider(
        create: (BuildContext context) =>
            SetDigitPinBloc()..add(SetDigitPinInitialEvent()),
        child: SetDigitPinScreen());
  }

  static Widget buildTermsConditionsScreen() {
    return BlocProvider(
        create: (BuildContext context) =>
            TermsConditionsBloc()..add(TermsConditionsInitialEvent()),
        child: TermsConditionScreen());
  }

  static Widget buildWebViewScreen(RouteSettings settings) {
    WebViewModel webViewModel;
    webViewModel = settings.arguments as WebViewModel;

    return BlocProvider(
        create: (BuildContext context) =>
            WebViewScreenBloc(webViewModel)..add(WebVeiwInitialEvent()),
        child: WebViewScreen());
  }

  static Widget buildWelcomeHomeScreen() {
    return BlocProvider(
        create: (BuildContext context) =>
            WelcomeHomeBloc()..add(WelcomeHomeInitialEvent()),
        child: WelcomeHomeScreen());
  }

  // static Widget buildSetMpinScreen() {
  //   return BlocProvider(
  //     create: (BuildContext context) =>
  //         SetMpinBloc()..add(SetMpinInitialEvent()),
  //     child: SetMpinScreen(),
  //   );
  // }
}

Widget addAuthBloc(BuildContext context, Widget widget) {
  return BlocListener(
    bloc: BlocProvider.of<AuthenticationBloc>(context),
    listener: (BuildContext context, Object? state) {
      if (state is AuthenticationUnAuthenticated) {
        while (Navigator.canPop(context)) {
          Navigator.pop(context);
        }

        Navigator.pushReplacementNamed(context, AppRoutes.personalDetailsScreen);
      }

      if (state is AuthenticationAuthenticated) {
        while (Navigator.canPop(context)) {
          Navigator.pop(context);
        }
        Navigator.pushReplacementNamed(context, AppRoutes.returningUserScreen);
      }

      if (state is SplashScreenState) {
        Navigator.pushNamed(context, AppRoutes.splashScreen);
      }

      if (state is WelcomeScreen) {
        // Navigator.pushNamed(context, AppRoutes.splashScreen);
        Navigator.pushReplacementNamed(
            context, AppRoutes.regularPocketsCardScreen);
      }

      if (state is ForgotMPinState) {
        while (Navigator.canPop(context)) {
          Navigator.pop(context);
        }

        Navigator.pushReplacementNamed(context, AppRoutes.otpVerificationScreen,
            arguments: true);
      }
    },
    child: BlocBuilder(
      bloc: BlocProvider.of<AuthenticationBloc>(context),
      builder: (BuildContext context, Object? state) {
        return widget;
      },
    ),
  );
}
