class AllServiceListModel {
  final bool newBool;
  final String title;
  final String image;

  AllServiceListModel(
      {required this.newBool, required this.title, required this.image});
}
