import 'package:flutter/material.dart';

class AllThemes{
  String? title;
  bool? isSelected = false;
  String? backgroundColor;
  String? backgroundImage;

  AllThemes({
    this.backgroundColor, this.backgroundImage,
    
      this.title, 
      this.isSelected,
  }
  
  );
}

