import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Profile {
  String leadingImage;
  String title;
  String subTitle;
  GestureTapCallback? onTap;
  String trailing;
  bool isTrailingIcon;
  bool isTitleColor;
  

  Profile(
      this.leadingImage, 
      this.title,
      
      {this.subTitle = '',
      
      this.onTap,
      this.trailing = '',
      this.isTrailingIcon = false,
      this.isTitleColor = false,
      
      });
}
