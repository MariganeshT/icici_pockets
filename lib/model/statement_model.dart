class StatementModel {
  String? imageAddress;
  String name;
  String dateTime;
  double amount;
  int debitORCredit; //1 - Credit || 2 - Debit

  StatementModel(this.imageAddress, this.name, this.dateTime, this.amount,
      this.debitORCredit);
}
