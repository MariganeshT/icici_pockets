class AddressModel {
  String pincode;
  String houseNumber;
  String address1;
  String? address2;
  String? name;

  AddressModel(this.pincode, this.houseNumber, this.address1, this.address2,
      {this.name});
}
