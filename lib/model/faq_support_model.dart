import 'package:flutter/cupertino.dart';

class Support {
  String leadingImage;
  String title;
  String subTitle;
  GestureTapCallback? onTap;
  String subTitle2;
  bool isTitleColor;

  Support(
    this.leadingImage,
    this.title, {
    this.subTitle = '',
    this.onTap,
    this.subTitle2 = '',
    this.isTitleColor = false,
  });
}

class FaqModel {
  bool expanded;
  String header;
  String description;
  String? category;

  FaqModel({
    this.expanded: false,
    required this.header,
    required this.description,
    this.category,
  });
}