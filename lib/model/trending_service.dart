class TrendingService {
  String iconName;
  String title;
  bool isRequired = false;
  String? requiredText;

  TrendingService(this.iconName, this.title, this.isRequired,
      {this.requiredText});
}
