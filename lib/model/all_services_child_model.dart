class AllServiceChildModel {
  String? name;
  bool? isVisible;
  String? icon;
  String? title; // if it new or some update timely

  AllServiceChildModel({
    this.name,
    this.icon,
    this.title,
    this.isVisible = true,
  });
}
