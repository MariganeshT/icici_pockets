class NotificationModel {
  String? imageAddress;
  String? name;
  String? dateTime;
  String? message;

  NotificationModel({
    this.imageAddress,
    this.name,
    this.dateTime,
    this.message,
  });
}
