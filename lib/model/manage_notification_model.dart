import 'package:flutter/cupertino.dart';

class ManageNotificationModel {
  bool switchAction;
  String title;
  String subTitle;
  String leading;

  ManageNotificationModel({
    this.switchAction: true,
    required this.title,
    required this.subTitle,
    required this.leading,
  });
}