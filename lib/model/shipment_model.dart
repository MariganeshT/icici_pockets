import 'package:icici/model/shipment_tracking_step_model.dart';

class ShipmentModel {
  String? name;
  String? orderedDate;
  String? estimatedDeliveryDate;
  double? amount;
  String? deliveryAddress;
  List<ShipmentTrackingStepModel>? shipmentTrack;

  String? toEstimatedDeliveryDate;

  ShipmentModel(
      {this.name,
      this.orderedDate,
      this.estimatedDeliveryDate,
      this.amount,
      this.deliveryAddress,
      this.shipmentTrack,
      this.toEstimatedDeliveryDate});
}
