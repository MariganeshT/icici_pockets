class StatementTypeModel {
  String name;
  bool isSelected;
  bool isDateRange;
  bool isDownload;

  StatementTypeModel(this.name, this.isSelected, this.isDateRange,this.isDownload);
}
