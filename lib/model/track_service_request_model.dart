class TrackServiceRequest {
  String number;
  String date;

  String upi;

  String details;

  TrackServiceRequest(this.number, this.date, this.upi, this.details);
}
