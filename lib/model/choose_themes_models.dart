import 'package:flutter/cupertino.dart';
import 'package:icici/utils/color_resource.dart';

class ChooseThemes{
  Color? backgroundColor;
  String? image;
  bool? isSelected = false;

  ChooseThemes({
    this.backgroundColor,
    this.image,
    this.isSelected
    });
}