class ProfileAddressModel {
  String? pinCode;
  String? houseNumber;
  String? address1;
  String? address2;
  String? name;

  ProfileAddressModel(
      this.pinCode, this.houseNumber, this.address1, this.address2, this.name);
}
