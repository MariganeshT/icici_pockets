import 'package:flutter/cupertino.dart';

class ShareOptions {
  String image;
  String text;
  GestureTapCallback? onTap;

  ShareOptions(
    this.image,
    this.text, {
    this.onTap,
  });
}

class ContactListModel {
  String name;
  String image;
  String mobileNo;
  GestureTapCallback? onTap;

  ContactListModel(
     {
    required this.image,
    this.onTap,
    required this.name,
    required this.mobileNo,
  });
}

class MyReferalModel {
  String image;
  String name;
  String mobileNo;
  bool inviteResponse;

  MyReferalModel(
    this.image,
    this.name,
    this.mobileNo,
    this.inviteResponse,);
}
