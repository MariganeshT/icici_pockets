import 'all_services_child_model.dart';

class AllServiceMainModel {
  String? titleName;
  int? noOfRow;
  int? gridCount;
  List<AllServiceChildModel>? ListOfIcons;

  AllServiceMainModel({
    this.titleName,
    this.noOfRow,
    this.gridCount = 4,
    this.ListOfIcons,
  });
}
