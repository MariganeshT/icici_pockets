class ShipmentTrackingStepModel {
  String? description;
  bool? isActive;

  ShipmentTrackingStepModel({this.description, this.isActive});
}
