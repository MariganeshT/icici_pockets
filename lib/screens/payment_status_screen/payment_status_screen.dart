import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/base/base_state.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/screens/payment_status_screen/bloc/paymentstatusscreen_bloc.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:icici/widgets/custom_button.dart';
import 'package:icici/widgets/custom_dialog.dart';
import 'package:icici/widgets/widget_utils.dart';
import 'package:permission_handler/permission_handler.dart';

class PaymentStatusScreen extends StatefulWidget {
  const PaymentStatusScreen({Key? key}) : super(key: key);

  @override
  _PaymentStatusScreenState createState() => _PaymentStatusScreenState();
}

class _PaymentStatusScreenState extends State<PaymentStatusScreen> {
  late PaymentstatusscreenBloc bloc;
  bool showDownload = false;
  final PageController pageController = PageController(
    initialPage: 0,
  );
  int currentPage = 0;

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<PaymentstatusscreenBloc>(context);
    pageController.addListener(() {
      setState(() {
        currentPage = pageController.page!.toInt();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
        bloc: bloc,
        listener: (BuildContext context, BaseState state) {},
        child: BlocBuilder(
            bloc: bloc,
            builder: (BuildContext context, BaseState state) {
              if (state is LoadingState) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
              return Scaffold(
                  backgroundColor: ColorResource.colorFAFAFA,
                  body: Column(
                    children: [
                      Expanded(
                        flex: 0,
                        child: CustomAppbar(
                          titleString: Languages.of(context)!.paymentStatus,
                          iconEnumValues: IconEnum.close,
                          onItemSelected: (values) {
                            if (values == 'download') {
                              permissionRequest(context);
                            } else {
                              Navigator.pop(context);
                            }
                          },
                          backgroundColor: ColorResource.color641653,
                          showDownload: true,
                        ),
                      ),
                      Expanded(
                        child: SingleChildScrollView(
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                              Card(
                                color: ColorResource.colorFFFFFF,
                                elevation: 2,
                                shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(14.0)),
                                ),
                                margin: EdgeInsets.only(
                                    left: 16, right: 16, top: 16),
                                child: Container(
                                  padding: EdgeInsets.only(
                                      left: 16, right: 16, top: 24, bottom: 20),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      CustomText(
                                        Languages.of(context)!.sentTo,
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle1!
                                            .copyWith(
                                                color:
                                                    ColorResource.color787878,
                                                fontWeight: FontWeight.w400),
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          CustomText(
                                            Languages.of(context)!.sentAmountId,
                                            style: Theme.of(context)
                                                .textTheme
                                                .subtitle1!
                                                .copyWith(
                                                    color: ColorResource
                                                        .color222222,
                                                    fontWeight:
                                                        FontWeight.w600),
                                          ),
                                          Container(
                                            height: 48,
                                            width: 48,
                                            decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: ColorResource.color10CB00,
                                            ),
                                            child: Icon(
                                              Icons.done,
                                              color: ColorResource.colorFFFFFF,
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      CustomText(
                                        Languages.of(context)!.amountPaid,
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle1!
                                            .copyWith(
                                                color:
                                                    ColorResource.color787878,
                                                fontWeight: FontWeight.w400),
                                      ),
                                      SizedBox(
                                        height: 6,
                                      ),
                                      CustomText(
                                        Languages.of(context)!.paidAmount,
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline5!
                                            .copyWith(
                                                color:
                                                    ColorResource.color222222,
                                                fontWeight: FontWeight.w700),
                                      ),
                                      SizedBox(
                                        height: 2,
                                      ),
                                      CustomText(
                                        Languages.of(context)!.sentDate,
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle2!
                                            .copyWith(
                                                color:
                                                    ColorResource.color787878,
                                                fontWeight: FontWeight.w400),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Card(
                                elevation: 2,
                                shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(14.0)),
                                ),
                                margin: EdgeInsets.only(
                                    left: 16, right: 16, top: 16),
                                child: Container(
                                  padding: EdgeInsets.only(
                                      left: 16, right: 16, top: 24),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      CustomText(
                                        Languages.of(context)!.transactionId,
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle1!
                                            .copyWith(
                                                color:
                                                    ColorResource.color787878,
                                                fontWeight: FontWeight.w400),
                                      ),
                                      SizedBox(
                                        height: 2,
                                      ),
                                      CustomText(
                                        Languages.of(context)!.transRefNo,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1!
                                            .copyWith(
                                                color:
                                                    ColorResource.color222222,
                                                fontWeight: FontWeight.w600),
                                      ),
                                      SizedBox(
                                        height: 24,
                                      ),
                                      CustomText(
                                        Languages.of(context)!.transferVia,
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle1!
                                            .copyWith(
                                                color:
                                                    ColorResource.color787878,
                                                fontWeight: FontWeight.w400),
                                      ),
                                      SizedBox(
                                        height: 2,
                                      ),
                                      CustomText(
                                        Languages.of(context)!.vpa,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1!
                                            .copyWith(
                                                color:
                                                    ColorResource.color222222,
                                                fontWeight: FontWeight.w600),
                                      ),
                                      SizedBox(
                                        height: 24,
                                      ),
                                      CustomText(
                                        Languages.of(context)!.sentFrom,
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle1!
                                            .copyWith(
                                                color:
                                                    ColorResource.color787878,
                                                fontWeight: FontWeight.w400),
                                      ),
                                      SizedBox(
                                        height: 2,
                                      ),
                                      CustomText(
                                        Languages.of(context)!.sentAmountId,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1!
                                            .copyWith(
                                                color:
                                                    ColorResource.color222222,
                                                fontWeight: FontWeight.w600),
                                      ),
                                      SizedBox(
                                        height: 24,
                                      ),
                                      CustomText(
                                        Languages.of(context)!.remark,
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle1!
                                            .copyWith(
                                                color:
                                                    ColorResource.color787878,
                                                fontWeight: FontWeight.w400),
                                      ),
                                      SizedBox(
                                        height: 2,
                                      ),
                                      CustomText(
                                        Languages.of(context)!.na,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1!
                                            .copyWith(
                                                color:
                                                    ColorResource.color222222,
                                                fontWeight: FontWeight.w600),
                                      ),
                                      SizedBox(
                                        height: 24,
                                      ),
                                      CustomText(
                                        Languages.of(context)!.category,
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle1!
                                            .copyWith(
                                                color:
                                                    ColorResource.color787878,
                                                fontWeight: FontWeight.w400),
                                      ),
                                      SizedBox(
                                        height: 2,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          CustomText(
                                            Languages.of(context)!
                                                .outgoingTransfer,
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyText1!
                                                .copyWith(
                                                    color: ColorResource
                                                        .color222222,
                                                    fontWeight:
                                                        FontWeight.w600),
                                          ),
                                          CustomText(
                                            Languages.of(context)!.edit,
                                            style: Theme.of(context)
                                                .textTheme
                                                .subtitle1!
                                                .copyWith(
                                                  color:
                                                      ColorResource.colorF58220,
                                                  fontWeight: FontWeight.w600,
                                                ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 32,
                              ),
                              Center(
                                child: Container(
                                  height: 38,
                                  width: 125,
                                  child: CustomButton(
                                    Languages.of(context)!.raiseDispute,
                                    buttonBackgroundColor:
                                        ColorResource.colorFAFAFA,
                                    textColor: ColorResource.colorFF781F,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 35,
                              ),
                              SizedBox(
                                child: Stack(
                                  alignment: Alignment.bottomCenter,
                                  children: [
                                    PageView.builder(
                                      controller: pageController,
                                      itemCount: bloc.bannerList.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return bloc.bannerList[index];
                                      },
                                    ),
                                    Flex(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      direction: Axis.horizontal,
                                      children: [
                                        Flexible(
                                          child: Container(
                                            margin: const EdgeInsets.all(20),
                                            height: 10,
                                            child: WidgetUtils
                                                ?.buildPageViewIndicator(
                                                    bloc.bannerList.length,
                                                    ColorResource.colorFF781F,
                                                    ColorResource.colorFFFFFF
                                                        .withOpacity(0.5),
                                                    ColorResource.colorFF781F,
                                                    currentPage),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                height: 150,
                              ),
                              const SizedBox(
                                height: 50,
                              )
                            ])),
                      ),
                    ],
                  ));
            }));
  }

  Future<void> permissionRequest(BuildContext buildContext) async {
    final PermissionStatus permission = await Permission.storage.request();
    if (permission == PermissionStatus.granted) {
    } else {
      await DialogUtils.showDialog(
        buildContext: context,
        title: Languages.of(buildContext)!.storagePermissionTitle,
        description:
            Languages.of(buildContext)!.storagePermissionDescriptionForDownload,
        okBtnText: Languages.of(buildContext)!.allow,
        cancelBtnText: Languages.of(buildContext)!.deny,
        okBtnFunction: (String value) async {
          await openAppSettings();
        },
      );
    }
  }
}
