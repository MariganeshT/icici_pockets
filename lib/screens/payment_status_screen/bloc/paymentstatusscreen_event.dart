part of 'paymentstatusscreen_bloc.dart';

@immutable
abstract class PaymentstatusscreenEvent extends BaseEquatable {}

class PaymentStatusScreenIntialEvent extends PaymentstatusscreenEvent{
  BuildContext context;
  PaymentStatusScreenIntialEvent(this.context);
}
