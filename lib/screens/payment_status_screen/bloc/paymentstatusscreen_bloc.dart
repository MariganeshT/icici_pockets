import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:icici/base/base_state.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:icici/widgets/widget_utils.dart';
import 'package:meta/meta.dart';

part 'paymentstatusscreen_event.dart';

class PaymentstatusscreenBloc
    extends Bloc<PaymentstatusscreenEvent, BaseState> {
  PaymentstatusscreenBloc() : super(InitialState());
  List<Widget> bannerList = [];

  @override
  Stream<BaseState> mapEventToState(
    PaymentstatusscreenEvent event,
  ) async* {
    if (event is PaymentStatusScreenIntialEvent) {
      yield SuccessState();
      bannerList.addAll([
        WidgetUtils.shopAndEarn(event.context, 'Shop & Earn',
            '15% OFF on your bill', 'Use Promo Code: SHOP15'),
        WidgetUtils.KYCTrack(event.context, 'Complete your KYC',
            'Lorem ipsum dolor consectetur adipiscing elit', 'Complete'),
        WidgetUtils.KYCTrack(event.context, 'Track KYC Status',
            'Lorem ipsum dolor consectetur adipiscing elit', 'Track')
      ]);
    }
  }
}
