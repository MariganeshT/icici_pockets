import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:icici/widgets/custom_dropdown_widget.dart';

class AuthenticateAccountScreen extends StatefulWidget {
  @override
  _AuthenticateAccountScreenState createState() =>
      _AuthenticateAccountScreenState();
}

class _AuthenticateAccountScreenState extends State<AuthenticateAccountScreen> {
  TextEditingController gridNumberController_1 = TextEditingController();
  TextEditingController gridNumberController_2 = TextEditingController();
  TextEditingController gridNumberController_3 = TextEditingController();
  FocusNode focusNode1 = FocusNode();
  FocusNode focusNode2 = FocusNode();
  bool error = false;
  FocusNode focusNode3 = FocusNode();

  TextEditingController atmPin = TextEditingController();
  String? atmPinNumber = '1111';
  bool isEnableWrongPin = false;
  bool isWrongGridNo = false;
  bool requiredText = false;
  String selectedAccountText = bankList1[0].accountNumber;

  String selectedDebitText = debitList1[0].cardtext;
  int radioValue = 0;

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: Colors.transparent));
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomAppbar(
              titleString: Languages.of(context)!.authenticateAccount,
              iconEnumValues: IconEnum.back,
              backgroundColor: ColorResource.color641653,
              onItemSelected: (values) {
                Navigator.pop(context);
              },
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                elevation: 0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(24.0)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 20, left: 25),
                      child: CustomText(
                        Languages.of(context)!.selectBankAccount,
                      ),
                    ),
                    GestureDetector(
                      child: Container(
                        margin: EdgeInsets.only(
                            left: 25, right: 25, bottom: 48, top: 8),
                        child: Row(
                          children: [
                            CustomText(
                              selectedAccountText,
                              fontSize: 20,
                              fontWeight: FontWeight.w700,
                            ),
                            SizedBox(
                              width: 13,
                            ),
                            Image.asset(ImageResource.downArrow),
                          ],
                        ),
                      ),
                      onTap: () {
                        setState(() {
                          showBankBottomDropdown(context);
                        });
                      },
                    ),
                    // Container(
                    //   margin: EdgeInsets.only(left: 25,right: 25,bottom: 32,top: 15),
                    //   child: CustomDropDownWidget('XXXX XXXX XXXX 2345','XXXX 3425 XXXX 2347','XXXX 2356 XXXX 3215'),
                    // ),
                    Padding(
                      padding: const EdgeInsets.only(left: 25),
                      child: CustomText(Languages.of(context)!.selectDebitCard),
                    ),
                    GestureDetector(
                      child: Container(
                        margin: EdgeInsets.only(
                            left: 25, right: 25, bottom: 48, top: 8),
                        child: Row(
                          children: [
                            CustomText(
                              selectedDebitText,
                              fontSize: 20,
                              fontWeight: FontWeight.w700,
                            ),
                            SizedBox(
                              width: 13,
                            ),
                            Image.asset(ImageResource.downArrow),
                          ],
                        ),
                      ),
                      onTap: () {
                        setState(() {
                          showDebitBottomDropdown(context);
                        });
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 25, right: 25, bottom: 15),
                      child: CustomText(Languages.of(context)!.gridNumber),
                    ),
                    Stack(
                      children: [
                        Container(
                          height: 188,
                          width: 330,
                          margin:
                              EdgeInsets.only(left: 25, right: 25, bottom: 12),
                          decoration: BoxDecoration(
                              color: ColorResource.color060145,
                              borderRadius: BorderRadius.circular(12)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Container(
                                margin: EdgeInsets.only(top: 50),
                                height: 56,
                                width: 56,
                                decoration: BoxDecoration(
                                  color: ColorResource.colorFFFFFF,
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                child: TextField(
                                  textAlign: TextAlign.center,
                                  style: const TextStyle(
                                      color: ColorResource.colore222222,
                                      fontSize: 22,
                                      fontWeight: FontWeight.w700),
                                  controller: gridNumberController_1,
                                  focusNode: focusNode1,
                                  onChanged: (String values) {
                                    if (values.length == 2) {
                                      focusNode1.unfocus();
                                      focusNode2.requestFocus(FocusNode());
                                    }else{
                                      if (values.isEmpty) {
                                        focusNode1.unfocus();
                                      }
                                    }
                                  },
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: 'I',
                                    hintStyle: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        fontSize: 24,
                                        color: ColorResource.color787878.withOpacity(0.7),

                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 50),
                                height: 56,
                                width: 56,
                                decoration: BoxDecoration(
                                  color: ColorResource.colorFFFFFF,
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                child: TextField(
                                  textAlign: TextAlign.center,
                                  style: const TextStyle(
                                      color: ColorResource.colore222222,
                                      fontSize: 22,
                                      fontWeight: FontWeight.w700),
                                  controller: gridNumberController_2,
                                  keyboardType: TextInputType.number,
                                  focusNode: focusNode2,
                                  // inputFormatters: [
                                  //   FilteringTextInputFormatter.digitsOnly
                                  // ],
                                  onChanged: (String vales) {
                                    if (vales.length == 2) {
                                      focusNode2.unfocus();
                                      focusNode3.requestFocus(FocusNode());
                                    } else if (vales.isEmpty) {
                                      focusNode2.unfocus();
                                      focusNode1.requestFocus(FocusNode());
                                    }
                                  },
                                  decoration:  InputDecoration(
                                     hintText: 'H',
                                    hintStyle: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        fontSize: 24,
                                      color: ColorResource.color787878.withOpacity(0.7),

                                    ),
                                    border: InputBorder.none,
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 50),
                                height: 56,
                                width: 56,
                                decoration: BoxDecoration(
                                  color: ColorResource.colorFFFFFF,
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                child: TextField(
                                  textAlign: TextAlign.center,
                                  style: const TextStyle(
                                      color: ColorResource.colore222222,
                                      fontSize: 22,
                                      fontWeight: FontWeight.w700),
                                  controller: gridNumberController_3,
                                  keyboardType: TextInputType.number,
                                  focusNode: focusNode3,
                                  decoration:  InputDecoration(
                                    border: InputBorder.none,
                                    hintText: 'D',
                                     hintStyle: TextStyle(
                                       fontWeight: FontWeight.w700,
                                       fontSize: 24,
                                       color: ColorResource.color787878.withOpacity(0.7),

                                       ),

                                  ),
                                  onChanged: (String value) {
                                    if (value.isEmpty) {
                                      focusNode3.unfocus();
                                      focusNode2.requestFocus(FocusNode());
                                    }
                                    if (gridNumberController_1.text.isNotEmpty &&
                                        gridNumberController_2
                                            .text.isNotEmpty &&
                                        gridNumberController_3
                                            .text.isNotEmpty &&
                                        value.length == 2) {
                                      debitCardPin(context);
                                      setState(() {
                                        isWrongGridNo = false;
                                      });
                                    } else {
                                      setState(() {
                                        isWrongGridNo = true;
                                      });
                                    }
                                  },
                                  //  onEditingComplete: (){
                                  //    debitCardPin();
                                  //  },
                                ),
                              )
                            ],
                          ),
                        ),
                        Container(
                            margin:
                                EdgeInsets.only(left: 25, right: 25, top: 25),
                            child: Image.asset(ImageResource.cardRectangle)),
                      ],
                    ),
                    //  const SizedBox(height: 12,),
                    isWrongGridNo
                        ? Center(
                            child: CustomText(
                              Languages.of(context)!.gridWrongHint,
                              fontSize: 12,
                              color: ColorResource.colorf92538,
                            ),
                          )
                        : SizedBox(),
                    const SizedBox(
                      height: 16,
                    ),
                    Container(
                        margin:
                            EdgeInsets.only(left: 50, right: 40, bottom: 120),
                        child: CustomText(
                          Languages.of(context)!.gridHint,
                          lineHeight: 1.7,
                          fontSize: 12,
                        )),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  debitCardPin(BuildContext context) {
    return showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        enableDrag: true,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(32.0), topRight: Radius.circular(32.0)),
        ),
        backgroundColor: ColorResource.colorFFFFFF,
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(builder: (BuildContext context, setState) {
            return Padding(
              padding: MediaQuery.of(context).viewInsets,
              child: Container(
                constraints: const BoxConstraints(
                  minHeight: 240,
                  maxHeight: 260,
                ),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 16,
                    ),
                    CustomText(
                      'One last thing!!',
                      color: ColorResource.color787878,
                      fontSize: 12,
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    CustomText(
                      'Debit Card PIN (ATM PIN)',
                      color: ColorResource.colore222222,
                      fontWeight: FontWeight.w700,
                      fontSize: 18,
                    ),
                    const SizedBox(
                      height: 40,
                    ),
                    PinCodeTextField(
                      appContext: context,
                      onChanged: (String pin) {},
                      autoFocus: true,
                      onCompleted: (String pin) {
                        if (pin == atmPinNumber) {
                          setState(() {
                            isEnableWrongPin = false;

                          });

                          while (Navigator.canPop(context)) {
                            Navigator.pop(context);
                          }
                        } else {
                          setState(() {
                            isEnableWrongPin = true;

                          });
                        }
                      },
                      length: 4,
                      textStyle: Theme.of(context)
                          .textTheme
                          .headline5!
                          .copyWith(color: ColorResource.color222222),
                      animationType: AnimationType.scale,
                      animationDuration: const Duration(milliseconds: 300),
                      keyboardType: TextInputType.number,
                      // controller: atmPin,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,

                      pinTheme: PinTheme(
                        shape: PinCodeFieldShape.box,
                        borderWidth: 1,
                        borderRadius: BorderRadius.circular(8),
                        inactiveColor: error
                            ? ColorResource.colorF92538
                            : ColorResource.color222222,
                        selectedColor: error
                            ? ColorResource.colorF92538
                            : ColorResource.color222222,
                        activeColor: error
                            ? ColorResource.colorF92538
                            : ColorResource.color222222,
                        fieldHeight: 56,
                        fieldWidth: 56,
                      ),
                    ),
                    isEnableWrongPin
                        ? const SizedBox(
                            height: 10,
                          )
                        : SizedBox(),
                    isEnableWrongPin
                        ? CustomText(
                            'Please enter the correct PIN',
                            color: ColorResource.colorf92538,
                            fontSize: 12,
                          )
                        : SizedBox(),
                    isEnableWrongPin
                        ? const SizedBox(
                            height: 23,
                          )
                        : const SizedBox(
                            height: 50,
                          ),
                  ],
                ),
              ),
            );
          });
        });
  }

  dynamic showDebitBottomDropdown(BuildContext context) {
    return showModalBottomSheet(
      isScrollControlled: true,
      isDismissible: false,
      enableDrag: false,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(32.0), topRight: Radius.circular(32.0)),
      ),
      backgroundColor: ColorResource.colorFFFFFF,
      context: context,
      builder: (BuildContext context) {
        return Container(
          constraints: const BoxConstraints(
            minHeight: 100,
            maxHeight: 230,
          ),
          child: Stack(
            children: [
              Padding(
                padding: const EdgeInsets.all(11.0),
                child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Image.asset(
                    ImageResource.close,
                    height: 27,
                    width: 27,
                  ),
                ),
              ),
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                const SizedBox(
                  height: 18,
                ),
                const Center(
                  child: Text(
                    'Select Debit Card',
                    style: TextStyle(
                        fontSize: 18,
                        color: ColorResource.color222222,
                        fontWeight: FontWeight.w700),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 31),
                  child: Wrap(
                    children: [
                      SizedBox(
                        height: debitList1.length * 80.0,
                        child: ListView.builder(
                            itemCount: debitList1.length,
                            itemBuilder: (BuildContext context, int index) {
                              return Padding(
                                padding: const EdgeInsets.only(bottom: 18.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(9.5),
                                      border: Border.all(
                                          color: ColorResource.colorEFEFEF)),
                                  child: GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        radioValue = index;
                                        selectedDebitText =
                                            debitList1[index].cardtext;
                                        SharedPreferences.getInstance()
                                            .then((pref) {
                                          pref.setInt(
                                              'radioValueKey', radioValue);
                                          pref.setString('radioTextKey',
                                              selectedDebitText);
                                        });
                                      });
                                      Navigator.pop(context);
                                    },
                                    child: ListTile(
                                      leading: CircleAvatar(
                                        radius: 11.5,
                                        backgroundColor: ColorResource
                                            .colorFF781F
                                            .withOpacity(.35),
                                      ),
                                      title: Text(
                                        debitList1[index].cardtext,
                                        style: TextStyle(
                                            color: ColorResource.colore222222),
                                      ),
                                      trailing: radioValue == index
                                          ? CircleAvatar(
                                              radius: 12,
                                              backgroundColor:
                                                  ColorResource.color10CB00,
                                              child: Center(
                                                  child: Icon(
                                                Icons.done,
                                                size: 10,
                                                color:
                                                    ColorResource.colorFFFFFF,
                                              )))
                                          : Container(
                                              height: 22,
                                              width: 22,
                                              decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  color:
                                                      ColorResource.colorFFFFFF,
                                                  border: Border.all(
                                                    color: ColorResource
                                                        .color222222,
                                                  )),
                                            ),
                                    ),
                                  ),
                                ),
                              );
                            }),
                      ),
                    ],
                  ),
                )
              ]),
            ],
          ),
        );
      },
    );
  }

  dynamic showBankBottomDropdown(BuildContext context) {
    return showModalBottomSheet(
      isScrollControlled: true,
      isDismissible: false,
      enableDrag: false,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(32.0), topRight: Radius.circular(32.0)),
      ),
      backgroundColor: ColorResource.colorFFFFFF,
      context: context,
      builder: (BuildContext context) {
        return Container(
          constraints: const BoxConstraints(
            minHeight: 150,
            maxHeight: 230,
          ),
          child: Stack(
            children: [
              Padding(
                padding: const EdgeInsets.all(11.0),
                child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Image.asset(
                    ImageResource.close,
                    height: 27,
                    width: 27,
                  ),
                ),
              ),
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                const SizedBox(
                  height: 18,
                ),
                const Center(
                  child: Text(
                    'Select Bank Account',
                    style: TextStyle(
                        fontSize: 18,
                        color: ColorResource.color222222,
                        fontWeight: FontWeight.w700),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 31),
                  child: Wrap(
                    children: [
                      SizedBox(
                        height: bankList1.length * 80.0,
                        child: ListView.builder(
                            itemCount: bankList1.length,
                            itemBuilder: (BuildContext context, int index) {
                              return Padding(
                                padding: const EdgeInsets.only(bottom: 18.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(9.5),
                                      border: Border.all(
                                          color: ColorResource.colorEFEFEF)),
                                  child: GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        radioValue = index;
                                        selectedAccountText =
                                            bankList1[index].accountNumber;
                                        SharedPreferences.getInstance()
                                            .then((pref) {
                                          pref.setInt(
                                              'accountratio', radioValue);
                                          pref.setString('accountNumber',
                                              selectedAccountText);
                                        });
                                      });
                                      Navigator.pop(context);
                                    },
                                    child: ListTile(
                                      leading: CircleAvatar(
                                        radius: 11.5,
                                        backgroundColor: ColorResource
                                            .colorFF781F
                                            .withOpacity(.35),
                                      ),
                                      title: Text(
                                        bankList1[index].accountNumber,
                                        style: TextStyle(
                                            color: ColorResource.colore222222),
                                      ),
                                      trailing: radioValue == index
                                          ? CircleAvatar(
                                              radius: 12,
                                              backgroundColor:
                                                  ColorResource.color10CB00,
                                              child: Center(
                                                  child: Icon(
                                                Icons.done,
                                                size: 10,
                                                color:
                                                    ColorResource.colorFFFFFF,
                                              )))
                                          : Container(
                                              height: 22,
                                              width: 22,
                                              decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  color:
                                                      ColorResource.colorFFFFFF,
                                                  border: Border.all(
                                                    color: ColorResource
                                                        .color222222,
                                                  )),
                                            ),
                                    ),
                                  ),
                                ),
                              );
                            }),
                      ),
                    ],
                  ),
                )
              ]),
            ],
          ),
        );
      },
    );
  }
}

class DebitList {
  String cardtext;

  DebitList({required this.cardtext});
}

List<DebitList> debitList1 = [
  DebitList(cardtext: "3247 XXXX XXXX 2345"),
  DebitList(cardtext: "XXXX 2222 333X 3211"),
];

class BankList {
  String accountNumber;

  BankList({required this.accountNumber});
}

List<BankList> bankList1 = [
  BankList(accountNumber: "4799 XXXX XXXX 3427"),
  BankList(accountNumber: "8012 XXXX 9090 8956"),
];
