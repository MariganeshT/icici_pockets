part of 'authenticateaccountscreen_bloc.dart';

@immutable
abstract class AuthenticateAccountScreenEvent extends BaseEquatable{}

class AuthenticateAccountScreenIntialEvent extends AuthenticateAccountScreenEvent{}
