import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:meta/meta.dart';

part 'authenticateaccountscreen_event.dart';
part 'authenticateaccountscreen_state.dart';

class AuthenticateAccountScreenBloc extends Bloc<AuthenticateAccountScreenEvent,
    AuthenticateAccountScreenState> {
  AuthenticateAccountScreenBloc() : super(AuthenticateAccountScreenInitial());

  @override
  Stream<AuthenticateAccountScreenState> mapEventToState(
    AuthenticateAccountScreenEvent event,
  ) async* {
    // TODO: implement mapEventToState
  }
}
