part of 'dashboardscreen_bloc.dart';

@immutable
abstract class DashboardscreenEvent extends BaseEquatable {}

class DashboardInitialEvent extends DashboardscreenEvent {
  BuildContext? context;

  DashboardInitialEvent({this.context});
}

class QRCodeBottomSheetEvent extends DashboardscreenEvent {}

class NavigateProfileScreenEvent extends DashboardscreenEvent {}
