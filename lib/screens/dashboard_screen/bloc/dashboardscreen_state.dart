part of 'dashboardscreen_bloc.dart';

@immutable
abstract class DashboardscreenState {}

class DashboardscreenInitial extends DashboardscreenState {}

class DashboardscreenLoadingState extends DashboardscreenState {}

class DashboardscreenLoadedState extends DashboardscreenState {}

class QRCodeBottomSheetState extends DashboardscreenState {}

class NavigateProfileScreenState extends DashboardscreenState {}

