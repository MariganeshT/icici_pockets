import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:icici/model/trending_service.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/widgets/widget_utils.dart';
import 'package:meta/meta.dart';

part 'dashboardscreen_event.dart';

part 'dashboardscreen_state.dart';

class DashboardscreenBloc
    extends Bloc<DashboardscreenEvent, DashboardscreenState> {
  DashboardscreenBloc() : super(DashboardscreenInitial());

  List<TrendingService> trendingServices = [];
  List<Widget> bannerList = [];

  @override
  Stream<DashboardscreenState> mapEventToState(
    DashboardscreenEvent event,
  ) async* {
    if (event is DashboardInitialEvent) {
      yield DashboardscreenLoadingState();
      trendingServices.addAll([
        TrendingService(ImageResource.servicesIcon1, 'Pay to contacts', true),
        TrendingService(ImageResource.servicesIcon2, 'Fund Transfer', true,
            requiredText: 'KYC Required'),
        TrendingService(ImageResource.servicesIcon3, 'UPI', true,
            requiredText: 'New'),
        TrendingService(ImageResource.servicesIcon4, 'Request Money', false),
        TrendingService(ImageResource.servicesIcon5, 'Bill Pay', false),
        TrendingService(ImageResource.servicesIcon6, 'Recharge', false),
        TrendingService(ImageResource.all_service_fastag, 'FASTTag', true,
            requiredText: 'FastTag'),
        TrendingService(ImageResource.servicesIcon8, 'Offers', false),
      ]);
      bannerList.addAll([
        WidgetUtils.KYCTrack(event.context!, 'Complete your KYC',
            'Lorem ipsum dolor consectetur adipiscing elit', 'Complete'),
        WidgetUtils.KYCTrack(event.context!, 'Track KYC Status',
            'Lorem ipsum dolor consectetur adipiscing elit', 'Track'),
        WidgetUtils.shopAndEarn(event.context!, 'Shop & Earn',
            '15% OFF on your bill', 'Use Promo Code: SHOP15')
      ]);
      yield DashboardscreenLoadedState();
    }
    if (event is QRCodeBottomSheetEvent) {
      yield QRCodeBottomSheetState();
    }
    if (event is NavigateProfileScreenEvent) {
      yield NavigateProfileScreenState();
    }
  }
}
