import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/languages/app_locale_constant.dart';
import 'package:icici/model/trending_service.dart';
import 'package:icici/router.dart';
import 'package:icici/screens/dashboard_screen/bloc/dashboardscreen_bloc.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/constants.dart';
import 'package:icici/utils/font.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/preference_helper.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:icici/widgets/custom_button.dart';
import 'package:icici/widgets/dashboard_card_design_widget.dart';
import 'package:icici/widgets/display_qr.dart';
import 'package:icici/widgets/widget_utils.dart';

class DashBoardScreen extends StatefulWidget {
  const DashBoardScreen({Key? key}) : super(key: key);

  @override
  _DashBoardScreenState createState() => _DashBoardScreenState();
}

class _DashBoardScreenState extends State<DashBoardScreen> {
  Locale? _locale;
  String? selectedMenu;
  final PageController pageController = PageController(
    initialPage: 0,
  );
  int currentPage = 0;

  late DashboardscreenBloc bloc;

  @override
  void didChangeDependencies() {
    getLocale().then((Locale locale) {
      setState(() {
        _locale = locale;
      });
    });
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<DashboardscreenBloc>(context);
    selectedMenu = Constants.home;
    pageController.addListener(() {
      setState(() {
        currentPage = pageController.page!.toInt();
      });
    });
    dashboardBottomSheet();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: Colors.transparent));

    return BlocListener(
      bloc: bloc,
      listener: (BuildContext context, DashboardscreenState state) {
        if (state is NavigateProfileScreenState) {
          Navigator.pushNamed(context, AppRoutes.profileScreen);
        }
        if (state is QRCodeBottomSheetState) {
          displayQR('https://flutter.dev/');
        }
      },
      child: BlocBuilder(
        bloc: bloc,
        builder: (BuildContext context, DashboardscreenState state) {
          if (state is DashboardscreenLoadingState) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          return Scaffold(

            backgroundColor: ColorResource.colorFAFAFA,
            bottomNavigationBar:

            Container(
              decoration:  BoxDecoration(
              color: ColorResource.colorFFFFFF,
              borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20.0),
              topRight: Radius.circular(20.0),
          ),
                boxShadow: [
                  BoxShadow(
                    color: ColorResource.color787878.withOpacity(0.5),
                    blurRadius: 10,
                    offset: Offset(0, 4),

                  ),
                ]
              ),

              child: Container(
                padding: EdgeInsets.zero,
                decoration: const BoxDecoration(
                    color: ColorResource.colorFFFFFF,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.0),
                      topRight: Radius.circular(20.0),
                    ),
                  // boxShadow: [
                  //   BoxShadow(
                  //     blurRadius: 1,
                  //     offset: Offset(0, 8),
                  //     // spreadRadius: 1,
                  //   ),
                  // ]
                ),
                height: 67,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    GestureDetector(
                      child: AnimatedContainer(
                        width: 60,
                        duration: const Duration(milliseconds: 250),
                        child: Container(
                          margin: const EdgeInsets.only(bottom: 10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Column(
                                children: [
                                  Container(
                                    height: 3,
                                    width: 48,
                                    decoration: selectedMenu == Constants.home
                                        ? const BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                              Radius.circular(40),
                                            ),
                                            gradient: LinearGradient(colors: [
                                              ColorResource.colorFF792C,
                                              ColorResource.colorFC3C36,
                                            ], stops: [
                                              0.0,
                                              1.0
                                            ], tileMode: TileMode.decal),
                                          )
                                        : null,
                                  ),
                                ],
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              Image(
                                  image: const AssetImage(ImageResource.home),
                                  height: 20,
                                  width: 20,
                                  color: selectedMenu == Constants.home
                                      ? ColorResource.colorFF781F
                                      : ColorResource.color787878),
                              const SizedBox(
                                height: 6,
                              ),
                              CustomText(Languages.of(context)!.home,
                                  style: Theme.of(context)
                                      .textTheme
                                      .subtitle1!
                                      .copyWith(
                                          color: selectedMenu == Constants.home
                                              ? ColorResource.colorFF781F
                                              : ColorResource.color787878,
                                          fontFamily: Font.sourceSansProRegular
                                              .toString(),
                                          fontWeight: FontWeight.w600)),
                            ],
                          ),
                        ),
                      ),
                      onTap: () {
                        setState(() {
                          selectedMenu = Constants.home;
                        });
                      },
                    ),
                    GestureDetector(
                      onTap: () {
                        bloc.add(QRCodeBottomSheetEvent());
                      },
                      child: Container(
                        height: 55,
                        width: 55,
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(40),
                          ),
                          gradient: LinearGradient(colors: [
                            ColorResource.colorFF7B2B,
                            ColorResource.colorFC3B36,
                          ], stops: [
                            0.0,
                            1.0
                          ], tileMode: TileMode.decal),
                        ),
                        child: Container(
                          height: 26,
                          width: 30,
                          child: const Image(
                            image: AssetImage(
                              ImageResource.scan,
                            ),
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          selectedMenu = Constants.bag;
                        });
                      },
                      child: AnimatedContainer(
                        width: 60,
                        duration: const Duration(milliseconds: 250),
                        child: Container(
                          margin: const EdgeInsets.only(bottom: 10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Column(
                                children: [
                                  Container(
                                    height: 3,
                                    width: 48,
                                    decoration: selectedMenu == Constants.bag
                                        ? const BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                              Radius.circular(40),
                                            ),
                                            gradient: LinearGradient(colors: [
                                              ColorResource.colorFF792C,
                                              ColorResource.colorFC3C36,
                                            ], stops: [
                                              0.0,
                                              1.0
                                            ], tileMode: TileMode.decal),
                                          )
                                        : null,
                                  ),
                                ],
                              ),
                              //
                              const SizedBox(
                                height: 10,
                              ),
                              Image(
                                  image: const AssetImage(ImageResource.bag),
                                  height: 20,
                                  width: 20,
                                  color: selectedMenu == Constants.bag
                                      ? ColorResource.colorFF781F
                                      : ColorResource.color787878),
                              const SizedBox(
                                height: 6,
                              ),
                              CustomText(Languages.of(context)!.rewards,
                                  style: Theme.of(context)
                                      .textTheme
                                      .subtitle1!
                                      .copyWith(
                                          color: selectedMenu == Constants.bag
                                              ? ColorResource.colorFF781F
                                              : ColorResource.color787878,
                                          fontFamily: Font.sourceSansProRegular
                                              .toString(),
                                          fontWeight: FontWeight.w600)),
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            body: SafeArea(
              top: false,
              bottom: true,
              child: Stack(
                children: [
                  Column(
                    children: [
                      Expanded(
                          flex: 3,
                          child: Container(
                            decoration: const BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                    ImageResource.dashboard_background),
                                fit: BoxFit.cover,
                              ),
                            ),
                          )),
                      Expanded(
                          flex: 1,
                          child: Container(
                            color: ColorResource.colorFAFAFA,
                          )),
                    ],
                  ),
                  Flex(
                    direction: Axis.vertical,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Column(
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 16, right: 0),
                              child: CustomAppbar(
                                titleString:
                                    'Prince ${Languages.of(context)!.pockets}',
                                subTitle:
                                    Languages.of(context)!.goodToSeeYouBack,
                                showSearch: true,
                                showNotification: true,
                                showSettings: true,
                                iconEnumValues: IconEnum.mainMenu,
                                notificationCount: 11,
                                onItemSelected: (values) async {
                                  if (values == 'notificationTriggering') {
                                    await Navigator.pushNamed(context,
                                        AppRoutes.notificationScreen);
                                  }
                                  if (values == 'IconEnum.mainMenu') {
                                    // values = 'Profile';
                                    bloc.add(NavigateProfileScreenEvent());
                                  }
                                  if (values == 'settingTriggering') {
                                    await Navigator.pushNamed(context,
                                        AppRoutes.settingsMainScreen);
                                  }
                                  /*AppUtils.showToast(values.toString(),
                                      gravity: ToastGravity.BOTTOM);*/
                                },
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: SingleChildScrollView(
                                child: Column(
                                  children: [
                                    const DashBoardCardDesign(),
                                    const SizedBox(height: 15),
                                    Container(
                                      decoration: const BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(23),
                                          topRight: Radius.circular(23),
                                        ),
                                        color: ColorResource.colorFAFAFA,
                                      ),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            child: Stack(
                                              alignment:
                                                  Alignment.bottomCenter,
                                              children: [
                                                PageView.builder(
                                                  controller: pageController,
                                                  itemCount:
                                                      bloc.bannerList.length,
                                                  itemBuilder:
                                                      (BuildContext context,
                                                          int index) {
                                                    return bloc
                                                        .bannerList[index];
                                                  },
                                                ),
                                                Flex(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .center,
                                                  direction: Axis.horizontal,
                                                  children: [
                                                    Flexible(
                                                      child: Container(
                                                        margin:
                                                            const EdgeInsets
                                                                .all(20),
                                                        height: 10,
                                                        child: WidgetUtils
                                                            ?.buildPageViewIndicator(
                                                                bloc.bannerList
                                                                    .length,
                                                                ColorResource
                                                                    .colorFF781F,
                                                                ColorResource
                                                                    .colorFFFFFF
                                                                    .withOpacity(
                                                                        0.5),
                                                                ColorResource
                                                                    .colorFF781F,
                                                                currentPage),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                            height: 150,
                                          ),
                                          //
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                top: 5, left: 16, bottom: 10),
                                            child: CustomText(
                                              StringResource.trendingServices,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 20,
                                            ),
                                          ),
                                          Wrap(
                                            runSpacing: 45,
                                            children:
                                                buildTrendingServiceWidget(),
                                          ),
                                          Center(
                                            child: GestureDetector(
                                              onTap: () async {
                                                await Navigator.pushNamed(
                                                    context,
                                                    AppRoutes
                                                        .allServicesScreen);
                                              },
                                              child: Container(
                                                  alignment: Alignment.center,
                                                  margin:
                                                      const EdgeInsets.only(
                                                          top: 25),
                                                  padding: EdgeInsets.only(
                                                      left: 12),
                                                  height: 40,
                                                  width: 133,
                                                  decoration: BoxDecoration(
                                                      color: ColorResource
                                                          .colorFFFFFF,
                                                      borderRadius:
                                                          BorderRadius
                                                              .circular(8)),
                                                  child: Center(
                                                      child: Row(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .center,
                                                          children: [
                                                        CustomText(
                                                            Languages.of(
                                                                    context)!
                                                                .seeAllServices,
                                                            style: Theme.of(
                                                                    context)
                                                                .textTheme
                                                                .subtitle1!
                                                                .copyWith(
                                                                    color: ColorResource
                                                                        .color222222,
                                                                    fontSize:
                                                                        12,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w700)),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  left: 7),
                                                          child: Image.asset(
                                                              ImageResource
                                                                  .servicesIcon9),
                                                        ),
                                                      ]))),
                                            ),
                                          ),
                                          Container(
                                            height: 175,
                                            decoration: const BoxDecoration(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(18.0)),
                                              color: ColorResource
                                                  .coloreffffffff,
                                              image: DecorationImage(
                                                fit: BoxFit.fill,
                                                image: AssetImage(
                                                    ImageResource
                                                        .suppor_background),
                                              ),
                                            ),
                                            margin:
                                                const EdgeInsets.symmetric(
                                                    horizontal: 16.0,
                                                    vertical: 24.0),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Container(
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          CustomText(
                                                            '${Languages.of(context)!.needHelp} ?',
                                                            style: Theme.of(
                                                                    context)
                                                                .textTheme
                                                                .bodyText1!
                                                                .copyWith(
                                                                    color: ColorResource
                                                                        .color222222,
                                                                    fontStyle:
                                                                        FontStyle
                                                                            .normal,
                                                                    locale:
                                                                        _locale,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w700),
                                                          ),
                                                          const SizedBox(
                                                            height: 10,
                                                          ),
                                                          CustomText(
                                                            Languages.of(
                                                                    context)!
                                                                .supportingHours,
                                                            style: Theme.of(
                                                                    context)
                                                                .textTheme
                                                                .subtitle1!
                                                                .copyWith(
                                                                    color: ColorResource
                                                                        .color787878,
                                                                    fontStyle:
                                                                        FontStyle
                                                                            .normal,
                                                                    locale:
                                                                        _locale,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400),
                                                          ),
                                                        ],
                                                      ),
                                                      const SizedBox(
                                                        height: 15,
                                                      ),
                                                      GestureDetector(
                                                        child: Container(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(10.0),
                                                          alignment: Alignment
                                                              .centerLeft,
                                                          decoration: BoxDecoration(
                                                              borderRadius:
                                                                  const BorderRadius
                                                                          .all(
                                                                      Radius.circular(
                                                                          5.0)),
                                                              color: ColorResource
                                                                  .colorefffffff,
                                                              border: Border.all(
                                                                  color: ColorResource
                                                                      .colorFF781F,
                                                                  width:
                                                                      1.0)),
                                                          child: CustomText(
                                                            Languages.of(
                                                                    context)!
                                                                .liveSupport,
                                                            style: Theme.of(
                                                                    context)
                                                                .textTheme
                                                                .subtitle2!
                                                                .copyWith(
                                                                    color: ColorResource
                                                                        .colorFF781F,
                                                                    locale:
                                                                        _locale,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w700),
                                                          ),
                                                        ),
                                                        onTap: () {
                                                          Navigator.pushNamed(
                                                              context,
                                                              AppRoutes
                                                                  .supportScreen);
                                                        },
                                                      )
                                                    ],
                                                  ),
                                                  margin:
                                                      const EdgeInsets.only(
                                                          left: 5, top: 43),
                                                  padding: EdgeInsets.only(
                                                      left: 10),
                                                ),
                                                Expanded(
                                                  flex: 1,
                                                  child: Container(
                                                    child: const Image(
                                                        alignment:
                                                            Alignment.center,
                                                        image: AssetImage(
                                                            ImageResource
                                                                .support_image)),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  List<Widget> buildTrendingServiceWidget() {
    final List<Widget> widgetList = [];
    bloc.trendingServices.forEach((TrendingService element) {
      widgetList.add(
        GestureDetector(
          onTap: () {
            if (element.requiredText != null &&
                element.requiredText!.toLowerCase().contains('kyc')) {
              kycRequiredDialog();
            }
          },
          child: Container(
            width: MediaQuery.of(context).size.width / 4,
            child: Column(
              children: [
                Container(
                  height: 25,
                  alignment: Alignment.center,
                  child: Visibility(
                    visible: element.isRequired,
                    child: Container(
                      margin: const EdgeInsets.only(
                          left: 24, right: 24, bottom: 3.0),
                      padding: const EdgeInsets.all(3),
                      child: CustomText(
                        element.requiredText != null
                            ? element.requiredText.toString()
                            : 'Required',
                        style: Theme.of(context).textTheme.caption!.copyWith(
                            color: ColorResource.color222222,
                            fontWeight: FontWeight.w700),
                        textAlign: TextAlign.center,
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: ColorResource.colorF9BE25,
                      ),
                      // height: 19,
                      // width: 86,
                    ),
                  ),
                ),
                Container(
                    // alignment: Alignment.center,
                    // margin: const EdgeInsets.only(left: 22, right: 24),
                    child: Image.asset(
                  element.iconName,
                  width: 32,
                  height: 32,
                )),
                Container(
                  // alignment: Alignment.center,
                  padding: const EdgeInsets.only(left: 10, right: 10, top: 10),
                  child: CustomText(
                    element.title,
                    style: Theme.of(context).textTheme.subtitle1!.copyWith(
                        color: ColorResource.color222222,
                        fontWeight: FontWeight.w400),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    });
    return widgetList;
  }

  dynamic kycRequiredDialog() {
    showModalBottomSheet(
        isDismissible: false,
        enableDrag: false,
        isScrollControlled: true,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
        ),
        backgroundColor: ColorResource.colorFFFFFF,
        context: context,
        builder: (BuildContext context) {
          return Container(
            constraints: const BoxConstraints(
              minHeight: 150,
              maxHeight: 430,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Stack(
                  children: [
                    Container(
                        alignment: Alignment.center,
                        margin: const EdgeInsets.only(left: 10, top: 7),
                        child: Image.asset(
                          ImageResource.kyc_dialog_image,
                          width: 200,
                          height: 200,
                        )),
                    Container(
                      height: 32,
                      width: 32,
                      margin: const EdgeInsets.only(left: 10, top: 10),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: CircleAvatar(
                          backgroundColor:
                              ColorResource.colorBFCBD7.withOpacity(0.3),
                          child: const Icon(
                            Icons.close,
                            size: 20,
                            color: ColorResource.color222222,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                Container(
                  margin:
                      const EdgeInsets.symmetric(horizontal: 24, vertical: 6),
                  child: CustomText(Languages.of(context)!.kycComplete,
                      style: Theme.of(context).textTheme.headline5!.copyWith(
                          color: ColorResource.color222222,
                          fontWeight: FontWeight.w700)),
                ),
                SizedBox(
                  height: 8,
                ),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 24),
                  child: CustomText(
                      Languages.of(context)!.kycCompleteDescription,
                      style: Theme.of(context).textTheme.subtitle1!.copyWith(
                          color: ColorResource.color222222.withOpacity(0.5),
                          fontWeight: FontWeight.w400)),
                ),

                Container(
                  margin: const EdgeInsets.only(
                      top: 22, left: 24, right: 24, bottom: 8),
                  child: CustomButton(
                    Languages.of(context)!.completeKYC,
                    onTap: () {
                      Navigator.pop(context);
                    },
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    PreferenceHelper.setPreference(
                        Constants.kycDontAskMeAgain, true);
                    Navigator.pop(context);
                  },
                  child: Container(
                    alignment: Alignment.center,
                    margin: const EdgeInsets.only(top: 12,bottom: 3),
                    child: CustomText(Languages.of(context)!.dontShowMeAgain,
                        style: Theme.of(context).textTheme.subtitle2!.copyWith(
                            color: ColorResource.colorFF781F,
                            fontWeight: FontWeight.w700)),
                  ),
                ),
              ],
            ),
          );
        });
  }

  void displayQR(String qrtext) {
    showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        context: context,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(32.0), topRight: Radius.circular(32.0)),
        ),
        backgroundColor: ColorResource.colorFFFFFF,
        builder: (BuildContext context) {
          Future.delayed(const Duration(minutes: 2), () {
            Navigator.pop(context);
          });
          return DisplayQR(
            qrtext,
          );
        });
  }

  dynamic dashboardBottomSheet() {
    WidgetsBinding.instance!.addPostFrameCallback((_) async {
      await showModalBottomSheet(
          isScrollControlled: true,
          isDismissible: false,
          enableDrag: false,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(32.0),
                topRight: Radius.circular(32.0)),
          ),
          backgroundColor: ColorResource.colorFFFFFF,
          context: context,
          builder: (BuildContext context) {
            return Container(
              constraints: const BoxConstraints(
                minHeight: 150,
                maxHeight: 450,
              ),
              child: WillPopScope(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 6,
                      ),
                      Stack(
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: 16, top: 10),
                            child: GestureDetector(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: Image.asset(ImageResource.close)),
                          ),
                          Center(
                            child: Container(
                                alignment: Alignment.center,
                                height: 150,
                                width: 150,
                                child: Image.asset(
                                    ImageResource.pocketRewardBottomSheet)),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 6,
                      ),
                      Container(
                        margin:
                            const EdgeInsets.only(top: 2, left: 24, right: 24),
                        child: CustomText(
                          Languages.of(context)!.pocketsRewards,
                          style: Theme.of(context)
                              .textTheme
                              .subtitle1!
                              .copyWith(
                                  color: ColorResource.color222222,
                                  fontSize: 22,
                                  fontWeight: FontWeight.w600),
                        ),
                      ),
                      Container(
                        margin:
                            const EdgeInsets.only(top: 22, left: 24, right: 24),
                        child: CustomText(
                          Languages.of(context)!.pocketsRewardsDes,
                          style: Theme.of(context)
                              .textTheme
                              .subtitle1!
                              .copyWith(
                                  color: ColorResource.color222222
                                      .withOpacity(0.5),
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400),
                        ),
                      ),
                      Container(
                        margin:
                            const EdgeInsets.only(top: 22, left: 24, right: 24),
                        child: CustomText(
                          Languages.of(context)!.pocketsRewardsDes1,
                          style: Theme.of(context)
                              .textTheme
                              .subtitle1!
                              .copyWith(
                                  color: ColorResource.color222222
                                      .withOpacity(0.5),
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400),
                        ),
                      ),
                      Container(
                        margin:
                            const EdgeInsets.only(top: 22, left: 24, right: 24),
                        child: CustomButton(
                          Languages.of(context)!.availNow,
                          onTap: () {
                            Navigator.pop(context);
                          },
                        ),
                      ),
                      Center(
                        child: Container(
                          margin: const EdgeInsets.only(
                              top: 22, left: 24, right: 24),
                          child: CustomText(
                            Languages.of(context)!.termsAndConditionsApply,
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                                    color: ColorResource.color787878,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400),
                          ),
                        ),
                      ),
                    ],
                  ),
                  onWillPop: () async => false),
            );
          });
    });
  }
}
