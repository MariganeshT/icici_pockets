import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/screens/confirm_passcode_screen/confirm_success.dart';
import 'package:icici/screens/set_digit_pin_screen/bloc/set_digit_pin_bloc.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/font.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:icici/widgets/custom_button.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import 'confirm_pin_screen.dart';

class SetDigitPinScreen extends StatefulWidget {
  @override
  _SetDigitPinScreenState createState() => _SetDigitPinScreenState();
}

class _SetDigitPinScreenState extends State<SetDigitPinScreen> {
  late SetDigitPinBloc bloc;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController passcodeController = TextEditingController();
  final TextEditingController confirmPasscodeController =
      TextEditingController();

  bool error = false;
  bool buttonVisible = false;

  @override
  void initState() {
    bloc = SetDigitPinBloc()..add(SetDigitPinInitialEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        key: _formKey,
        backgroundColor: ColorResource.colorFAFAFA,
        body: BlocListener<SetDigitPinBloc, SetDigitPinState>(
          bloc: bloc,
          listener: (context, state) {},
          child: BlocBuilder<SetDigitPinBloc, SetDigitPinState>(
              bloc: bloc,
              builder: (context, state) {
                if (state is SetDigitPinLoading) {
                  return Scaffold(
                    body: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
                } else if (state is SetDigitPinLoaded) {
                  return Column(
                    children: [
                      SizedBox(
                        child: CustomAppbar(
                          backgroundColor: ColorResource.color641653,
                          titleString: Languages.of(context)!.set4DigitPIN,
                          titleSpacing: 8,
                          style: Theme.of(context)
                              .textTheme
                              .headline5!
                              .copyWith(color: ColorResource.colorFFFFFF),
                          iconEnumValues: IconEnum.close,
                          onItemSelected: (selectedItem) {
                            Navigator.pop(context);
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: CustomText(
                          Languages.of(context)!.thisPasscode,
                          textAlign: TextAlign.center,
                          style: Theme.of(context)
                              .textTheme
                              .subtitle1!
                              .copyWith(color: ColorResource.color787878),
                        ),
                      ),
                      const SizedBox(height: 45),
                      CustomText(
                        Languages.of(context)!.enter4DigitPIN,
                        style: Theme.of(context).textTheme.subtitle1!.copyWith(
                            fontWeight: FontWeight.w400,
                            color: ColorResource.color222222),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 45.0),
                        child: PinCodeTextField(
                          appContext: context,
                          onChanged: (String value) {},
                          autoFocus: true,
                          onCompleted: (String enteredText) {},
                          obscureText: true,
                          length: 4,
                          textStyle: Theme.of(context)
                              .textTheme
                              .headline5!
                              .copyWith(color: ColorResource.color222222),
                          animationType: AnimationType.scale,
                          animationDuration: const Duration(milliseconds: 300),
                          keyboardType: TextInputType.number,
                          controller: passcodeController,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          pinTheme: PinTheme(
                            shape: PinCodeFieldShape.box,
                            borderWidth: 1,
                            borderRadius: BorderRadius.circular(8),
                            inactiveColor: ColorResource.color222222,
                            selectedColor: ColorResource.color222222,
                            activeColor: ColorResource.color222222,
                            fieldHeight: 60.89,
                            fieldWidth: 48,
                          ),
                        ),
                      ),
                      const SizedBox(height: 21),
                      CustomText(
                        Languages.of(context)!.reEnter4DigitPIN,
                        style: Theme.of(context).textTheme.subtitle1!.copyWith(
                            fontWeight: FontWeight.w400,
                            color: ColorResource.color222222),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 45.0, right: 45.0),
                        child: PinCodeTextField(
                          appContext: context,
                          onChanged: (String value) {},
                          autoFocus: true,
                          onCompleted: (String enteredText) {
                            if (passcodeController.text.isNotEmpty &&
                                confirmPasscodeController.text.isNotEmpty) {
                              if (passcodeController.text ==
                                  confirmPasscodeController.text) {
                                setState(() {
                                  error = false;
                                  buttonVisible = true;
                                });
                              } else {
                                setState(() {
                                  error = true;
                                  buttonVisible = false;
                                });
                              }
                            }
                          },
                          obscureText: true,
                          length: 4,
                          textStyle: Theme.of(context)
                              .textTheme
                              .headline5!
                              .copyWith(color: ColorResource.color222222),
                          animationType: AnimationType.scale,
                          animationDuration: const Duration(milliseconds: 300),
                          keyboardType: TextInputType.number,
                          controller: confirmPasscodeController,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          pinTheme: PinTheme(
                            shape: PinCodeFieldShape.box,
                            borderWidth: 1,
                            borderRadius: BorderRadius.circular(8),
                            inactiveColor: error
                                ? ColorResource.colorF92538
                                : ColorResource.color222222,
                            selectedColor: error
                                ? ColorResource.colorF92538
                                : ColorResource.color222222,
                            activeColor: error
                                ? ColorResource.colorF92538
                                : ColorResource.color222222,
                            fieldHeight: 60.89,
                            fieldWidth: 48,
                          ),
                        ),
                      ),
                      const SizedBox(height: 10),
                      error
                          ? CustomText(
                              Languages.of(context)!.notMatchedText,
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle2!
                                  .copyWith(
                                      fontWeight: FontWeight.w400,
                                      color: Colors.red),
                            )
                          : const SizedBox(),
                      const Spacer(),
                      buttonVisible
                          ? Padding(
                              padding: const EdgeInsets.only(
                                  bottom: 30, right: 24, left: 24),
                              child: CustomButton(
                                StringResource.setPin,
                                onTap: () {
                                  changePasscodeBottomSheet();
                                },
                              ),
                            )
                          : SizedBox()
                    ],
                  );
                }
                return const Scaffold(
                  body: CircularProgressIndicator(),
                );
              }),
        ),
      ),
    );
  }

  void changePasscodeBottomSheet() {
    showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        context: context,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(32.0), topRight: Radius.circular(32.0)),
        ),
        backgroundColor: ColorResource.colorFFFFFF,
        builder: (BuildContext context) {
          return const ConfirmPinSuccess();
        });
  }
}
