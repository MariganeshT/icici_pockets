import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:meta/meta.dart';

part 'set_digit_pin_event.dart';
part 'set_digit_pin_state.dart';

class SetDigitPinBloc extends Bloc<SetDigitPinEvent, SetDigitPinState> {
  SetDigitPinBloc() : super(SetDigitPinInitial());

  @override
  Stream<SetDigitPinState> mapEventToState(
    SetDigitPinEvent event,
  ) async* {
    if (event is SetDigitPinInitialEvent) {
      yield SetDigitPinLoaded();
    }
  }
}
