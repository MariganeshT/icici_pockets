part of 'set_digit_pin_bloc.dart';

@immutable
class SetDigitPinState extends BaseEquatable {}

class SetDigitPinInitial extends SetDigitPinState {}

class SetDigitPinLoading extends SetDigitPinState {}

class SetDigitPinLoaded extends SetDigitPinState {}
