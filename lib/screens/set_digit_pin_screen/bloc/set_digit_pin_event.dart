part of 'set_digit_pin_bloc.dart';

@immutable
class SetDigitPinEvent extends BaseEquatable {}

class SetDigitPinInitialEvent extends SetDigitPinEvent {}

