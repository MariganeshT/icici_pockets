import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:icici/router.dart';
import 'package:icici/screens/personal_detail_screen/bloc/personal_details_bloc.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/font.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:icici/widgets/custom_button.dart';
import 'package:icici/widgets/custom_textfield.dart';
import 'package:intl/intl.dart';

// ignore: use_key_in_widget_constructors
class PersonalDetailScreen extends StatefulWidget {
  @override
  _PersonalDetailScreenState createState() => _PersonalDetailScreenState();
}

class _PersonalDetailScreenState extends State<PersonalDetailScreen> {
  late PersonalDetailsBloc personalDetailsBloc;
  TextEditingController nameController = TextEditingController();
  TextEditingController dobController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController refCodeController = TextEditingController();
  // ignore: always_specify_types
  final _form = GlobalKey<FormState>(); //for storing form state.
  String selectedGender = '';

  late FocusNode fullNameFocus;
  late FocusNode dobFocus;
  late FocusNode emailFocus;
  late FocusNode referralCodeFocus;
  double fullNameBottomMargin = 12.0;

  @override
  void initState() {
    super.initState();
    personalDetailsBloc = PersonalDetailsBloc()
      ..add(PersonalDetailsInitialEvent());

    // ignore: prefer_single_quotes
    dobController.text = "";
    personalDetailsSheet();
    fullNameFocus = FocusNode();
    dobFocus = FocusNode();
    emailFocus = FocusNode();
    referralCodeFocus = FocusNode();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: Colors.transparent));
    // ignore: always_specify_types
    return BlocListener(
        bloc: personalDetailsBloc,
        listener: (BuildContext context, PersonalDetailsState state) {},
        // ignore: always_specify_types
        child: BlocBuilder(
            bloc: personalDetailsBloc,
            builder: (BuildContext context, PersonalDetailsState state) {
              return SafeArea(
                top: false,
                child: Scaffold(
                  bottomNavigationBar: Container(
                    color: ColorResource.colorFAFAFA,
                    padding:
                        const EdgeInsets.only(left: 24, right: 24, bottom: 20),
                    child: CustomButton(
                      StringResource.continueText,
                      onTap: () {
                        if (selectedGender.isNotEmpty) {
                          _saveForm();
                        } else {
                          Fluttertoast.showToast(
                              msg: StringResource.genderErrorMsg,
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              backgroundColor: ColorResource.colorED2020,
                              textColor: Colors.white,
                              fontSize: 12.0);
                        }
                      },
                    ),
                  ),
                  backgroundColor: ColorResource.colorFAFAFA,
                  body: Form(
                    key: _form,
                    child: Column(
                      children: [
                        Stack(children: [
                          Image.asset(
                            ImageResource.personalDetailBackImage1,
                            fit: BoxFit.contain,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 12, top: 3),
                                child: CustomAppbar(
                                  titleString: StringResource.personalDetails,
                                  indicatorIndex: 1,
                                  isAuthentication: true,
                                  onItemSelected: (dynamic selectedItem) {
                                    if (selectedItem is String) {
                                      Navigator.pop(context);
                                    }
                                  },
                                ),
                              ),
                              SizedBox(
                                width: 278,
                                child: Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(12, 0, 16, 20),
                                  child: CustomText(
                                    StringResource.personaldetailsDeshint,
                                    font: Font.mulishLight,
                                    fontSize: FontSize.sixteen,
                                    color: ColorResource.colorFFFFFF,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ]),
                        Expanded(
                          child: SingleChildScrollView(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              // ignore: always_specify_types
                              children: [
                                Container(
                                  margin: const EdgeInsets.only(top: 24),
                                  child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      // ignore: always_specify_types
                                      children: [
                                        GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              selectedGender =
                                                  StringResource.male;
                                            });
                                          },
                                          child: customGender(
                                            StringResource.male,
                                            ImageResource.maleIcon,
                                          ),
                                        ),
                                        GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              selectedGender =
                                                  StringResource.female;
                                            });
                                          },
                                          child: customGender(
                                              StringResource.female,
                                              ImageResource.femaleIcon),
                                        ),
                                        GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              selectedGender =
                                                  StringResource.transGender;
                                            });
                                          },
                                          child: customGender(
                                              StringResource.transGender,
                                              ImageResource.transGenderIcon),
                                        ),
                                      ]),
                                ),
                                Container(
                                    margin: EdgeInsets.only(
                                        left: 24,
                                        right: 24,
                                        top: 32,
                                        bottom: fullNameBottomMargin),
                                    child: CustomTextField(
                                      StringResource.fullName,
                                      nameController,
                                      focusNode: fullNameFocus,
                                      focusTextColor: ColorResource.color222222,

                                      descriptionText:
                                          StringResource.asPerPanCard,
                                      // ignore: avoid_redundant_argument_values
                                      keyBoardType: TextInputType.name,
                                      onEditing: () {
                                        dobFocus.requestFocus(FocusNode());
                                        _form.currentState!.validate();
                                      },

                                      // ignore: prefer_const_literals_to_create_immutables
                                      validationRules: ['required'],
                                      validatorCallBack: (bool values) {
                                        setState(() {
                                          if (values) {
                                            fullNameBottomMargin = 2.0;
                                          } else {
                                            fullNameBottomMargin = 12.0;
                                          }
                                        });
                                      },
                                      // ignore: lines_longer_than_80_chars
                                    )),
                                Container(
                                  margin: const EdgeInsets.only(
                                      top: 20, left: 24, right: 24),
                                  child: CustomTextField(
                                    StringResource.dateOfBirth,
                                    dobController,

                                    focusNode: dobFocus,
                                    focusTextColor: ColorResource.color222222,

                                    // ignore: prefer_const_literals_to_create_immutables
                                    validationRules: ['required'],
                                    validatorCallBack: (bool values) {},
                                    suffixWidget:
                                        const Icon(Icons.calendar_today),
                                    isReadOnly: true,

                                    onTapped: () async {
                                      // ignore: prefer_final_locals

                                      // ignore: prefer_final_locals
                                      DateTime? pickedDate =
                                          await showDatePicker(
                                              context: context,
                                              initialDate: DateTime.now(),
                                              firstDate: DateTime(1900),
                                              lastDate: DateTime.now(),

                                              initialDatePickerMode: DatePickerMode.year,
                                              builder:
                                                  // ignore: always_specify_types
                                                  (context, child) {
                                                return Theme(
                                                  data: Theme.of(context)
                                                      .copyWith(
                                                    textTheme: TextTheme(
                                                      subtitle1: TextStyle(
                                                          fontSize: 10.0), // <-- here you can do your font smaller
                                                      headline1: TextStyle(fontSize: 8.0),
                                                    ),
                                                    colorScheme:
                                                        const ColorScheme
                                                            .light(
                                                      primary: ColorResource
                                                          // ignore: lines_longer_than_80_chars
                                                          .color641653, // header background color

                                                      // ignore: avoid_redundant_argument_values
                                                      onPrimary: ColorResource
                                                          // ignore: lines_longer_than_80_chars
                                                          .colorFFFFFF, // header text color

                                                      // ignore: avoid_redundant_argument_values
                                                      onSurface:
                                                          Colors.black,
                                                      // body text color
                                                    ),
                                                    textButtonTheme:
                                                        TextButtonThemeData(
                                                      style: TextButton
                                                          .styleFrom(
                                                        primary: ColorResource
                                                            // ignore: lines_longer_than_80_chars
                                                            .color641653, // button text color
                                                      ),
                                                    ),
                                          ), child: child!,);
                                        }
                                      );

                                      if (pickedDate != null) {
                                        // ignore: prefer_final_locals
                                        String formattedDate =
                                            DateFormat('dd-MM-yyyy')
                                                .format(pickedDate);

                                        setState(() {
                                          dobController.text = formattedDate;
                                        });
                                        emailFocus.requestFocus(FocusNode());
                                        _form.currentState!.validate();
                                      }
                                    },
                                  ),
                                ),
                                Container(
                                    margin: const EdgeInsets.only(
                                        top: 36, left: 24, right: 24),
                                    child: CustomTextField(
                                      StringResource.emailAddress,
                                      emailController,

                                      focusNode: emailFocus,
                                      focusTextColor: ColorResource.color222222,

                                      keyBoardType: TextInputType.emailAddress,
                                      onEditing: () {
                                        emailFocus.unfocus();
                                        _form.currentState!.validate();
                                      },
                                      // ignore: prefer_const_literals_to_create_immutables
                                      validationRules: ['email'],
                                      validatorCallBack: (bool values) {},
                                    )),
                                Container(
                                  margin: const EdgeInsets.only(
                                      top: 36, left: 24, right: 24, bottom: 10),
                                  child: CustomTextField(
                                    StringResource.referralCode,
                                    refCodeController,
                                    focusNode: referralCodeFocus,
                                    focusTextColor: ColorResource.color222222,
                                    validatorCallBack: (bool values) {},
                                    onEditing: () {
                                      referralCodeFocus.unfocus();
                                      _form.currentState!.validate();
                                    },
                                  ),
                                  // const Spacer(),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            }));
  }

  //saving form after validation
  void _saveForm() {
    final bool isValid = _form.currentState!.validate();
    if (!isValid) {
      return;
    } else {
      Navigator.pushNamed(context, AppRoutes.verifyIdentityScreen);
    }
  }

  //////
//bottom sheet

////
  dynamic personalDetailsSheet() {
    WidgetsBinding.instance!.addPostFrameCallback((_) async {
      await showModalBottomSheet(
          isScrollControlled: true,
          isDismissible: false,
          enableDrag: false,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(15.0),
                topRight: Radius.circular(15.0)),
          ),
          backgroundColor: ColorResource.colorFFFFFF,
          context: context,
          builder: (BuildContext context) {
            return Container(
              constraints: const BoxConstraints(
                minHeight: 150,
                maxHeight: 420,
              ),
              child: WillPopScope(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,

                    // ignore: always_specify_types
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 32, left: 24, bottom: 20),
                        child: CustomText(
                          StringResource.personalDetailsReq,
                          style: Theme.of(context)
                              .textTheme
                              .headline5!
                              .copyWith(
                                  color: ColorResource.color222222,
                                  fontWeight: FontWeight.bold),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 24, right: 24, bottom: 20),
                        child: CustomText(
                          StringResource.toVerifyYourIdentity,
                          style: Theme.of(context)
                              .textTheme
                              .subtitle1!
                              .copyWith(
                                  color: ColorResource.color222222
                                      .withOpacity(0.5),
                                  height: 1.5,
                                  fontWeight: FontWeights.normal),
                        ),
                      ),
                      Center(
                        child:
                            Container(
                              height: 190,
                                width: 190,
                                child: Image.asset(ImageResource.personLDetailBottomsheet)),
                      ),
                      Container(
                        margin: const EdgeInsets.only(
                            top: 22, left: 24, right: 24, bottom: 10),
                        child: CustomButton(
                          StringResource.verify,
                          onTap: () {
                            Navigator.pop(context);
                          },
                        ),
                      ),
                    ],
                  ),
                  onWillPop: () async => false),
            );
          });
    });
  }

  Widget customGender(String genderName, String genderIcon) {
    // ignore: avoid_unnecessary_containers
    return Container(
      child: Column(
        // ignore: always_specify_types
        children: [
          CustomText(
            genderName,
            style: Theme.of(context).textTheme.subtitle1!.copyWith(
                color: ColorResource.color8d9091, fontWeight: FontWeights.bold),
          ),
          Container(
            margin: const EdgeInsets.only(top: 8),
            padding: const EdgeInsets.all(15),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: selectedGender == genderName
                  ? ColorResource.color641653
                  : ColorResource.colorEFEFEF,
              border: Border.all(
                  color: selectedGender == genderName
                      ? ColorResource.color641653
                      : ColorResource.colorFFFFFF),
            ),
            child: Image.asset(
              genderIcon,
              color: selectedGender == genderName
                  ? ColorResource.colorFFFFFF
                  : ColorResource.color8d9091,
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    fullNameFocus.dispose();
    dobFocus.dispose();
    emailFocus.dispose();
    referralCodeFocus.dispose();
    super.dispose();
  }
}
