import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:meta/meta.dart';

part 'personal_details_event.dart';
part 'personal_details_state.dart';

class PersonalDetailsBloc
    extends Bloc<PersonalDetailsEvent, PersonalDetailsState> {
  PersonalDetailsBloc() : super(PersonalDetailsInitial());

  @override
  Stream<PersonalDetailsState> mapEventToState(
    PersonalDetailsEvent event,
  ) async* {
    if (event is PersonalDetailsInitialEvent) {}
  }
}
