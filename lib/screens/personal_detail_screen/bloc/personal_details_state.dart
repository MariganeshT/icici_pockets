part of 'personal_details_bloc.dart';

@immutable
abstract class PersonalDetailsState {}

class PersonalDetailsInitial extends PersonalDetailsState {}

class PersonalDetailsLoadingState extends PersonalDetailsState {}

class PersonalDetailsLoadedState extends PersonalDetailsState {}

class PersonalDetailsFailureState extends PersonalDetailsState {}
