part of 'personal_details_bloc.dart';

@immutable
abstract class PersonalDetailsEvent extends BaseEquatable {}

class PersonalDetailsInitialEvent extends PersonalDetailsEvent {}

class PersonalDetailsBottomssheetEvent extends PersonalDetailsEvent {}
