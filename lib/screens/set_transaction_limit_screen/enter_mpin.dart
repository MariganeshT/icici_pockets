import 'package:flutter/material.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import 'limit_success.dart';

class EnterMpin extends StatefulWidget {
  Function onOTPComplete;

  EnterMpin(this.onOTPComplete);
  @override
  _EnterMpinState createState() => _EnterMpinState();
}

class _EnterMpinState extends State<EnterMpin> {
  TextEditingController mpinController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      child: Scaffold(
        backgroundColor: ColorResource.colorFAFAFA,
        body: Column(
          children: <Widget>[
            CustomAppbar(
              backgroundColor: ColorResource.color641653,
              titleString: StringResource.pleaseEnterMPIN,
              titleSpacing: 8,
              style: Theme.of(context)
                  .textTheme
                  .headline5!
                  .copyWith(color: ColorResource.colorFFFFFF),
              iconEnumValues: IconEnum.close,
              onItemSelected: (selectedItem) {
                Navigator.pop(context);
              },
            ),
            Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 22, left: 42, right: 42),
                  child: CustomText(
                    StringResource.fourDigitPin,
                    style: Theme.of(context)
                        .textTheme
                        .subtitle1!
                        .copyWith(color: ColorResource.color787878),
                  ),
                ),
                SizedBox(
                  height: 68,
                ),
                CustomText(
                  StringResource.mpin,
                  style: Theme.of(context)
                      .textTheme
                      .subtitle1!
                      .copyWith(color: ColorResource.color222222),
                ),
                SizedBox(
                  height: 10,
                ),
                PinCodeTextField(
                  appContext: context,
                  onChanged: (String value) {
                    // user.otp = value;
                  },
                  onCompleted: (String enteredText) {
                    widget.onOTPComplete();
                    // limitSuccessBottomSheet();
                    // while (Navigator.canPop(context)) {
                    //   Navigator.pop(context);
                    // }

                    // if (widget.isForgotMpinFlow) {
                    //   Navigator.pushReplacementNamed(
                    //       context, AppRoutes.setPinScreen,
                    //       arguments: widget.isForgotMpinFlow);
                    // } else {
                    //   Navigator.pushReplacementNamed(
                    //       context, AppRoutes.personalDetailsScreen);
                    // }
                  },
                  length: 4,
                  textStyle: Theme.of(context)
                      .textTheme
                      .headline5!
                      .copyWith(color: ColorResource.color222222),
                  animationType: AnimationType.scale,
                  animationDuration: const Duration(milliseconds: 300),
                  keyboardType: TextInputType.number,
                  controller: mpinController,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  pinTheme: PinTheme(
                    shape: PinCodeFieldShape.box,
                    borderWidth: 1,
                    borderRadius: BorderRadius.circular(8),
                    inactiveColor: ColorResource.color222222,
                    selectedColor: ColorResource.color222222,
                    activeColor: ColorResource.color222222,
                    fieldHeight: 56,
                    fieldWidth: 48,
                  ),
                ),
              ],
            )
          ],
        ),

        //       return  Column(
        //         children: [

        //         CustomAppbar(
        //         titleString: StringResource.pleaseEnterMPIN,
        //         iconEnumValues: IconEnum.close,
        //         style: Theme.of(context)
        //         .textTheme
        //         .headline5!
        //         .copyWith(color: ColorResource.colorFFFFFF),
        //       ),
        //       Center(
        //         child: Column(
        //           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        //           children: <Widget>[
        //             CustomText(
        //               StringResource.fourDigitPin,
        //               style: Theme.of(context)
        //               .textTheme
        //               .subtitle1!
        //               .copyWith(color: ColorResource.color787878),
        //             ),
        //             const SizedBox(
        //               height: 68,
        //             ),
        //             Center(
        //               child: CustomText(
        //                 StringResource.mpin,
        //               ),

        //               ),
        //               const SizedBox(
        //                 height: 16,
        //                 ),
        //                 // PinCodeTextField(
        //                 //   appContext: context,
        //                 //   length: 4,
        //                 //   // onChanged: (),
        //                 //   )
        //           ],),
        //       )
        //     ]
        // ),
      ),
    );
  }

  void limitSuccessBottomSheet() {
    showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        context: context,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(32.0), topRight: Radius.circular(32.0)),
        ),
        backgroundColor: ColorResource.colorFFFFFF,
        builder: (BuildContext context) {
          return const LimitSuccess();
        });
  }
}
