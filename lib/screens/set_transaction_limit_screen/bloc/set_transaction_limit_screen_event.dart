part of 'set_transaction_limit_screen_bloc.dart';

@immutable
abstract class SetTransactionLimitScreenEvent extends BaseEquatable{}

class SetTransactionLimitScreenInitialEvent extends SetTransactionLimitScreenEvent{}

class LimitBottomSheetEvent extends SetTransactionLimitScreenEvent{}
