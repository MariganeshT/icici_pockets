import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:icici/Model/settransaction_limit_models.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:meta/meta.dart';

part 'set_transaction_limit_screen_event.dart';

part 'set_transaction_limit_screen_state.dart';

class SetTransactionLimitScreenBloc extends Bloc<SetTransactionLimitScreenEvent,
    SetTransactionLimitScreenState> {
  SetTransactionLimitScreenBloc() : super(SetTransactionLimitScreenInitial());
  List<SetTransaction> billPayRecharge = [];
  List<SetTransaction> fundTransfer = [];
  List<SetTransaction> pointOfSale = [];
  List<SetTransaction> onlineTransaction = [];

  @override
  Stream<SetTransactionLimitScreenState> mapEventToState(
    SetTransactionLimitScreenEvent event,
  ) async* {
    if (event is SetTransactionLimitScreenInitialEvent) {
      yield SetTransactionLimitScreenLoadingState();
      billPayRecharge.addAll([
        SetTransaction(
          ImageResource.billpay,
          StringResource.billPayRecharges,
          StringResource.billpayandrecharges,
          StringResource.daily,
          StringResource.monthly,
          StringResource.editLimit,
          StringResource.tenthousand,
          StringResource.tenthousand,
          onTap: () {
            // print("z");
            this.add(LimitBottomSheetEvent());
          },
        ),
      ]);
      fundTransfer.addAll([
        SetTransaction(
            ImageResource.fundtransfer,
            StringResource.fundtransfer,
            StringResource.fundtransferswitch,
            StringResource.daily,
            StringResource.monthly,
            StringResource.editLimit,
            StringResource.tenthousand,
            StringResource.tenthousand, onTap: () {
          // print("z");
          this.add(LimitBottomSheetEvent());
        }),
      ]);
      pointOfSale.addAll([
        SetTransaction(
            ImageResource.pointofsale,
            StringResource.pointofsale,
            StringResource.fundtransferswitch,
            StringResource.daily,
            StringResource.monthly,
            StringResource.editLimit,
            StringResource.tenthousand,
            StringResource.tenthousand, onTap: () {
          this.add(LimitBottomSheetEvent());
        }),
      ]);
      onlineTransaction.addAll([
        SetTransaction(
          ImageResource.onlinetransaction,
          StringResource.onlinetransactions,
          StringResource.fundtransferswitch,
          StringResource.daily,
          StringResource.monthly,
          StringResource.editLimit,
          StringResource.tenthousand,
          StringResource.tenthousand,
          onTap: () {
            this.add(LimitBottomSheetEvent());
           
          },
        ),
      ]);
      yield SetTransactionLimitScrenLoadedState();
    }
    if (event is LimitBottomSheetEvent) {
      yield LimitBottomSheetState();
    }
  }
}
