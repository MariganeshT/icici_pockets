part of 'set_transaction_limit_screen_bloc.dart';

@immutable
abstract class SetTransactionLimitScreenState {}

class LimitBottomSheetState extends SetTransactionLimitScreenState {}

class SetTransactionLimitScreenInitial extends SetTransactionLimitScreenState {}

class SetTransactionLimitScreenLoadingState extends SetTransactionLimitScreenState {}

class SetTransactionLimitScrenLoadedState extends SetTransactionLimitScreenState {}


