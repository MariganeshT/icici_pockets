import 'package:flutter/material.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_button.dart';

class LimitSuccess extends StatefulWidget {
  const LimitSuccess({Key? key}) : super(key: key);

  @override
  _LimitSuccessState createState() => _LimitSuccessState();
}

class _LimitSuccessState extends State<LimitSuccess> {
  @override
  Widget build(BuildContext context) {
    return StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) {
      return WillPopScope(
        onWillPop: () async => false,
        child: Container(
          padding: MediaQuery.of(context).viewInsets,
          child: Container(
            decoration: const BoxDecoration(
              color: ColorResource.colorFFFFFF,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(32.0),
                  topRight: Radius.circular(32.0)),
            ),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(24, 2, 24, 30),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 16),
                    child: Center(
                        child: Image.asset(
                      ImageResource.success_image,
                      height: 200,
                    )),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  CustomText(
                    StringResource.dailyLimitSuccess,
                    style: Theme.of(context).textTheme.headline5!.copyWith(
                          fontWeight: FontWeight.w600,
                          // fontSize: 20
                        ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  CustomText(
                    StringResource.increaseLimit,
                    style: Theme.of(context).textTheme.subtitle1!.copyWith(
                        color: Color.fromRGBO(34, 34, 34, 0.5),
                        // fontSize: 16,
                        fontWeight: FontWeight.w400),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  CustomButton(
                    StringResource.done,
                    onTap: () {
                      Navigator.pop(context);
                      if (Navigator.canPop(context)) {
                        Navigator.pop(context);
                      }
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    });
  }
}
