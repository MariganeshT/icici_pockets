import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:icici/router.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_textfield.dart';

class ChangeLimit extends StatefulWidget {
  const ChangeLimit({Key? key}) : super(key: key);

  @override
  _ChangeLimitState createState() => _ChangeLimitState();
}

class _ChangeLimitState extends State<ChangeLimit> {
  TextEditingController dailyLimitController = TextEditingController();
  TextEditingController monthlyLimitController = TextEditingController();

  final _form = GlobalKey<FormState>();

  late FocusNode dailyLimitFocus;
  late FocusNode monthlyLimitFocus;

  @override
  void initState() {
    super.initState();

    dailyLimitController.text;
    monthlyLimitController.text;

    dailyLimitFocus = FocusNode();
    monthlyLimitFocus = FocusNode();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: Colors.transparent));
    return Container(
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 16.0, top: 12),
                  child: GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Image.asset(ImageResource.close)),
                ),
                const SizedBox(width: 100),
                CustomText(StringResource.changeLimit,
                    style: Theme.of(context).textTheme.bodyText1),
              ],
            ),
            Form(
              key: _form,
              child: Column(
                children: <Widget>[
                  Container(
                      margin: const EdgeInsets.only(
                        left: 24,
                        right: 24,
                        top: 32,
                      ),
                      child: CustomTextField(
                        StringResource.dailyLimit,
                        dailyLimitController,

                        focusNode: dailyLimitFocus,
                        focusTextColor: ColorResource.color222222,
                        keyBoardType: TextInputType.number,
                        // onEditing: () {

                        // },
                        validationRules: ['required'],
                        validatorCallBack: (bool values) {},
                      )),
                  Padding(
                    padding: const EdgeInsets.only(left: 30, top: 8, right: 20),
                    child: CustomText(
                      StringResource.dailyexceed,
                      style: Theme.of(context)
                          .textTheme
                          .subtitle2!
                          .copyWith(color: ColorResource.color787878),
                    ),
                  ),
                  Container(
                      margin:
                          const EdgeInsets.only(top: 36, left: 24, right: 24),
                      child: CustomTextField(
                        StringResource.monthlyLimit,
                        monthlyLimitController,
                        focusNode: monthlyLimitFocus,
                        focusTextColor: ColorResource.color222222,
                        keyBoardType: TextInputType.number,
                        // onEditing: () {
                        //
                        // },
                        validationRules: ['required'],
                        validatorCallBack: (bool values) {},
                      )),
                  Padding(
                    padding: const EdgeInsets.only(left: 30, right: 20, top: 8),
                    child: CustomText(
                      StringResource.monthlyexceed,
                      style: Theme.of(context).textTheme.subtitle2!.copyWith(
                            color: ColorResource.color787878,
                          ),
                    ),
                  ),
                  const SizedBox(
                    height: 55,
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 24, right: 24, bottom: 28),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.pushNamed(context, AppRoutes.setMpinScreen,
                            arguments: 'TransactionSuccessBottomSheet');
                      },
                      child: Container(
                        height: 56,
                        width: double.infinity,
                        decoration: BoxDecoration(
                            color: ColorResource.colorFF781F,
                            borderRadius: BorderRadius.circular(8)),
                        child: Center(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: CustomText(
                              StringResource.confirm,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1!
                                  .copyWith(
                                    color: ColorResource.colorFFFFFF,
                                  ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
