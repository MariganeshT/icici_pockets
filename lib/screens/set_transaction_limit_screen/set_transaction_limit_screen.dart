import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/screens/set_transaction_limit_screen/change_limit.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';

import 'bloc/set_transaction_limit_screen_bloc.dart';

class SetTransactionLimit extends StatefulWidget {
  const SetTransactionLimit({Key? key}) : super(key: key);

  @override
  _SetTransactionLimitState createState() => _SetTransactionLimitState();
}

class _SetTransactionLimitState extends State<SetTransactionLimit> {
  late SetTransactionLimitScreenBloc bloc;
  bool status = false;
  bool status1 = false;
  bool status2 = false;
  bool status3 = false;
  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<SetTransactionLimitScreenBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorResource.colorFAFAFA,
      body: BlocListener<SetTransactionLimitScreenBloc,
              SetTransactionLimitScreenState>(
          bloc: bloc,
          listener:
              (BuildContext context, SetTransactionLimitScreenState state) {
            if (state is LimitBottomSheetState) {
              limitBottomSheet();
            }
          },
          child: BlocBuilder<SetTransactionLimitScreenBloc,
              SetTransactionLimitScreenState>(
            bloc: bloc,
            builder:
                (BuildContext context, SetTransactionLimitScreenState state) {
              if (state is SetTransactionLimitScreenLoadingState) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
              return Column(
                children: <Widget>[
                  CustomAppbar(
                    backgroundColor: ColorResource.color641653,
                    titleString: StringResource.settransactionlimit,
                    titleSpacing: 8,
                    style: Theme.of(context)
                        .textTheme
                        .headline5!
                        .copyWith(color: ColorResource.colorFFFFFF),
                    iconEnumValues: IconEnum.back,
                    onItemSelected: (selectedItem) {
                      Navigator.pop(context);
                    },
                  ),
                  Expanded(
                      child: Padding(
                          padding: const EdgeInsets.only(
                              top: 24, left: 12, right: 12),
                          child: SingleChildScrollView(
                            child: Column(
                              children: [
                                Container(
                                  // height: 180,
                                  // width: double.infinity,
                                  decoration: BoxDecoration(
                                      color: ColorResource.colorFFFFFF,
                                      borderRadius: BorderRadius.circular(14)),
                                  child: Column(
                                    children: buildbillPayRechargeWidget(),
                                  ),
                                ),
                                const SizedBox(
                                  height: 24,
                                ),
                                Container(
                                  // height: 180,
                                  decoration: BoxDecoration(
                                    color: ColorResource.colorFFFFFF,
                                    borderRadius: BorderRadius.circular(14),
                                  ),
                                  child: Column(
                                    children: buildfundTransferWidget(),
                                  ),
                                ),
                                const SizedBox(
                                  height: 24,
                                ),
                                Container(
                                  // height: 180,
                                  decoration: BoxDecoration(
                                    color: ColorResource.colorFFFFFF,
                                    borderRadius: BorderRadius.circular(14),
                                  ),
                                  child: Column(
                                      children: buildpointOfSaleWidget()),
                                ),
                                const SizedBox(
                                  height: 24,
                                ),
                                Container(
                                  // height: 180,
                                  decoration: BoxDecoration(
                                    color: ColorResource.colorFFFFFF,
                                    borderRadius: BorderRadius.circular(14),
                                  ),
                                  child: Column(
                                      children: buildonlineTansactionWidget()),
                                ),
                              ],
                            ),
                          )))
                ],
              );
            },
          )),
    );
  }

  List<Widget> buildbillPayRechargeWidget() {
    List<Widget> widgets = [];
    bloc.billPayRecharge.forEach((element) {
      widgets.add(Column(children: [
        ListTile(
          contentPadding: EdgeInsets.only(right: 3, left: 15),
          minLeadingWidth: 14,
          leading:
              SizedBox(width: 24, child: Image.asset(element.leadingImage)),
          title: Padding(
            padding: const EdgeInsets.only(top: 15),
            child: CustomText(
              element.title,
              // maxLines: 40,
              style: Theme.of(context).textTheme.subtitle1!.copyWith(
                  color: ColorResource.color222222,
                  // fontSize: 16,
                  fontWeight: FontWeight.w600),
            ),
          ),
          subtitle: Padding(
            padding: const EdgeInsets.only(top: 6.0),
            child: CustomText(
              element.subTitle,
              // maxLines: 2,
              style: Theme.of(context).textTheme.subtitle1!.copyWith(
                    color: ColorResource.color787878,
                  ),
            ),
          ),
          trailing: GestureDetector(
            onTap: element.onTap,
            child: Transform.scale(
              scale: 0.7,
              child: CupertinoSwitch(
                value: status,
                activeColor: ColorResource.color641653,
                onChanged: (value) {
                  print("VALUE : $value");
                  setState(() {
                    status = value;
                  });
                },
              ),
            ),
          ),
          isThreeLine: true,
        ),
        Divider(),
        Padding(
          padding:
              const EdgeInsets.only(left: 18, right: 15, bottom: 24, top: 15),
          child: Row(
            // mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      CustomText(
                        element.title1,
                        style: Theme.of(context).textTheme.subtitle1!.copyWith(
                              color: ColorResource.color787878,
                              // fontSize: 14,
                              // fontWeight: FontWeight.w400
                            ),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      CustomText(
                        element.subtitle1,
                        style: Theme.of(context).textTheme.subtitle1!.copyWith(
                            color: ColorResource.color222222,
                            // fontSize: 16,
                            fontWeight: FontWeight.w800),
                      ),
                    ]),
              ),
              Expanded(
                child: Column(
                  // crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomText(
                      element.title2,
                      style: Theme.of(context)
                          .textTheme
                          .subtitle1!
                          .copyWith(color: ColorResource.color787878),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 8),
                      child: CustomText(
                        element.subtitle1,
                        style: Theme.of(context).textTheme.subtitle1!.copyWith(
                            color: ColorResource.color222222,
                            fontWeight: FontWeight.w800),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  children: [
                    Spacer(),
                    Padding(
                      padding: const EdgeInsets.only(
                        bottom: 20,
                      ),
                      child: GestureDetector(
                        onTap: element.onTap,
                        child: CustomText(
                          element.title3,
                          style: Theme.of(context)
                              .textTheme
                              .subtitle1!
                              .copyWith(
                                  color: ColorResource.colorFF781F,
                                  fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        )
      ]));
    });
    return widgets;
  }

  List<Widget> buildfundTransferWidget() {
    List<Widget> widgets = [];
    bloc.fundTransfer.forEach((element) {
      widgets.add(Column(children: [
        ListTile(
          contentPadding: EdgeInsets.only(right: 3, left: 15),
          minLeadingWidth: 14,
          leading:
              SizedBox(width: 24, child: Image.asset(element.leadingImage)),
          title: Padding(
            padding: const EdgeInsets.only(top: 15),
            child: CustomText(
              element.title,
              // maxLines: 40,
              style: Theme.of(context).textTheme.subtitle1!.copyWith(
                  color: ColorResource.color222222,
                  // fontSize: 16,
                  fontWeight: FontWeight.w600),
            ),
          ),
          subtitle: Padding(
            padding: const EdgeInsets.only(top: 6.0),
            child: CustomText(
              element.subTitle,
              // maxLines: 2,
              style: Theme.of(context).textTheme.subtitle1!.copyWith(
                    color: ColorResource.color787878,
                  ),
            ),
          ),
          trailing: GestureDetector(
            onTap: element.onTap,
            child: Transform.scale(
              scale: 0.7,
              child: CupertinoSwitch(
                value: status1,
                activeColor: ColorResource.color641653,
                onChanged: (value) {
                  print("VALUE : $value");
                  setState(() {
                    status1 = value;
                  });
                },
              ),
            ),
          ),
          isThreeLine: true,
        ),
        Divider(),
        Padding(
          padding:
              const EdgeInsets.only(left: 18, right: 15, bottom: 24, top: 15),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      CustomText(
                        element.title1,
                        style: Theme.of(context).textTheme.subtitle1!.copyWith(
                              color: ColorResource.color787878,
                            ),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      CustomText(
                        element.subtitle1,
                        style: Theme.of(context).textTheme.subtitle1!.copyWith(
                            color: ColorResource.color222222,
                            fontWeight: FontWeight.w800),
                      ),
                    ]),
              ),
              Expanded(
                child: Column(
                  // crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    CustomText(
                      element.title2,
                      style: Theme.of(context)
                          .textTheme
                          .subtitle1!
                          .copyWith(color: ColorResource.color787878),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 8),
                      child: CustomText(
                        element.subtitle1,
                        style: Theme.of(context).textTheme.subtitle1!.copyWith(
                            color: ColorResource.color222222,
                            fontWeight: FontWeight.w800),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Row(children: [
                  Spacer(),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                    child: GestureDetector(
                      onTap: element.onTap,
                      child: CustomText(
                        element.title3,
                        style: Theme.of(context).textTheme.subtitle1!.copyWith(
                            color: ColorResource.colorFF781F,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                ]),
              ),
            ],
          ),
        )
      ]));
    });
    return widgets;
  }

  List<Widget> buildpointOfSaleWidget() {
    List<Widget> widgets = [];
    bloc.pointOfSale.forEach((element) {
      widgets.add(
        Column(
          children: [
            ListTile(
              contentPadding: EdgeInsets.only(right: 3, left: 15),
              minLeadingWidth: 14,
              leading:
                  SizedBox(width: 24, child: Image.asset(element.leadingImage)),
              title: Padding(
                padding: const EdgeInsets.only(top: 15),
                child: CustomText(
                  element.title,
                  // maxLines: 40,
                  style: Theme.of(context).textTheme.subtitle1!.copyWith(
                      color: ColorResource.color222222,
                      // fontSize: 16,
                      fontWeight: FontWeight.w600),
                ),
              ),
              subtitle: Padding(
                padding: const EdgeInsets.only(top: 6.0),
                child: CustomText(
                  element.subTitle,
                  // maxLines: 2,
                  style: Theme.of(context).textTheme.subtitle1!.copyWith(
                        color: ColorResource.color787878,
                      ),
                ),
              ),
              trailing: GestureDetector(
                onTap: element.onTap,
                child: Transform.scale(
                  scale: 0.7,
                  child: CupertinoSwitch(
                    value: status2,
                    activeColor: ColorResource.color641653,
                    onChanged: (value) {
                      print("VALUE : $value");
                      setState(() {
                        status2 = value;
                      });
                    },
                  ),
                ),
              ),
              isThreeLine: true,
            ),
            Divider(),
            Padding(
              padding: const EdgeInsets.only(
                  left: 18, right: 15, bottom: 24, top: 15),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Expanded(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            CustomText(
                              element.title1,
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle1!
                                  .copyWith(color: ColorResource.color787878),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            CustomText(
                              element.subtitle1,
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle1!
                                  .copyWith(
                                      color: ColorResource.color222222,
                                      fontWeight: FontWeight.w800),
                            ),
                          ]),
                    ),
                    Expanded(
                      child: Column(
                        // crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          CustomText(
                            element.title2,
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(color: ColorResource.color787878),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 8),
                            child: CustomText(
                              element.subtitle1,
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle1!
                                  .copyWith(
                                      color: ColorResource.color222222,
                                      fontWeight: FontWeight.w800),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Row(children: [
                        Spacer(),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 20),
                          child: GestureDetector(
                            onTap: element.onTap,
                            child: CustomText(
                              element.title3,
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle1!
                                  .copyWith(
                                      color: ColorResource.colorFF781F,
                                      fontWeight: FontWeight.w600),
                            ),
                          ),
                        ),
                      ]),
                    ),
                  ]),
            ),
          ],
        ),
      );
    });
    return widgets;
  }

  List<Widget> buildonlineTansactionWidget() {
    List<Widget> widgets = [];
    bloc.onlineTransaction.forEach((element) {
      widgets.add(
        Column(
          children: [
            ListTile(
              contentPadding: EdgeInsets.only(right: 3, left: 15),
              minLeadingWidth: 14,
              leading:
                  SizedBox(width: 24, child: Image.asset(element.leadingImage)),
              title: Padding(
                padding: const EdgeInsets.only(top: 15),
                child: CustomText(
                  element.title,
                  // maxLines: 40,
                  style: Theme.of(context).textTheme.subtitle1!.copyWith(
                        color: ColorResource.color222222,
                        fontWeight: FontWeight.w600,
                        // fontSize: 16
                      ),
                ),
              ),
              subtitle: Padding(
                padding: const EdgeInsets.only(top: 6.0),
                child: CustomText(
                  element.subTitle,
                  // maxLines: 2,
                  style: Theme.of(context).textTheme.subtitle1!.copyWith(
                        color: ColorResource.color787878,
                      ),
                ),
              ),
              trailing: GestureDetector(
                onTap: element.onTap,
                child: Transform.scale(
                  scale: 0.7,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: CupertinoSwitch(
                      value: status3,
                      activeColor: ColorResource.color641653,
                      onChanged: (value) {
                        print("VALUE : $value");
                        setState(() {
                          status3 = value;
                        });
                      },
                    ),
                  ),
                ),
              ),
              isThreeLine: true,
            ),
            Divider(),
            Padding(
              padding: const EdgeInsets.only(
                  left: 18, right: 15, bottom: 24, top: 15),
              child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            CustomText(
                              element.title1,
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle1!
                                  .copyWith(color: ColorResource.color787878),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            CustomText(
                              element.subtitle1,
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle1!
                                  .copyWith(
                                      color: ColorResource.color222222,
                                      fontWeight: FontWeight.w800),
                            ),
                          ]),
                    ),
                    Expanded(
                      child: Column(
                        // crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          CustomText(
                            element.title2,
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(color: ColorResource.color787878),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 8),
                            child: CustomText(
                              element.subtitle1,
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle1!
                                  .copyWith(
                                      color: ColorResource.color222222,
                                      fontWeight: FontWeight.w800),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Row(children: [
                        Spacer(),
                        Padding(
                          padding: const EdgeInsets.only(
                            bottom: 20,
                          ),
                          child: GestureDetector(
                            onTap: element.onTap,
                            child: CustomText(
                              element.title3,
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle1!
                                  .copyWith(
                                    color: ColorResource.colorFF781F,
                                    fontWeight: FontWeight.w600,
                                  ),
                            ),
                          ),
                        ),
                      ]),
                    ),
                  ]),
            ),
          ],
        ),
      );
    });
    return widgets;
  }

  limitBottomSheet() async {
    await showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        backgroundColor: ColorResource.colorFFFFFF,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(32),
          ),
        ),
        context: context,
        builder: (BuildContext context) {
          return ChangeLimit();

          // bloc.add((SetTransactionLimitScreenInitialEvent()));
        });
  }
}
