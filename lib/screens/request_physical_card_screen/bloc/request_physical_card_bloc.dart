import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:meta/meta.dart';

part 'request_physical_card_event.dart';
part 'request_physical_card_state.dart';

class RequestPhysicalCardBloc
    extends Bloc<RequestPhysicalCardEvent, RequestPhysicalCardState> {
  RequestPhysicalCardBloc() : super(RequestPhysicalCardInitialState());

  @override
  Stream<RequestPhysicalCardState> mapEventToState(
    RequestPhysicalCardEvent event,
  ) async* {}
}
