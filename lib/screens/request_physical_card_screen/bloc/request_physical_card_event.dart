part of 'request_physical_card_bloc.dart';

@immutable
abstract class RequestPhysicalCardEvent extends BaseEquatable {}

class RequestPhysicalCardInitialEvent extends RequestPhysicalCardEvent {}
