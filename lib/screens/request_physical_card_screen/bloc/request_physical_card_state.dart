part of 'request_physical_card_bloc.dart';

@immutable
abstract class RequestPhysicalCardState extends BaseEquatable {}

class RequestPhysicalCardInitialState extends RequestPhysicalCardState {}
