import 'package:carousel_slider/carousel_controller.dart';
import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/router.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:icici/widgets/custom_button.dart';

import 'bloc/request_physical_card_bloc.dart';

class RequestPhysicalCardScreen extends StatefulWidget {
  @override
  _RequestPhysicalCardScreenState createState() =>
      _RequestPhysicalCardScreenState();
}

class _RequestPhysicalCardScreenState extends State<RequestPhysicalCardScreen> {
  RequestPhysicalCardBloc? bloc;

  int currentPage = 0;
  CarouselControllerImpl _carouselController = CarouselControllerImpl();

  void onPageChange(int index, CarouselPageChangedReason changeReason) {
    setState(() {
      currentPage = index;
    });
  }

  @override
  void initState() {
    super.initState();
    bloc = RequestPhysicalCardBloc()..add(RequestPhysicalCardInitialEvent());
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);

    return SafeArea(
      top: false,
      child: Scaffold(
          backgroundColor: ColorResource.color641653,
          body: BlocListener<RequestPhysicalCardBloc, RequestPhysicalCardState>(
            bloc: bloc,
            listener: (BuildContext context, RequestPhysicalCardState state) {},
            child:
                BlocBuilder<RequestPhysicalCardBloc, RequestPhysicalCardState>(
              bloc: bloc,
              builder: (BuildContext context, RequestPhysicalCardState state) {
                return Container(
                  color: ColorResource.color641653,
                  child: MediaQuery.removePadding(
                    context: context,
                    removeRight: true,
                    removeBottom: true,
                    removeLeft: true,
                    child: Flex(
                      direction: Axis.vertical,
                      children: <Widget>[
                        CustomAppbar(
                          titleString:
                              Languages.of(context)!.requestForPhysicalCard,
                          titleSpacing: 8,
                          iconEnumValues: IconEnum.back,
                          onItemSelected: (values) async {
                            Navigator.pop(context);
                          },
                        ),
                        Expanded(
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Expanded(
                                  child: CarouselSlider(
                                    carouselController: _carouselController,
                                    options: CarouselOptions(
                                        onPageChanged: onPageChange,
                                        disableCenter: true,
                                        pageSnapping: false,
                                        viewportFraction: 0.70,
                                        enableInfiniteScroll: false),
                                    items: [
                                      Image.asset(ImageResource.regular),
                                      Image.asset(ImageResource.expression),
                                    ],
                                  ),
                                ),
                                const SizedBox(height: 19),
                                Expanded(
                                  child: SizedBox(
                                    width: MediaQuery.of(context).size.width,
                                    height: 400,
                                    child: Container(
                                      decoration: const BoxDecoration(
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(14),
                                              topRight: Radius.circular(14)),
                                          color: ColorResource.colorFFFFFF),
                                      child: currentPage == 0
                                          ? regularPocketsCardWidget()
                                          : expressionPocketsCardWidget(),
                                    ),
                                  ),
                                ),
                              ]),
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          )),
    );
  }

  Widget regularPocketsCardWidget() {
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 16.0),
            child: CustomText(Languages.of(context)!.regularPocketsCard,
                style: Theme.of(context).textTheme.headline5!.copyWith(
                    color: ColorResource.color222222,
                    fontWeight: FontWeight.w700)),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const SizedBox(
                  height: 18,
                  child: Center(
                    child: Icon(
                      Icons.circle_sharp,
                      size: 10,
                    ),
                  ),
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: CustomText(
                      // Languages.of(context)!.weHappy,
                      'Use the card for all your shopping requirement at stores.',
                      style: Theme.of(context).textTheme.subtitle1!.copyWith(
                            color: ColorResource.color222222.withOpacity(0.5),
                          )),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: <Widget>[
                const Icon(
                  Icons.circle_sharp,
                  size: 10,
                ),
                const SizedBox(width: 10),
                CustomText(
                    // Languages.of(context)!.weHappy,
                    'Fees: ₹100.00+GST.',
                    style: Theme.of(context).textTheme.subtitle1!.copyWith(
                          color: ColorResource.color222222.withOpacity(0.5),
                        )),
              ],
            ),
          ),
          const Spacer(),
          Flexible(
            child: CustomButton(
              Languages.of(context)!.applyNow,
              onTap: () {
                Navigator.pushNamed(
                    context, AppRoutes.regularPocketsCardScreen);
              },
            ),
          )
        ],
      ),
    );
  }

  Widget expressionPocketsCardWidget() {
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 16.0),
            child: CustomText(Languages.of(context)!.expressionPocketsCard,
                style: Theme.of(context).textTheme.headline5!.copyWith(
                    color: ColorResource.color222222,
                    fontWeight: FontWeight.w700)),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const SizedBox(
                  height: 18,
                  child: Center(
                    child: Icon(
                      Icons.circle_sharp,
                      size: 10,
                    ),
                  ),
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: CustomText(
                      // Languages.of(context)!.weHappy,
                      'Personalize your card design choose from a gallery of 100+ awesome designs.',
                      style: Theme.of(context).textTheme.subtitle1!.copyWith(
                            color: ColorResource.color222222.withOpacity(0.5),
                          )),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const SizedBox(
                  height: 18,
                  child: Center(
                    child: Icon(
                      Icons.circle_sharp,
                      size: 10,
                    ),
                  ),
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: CustomText(
                      // Languages.of(context)!.weHappy,
                      'Get some great offers and discounts using this card.',
                      style: Theme.of(context).textTheme.subtitle1!.copyWith(
                            color: ColorResource.color222222.withOpacity(0.5),
                          )),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: <Widget>[
                const Icon(
                  Icons.circle_sharp,
                  size: 10,
                ),
                const SizedBox(width: 10),
                CustomText(
                    // Languages.of(context)!.weHappy,
                    'Fees: ₹₹250.00+GST.',
                    style: Theme.of(context).textTheme.subtitle1!.copyWith(
                          color: ColorResource.color222222.withOpacity(0.5),
                        )),
              ],
            ),
          ),
          const Spacer(),
          CustomButton(
            Languages.of(context)!.applyNow,
            onTap: () {
              Navigator.pushNamed(context, AppRoutes.expressionPocketsScreen);
            },
          )
        ],
      ),
    );
  }
}
