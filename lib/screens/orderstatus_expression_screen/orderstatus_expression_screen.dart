import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:icici/widgets/custom_button.dart';

import '../../router.dart';
import 'bloc/order_expression_bloc.dart';

class OrderExpression extends StatefulWidget {
  const OrderExpression({Key? key}) : super(key: key);

  @override
  _OrderExpressionState createState() => _OrderExpressionState();
}

class _OrderExpressionState extends State<OrderExpression> {
  bool isAddFund = false;
  String _copy = 'PKT0001234';
  OrderExpressionBloc? bloc;

  @override
  void initState() {
    super.initState();
    bloc = OrderExpressionBloc()..add(OrderExpressionIniialEvent());
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: Colors.transparent));
    return Scaffold(
        backgroundColor: ColorResource.color641653,
        body: BlocListener<OrderExpressionBloc, OrderExpressionState>(
          bloc: bloc,
          listener: (BuildContext context, OrderExpressionState state) {},
          child: BlocBuilder<OrderExpressionBloc, OrderExpressionState>(
            bloc: bloc,
            builder: (BuildContext context, OrderExpressionState state) {
              return Container(
                color: ColorResource.color641653,
                child: MediaQuery.removePadding(
                    context: context,
                    removeRight: true,
                    removeBottom: true,
                    removeLeft: true,
                    child: Column(children: <Widget>[
                      CustomAppbar(
                        titleString: Languages.of(context)!.orderStatus,
                        iconEnumValues: IconEnum.back,
                        titleSpacing: 8,
                        onItemSelected: (values) async {
                          Navigator.pop(context);
                        },
                      ),
                      Expanded(
                        child: Column(children: <Widget>[
                          Expanded(
                              flex: 6,
                              child:
                                  Image.asset(ImageResource.expressiontheme)),
                          Expanded(
                            flex: 8,
                            child: SizedBox(
                              width: MediaQuery.of(context).size.width,
                              child: Container(
                                  decoration: const BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(14),
                                          topRight: Radius.circular(14)),
                                      color: ColorResource.colorFFFFFF),
                                  child: orderExpressionStatusBottomSheet()),
                            ),
                          ),
                        ]),
                      ),
                    ])),
              );
            },
          ),
        ));
  }

  orderExpressionStatusBottomSheet() {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: CustomText('Hi, Prince Anto,',
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      color: ColorResource.color222222,
                      fontWeight: FontWeight.w700)),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 15.0),
              child: CustomText(Languages.of(context)!.thankYou,
                  style: Theme.of(context).textTheme.subtitle1!.copyWith(
                        color: ColorResource.color909090,
                      )),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 6.0),
              child: CustomText(Languages.of(context)!.orderDetails,
                  style: Theme.of(context).textTheme.subtitle1!.copyWith(
                        color: ColorResource.color222222,
                      )),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 6.0, right: 10),
              child: Row(
                children: <Widget>[
                  CustomText('Reference ID:',
                      // Languages.of(context)!.deliverTo,
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          color: ColorResource.color222222,
                          fontWeight: FontWeight.w700)),
                  CustomText(_copy,
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          color: ColorResource.color222222,
                          fontWeight: FontWeight.w700)),
                  const SizedBox(width: 20),
                  GestureDetector(
                      onTap: () {


                        copyToClipboard();
                      },
                      child: Image.asset(ImageResource.copyOrder)),

                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 6.0, right: 10),
              child: Row(
                children: <Widget>[
                  CustomText('Date of Order:',
                      // Languages.of(context)!.deliverTo,
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          color: ColorResource.color222222,
                          fontWeight: FontWeight.w700)),
                  CustomText(' May 30, 2021',
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          color: ColorResource.color222222,
                          fontWeight: FontWeight.w700)),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 22.0, right: 10),
              child: Row(
                children: <Widget>[
                  CustomText('Amount Paid:',
                      // Languages.of(context)!.deliverTo,
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          color: ColorResource.color222222,
                          fontWeight: FontWeight.w700)),
                  CustomText(' ₹299.00',
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          color: ColorResource.color222222,
                          fontWeight: FontWeight.w700)),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 6.0),
              child: CustomText(Languages.of(context)!.estimatedDelivery,
                  style: Theme.of(context).textTheme.subtitle1!.copyWith(
                        color: ColorResource.color222222,
                      )),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  CustomText('5 Jun-6 Jun',
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          color: ColorResource.color222222,
                          fontWeight: FontWeight.w700)),
                  GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(
                          context, AppRoutes.trackShipmentScreen);
                    },
                    child: CustomText(Languages.of(context)!.trackShipment,
                        style: Theme.of(context).textTheme.subtitle1!.copyWith(
                              color: ColorResource.colorF58220,
                            )),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 15.0, right: 10),
              child: CustomButton(
                Languages.of(context)!.returntoAccountDetails,
                onTap: () {
                  int count = 0;
                  Navigator.popUntil(context, (route) {
                    return count++ == 5;
                  });
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> copyToClipboard() async {
    await Clipboard.setData(ClipboardData(text: _copy));
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: CustomText(
        Languages.of(context)!.copyToClipboard,
        color: ColorResource.colorFFFFFF,
      ),
    ));
  }
}
