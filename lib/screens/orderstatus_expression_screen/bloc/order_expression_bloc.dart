import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:meta/meta.dart';

part 'order_expression_event.dart';
part 'order_expression_state.dart';

class OrderExpressionBloc extends Bloc<OrderExpressionEvent, OrderExpressionState> {
  OrderExpressionBloc() : super(OrderExpressionInitial());

  @override
  Stream<OrderExpressionState> mapEventToState(
    OrderExpressionEvent event,
  ) async* {
    
  }
}
