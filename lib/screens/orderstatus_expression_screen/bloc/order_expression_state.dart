part of 'order_expression_bloc.dart';

@immutable
abstract class OrderExpressionState extends BaseEquatable{}

class OrderExpressionInitial extends OrderExpressionState {}
