import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:icici/model/address_model.dart';
import 'package:icici/model/profile_models.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:meta/meta.dart';

part 'profile_event.dart';
part 'profile_state.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  ProfileBloc() : super(ProfileInitial());

  List<Profile> profile = [];
  List<Profile> kyc = [];
  List<Profile> others = [];
  List<String> upiIdSuggestions = [
    'PRINCE234295PKT',
    '6202589829PKT',
    '620258GANESH',
    // 'PRINCE234295PKT',
    // '6202589829PKT',
    // '620258GANESH',
    // 'PRINCE234295PKT',
    // '6202589829PKT',
    // '620258GANESH',
    // 'PRINCE234295PKT',
    // '6202589829PKT',
    // '620258GANESH',
    // 'PRINCE234295PKT',
    // '6202589829PKT',
    // '620258GANESH',
    // 'PRINCE234295PKT',
    // '6202589829PKT',
    // '620258GANESH',
    // 'PRINCE234295PKT',
    // '6202589829PKT',
    // '620258GANESH',
    // 'PRINCE234295PKT',
    // '6202589829PKT',
    // '620258GANESH',
  ];

  String noAddress = 'No address';
  // String selectedUpiId = '';

  String findScreen = 'profileScreen';
  AddressModel? address;
  bool isAddressSaved = false;
  String emailId = 'prince.anto@gmail.com';
  bool isEmailVerified = false;
  String upiid = '6202589829@pockets';

  @override
  Stream<ProfileState> mapEventToState(
    ProfileEvent event,
  ) async* {
    if (event is ProfileInitialEvent) {
      profile.clear();
      yield ProfileLoadingState();
      profile.clear();
      kyc.clear();
      others.clear();
      profile.addAll([
        Profile(
          ImageResource.mobileNumber,
          StringResource.mobileNumber,
          subTitle: '+91 9582671900',
        ),
        Profile(ImageResource.emailId, StringResource.emailAddress,
            subTitle: emailId, onTap: () {
          this.add(ProfileVerifyBottomSheetEvent());
        },
            trailing: isEmailVerified
                ? StringResource.update
                : StringResource.verify),
        Profile(ImageResource.address, StringResource.address,
            subTitle: isAddressSaved
                ? '${address!.houseNumber},${address!.address1},${address!.address2}${(address!.address2 != null && address!.address2!.isNotEmpty) ? ',' : ''}${address!.pincode}'
                : noAddress, onTap: () {
          this.add(ProfileBottomSheetEvent());
        },
            trailing: isAddressSaved
                ? StringResource.update
                : StringResource.addAddress),
      ]);
      kyc.addAll([
        Profile(ImageResource.kyc, StringResource.kyc,
            subTitle: 'Min KYC',
            onTap: () {},
            trailing: StringResource.completeKyc),
        Profile(ImageResource.pocketsCard, StringResource.pocketsCard,
            subTitle: 'No', onTap: () {
          add(ProfilePocketCardApplyEvent());
        }, trailing: StringResource.applyForPhysicalCard),
        Profile(ImageResource.upi, StringResource.upiId,
            subTitle: upiid, trailing: StringResource.edit, onTap: () {
          add(ProfileEditUpiIdBottomSheetEvent());
        }),
        Profile(ImageResource.qrCode, StringResource.myQrCode,
            subTitle: 'Tap on Display', onTap: () {
          this.add(DisplayQRBottomSheetEvent());
        }, trailing: StringResource.displayQrCode),
        Profile(
          ImageResource.kycDocument,
          StringResource.kycDocuments,
          subTitle: 'Aadhar card',
        ),
      ]);
      yield ProfileLoadedState();
    }
    if (event is ProfileBottomSheetEvent) {
      yield ProfileBottomSheetState();
    }
    if (event is DisplayQRBottomSheetEvent) {
      yield DisplayQRState();
    }
    if (event is ProfileVerifyBottomSheetEvent) {
      yield ProfileVerifyEmailBottomSheetState();
    }

    if (event is ProfileEditUpiIdBottomSheetEvent) {
      yield ProfileEditUpiIdBottomSheetState();
    }

    if (event is ProfileRefreshEvent) {
      yield ProfileLoadedState();
    }

    if (event is ProfilePocketCardApplyEvent) {
      yield ProfilePocketCardApplyState();
    }
  }
}
