part of 'profile_bloc.dart';

@immutable
abstract class ProfileState extends BaseEquatable {}

class ProfileInitial extends ProfileState {}

class ProfileBottomSheetState extends ProfileState {}

class ProfileVerifyEmailBottomSheetState extends ProfileState {}

class ProfileEditUpiIdBottomSheetState extends ProfileState {}

class ProfileLoadingState extends ProfileState {}

class ProfileLoadedState extends ProfileState {}

class DisplayQRState extends ProfileState {}

class ProfilePocketCardApplyState extends ProfileState {}
