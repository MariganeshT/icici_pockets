part of 'profile_bloc.dart';

abstract class ProfileEvent extends BaseEquatable {}

class ProfileInitialEvent extends ProfileEvent {}

class ProfileBottomSheetEvent extends ProfileEvent {}

class DisplayQRBottomSheetEvent extends ProfileEvent {}

class ProfileVerifyBottomSheetEvent extends ProfileEvent {}

class ProfileEditUpiIdBottomSheetEvent extends ProfileEvent {}

class ProfileRefreshEvent extends ProfileEvent {}

class ProfilePocketCardApplyEvent extends ProfileEvent {}
