import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_button.dart';
import 'package:icici/widgets/custom_textfield.dart';

class EditUpiIdBottomSheet extends StatefulWidget {
  // ignore: use_key_in_widget_constructors
  EditUpiIdBottomSheet(this.upiid, this.upiIdSuggestions, this.onClose,
      this.onContinue, this.scrollController);

  String upiid;
  List<String> upiIdSuggestions;
  Function onClose;
  Function onContinue;
  ScrollController scrollController;

  @override
  _EditUpiIdBottomSheetState createState() => _EditUpiIdBottomSheetState();
}

class _EditUpiIdBottomSheetState extends State<EditUpiIdBottomSheet> {
  TextEditingController upiIdController = TextEditingController();

  final _form = GlobalKey<FormState>();

  late FocusNode upiFocus;

  String selectedUpiId = '';

  @override
  void initState() {
    super.initState();
    upiIdController.text = widget.upiid;
    upiFocus = FocusNode();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25),
        color: Colors.white,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(
                    left: 16.0,
                  ),
                  child: GestureDetector(
                      onTap: () {
                        widget.onClose();
                      },
                      child: Image.asset(ImageResource.close)),
                ),
                const SizedBox(width: 100),
                CustomText(
                  Languages.of(context)!.upiid,
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ],
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              controller: widget.scrollController,
              child: Form(
                key: _form,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  child: Column(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                              margin:
                                  const EdgeInsets.only(top: 32, bottom: 16),
                              child: CustomTextField(
                                StringResource.upiId,
                                upiIdController,
                                focusNode: upiFocus,
                                focusTextColor: ColorResource.color222222,
                                keyBoardType: TextInputType.text,
                                validationRules: ['required'],
                                validatorCallBack: (bool values) {},
                              )),
                          Row(
                            children: [
                              Visibility(
                                visible: selectedUpiId.isNotEmpty,
                                child: Row(
                                  children: [
                                    Container(
                                      margin: const EdgeInsets.only(right: 10),
                                      width: 13,
                                      height: 13,
                                      child: Image.asset(
                                          selectedUpiId == widget.upiid
                                              ? ImageResource.unavailble
                                              : ImageResource
                                                  .shipment_tracking_activate),
                                    ),
                                    CustomText(
                                      selectedUpiId == widget.upiid
                                          ? Languages.of(context)!.notAvailable
                                          : Languages.of(context)!.available,
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle1!
                                          .copyWith(
                                              color: ColorResource.color222222,
                                              fontWeight: FontWeight.w500),
                                    ),
                                  ],
                                ),
                              ),
                              Spacer(),
                              Container(
                                width: 152,
                                child: CustomButton(
                                  Languages.of(context)!.checkAvailability,
                                  buttonBackgroundColor: Colors.white,
                                  textColor: ColorResource.colorFF781F,
                                  isEnabled: selectedUpiId.isEmpty,
                                  onTap: () {
                                    setState(() {
                                      selectedUpiId = widget.upiid;
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 32.0),
                            child: CustomText(
                              Languages.of(context)!.alternateSuggestedUpiid,
                              color: ColorResource.color787878,
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle1!
                                  .copyWith(
                                      color: ColorResource.color787878,
                                      fontWeight: FontWeight.w400),
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(top: 18.0, bottom: 50),
                            child: Wrap(
                              runSpacing: 14,
                              spacing: 22,
                              children: _buildUpiSuggestions(),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Container(
            margin:
                const EdgeInsets.only(bottom: 20, left: 24, right: 24, top: 10),
            child: CustomButton(
              Languages.of(context)!.continueText,
              onTap: () {
                _saveForm();
              },
            ),
          ),
        ],
      ),
    );
  }

  void _saveForm() {
    final bool isValid = _form.currentState!.validate();

    if (!isValid) {
      return;
    }
    _form.currentState!.save();

    widget.onContinue(upiIdController.text.trim());
  }

  Widget _buildUpiIdWidget(String upiid) {
    return InkWell(
      onTap: () {
        setState(() {
          upiIdController.text = upiid;
          selectedUpiId = upiid;
        });
      },
      child: Container(
        padding: const EdgeInsets.all(14),
        decoration: BoxDecoration(
          border: Border.all(color: ColorResource.colorefefef),
          borderRadius: BorderRadius.circular(56),
          color:
              upiid == selectedUpiId ? ColorResource.color641653 : Colors.white,
        ),
        child: CustomText(
          upiid,
          style: Theme.of(context).textTheme.button!.copyWith(
                color: upiid == selectedUpiId
                    ? Colors.white
                    : ColorResource.color222222,
              ),
        ),
      ),
    );
  }

  List<Widget> _buildUpiSuggestions() {
    List<Widget> widgets = [];
    widget.upiIdSuggestions.forEach((element) {
      widgets.add(_buildUpiIdWidget(element));
    });
    return widgets;
  }

  @override
  void dispose() {
    upiFocus.dispose();

    super.dispose();
  }
}
