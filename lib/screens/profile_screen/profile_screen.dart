import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/model/address_model.dart';
import 'package:icici/model/profile_models.dart';
import 'package:icici/router.dart';
import 'package:icici/screens/profile_screen/add_address.dart';
import 'package:icici/screens/profile_screen/verify_email_bottom_sheet.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:icici/widgets/display_qr.dart';
import 'package:image_picker/image_picker.dart';

import 'bloc/profile_bloc.dart';
import 'edit_upi_id_bottom_sheet.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  File? image;

  Future pickImage(
      ImageSource source, BuildContext cameraDialogueContext) async {
    try {
      final image = await ImagePicker().pickImage(source: source);
      if (image == null) return;

      final imageTemporary = File(image.path);
      setState(() => this.image = imageTemporary);
    } on PlatformException catch (e) {
      'error';
    }
    Navigator.pop(cameraDialogueContext);
  }

  late ProfileBloc bloc;

  TextEditingController emailTextController = TextEditingController();

  final _form = GlobalKey<FormState>();

  late FocusNode emailFocus;

  @override
  void initState() {
    super.initState();
    emailFocus = FocusNode();
    bloc = BlocProvider.of<ProfileBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);

    return Scaffold(
        backgroundColor: ColorResource.colorFAFAFA,
        body: BlocListener<ProfileBloc, ProfileState>(
          bloc: bloc,
          listener: (BuildContext context, ProfileState state) {
            if (state is ProfileBottomSheetState) {
              addressBottomSheet();
            }
            if (state is DisplayQRState) {
              displayQR(context, 'ICICI Pockets QR code');
            }
            if (state is ProfileVerifyEmailBottomSheetState) {
              verifyEmail();
            }
            if (state is ProfilePocketCardApplyState) {
              Navigator.pushNamed(context, AppRoutes.requestPhysicalCardScreen);
            }
          },
          child: BlocBuilder<ProfileBloc, ProfileState>(
            bloc: bloc,
            builder: (BuildContext context, ProfileState state) {
              if (state is ProfileLoadingState) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
              return Stack(
                children: [
                  Column(
                    children: <Widget>[
                      SizedBox(
                        child: CustomAppbar(
                          backgroundColor: ColorResource.color641653,
                          titleString: StringResource.myProfile,
                          titleSpacing: 8,
                          style: Theme.of(context)
                              .textTheme
                              .headline5!
                              .copyWith(color: ColorResource.colorFFFFFF),
                          iconEnumValues: IconEnum.back,
                          onItemSelected: (selectedItem) {
                            Navigator.pop(context);
                          },
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: SingleChildScrollView(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Stack(alignment: Alignment.topRight, children: <
                                    Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(top: 40),
                                    child: Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(14),
                                        color: ColorResource.colorFFFFFF,
                                      ),
                                      width: double.infinity,
                                      height: 85,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 12, top: 12, bottom: 2),
                                            child: CustomText(
                                              StringResource.overview,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .subtitle2!
                                                  .copyWith(
                                                      color: ColorResource
                                                          .color787878),
                                            ),
                                          ),
                                          Padding(
                                            padding:
                                                const EdgeInsets.only(left: 12),
                                            child: CustomText(
                                              'Prince Anto',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline5!
                                                  .copyWith(
                                                      color: ColorResource
                                                          .color222222),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      right: 15,
                                    ),
                                    child: Stack(
                                      children: <Widget>[
                                        // imageURI == null
                                        image == null
                                            ? Container(
                                                child: Image.asset(
                                                    ImageResource.profile),
                                                width: 107,
                                                height: 105,
                                                decoration: BoxDecoration(
                                                  color:
                                                      ColorResource.colorBFCBD7,
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          52.5),
                                                ),
                                              )
                                            : SizedBox(
                                                width: 107,
                                                height: 105,
                                                child: CircleAvatar(
                                                    radius: 52,
                                                    backgroundImage: FileImage(
                                                      File(image!.path
                                                          // imageURI!.path,
                                                          ),
                                                    ))),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              top: 92.0, left: 45),
                                          child: GestureDetector(
                                            child: Image.asset(
                                                ImageResource.camera),
                                            onTap: () {
                                              camera();
                                            },
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ]),
                                const SizedBox(height: 24),
                                Container(
                                  decoration: const BoxDecoration(
                                      color: ColorResource.colorFFFFFF,
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(14.0),
                                      )),
                                  child: Column(children: buildProfileWidget()),
                                ),
                                const SizedBox(height: 24),
                                Container(
                                  decoration: const BoxDecoration(
                                      color: ColorResource.colorFFFFFF,
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(14.0),
                                      )),
                                  child: Column(children: buildKycWidget()),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  if (state is ProfileEditUpiIdBottomSheetState) editUpiId()
                ],
              );
            },
          ),
        ));
  }

  List<Widget> buildProfileWidget() {
    final List<Widget> widgets = [];
    bloc.profile.forEach((Profile element) {
      widgets.add(
        ListTile(
          minLeadingWidth: 14,
          leading: SizedBox(
              width: 24,
              child: Center(
                child: Image.asset(
                  element.leadingImage,
                  fit: BoxFit.fitHeight,
                ),
              )),
          title: CustomText(
            element.title,
            style: Theme.of(context)
                .textTheme
                .subtitle1!
                .copyWith(color: ColorResource.color787878),
          ),
          subtitle: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: CustomText(
                  element.subTitle,
                  style: Theme.of(context)
                      .textTheme
                      .subtitle1!
                      .copyWith(color: ColorResource.color222222),
                ),
              ),
              GestureDetector(
                onTap: element.onTap,
                child: element.isTrailingIcon == false
                    ? Container(
                        padding: const EdgeInsets.all(8),
                        child: CustomText(
                          element.trailing,
                          style: Theme.of(context)
                              .textTheme
                              .subtitle1!
                              .copyWith(color: ColorResource.colorF58220),
                        ),
                      )
                    : Image.asset(element.trailing),
              ),
            ],
          ),
        ),
      );
    });
    return widgets;
  }

  List<Widget> buildKycWidget() {
    final List<Widget> widgets = [];
    bloc.kyc.forEach((Profile element) {
      widgets.add(ListTile(
        minLeadingWidth: 14,
        leading: SizedBox(
            width: 24,
            child: Center(
              child: Image.asset(
                element.leadingImage,
                fit: BoxFit.cover,
              ),
            )),
        title: element.isTitleColor == false
            ? CustomText(
                element.title,
                style: Theme.of(context)
                    .textTheme
                    .subtitle1!
                    .copyWith(color: ColorResource.color787878),
              )
            : CustomText(
                element.title,
                style: Theme.of(context)
                    .textTheme
                    .subtitle1!
                    .copyWith(color: ColorResource.colorF58220),
              ),
        subtitle: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: CustomText(
                element.subTitle,
                style: Theme.of(context)
                    .textTheme
                    .subtitle1!
                    .copyWith(color: ColorResource.color222222),
              ),
            ),
            GestureDetector(
              onTap: element.onTap,
              child: Container(
                padding: const EdgeInsets.all(8),
                child: element.isTrailingIcon == false
                    ? CustomText(
                        element.trailing,
                        style: Theme.of(context)
                            .textTheme
                            .subtitle1!
                            .copyWith(color: ColorResource.colorF58220),
                      )
                    : Image.asset(element.trailing),
              ),
            ),
          ],
        ),
      ));
    });
    return widgets;
  }

  addressBottomSheet() async {
    await showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        backgroundColor: ColorResource.colorFFFFFF,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(32),
          ),
        ),
        context: context,
        builder: (BuildContext context) {
          return AddAddress(
            bloc.noAddress,
            bloc.address,
            bloc.isAddressSaved,
            (AddressModel updatedAddress) {
              bloc.isAddressSaved = true;
              setState(() {
                bloc.address = updatedAddress;
              });

              bloc.add(ProfileInitialEvent());
            },
            bloc.findScreen,
          );
        });
  }

  camera() async {
    await showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: true,
        backgroundColor: ColorResource.colorFFFFFF,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(32),
          ),
        ),
        context: context,
        builder: (BuildContext context) {
          return Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 20.0),
                  child: ListTile(
                    leading: Image.asset(ImageResource.camera1),
                    title: CustomText(
                      Languages.of(context)!.camera,
                      style: Theme.of(context)
                          .textTheme
                          .subtitle1!
                          .copyWith(color: ColorResource.color222222),
                    ),
                    onTap: () {
                      pickImage(ImageSource.camera, context);
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 20.0),
                  child: ListTile(
                    leading: Image.asset(ImageResource.gallery),
                    title: CustomText(
                      Languages.of(context)!.gallery,
                      style: Theme.of(context)
                          .textTheme
                          .subtitle1!
                          .copyWith(color: ColorResource.color222222),
                    ),
                    onTap: () {
                      pickImage(ImageSource.gallery, context);
                    },
                  ),
                ),
              ],
            ),
          );
        });
  }

  void displayQR(BuildContext buildContext, String qrtext) {
    showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        context: buildContext,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(32.0), topRight: Radius.circular(32.0)),
        ),
        backgroundColor: ColorResource.colorFFFFFF,
        builder: (BuildContext context) {
          Future.delayed(const Duration(minutes: 2), () {
            Navigator.pop(context);
          });
          return DisplayQR(
            qrtext,
          );
        });
  }

  verifyEmail() async {
    await showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        backgroundColor: ColorResource.colorFFFFFF,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(32),
          ),
        ),
        context: context,
        builder: (BuildContext context) {
          return Padding(
            padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).viewInsets.bottom),
            child: VerifyEmailBottomSheet(bloc.emailId, bloc.isEmailVerified,
                (String emailId) {
              Navigator.pushNamed(
                      context, AppRoutes.customOtpVerificationScreen,
                      arguments: bloc.emailId)
                  .then((value) {
                if (value != null) {
                  if (!bloc.isEmailVerified) {
                    bloc.isEmailVerified = true;
                  }
                  bloc.emailId = emailId;
                }
                bloc.add(ProfileInitialEvent());
              });
            }),
          );
        });
  }

  Widget editUpiId() {
    double height = (411 / MediaQuery.of(context).size.height);
    return Stack(
      children: [
        InkWell(
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Colors.black.withOpacity(0.30),
          ),
          onTap: () {
            // bottomsheet dismiss event
            bloc.add(ProfileRefreshEvent());
          },
        ),
        DraggableScrollableSheet(
          initialChildSize: height,
          minChildSize: height,
          maxChildSize: height,
          builder: (BuildContext context, ScrollController myscrollController) {
            return ClipRRect(
              borderRadius: BorderRadius.circular(25),
              child:
                  EditUpiIdBottomSheet(bloc.upiid, bloc.upiIdSuggestions, () {
                bloc.add(ProfileRefreshEvent());
              }, (String selectredUpiId) {
                bloc.upiid = selectredUpiId;
                bloc.add(ProfileInitialEvent());
              }, myscrollController),
            );
          },
        )
      ],
    );
  }
}
