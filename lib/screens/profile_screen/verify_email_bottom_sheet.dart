import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_button.dart';
import 'package:icici/widgets/custom_textfield.dart';

class VerifyEmailBottomSheet extends StatefulWidget {
  VerifyEmailBottomSheet(this.emailId, this.isEmailVerified, this.onSaved)
      : super();

  Function onSaved;
  String emailId;
  bool isEmailVerified;

  @override
  _VerifyEmailBottomSheetState createState() => _VerifyEmailBottomSheetState();
}

class _VerifyEmailBottomSheetState extends State<VerifyEmailBottomSheet> {
  TextEditingController emailTextController = TextEditingController();

  final _form = GlobalKey<FormState>();

  late FocusNode emailFocus;

  @override
  void initState() {
    super.initState();
    emailTextController.text = widget.emailId;
    emailFocus = FocusNode();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 16.0, top: 12),
              child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Image.asset(ImageResource.close)),
            ),
            const SizedBox(width: 100),
            CustomText(
              widget.isEmailVerified
                  ? Languages.of(context)!.updateEmail
                  : Languages.of(context)!.verifyEmail,
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ],
        ),
        Form(
          key: _form,
          child: Column(
            children: <Widget>[
              Container(
                  margin: const EdgeInsets.only(
                      left: 24, right: 24, top: 32, bottom: 50),
                  child: CustomTextField(
                    StringResource.emailId,
                    emailTextController,
                    focusNode: emailFocus,
                    focusTextColor: ColorResource.color222222,
                    keyBoardType: TextInputType.emailAddress,
                    validationRules: ['required', 'email'],
                    validatorCallBack: (bool values) {},
                  )),
              Container(
                color: ColorResource.colorFAFAFA,
                padding: const EdgeInsets.only(left: 24, right: 24, bottom: 20),
                child: CustomButton(
                  StringResource.verify,
                  onTap: () {
                    _saveForm();
                  },
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  void _saveForm() {
    final bool isValid = _form.currentState!.validate();

    if (!isValid) {
      return;
    }
    _form.currentState!.save();
    Navigator.of(context).pop();
    this.widget.onSaved(emailTextController.text);
    // } else {
    //   Navigator.pushNamed(context, AppRoutes.verifyIdentityScreen);
    // }
  }

  @override
  void dispose() {
    emailFocus.dispose();

    super.dispose();
  }
}
