import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/model/address_model.dart';
import 'package:icici/router.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_button.dart';
import 'package:icici/widgets/custom_textfield.dart';

class AddAddress extends StatefulWidget {
  AddAddress(
    this.noAddress,
    this.address,
    this.isAddressSaved,
    this.saved,
    this.findScreen,
  ) : super();

  Function saved;
  String noAddress;
  AddressModel? address;
  bool isAddressSaved;
  String findScreen;

  @override
  _AddAddressState createState() => _AddAddressState();
}

class _AddAddressState extends State<AddAddress> {
  TextEditingController pincodeController = TextEditingController();
  TextEditingController houseController = TextEditingController();
  TextEditingController address1Controller = TextEditingController();
  TextEditingController address2Controller = TextEditingController();
  final GlobalKey<FormState> _form = GlobalKey<FormState>();

  late FocusNode pincodeFocus;
  late FocusNode houseFocus;
  late FocusNode address1Focus;
  late FocusNode address2Focus;

  @override
  void initState() {
    super.initState();

    if (widget.address != null) {
      pincodeController.text = widget.address!.pincode;
      houseController.text = widget.address!.houseNumber;
      address1Controller.text = widget.address!.address1;
      address2Controller.text = widget.address!.address2 ?? '';
    }

    pincodeFocus = FocusNode();
    houseFocus = FocusNode();
    address1Focus = FocusNode();
    address2Focus = FocusNode();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: Colors.transparent));

    return Container(
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: SingleChildScrollView(
        child: Column(
          children: [
            Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 16.0, top: 20),
                      child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Image.asset(ImageResource.close)),
                    ),
                    const SizedBox(width: 100),
                    CustomText(
                      widget.address != null
                          ? Languages.of(context)!.updateAddress
                          : Languages.of(context)!.addAddress,
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ],
                ),
                Form(
                  key: _form,
                  child: Column(
                    children: <Widget>[
                      Container(
                          margin: const EdgeInsets.only(
                            left: 24,
                            right: 24,
                            top: 15,
                          ),
                          child: CustomTextField(
                            StringResource.pincode,
                            pincodeController,
                            focusNode: pincodeFocus,
                            focusTextColor: ColorResource.color222222,
                            keyBoardType: TextInputType.number,
                            inputformaters: [
                              LengthLimitingTextInputFormatter(6)
                            ],
                            onEditing: () {
                              houseFocus.requestFocus(FocusNode());
                              _saveForm();
                            },
                            validationRules: ['required'],
                            validatorCallBack: (bool values) {},
                          )),
                      Container(
                          margin: const EdgeInsets.only(
                              top: 10, left: 24, right: 24),
                          child: CustomTextField(
                            StringResource.house,
                            houseController,
                            focusNode: houseFocus,
                            focusTextColor: ColorResource.color222222,
                            keyBoardType: TextInputType.streetAddress,
                            onEditing: () {
                              address1Focus.requestFocus(FocusNode());
                              _saveForm();
                            },
                            validationRules: ['required'],
                            validatorCallBack: (bool values) {},
                          )),
                      Container(
                        margin:
                            const EdgeInsets.only(top: 10, left: 24, right: 24),
                        child: CustomTextField(
                          StringResource.address1,
                          address1Controller,
                          focusNode: address1Focus,
                          focusTextColor: ColorResource.color222222,
                          keyBoardType: TextInputType.streetAddress,
                          onEditing: () {
                            address2Focus.requestFocus(FocusNode());
                            // _saveForm();
                          },
                          validationRules: ['required'],
                          validatorCallBack: (bool values) {},
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(
                            top: 10, left: 24, right: 24, bottom: 30),
                        child: CustomTextField(
                          StringResource.address2,
                          address2Controller,
                          focusNode: address2Focus,
                          focusTextColor: ColorResource.color222222,
                          validatorCallBack: (bool values) {},
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Container(
              color: ColorResource.colorFAFAFA,
              padding: const EdgeInsets.only(left: 24, right: 24, bottom: 2),
              child: CustomButton(
                StringResource.save,
                onTap: () {
                  _saveForm();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _saveForm() {
    final bool isValid = _form.currentState!.validate();

    if (!isValid) {
      return;
    }
    _form.currentState!.save();

    AddressModel updatedAddress = AddressModel(pincodeController.text,
        houseController.text, address1Controller.text, address2Controller.text);
    if (widget.findScreen == 'profileScreen') {
      Navigator.of(context).pop();
      this.widget.saved(updatedAddress);
    } else if (widget.findScreen == 'regularPocketsCardScreen') {
      Navigator.pushNamed(context, AppRoutes.placeOrderScreen,
          arguments: updatedAddress);
    } else if (widget.findScreen == 'expressionPocketsScreen') {
      Navigator.pushNamed(context, AppRoutes.placeOrderExpressionScreen,
          arguments: updatedAddress);
    }
  }

  @override
  void dispose() {
    pincodeFocus.dispose();
    houseFocus.dispose();
    address1Focus.dispose();
    address2Focus.dispose();
    super.dispose();
  }
}
