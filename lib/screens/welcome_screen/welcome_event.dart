
import 'package:icici/utils/base_equatable.dart';

abstract class WelcomeEvent extends BaseEquatable {}

class LoadingEvent extends WelcomeEvent {}
