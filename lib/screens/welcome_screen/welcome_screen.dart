import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_sms/flutter_sms.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/languages/app_locale_constant.dart';
import 'package:icici/router.dart';
import 'package:icici/screens/otp_verification_screen/otp_verification_screen.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/constants.dart';
import 'package:icici/utils/font.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/widgets/clip_path_utils.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_button.dart';
import 'package:permission_handler/permission_handler.dart';

class WelcomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen>
    with WidgetsBindingObserver {
  Locale? _locale;
  bool sentMessage = false;
  static const platform = MethodChannel('icici.flutter.methodChannel');

  @override
  void initState() {
    WidgetsBinding.instance!.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    getLocale().then((Locale locale) {
      setState(() {
        _locale = locale;
      });
    });
    super.didChangeDependencies();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.resumed:
        if (sentMessage) {
          // verifyingNumberView();
        }
        break;
      case AppLifecycleState.inactive:
        // TODO: Handle this case.
        break;
      case AppLifecycleState.paused:
        // TODO: Handle this case.
        break;
      case AppLifecycleState.detached:
        // TODO: Handle this case.
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorResource.colorFFFFFF,
      child: Flex(
        direction: Axis.vertical,
        children: <Widget>[
          Column(
            children: [
              SizedBox(
                child: Container(
                  height: 340,
                  alignment: Alignment.center,
                  child: ClipPath(
                    clipper: ClipPathClass(),
                    child: Container(
                      color: ColorResource.color641653,
                      child: Stack(
                        children: [
                          Flex(
                            direction: Axis.vertical,
                            children: [
                              Expanded(
                                flex: 2,
                                // ignore: sized_box_for_whitespace
                                child: Container(
                                  height: 370,
                                  child: Image.asset(
                                    ImageResource.welcomeImagePNG,
                                    fit: BoxFit.fitWidth,
                                  ),
                                  width: double.maxFinite,
                                ),
                              ),
                            ],
                          ),
                          Container(
                            // height: 430,
                            padding: const EdgeInsets.all(36.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                CustomText(
                                  Languages.of(context)!.welcomeString,
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline6!
                                      .copyWith(
                                          color: ColorResource.colorFF781F),
                                ),
                                CustomText(
                                  '${Languages.of(context)!.pockets}!!!',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline6!
                                      .copyWith(
                                          color: ColorResource.colorFFFFFF),
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                CustomText(
                                  Languages.of(context)!
                                      .welcomeDescriptionString,
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1!
                                      .copyWith(
                                          color: ColorResource.colorFFFFFF
                                              .withOpacity(0.8)),
                                  lineHeight: 28,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                padding:
                    const EdgeInsets.only(left: 36.0, right: 36.0, top: 10.0),
                width: MediaQuery.of(context).size.width,
                alignment: Alignment.bottomCenter,
                // padding: const EdgeInsets.all(36),
                child: Flex(
                  direction: Axis.vertical,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ListView.builder(
                        physics: const NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        primary: false,
                        itemCount: Constants.verificationList.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            height: 65,
                            decoration: const BoxDecoration(
                              color: ColorResource.colorf8f8fa,
                              borderRadius: BorderRadius.all(
                                Radius.circular(8.0),
                              ),
                            ),
                            margin: const EdgeInsets.only(bottom: 20.0),
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                Card(
                                  child: Container(
                                    decoration: const BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(8.0),
                                      ),
                                    ),
                                    child: Image.asset(
                                      Constants
                                          .verificationList[index].iconName,
                                      width: 40,
                                      height: 48,
                                    ),
                                  ),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                  elevation: 2,
                                ),
                                const SizedBox(
                                  width: 20,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    CustomText(
                                      Constants
                                          .verificationList[index].setupNumber,
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle2!
                                          .copyWith(
                                              color: ColorResource.color787878,
                                              height: 2),
                                    ),
                                    CustomText(
                                      Constants
                                          .verificationList[index].titleName,
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText1!
                                          .copyWith(
                                              color: ColorResource.color222222,
                                              height: 1.33),
                                    )
                                  ],
                                )
                              ],
                            ),
                          );
                        }),
                    GestureDetector(
                      child: CustomButton(
                        Languages.of(context)!.verifyYourMobileNumber,
                        onTap: () async {
                          if (Platform.isIOS) {
                            await smsRequest(buildContext: context);
                            while (Navigator.canPop(context)) {
                              Navigator.pop(context);
                            }
                            // await verifyingBottomSheet();
                          } else if (Platform.isAndroid) {
                            await phoneRequest(context);
                            // await verifyingBottomSheet();
                          }
                        },
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                      child: CustomText(
                        Languages.of(context)!.noteWelcomeScreen,
                        style: Theme.of(context).textTheme.subtitle2!.copyWith(
                              color: ColorResource.color787878,
                            ),
                      ),
                      alignment: Alignment.center,
                    ),
                    const SizedBox(
                      height: 46,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  validateMobileNumberThroughMessage() async {
    List<String> people = [];
    people.addAll(['+919597304478']);
    String? _message;
    try {
      await sendSMS(
          message: "j123scc*jvjdjfbklsljofsdssmssdvjsd", recipients: people);
      verifyingNumberView();
    } catch (error) {
      setState(() => _message = error.toString());
    }
  }

  Future<void> phoneRequest(BuildContext buildContext) async {
    final PermissionStatus permissionStatus = await Permission.phone.request();
    if (permissionStatus.isGranted) {
      // await initMobileNumberState();
      await simDetection(buildContext);
    } else if (permissionStatus.isPermanentlyDenied) {
      await openAppSettings();
    }
  }

  Future<void> simDetection(BuildContext buildContext) async {
    try {
      final dynamic result =
          await platform.invokeMethod('activeSubscriptionInfoList');
      if (result.cast<String>().length > 0) {
        if (result.cast<String>().length == 1) {
          await smsRequest(
              selectedSimIndex: 0,
              buildContext: buildContext,
              selectedSimName: result.cast<String>()[0].toString());
        } else {
          bottomSheetToChooseMobileNumber(buildContext,
              simCarrierNameList: result.cast<String>());
        }
      }
    } on PlatformException catch (e) {
      throw e.message.toString();
    }
  }

  void bottomSheetToChooseMobileNumber(BuildContext buildContext,
      {List<String>? simCarrierNameList}) {
    showModalBottomSheet(
        enableDrag: false,
        isDismissible: false,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(24.0), topRight: Radius.circular(24.0)),
        ),
        context: buildContext,
        isScrollControlled: true,
        backgroundColor: ColorResource.colorFFFFFF,
        builder: (BuildContext context) {
          int? selectedIndex;
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
            return WillPopScope(
              onWillPop: () async => false,
              child: Container(
                padding: MediaQuery.of(context).viewInsets,
                child: Container(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      CustomText(
                        Languages.of(context)!.selectYourSim,
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            color: ColorResource.color222222,
                            // height: 1.33,
                            fontSize: FontSize.twentyFour,
                            fontFamily: Font.sourceSansProRegular.toString()),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      ListView.builder(
                          shrinkWrap: true,
                          itemCount: simCarrierNameList!.length,
                          itemBuilder: (BuildContext context, int index) {
                            return GestureDetector(
                              onTap: () {
                                setState(() {
                                  selectedIndex = index;
                                });
                              },
                              child: Container(
                                margin: const EdgeInsets.only(
                                    left: 5.0, right: 5.0, bottom: 24.0),
                                width: MediaQuery.of(context).size.height * .52,
                                // ignore: lines_longer_than_80_chars
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 15.0, vertical: 2.0),
                                height: 70.0,
                                decoration: BoxDecoration(
                                  color: ColorResource.colorf8f8fa
                                      .withOpacity(0.8),
                                  border: Border.all(
                                    color: ColorResource.colorFFFFFF
                                        .withOpacity(0.3),
                                  ),
                                  borderRadius: const BorderRadius.all(
                                    Radius.circular(8.0),
                                  ),
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        Container(
                                          alignment: Alignment.center,
                                          child: const Icon(
                                            Icons.sim_card,
                                            size: 33.0,
                                            color: ColorResource.color787878,
                                          ),
                                        ),
                                        const SizedBox(
                                          width: 15,
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            CustomText(
                                              '''${Languages.of(context)!.sim} ${index + 1}''',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .subtitle2!
                                                  .copyWith(
                                                    fontSize: FontSize.sixteen,
                                                    fontWeight: FontWeight.w600,
                                                    color: ColorResource
                                                        .color787878,
                                                  ),
                                            ),
                                            Visibility(
                                              visible: simCarrierNameList[index]
                                                      .toString()
                                                      .contains('SIM')
                                                  ? false
                                                  : true,
                                              child: CustomText(
                                                simCarrierNameList[index]
                                                    .toString(),
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .subtitle2!
                                                    .copyWith(
                                                      fontSize:
                                                          FontSize.sixteen,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      color: ColorResource
                                                          .color222222,
                                                    ),
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                    Container(
                                      child: selectedIndex == index
                                          ? const Padding(
                                              padding: EdgeInsets.all(5),
                                              child: Icon(
                                                  Icons.radio_button_checked,
                                                  color:
                                                      ColorResource.colorFF781F,
                                                  size: 32),
                                            )
                                          : Padding(
                                              padding: const EdgeInsets.all(5),
                                              child: Icon(
                                                Icons.radio_button_off_outlined,
                                                color: ColorResource.color222222
                                                    .withOpacity(0.3),
                                                size: 32,
                                              ),
                                            ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          }),
                      const SizedBox(
                        height: 10,
                      ),
                      GestureDetector(
                        child: CustomButton(
                          Languages.of(context)!.verify,
                          buttonBackgroundColor: selectedIndex != null
                              ? ColorResource.colorF58220
                              : ColorResource.colorF58220.withOpacity(0.5),
                          onTap: () async {
                            if (selectedIndex != null) {
                              await smsRequest(
                                  selectedSimIndex: selectedIndex,
                                  buildContext: buildContext,
                                  selectedSimName:
                                      simCarrierNameList[selectedIndex!]
                                          .toString());
                              /*  .then((value) async {
                                */ /*await new Future.delayed(
                                    const Duration(seconds: 3));
                                while (Navigator.canPop(context)) {
                                  Navigator.pop(context);
                                }*/ /* Navigator.pop(context);

                                Navigator.pushReplacementNamed(
                                    context, AppRoutes.otpVerificationScreen);
                              });*/
                            }
                          },
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      CustomText(
                        Languages.of(context)!.noteWelocomeScreen,
                        style: Theme.of(context).textTheme.bodyText2!.copyWith(
                              fontSize: FontSize.twelve,
                              color: ColorResource.color787878,
                            ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      CustomText(
                        Languages.of(context)!.exisitingUserRegisterMessage,
                        style: Theme.of(context).textTheme.bodyText2!.copyWith(
                              fontSize: FontSize.twelve,
                              color: ColorResource.color787878,
                            ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          });
        });
  }

  Future<void> smsRequest({
    BuildContext? buildContext,
    int? selectedSimIndex,
    String? selectedSimName,
  }) async {
    if (Platform.isIOS) {
      await showCupertinoDialog(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
          title: CustomText(
            Languages.of(buildContext!)!.smsSendDescription,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                color: ColorResource.color000000, fontSize: FontSize.sixteen),
          ),
          content: CustomText(
            Languages.of(buildContext)!.smsSendDescriptionNote,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.subtitle2!.copyWith(
                color: ColorResource.color787878, fontSize: FontSize.fourteen),
          ),
          actions: <Widget>[
            CupertinoDialogAction(
              child: Text(Languages.of(buildContext)!.dontAllow),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            CupertinoDialogAction(
              onPressed: () {
                Navigator.pop(context);
                // verifyingNumberView(simCardInformation: simCardInformation);
                validateMobileNumberThroughMessage();
              },
              child: Text(Languages.of(buildContext)!.ok),
            )
          ],
        ),
      );
    } else if (Platform.isAndroid) {
      final PermissionStatus permissionStatus = await Permission.sms.request();
      if (permissionStatus.isGranted) {
        Navigator.pop(context);
        await verifyingNumberView(
            selectedSimIndex: selectedSimIndex,
            selectedSimName: selectedSimName);
      } else if (permissionStatus.isPermanentlyDenied) {
        await openAppSettings();
      }
    }
  }

  Future<void> verifyingNumberView({
    int? selectedSimIndex,
    String? selectedSimName,
  }) async {
    if (Platform.isAndroid) {
      dynamic values;
      try {
        final dynamic result = await platform.invokeMethod('SMS', {
          'selectedSimSlotNumber': selectedSimIndex,
          'selectedSimSlotName': selectedSimName!,
        });
        if (result) {
          await verifyingBottomSheet();
        }
      } on PlatformException catch (e) {
        throw e.message.toString();
      }
    } else if (Platform.isIOS) {
      await verifyingBottomSheet();
    }
  }

  Future<void> verifyingBottomSheet() async {
    await showModalBottomSheet(
        isDismissible: false,
        enableDrag: false,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(24.0), topRight: Radius.circular(24.0)),
        ),
        context: context,
        isScrollControlled: true,
        backgroundColor: ColorResource.colorFFFFFF,
        builder: (BuildContext context) {
          Future.delayed(const Duration(milliseconds: 5000), () async {
            OTPVerificationModel model = OTPVerificationModel();
            Navigator.pop(context);
            await Navigator.pushNamed(context, AppRoutes.otpVerificationScreen,
                arguments: model);
          });
          return WillPopScope(
            onWillPop: () async => false,
            child: Container(
              padding: const EdgeInsets.all(24.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Center(
                    child: Image.asset(ImageResource.mobileVerificationLoading,
                        height: 192, width: 192),
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  CustomText(
                    Languages.of(context)!.numberVerificationString,
                    style: Theme.of(context).textTheme.headline5!.copyWith(
                          color: ColorResource.color222222,
                        ),
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  CustomText(
                    Languages.of(context)!.numberVerificationDescriptionString,
                    style: Theme.of(context).textTheme.subtitle1!.copyWith(
                          color: ColorResource.color787878,
                        ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
          );
        });
  }
}
