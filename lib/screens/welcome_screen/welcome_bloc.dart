import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:icici/base/base_state.dart';
import 'package:icici/screens/welcome_screen/welcome_event.dart';

class WelcomeBloc extends Bloc<WelcomeEvent, BaseState> {
  WelcomeBloc() : super(LoadingState());
  @override
  Stream<BaseState> mapEventToState(
    WelcomeEvent event,
  ) async* {
    // TODO: implement mapEventToState
  }
}
