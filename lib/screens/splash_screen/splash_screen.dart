import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:package_info_plus/package_info_plus.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  String version = "";

  @override
  void initState() {
    _initPackageInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Stack(
          children: [
            Center(
              child: SizedBox(
                width: 260,
                height: 113,
                child: Image.asset(
                  'assets/splash_logo.png',
                ),
              ),
            ),
            Column(
              children: [
                Spacer(),
                Padding(
                  padding: const EdgeInsets.only(bottom: 17.0),
                  child: Center(
                    child: CustomText(
                      'v$version',
                      style: Theme.of(context)
                          .textTheme
                          .subtitle1!
                          .copyWith(color: ColorResource.color787878),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Future<void> _initPackageInfo() async {
    final info = await PackageInfo.fromPlatform();
    setState(() {
      version = info.version;
    });
  }
}
