import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/router.dart';
import 'package:icici/screens/wallet_success_screen/bloc/wallet_create_success_bloc.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_button.dart';

import '../../router.dart';

class WalletSuccessScreen extends StatefulWidget {
  const WalletSuccessScreen({Key? key}) : super(key: key);

  @override
  _WalletSuccessScreenState createState() => _WalletSuccessScreenState();
}

class _WalletSuccessScreenState extends State<WalletSuccessScreen> {
  late WalletSuccessBloc walletSuccessBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    walletSuccessBloc = WalletSuccessBloc()..add(WalletSuccessInitialEvent());
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: Colors.transparent));
    return SafeArea(
      top: false,
      child: Scaffold(
        backgroundColor: ColorResource.colorFFFFFF,
        body: BlocListener<WalletSuccessBloc, WalletSuccessState>(
          bloc: walletSuccessBloc,
          listener: (BuildContext context, WalletSuccessState state) {
            if (state is NavigateDashboardScreenState) {
              Navigator.pushNamedAndRemoveUntil(
                  context, AppRoutes.dashboardScreen, (route) => false);
            }
          },
          child: BlocBuilder<WalletSuccessBloc, WalletSuccessState>(
            bloc: walletSuccessBloc,
            builder: (BuildContext context, WalletSuccessState state) {
              return Scaffold(
                body: Column(
                  children: <Widget>[
                    Container(
                        height: 300,
                        child: Column(
                          children: [
                            const SizedBox(
                              height: 50,
                            ),
                            CustomText(
                              Languages.of(context)!.congratulations,
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6!
                                  .copyWith(color: ColorResource.colorFFFFFF),
                              isSingleLine: true,
                            ),
                            const SizedBox(
                              height: 12,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 40),
                              child: CustomText(
                                Languages.of(context)!.noteWalletSuccess,
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle1!
                                    .copyWith(
                                        color: ColorResource.colorFFFFFF
                                            .withOpacity(0.8),
                                        height: 1.5),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ],
                        ),
                        decoration: const BoxDecoration(
                          image: DecorationImage(
                            fit: BoxFit.fill,
                            image: AssetImage(ImageResource.walletBg),
                          ),
                        ),),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Container(
                          alignment: Alignment.bottomCenter,
                          width: MediaQuery.of(context).size.width,
                          child: Column(
                            children: <Widget>[
                              walletdetails(
                                  ImageResource.walletUPI,
                                  Languages.of(context)!.walletUPIid,
                                  '6465987663@pockets'),
                              walletdetails(
                                  ImageResource.walletLimit,
                                  Languages.of(context)!.walletLimit,
                                  '₹ 10,000/₹1,00,000'),
                              walletdetails(
                                  ImageResource.visaCard,
                                  Languages.of(context)!.virtualVISAcard,
                                  'XXXX XXXX XXXX 7876'),
                              // const SizedBox(
                              //   height: 20,
                              // ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              );
            },
          ),
        ),
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.only(
              top: 26.0, left: 24.0, right: 24, bottom: 20),
          child: CustomButton(
            Languages.of(context)!.goToDashboard,
            onTap: () {
              walletSuccessBloc.add(NavigateDashboardScreenEvent());
            },
          ),
        ),
      ),
    );
  }

  Widget walletdetails(String imgString, String title, String value) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(24, 0, 24, 20),
      child: Card(
        elevation: 0,
        color: ColorResource.colorF8F8FA,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 6.0, vertical: 12.0),
          child: Row(
            children: [
              Card(
                elevation: 4,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)),
                child: Container(
                    height: 48,
                    width: 48,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: ColorResource.colorFFFFFF,
                    ),
                    child: Padding(
                        padding: const EdgeInsets.all(14.0),
                        child: Image.asset(imgString))),
              ),
              const SizedBox(width: 20),
              Flexible(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomText(
                      title,
                      style: Theme.of(context).textTheme.subtitle1!.copyWith(
                          color: ColorResource.color787878, height: 1.5),
                    ),
                    CustomText(
                      value,
                      style: Theme.of(context)
                          .textTheme
                          .headline5!
                          .copyWith(height: 1.7),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
