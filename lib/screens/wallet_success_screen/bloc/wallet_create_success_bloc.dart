import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:meta/meta.dart';

part 'wallet_create_success_event.dart';
part 'wallet_create_success_state.dart';

class WalletSuccessBloc extends Bloc<WalletSuccessEvent, WalletSuccessState> {
  WalletSuccessBloc() : super(WalletSuccessInitial());

  @override
  Stream<WalletSuccessState> mapEventToState(
    WalletSuccessEvent event,
  ) async* {
    if (event is NavigateDashboardScreenEvent) {
      yield NavigateDashboardScreenState();
    }
  }
}
