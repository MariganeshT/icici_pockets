part of 'wallet_create_success_bloc.dart';

@immutable
abstract class WalletSuccessEvent extends BaseEquatable {}

class WalletSuccessInitialEvent extends WalletSuccessEvent {}

class NavigateDashboardScreenEvent extends WalletSuccessEvent {}

