part of 'wallet_create_success_bloc.dart';

@immutable
abstract class WalletSuccessState {}

class WalletSuccessInitial extends WalletSuccessState {}

class WalletSuccessLoadingState extends WalletSuccessState {}

class WalletSuccessLoadedState extends WalletSuccessState {}

class NavigateDashboardScreenState extends WalletSuccessState {}

