import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/screens/settings_main_screen/bloc/settings_main_screen_bloc.dart';
import 'package:icici/screens/web_view_screen/bloc/webviewscreen_bloc.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewScreen extends StatefulWidget {
  WebViewScreen({
    Key? key,
  }) : super(key: key);

  @override
  _WebViewScreenState createState() => _WebViewScreenState();
}

class _WebViewScreenState extends State<WebViewScreen> {
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  late WebViewScreenBloc bloc;
  bool isLoading = true;

  @override
  void initState() {
    bloc = BlocProvider.of<WebViewScreenBloc>(context);
    //bloc.add(WebVeiwInitialEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          title: CustomText(
            bloc.webViewModel.pageTitle,
            style: Theme.of(context).textTheme.headline5,
            // fontWeight: FontWeight.w600,
            // color: ColorResource.color222222,
          ),
          centerTitle: true,
          backgroundColor: ColorResource.colorFFFFFF,
          elevation: 1.0,
          actions: [
            GestureDetector(
              onTap: () => Navigator.pop(context),
              child: Image.asset(ImageResource.close),
            ),
          ],
        ),
        body: BlocListener<WebViewScreenBloc, WebViewScreenState>(
          listener: (context, state) {},
          child: BlocBuilder<WebViewScreenBloc, WebViewScreenState>(
            bloc: bloc,
            builder: (context, state) {
              // if (state is WebViewLoadingState) {
              //   return const Center(
              //     child: CircularProgressIndicator(),
              //   );
              // } else if (state is WebViewScreenLoadedState) {
              //   return WebView(
              //     initialUrl: bloc.urlAddress,
              //     onWebViewCreated: (WebViewController webViewController) {
              //       Completer<WebViewController>().complete(webViewController);
              //     },
              //   );
              // } else {
              //   return Center(
              //     child: CircularProgressIndicator(
              //       color: ColorResource.color222222,
              //     ),
              //   );
              // }
              return Stack(
                children: [
                  WebView(
                    initialUrl: bloc.webViewModel.UrlAddress,
                    onWebViewCreated: (WebViewController webViewController) {
                      Completer<WebViewController>()
                          .complete(webViewController);
                    },
                    onPageFinished: (finish) {
                      setState(() {
                        isLoading = false;
                      });
                    },
                  ),
                  isLoading
                      ? Center(
                          child: CircularProgressIndicator(
                            color: ColorResource.color222222,
                          ),
                        )
                      : Stack(),
                ],
              );
            },
          ),
        ));
  }
}
