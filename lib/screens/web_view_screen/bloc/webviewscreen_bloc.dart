import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:icici/model/web_view_model.dart';
import 'package:meta/meta.dart';

part 'webviewscreen_event.dart';
part 'webviewscreen_state.dart';

class WebViewScreenBloc extends Bloc<WebViewScreenEvent, WebViewScreenState> {
  WebViewScreenBloc(this.webViewModel) : super(WebViewScreenInitial());

  WebViewModel webViewModel;

  @override
  Stream<WebViewScreenState> mapEventToState(
    WebViewScreenEvent event,
  ) async* {
    if (event is WebVeiwInitialEvent) {
      yield WebViewScreenLoadingState();
    } else {
      yield WebViewScreenLoadedState();
    }
  }
}
