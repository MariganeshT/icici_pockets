part of 'webviewscreen_bloc.dart';

@immutable
abstract class WebViewScreenState {}

class WebViewScreenInitial extends WebViewScreenState {}

class WebViewScreenLoadingState extends WebViewScreenState {}

class WebViewScreenLoadedState extends WebViewScreenState {}
