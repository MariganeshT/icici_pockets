part of 'login_bloc.dart';

@immutable
abstract class LoginEvent extends BaseEquatable {}

class LoginInitialEvent extends LoginEvent {}
