import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:icici/widgets/custom_button.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            SizedBox(
              width: 131,
              child: CustomButton(
                'Continue',
                onTap: () {},
              ),
            ),
          ],
        ),
      ),
    );
  }
}
