import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/screens/otp_verification_screen/otp_verification_screen.dart';
import 'package:icici/screens/returning_user_screen/bloc/returning_user_bloc.dart';
import 'package:icici/utils/app_utils.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/constants.dart';
import 'package:icici/utils/focus_node_disable.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/preference_helper.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/boimetric_authentication_alertbox.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/widget_utils.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import '../../router.dart';

class ReturningUserScreen extends StatefulWidget {
  const ReturningUserScreen({Key? key}) : super(key: key);

  @override
  _ReturningUserScreenState createState() => _ReturningUserScreenState();
}

class _ReturningUserScreenState extends State<ReturningUserScreen> {
  int attemptCount = 0;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController mpinTextEditingController =
      TextEditingController();

  late ReturningUserBloc bloc;
  bool showNumberKeyBoard = false;

  List<int> keyBoardNumber = [];

  @override
  void initState() {
    bloc = BlocProvider.of<ReturningUserBloc>(context);
    super.initState();
    keyBoardNumberList();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
          bottomNavigationBar: showNumberKeyBoard
              ? Container(
                  margin: const EdgeInsets.only(bottom: 10.0),
                  child: Wrap(
                    children: [
                      WidgetUtils.shuffleNumberKeyboardWidget(
                          context, mpinTextEditingController, keyBoardNumber)
                    ],
                  ),
                )
              : const SizedBox(),
          backgroundColor: Colors.white,
          body: BlocListener<ReturningUserBloc, ReturningUserState>(
            bloc: bloc,
            listener: (BuildContext context, ReturningUserState state) {
              if (state is DashBoardNavigationState) {
                while (Navigator.canPop(context)) {
                  Navigator.pop(context);
                }
                Navigator.pushReplacementNamed(
                    context, AppRoutes.dashboardScreen);
              }
            },
            child: BlocBuilder<ReturningUserBloc, ReturningUserState>(
              bloc: bloc,
              builder: (BuildContext context, ReturningUserState state) {
                return Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 40),
                        child: Image.asset(ImageResource.returninguser),
                      ),
                      const SizedBox(
                        height: 40,
                      ),
                      Center(
                        child: Form(
                          key: _formKey,
                          child: GestureDetector(
                            child: PinCodeTextField(
                              onTap: () {
                                setState(() {
                                  if (showNumberKeyBoard) {
                                    showNumberKeyBoard = false;
                                  } else {
                                    showNumberKeyBoard = true;
                                  }
                                });
                              },
                              appContext: context,
                              controller: mpinTextEditingController,
                              obscureText: true,
                              focusNode: AlwaysDisabledFocusNode(),
                              obscuringCharacter: '*',
                              length: 4,
                              textStyle: const TextStyle(
                                  color: ColorResource.color222222),
                              onChanged: (String value) {},
                              onCompleted: (String value) {
                                AppUtils.hideKeyBoard(context);
                                if (value.length == 4) {
                                  if (value == '1111') {
                                    while (Navigator.canPop(context)) {
                                      Navigator.pop(context);
                                    }
                                    Navigator.pushReplacementNamed(
                                        context, AppRoutes.dashboardScreen);
                                  } else {
                                    if (attemptCount == 3) {
                                      AppUtils.showErrorToast(
                                          'Your number has been blocked for 6 hours');
                                      Navigator.pushReplacementNamed(
                                          context, AppRoutes.welcomeScreen);
                                    } else {
                                      AppUtils.showToast(
                                          '\n${3 - attemptCount} attempts remaining\n');
                                    }

                                    if (attemptCount != 3) {
                                      attemptCount++;
                                    }
                                    setState(() {
                                      mpinTextEditingController.clear();
                                      keyBoardNumberList();
                                    });
                                  }
                                }
                              },
                              animationType: AnimationType.scale,
                              animationDuration:
                                  const Duration(milliseconds: 300),
                              keyboardType: TextInputType.number,
                              mainAxisAlignment: MainAxisAlignment.center,
                              pinTheme: PinTheme(
                                fieldOuterPadding: const EdgeInsets.all(8),
                                shape: PinCodeFieldShape.circle,
                                borderRadius: BorderRadius.circular(12.5),
                                activeColor: ColorResource.color222222,
                                inactiveColor: ColorResource.color222222,
                                selectedColor: ColorResource.color222222,
                                borderWidth: 1,
                                fieldHeight: 24,
                                fieldWidth: 24,
                              ),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 35,
                      ),
                      GestureDetector(
                        onTap: () {
                          OTPVerificationModel model =
                              OTPVerificationModel(isForgotMpinFlow: true);
                          Navigator.pushNamed(
                              context, AppRoutes.otpVerificationScreen,
                              arguments: model);
                        },
                        child: CustomText(
                          StringResource.forgotyourPinCode,
                          style: Theme.of(context)
                              .textTheme
                              .subtitle1!
                              .copyWith(color: ColorResource.colorF58220),
                        ),
                      ),
                      const SizedBox(
                        height: 80,
                      ),
                      GestureDetector(
                          onTap: () async {
                            final bool bioMetricValue =
                                await PreferenceHelper.getBioMetricValue();
                            if (bioMetricValue) {
                              bloc.add(BioAuthEvent());
                            } else {
                              await showDialog(
                                  context: context,
                                  barrierDismissible: false,
                                  builder: (BuildContext context) {
                                    return FingerprintAlertDialogue(() async {
                                      PreferenceHelper.setBioMetricValue(true);
                                      Navigator.pop(context);
                                      bloc.add(BioAuthEvent());
                                    }, () {
                                      PreferenceHelper.setBioMetricValue(false);
                                      Navigator.pop(context);
                                    });
                                  });
                            }
                          },
                          child: Image.asset(ImageResource.facelock)),
                    ],
                  ),
                );
              },
            ),
          )),
    );
  }

  void keyBoardNumberList() {
    setState(() {
      keyBoardNumber.clear();
      keyBoardNumber = Constants.keyBoardNumberList();
    });
  }
}
