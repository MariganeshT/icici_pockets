import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/services.dart';
import 'package:icici/utils/app_utils.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:local_auth/error_codes.dart' as auth_error;
import 'package:local_auth/local_auth.dart';
import 'package:meta/meta.dart';

part 'returning_user_event.dart';
part 'returning_user_state.dart';

class ReturningUserBloc extends Bloc<ReturningUserEvent, ReturningUserState> {
  ReturningUserBloc() : super(ReturningUserInitial());

  @override
  Stream<ReturningUserState> mapEventToState(
    ReturningUserEvent event,
  ) async* {
    if (event is BioAuthEvent) {
      final LocalAuthentication localAuth = LocalAuthentication();
      final List<BiometricType> availableBiometrics =
          await localAuth.getAvailableBiometrics();
      try {
        final bool didAuthenticate = await localAuth.authenticate(
          localizedReason: 'Log In to your account instantly!',
          biometricOnly: true,
        );
        if (didAuthenticate) {
          // ignore: avoid_print
          yield DashBoardNavigationState();
        } else {
          // ignore: avoid_print
          // AppUtils.showErrorToast('Unauthenticated');
        }
      } on PlatformException catch (e) {
        if (e.code == auth_error.notAvailable) {
          // Handle this exception here.
          AppUtils.showErrorToast('Permission Denied');
        }
      }
    }
  }
}
