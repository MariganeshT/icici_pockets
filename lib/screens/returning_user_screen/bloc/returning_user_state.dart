part of 'returning_user_bloc.dart';

@immutable
abstract class ReturningUserState {}

class ReturningUserInitial extends ReturningUserState {}

class DashBoardNavigationState extends ReturningUserState {}

