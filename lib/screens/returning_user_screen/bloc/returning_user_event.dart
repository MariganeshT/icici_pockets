part of 'returning_user_bloc.dart';

@immutable
abstract class ReturningUserEvent extends BaseEquatable {}

class ReturningUserInitialEvent extends ReturningUserEvent {}

class BioAuthEvent extends ReturningUserEvent {}
