part of 'order_status_bloc.dart';

@immutable
class OrderStatusEvent extends BaseEquatable {}

class OrderStatusInitialEvent extends OrderStatusEvent {}
