part of 'order_status_bloc.dart';

@immutable
class OrderStatusState extends BaseEquatable {}

class OrderStatusInitial extends OrderStatusState {}
