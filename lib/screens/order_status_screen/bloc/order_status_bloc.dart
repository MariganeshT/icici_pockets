import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:meta/meta.dart';

part 'order_status_event.dart';
part 'order_status_state.dart';

class OrderStatusBloc extends Bloc<OrderStatusEvent, OrderStatusState> {
  OrderStatusBloc() : super(OrderStatusInitial());

  @override
  Stream<OrderStatusState> mapEventToState(
    OrderStatusEvent event,
  ) async* {
    // TODO: implement mapEventToState
  }
}
