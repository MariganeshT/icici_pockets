part of 'verify_identity_bloc.dart';

@immutable
abstract class VerifyIdentityState {}

class VerifyIdentityInitial extends VerifyIdentityState {}
