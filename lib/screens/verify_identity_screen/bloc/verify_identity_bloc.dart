import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:meta/meta.dart';

part 'verify_identity_event.dart';
part 'verify_identity_state.dart';

class VerifyIdentityBloc
    extends Bloc<VerifyIdentityEvent, VerifyIdentityState> {
  VerifyIdentityBloc() : super(VerifyIdentityInitial());

  @override
  Stream<VerifyIdentityState> mapEventToState(
    VerifyIdentityEvent event,
  ) async* {
    // TODO: implement mapEventToState
  }
}
