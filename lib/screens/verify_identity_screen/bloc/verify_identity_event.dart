part of 'verify_identity_bloc.dart';

@immutable
abstract class VerifyIdentityEvent extends BaseEquatable {}

class VerifyIdentityInitialEvent extends VerifyIdentityEvent {}
