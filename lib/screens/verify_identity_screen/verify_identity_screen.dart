import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:icici/utils/app_utils.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/custom_radio_edit_view.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:icici/widgets/custom_button.dart';
import '../../router.dart';
import 'bloc/verify_identity_bloc.dart';

class VerifyIdentityScreen extends StatefulWidget {
  // final MyInAppBrowser browser = MyInAppBrowser();

  @override
  _VerifyIdentityScreenState createState() => _VerifyIdentityScreenState();
}

class _VerifyIdentityScreenState extends State<VerifyIdentityScreen> {
  // InAppBrowserClassOptions options = InAppBrowserClassOptions(
  //     crossPlatform: InAppBrowserOptions(hideUrlBar: true),
  //     inAppWebViewGroupOptions:
  //         InAppWebViewGroupOptions(crossPlatform: InAppWebViewOptions()));

  final List<TextEditingController> textEditControllers = [
    TextEditingController(),
    TextEditingController(),
    TextEditingController(),
    TextEditingController(),
    TextEditingController()
  ];
  final List<FocusNode> focusNodes = [
    FocusNode(),
    FocusNode(),
    FocusNode(),
    FocusNode(),
    FocusNode(),
  ];

  bool accept = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    return SafeArea(
      top: false,
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: ColorResource.colorFAFAFA,
        body: BlocListener<VerifyIdentityBloc, VerifyIdentityState>(
          listener: (BuildContext context, VerifyIdentityState state) {},
          child: BlocBuilder<VerifyIdentityBloc, VerifyIdentityState>(
              builder: (BuildContext context, VerifyIdentityState state) {
            return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      Container(
                        width: double.infinity,
                        child: Image.asset(
                          ImageResource.verifyIdentity,
                          fit: BoxFit.contain,
                        ),
                        decoration: BoxDecoration(
                          color: ColorResource.color641653,
                          borderRadius: BorderRadius.vertical(
                              bottom: Radius.elliptical(
                                  MediaQuery.of(context).size.width, 31.0)),
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          CustomAppbar(
                            titleString: '',
                            iconEnumValues: IconEnum.back,
                            indicatorIndex: 2,
                            isAuthentication: true,
                            onItemSelected: (selectedItem) {
                              Navigator.pop(context);
                            },
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(bottom: 10, left: 12),
                            child: CustomText(StringResource.verifyYourIdentity,
                                style: Theme.of(context).textTheme.headline6),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 12),
                            child: SizedBox(
                              width: 287,
                              child: CustomText(
                                  StringResource.toVerifyYourIdentity,
                                  style: Theme.of(context)
                                      .textTheme
                                      .subtitle1!
                                      .copyWith(
                                          color: ColorResource.colorFFFFFF)),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Expanded(
                      child: Padding(
                          padding:
                              const EdgeInsets.only(left: 20.0, right: 20.0),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Expanded(
                                    child: CustomRadioEditTextField(
                                        textEditControllers, focusNodes,
                                        padding: 3,
                                        absoluteZeroSpacing: true,
                                        buttonLables: [
                                          StringResource.aadharNumber,
                                          StringResource.passport,
                                          StringResource.voterID,
                                          StringResource.drivingLicense,
                                          StringResource.nRegaJobCard,
                                        ],
                                        buttonValues: [
                                          StringResource.aadharNumber,
                                          StringResource.passport,
                                          StringResource.voterID,
                                          StringResource.drivingLicense,
                                          StringResource.nRegaJobCard,
                                        ],
                                        buttonIcons: [],
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1!
                                            .copyWith(
                                                color:
                                                    ColorResource.color203718)))
                              ])))
                ]);
          }),
        ),
        bottomNavigationBar: Padding(
          padding:
              const EdgeInsets.only(top: 8.0, left: 24, right: 24, bottom: 20),
          child: CustomButton(
            StringResource.done,
            onTap: () {
              var selectedText;
              textEditControllers.forEach((TextEditingController element) {
                if (element.text.isNotEmpty) {
                  selectedText = element.text;
                }
              });
              if (selectedText != null) {
                Navigator.pushReplacementNamed(context, AppRoutes.setPinScreen);
              } else {
                AppUtils.showErrorToast(StringResource.empty);
              }
            },
          ),
        ),
      ),
    );
  }
}

class MyInAppBrowser extends InAppBrowser {
  @override
  // ignore: always_specify_types
  Future onBrowserCreated() async {
    // ignore: avoid_print
    print('Browser Created!');
  }

  @override
  // ignore: always_specify_types
  Future onLoadStart(Uri? url) async {
    // ignore: avoid_print
    print('Started $url');
  }

  @override
  // ignore: always_specify_types
  Future onLoadStop(Uri? url) async {
    // ignore: avoid_print
    print('Stopped $url');
  }

  @override
  void onLoadError(Uri? url, int code, String message) {
    // ignore: avoid_print
    print("Can't load $url.. Error: $message");
  }

  @override
  void onProgressChanged(int progress) {
    // ignore: avoid_print
    print('Progress: $progress');
  }

  @override
  void onExit() {
    // ignore: avoid_print
    print('Browser closed!');
  }
}
