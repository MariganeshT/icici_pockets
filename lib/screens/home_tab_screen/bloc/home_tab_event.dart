part of 'home_tab_bloc.dart';

@immutable
abstract class HomeTabEvent extends BaseEquatable {}

class HomeTabInitialEvent extends HomeTabEvent{}
