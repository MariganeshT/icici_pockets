part of 'home_tab_bloc.dart';

@immutable
abstract class HomeTabState extends BaseEquatable {}

class HomeTabInitial extends HomeTabState {}
