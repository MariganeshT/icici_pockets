part of 'set_mpin_bloc.dart';

@immutable
abstract class SetMpinState {}

class SetMpinInitial extends SetMpinState {}

class SetMpinLoadingState extends SetMpinState {}

class SetMpinLoadedState extends SetMpinState {}

class NavigateTransactionLimitSuccessState extends SetMpinState {}

class NavigateWalletClosureBottomsheetState extends SetMpinState {}
