import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:meta/meta.dart';

part 'set_mpin_event.dart';
part 'set_mpin_state.dart';

class SetMpinBloc extends Bloc<SetMpinEvent, SetMpinState> {
  SetMpinBloc() : super(SetMpinInitial());

  @override
  Stream<SetMpinState> mapEventToState(
    SetMpinEvent event,
  ) async* {
    if (event is NavigateTransactionLimitSuccessEvent) {
      yield NavigateTransactionLimitSuccessState();
    }
    if (event is NavigateWalletClosureBottomsheetEvent) {
      yield NavigateWalletClosureBottomsheetState();
    }
  }
}
