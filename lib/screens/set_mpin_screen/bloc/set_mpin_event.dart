part of 'set_mpin_bloc.dart';

@immutable
abstract class SetMpinEvent extends BaseEquatable{}

class SetMpinInitialEvent extends SetMpinEvent{}

class NavigateTransactionLimitSuccessEvent extends SetMpinEvent{}

class NavigateWalletClosureBottomsheetEvent extends SetMpinEvent{}
