import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/screens/set_mpin_screen/bloc/set_mpin_bloc.dart';
import 'package:icici/screens/wallet_closure_screen/request_accept.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import '../set_transaction_limit_screen/limit_success.dart';
import 'bloc/set_mpin_bloc.dart';

class SetMpinScreen extends StatefulWidget {
  final String? navigateScreen;
  const SetMpinScreen({Key? key, this.navigateScreen}) : super(key: key);

  @override
  _SetMpinScreenState createState() => _SetMpinScreenState();
}

class _SetMpinScreenState extends State<SetMpinScreen> {
  TextEditingController mpinController = TextEditingController();
  late SetMpinBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<SetMpinBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ColorResource.colorFAFAFA,
        body: BlocListener<SetMpinBloc, SetMpinState>(
          bloc: bloc,
          listener: (BuildContext context, SetMpinState state) {
            if (state is NavigateTransactionLimitSuccessState) {
              limitSuccessBottomSheet();
            }
            if (state is NavigateWalletClosureBottomsheetState) {
              walletClosureBottomSheet();
            }
          },
          child: BlocBuilder<SetMpinBloc, SetMpinState>(
            builder: (BuildContext context, SetMpinState state) {
              return Column(
                children: <Widget>[
                  CustomAppbar(
                    backgroundColor: ColorResource.color641653,
                    titleString: StringResource.pleaseEnterMPIN,
                    titleSpacing: 8,
                    style: Theme.of(context)
                        .textTheme
                        .headline5!
                        .copyWith(color: ColorResource.colorFFFFFF),
                    iconEnumValues: IconEnum.close,
                    onItemSelected: (selectedItem) {
                      Navigator.pop(context);
                    },
                  ),
                  Column(
                    children: <Widget>[
                      Padding(
                        padding:
                            const EdgeInsets.only(top: 22, left: 42, right: 42),
                        child: CustomText(
                          StringResource.fourDigitPin,
                          style: Theme.of(context)
                              .textTheme
                              .subtitle1!
                              .copyWith(
                                  color: ColorResource.color787878,
                                  fontSize: 16),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      SizedBox(
                        height: 68,
                      ),
                      CustomText(
                        StringResource.mpin,
                        style: Theme.of(context).textTheme.subtitle1!.copyWith(
                            color: ColorResource.color222222, fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      PinCodeTextField(

                        enablePinAutofill: false,
                        appContext: context,
                        onChanged: (String value) {},
                        autoFocus: true,
                        onCompleted: (String enteredText) {
                          if (widget.navigateScreen == 'WalletBottomSheet') {
                            bloc.add(
                              NavigateWalletClosureBottomsheetEvent(),
                            );
                          } else if (widget.navigateScreen ==
                              'TransactionSuccessBottomSheet') {
                            bloc.add(NavigateTransactionLimitSuccessEvent());
                          }
                        },
                        obscureText: true,
                        length: 4,
                        textStyle: Theme.of(context)
                            .textTheme
                            .headline5!
                            .copyWith(color: ColorResource.color222222),
                        animationType: AnimationType.scale,
                        animationDuration: const Duration(milliseconds: 300),
                        keyboardType: TextInputType.number,
                        controller: mpinController,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        pinTheme: PinTheme(

                          shape: PinCodeFieldShape.box,
                          borderWidth: 1,
                          borderRadius: BorderRadius.circular(8),
                          inactiveColor: ColorResource.color222222,
                          selectedColor: ColorResource.color222222,
                          activeColor: ColorResource.color222222,
                          fieldHeight: 56,
                          fieldWidth: 48,
                        ),
                      ),
                    ],
                  )
                ],
              );
            },
          ),
        ));
  }

  void limitSuccessBottomSheet() {
    showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        context: context,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(32.0), topRight: Radius.circular(32.0)),
        ),
        backgroundColor: ColorResource.colorFFFFFF,
        builder: (BuildContext context) {
          return const LimitSuccess();
        });
  }

  void walletClosureBottomSheet() {
    showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        context: context,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(32.0), topRight: Radius.circular(32.0)),
        ),
        backgroundColor: ColorResource.colorFFFFFF,
        builder: (BuildContext context) {
          return const RequestAcceptBottomSheet();
        });
  }
}
