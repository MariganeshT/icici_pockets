import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/model/address_model.dart';
import 'package:icici/model/allthemes_models.dart';
import 'package:icici/model/choose_themes_models.dart';
import 'package:icici/screens/profile_screen/add_address.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:icici/widgets/custom_button.dart';

import '../../router.dart';
import 'bloc/expression_pockets_bloc.dart';

class ExpressionPockets extends StatefulWidget {
  const ExpressionPockets({Key? key}) : super(key: key);

  @override
  _ExpressionPocketsState createState() => _ExpressionPocketsState();
}

class _ExpressionPocketsState extends State<ExpressionPockets> {
  // ExpressionPocketsBloc? bloc;
  bool isLocationFound = true;
  AddressModel? address;
  String noAddress = 'No address';
  bool isAddressSaved = false;
  bool selectTheme = false;
  String selectedTheme = '';
  String selectedImage = ImageResource.choosethemeone;

  List<ChooseThemes> allChooseThemes = [
    ChooseThemes(
      backgroundColor: ColorResource.color641653,
      image: ImageResource.choosethemeone,
      isSelected: false,
    ),
    ChooseThemes(
      backgroundColor: ColorResource.colorF92538,
      image: ImageResource.choosethemetwo,
      isSelected: false,
    ),
    ChooseThemes(
      backgroundColor: ColorResource.color641653,
      image: ImageResource.choosethemeone,
      isSelected: false,
    ),
    ChooseThemes(
      backgroundColor: ColorResource.colorf92538,
      image: ImageResource.choosethemetwo,
      isSelected: false,
    )
  ];
  late ExpressionPocketsBloc bloc;

  List<AllThemes> allThemeList = [
    AllThemes(isSelected: true, title: 'All Theme'),
    AllThemes(isSelected: false, title: 'Abstract + Gradient'),
    AllThemes(isSelected: false, title: 'Adventure'),
    AllThemes(isSelected: false, title: 'Fashion'),
    AllThemes(isSelected: false, title: 'Foodie'),
    AllThemes(isSelected: false, title: 'Gaming'),
    AllThemes(isSelected: false, title: 'Matt Look'),
    AllThemes(isSelected: false, title: 'Movie Lover'),
    AllThemes(isSelected: false, title: 'Nature'),
    AllThemes(isSelected: false, title: 'Shopping'),
    AllThemes(isSelected: false, title: 'Social Media'),
    AllThemes(isSelected: false, title: 'Sports'),
    AllThemes(isSelected: false, title: 'Sports Lover'),
    AllThemes(isSelected: false, title: 'Super hero'),
    AllThemes(isSelected: false, title: 'Tech'),
    AllThemes(isSelected: false, title: 'Travel'),
    AllThemes(isSelected: false, title: 'Wild'),
  ];

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<ExpressionPocketsBloc>(context);
    selectedTheme = StringResource.alltheme;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ColorResource.color641653,
        body: BlocListener<ExpressionPocketsBloc, ExpressionPocketsState>(
            listener: (BuildContext context, ExpressionPocketsState state) {},
            child: BlocBuilder<ExpressionPocketsBloc, ExpressionPocketsState>(
                builder: (BuildContext context, ExpressionPocketsState state) {
              if (state is ExpressionPocketsLoadingState) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
              return Column(children: <Widget>[
                CustomAppbar(
                  titleString: Languages.of(context)!.expressionCard,
                  titleSpacing: 8,
                  style: Theme.of(context)
                      .textTheme
                      .headline5!
                      .copyWith(color: ColorResource.colorFFFFFF, fontSize: 24),
                  iconEnumValues: IconEnum.back,
                  onItemSelected: (selectedItem) {
                    Navigator.pop(context);
                  },
                ),
                Container(
                  // width: 246,
                  // height: 289,
                  decoration: BoxDecoration(
                      // borderRadius: BorderRadius.circular(28),
                      // border: Border.all(
                      //   color: ColorResource.colorFFFFFF,
                      // ),
                      image: DecorationImage(
                    image: AssetImage(
                      selectedImage,
                    ),
                    fit: BoxFit.fill,
                  )),
                  child: Container(
                    padding: const EdgeInsets.all(40),
                    alignment: Alignment.topLeft,
                    width: 246,
                    height: 289,
                    child: CustomText(
                      Languages.of(context)!.expressionPocketCard,
                      style: Theme.of(context).textTheme.headline5!.copyWith(
                          color: ColorResource.colorFFFFFF, height: 1.5),
                    ),
                  ),
                ),
                Expanded(
                    flex: 6,
                    child: SizedBox(
                        // height: 600,
                        // width: double.infinity,
                        width: double.infinity,
                        // width: 411,
                        child: Container(
                          decoration: BoxDecoration(
                            color: ColorResource.colorFFFFFF.withOpacity(0.95),
                            borderRadius: const BorderRadius.only(
                              topLeft: Radius.circular(20),
                              topRight: Radius.circular(20),
                            ),
                          ),
                          child: Column(
                              // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 95),
                                        child: CustomText(
                                          Languages.of(context)!.chooseTheme,
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1!
                                              .copyWith(
                                                color:
                                                    ColorResource.color000000,
                                              ),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                      const SizedBox(
                                        width: 20,
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          selectAddressExpressionBottomSheet();
                                        },
                                        child: CustomText(
                                          Languages.of(context)!.skip,
                                          style: Theme.of(context)
                                              .textTheme
                                              .subtitle1!
                                              .copyWith(
                                                  color: ColorResource
                                                      .colorFF781F),
                                          // textAlign: TextAlign.end,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                // ]),
                                const SizedBox(
                                  height: 20,
                                ),
                                Container(
                                  margin: const EdgeInsets.symmetric(
                                      horizontal: 10),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      CustomText(
                                        Languages.of(context)!.selectTheme,
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle2!
                                            .copyWith(
                                                color:
                                                    ColorResource.color787878),
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          selectThemeBottomSheet();
                                        },
                                        child: Row(
                                          children: [
                                            CustomText(
                                              selectedTheme,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .subtitle1!
                                                  .copyWith(
                                                    color: ColorResource
                                                        .color222222,
                                                    fontWeight: FontWeight.w700,
                                                  ),
                                              maxLines: 2,
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Image.asset(ImageResource.dropdown),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Expanded(
                                    flex: 4,
                                    child: ListView.builder(
                                        scrollDirection: Axis.horizontal,
                                        itemCount: allChooseThemes.length,
                                        itemBuilder: (context, index) {
                                          return GestureDetector(
                                              child: Stack(
                                                alignment: Alignment.center,
                                                children: [
                                                  Container(
                                                    child: Image(
                                                        image: AssetImage(
                                                            allChooseThemes[
                                                                    index]
                                                                .image!)),
                                                  ),
                                                  if (allChooseThemes[index]
                                                      .isSelected!)
                                                    Container(
                                                      decoration:
                                                          const BoxDecoration(
                                                              shape: BoxShape
                                                                  .circle,
                                                              color: ColorResource
                                                                  .color10CB00),
                                                      child: const Icon(
                                                        Icons.done,
                                                        color: ColorResource
                                                            .colorFFFFFF,
                                                      ),
                                                      width: 40,
                                                      height: 40,
                                                    )
                                                ],
                                              ),
                                              onTap: () {
                                                setState(() {
                                                  allChooseThemes
                                                      .forEach((element) {
                                                    element.isSelected = false;
                                                  });
                                                  allChooseThemes[index]
                                                      .isSelected = true;
                                                  selectedImage =
                                                      allChooseThemes[index]
                                                          .image!;
                                                  selectTheme = true;
                                                });
                                              });
                                        })),
                                Flexible(
                                  child: Visibility(
                                    visible: selectTheme,
                                    child: Container(
                                      decoration: const BoxDecoration(
                                        color: ColorResource.colorFFFFFF,
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(20),
                                            topRight: Radius.circular(20)),
                                      ),
                                      width: 411,
                                      height: 56,
                                      child: Center(
                                        child: GestureDetector(
                                            onTap: () {
                                              selectAddressExpressionBottomSheet();
                                            },
                                            child: CustomText(
                                              StringResource.proceed,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .subtitle1!
                                                  .copyWith(
                                                      color: ColorResource
                                                          .colorFF781F),
                                            )),
                                      ),
                                    ),
                                  ),
                                ),
                              ]),
                        ))),
              ]);
            })));
  }

  selectThemeBottomSheet() {
    showModalBottomSheet(
        isDismissible: false,
        isScrollControlled: true,
        backgroundColor: ColorResource.colorFFFFFF,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
          top: Radius.circular(32),
        )),
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(
              builder: (BuildContext buildContext, StateSetter setState) {
            return Container(
              margin: EdgeInsets.only(bottom: 40),
              height: 471,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    //  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 20, top: 15),
                        child: GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Image.asset(ImageResource.close)),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 80, top: 17),
                        child: CustomText(
                          StringResource.selectthemes,
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(color: ColorResource.color222222),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  Expanded(
                    flex: 1,
                    child: Scrollbar(
                      isAlwaysShown: true,
                      child: ListView.builder(
                          padding: EdgeInsets.symmetric(horizontal: 24),
                          itemCount: allThemeList.length,
                          itemBuilder: (context, index) {
                            return GestureDetector(
                              onTap: () {
                                setState(() {
                                  allThemeList.forEach((element) {
                                    if (allThemeList[index].title! ==
                                        element.title) {
                                      element.isSelected = true;
                                      setUpdate(element.title!);
                                    } else {
                                      element.isSelected = false;
                                    }
                                  });
                                  Navigator.pop(context);
                                });
                              },
                              child: Container(
                                color: Colors.transparent,
                                margin: EdgeInsets.symmetric(vertical: 16),
                                child: Row(
                                  children: [
                                    CustomText(allThemeList[index].title!,
                                        style: allThemeList[index].isSelected!
                                            ? Theme.of(context)
                                                .textTheme
                                                .bodyText1!
                                                .copyWith(
                                                    color: ColorResource
                                                        .color222222)
                                            : Theme.of(context)
                                                .textTheme
                                                .bodyText1!
                                                .copyWith(
                                                    color: ColorResource
                                                        .color787878)),
                                    Visibility(
                                      visible: allThemeList[index].isSelected!,
                                      child: Row(
                                        children: [
                                          const SizedBox(
                                            width: 8,
                                          ),
                                          Image.asset(ImageResource.tick),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            );
                          }),
                    ),
                  )
                ],
              ),
            );
          });
        });
  }

  setUpdate(String values) {
    setState(() {
      selectedTheme = values;
    });
  }

  selectAddressExpressionBottomSheet() {
    // DraggableScrollableSheet(
    showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        context: context,
        backgroundColor: ColorResource.colorFFFFFF,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(32),
          ),
        ),
        builder: (BuildContext context) {
          return SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding:
                      const EdgeInsets.only(top: 16.0, bottom: 24, left: 16),
                  child: Row(
                    children: <Widget>[
                      GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Image.asset(ImageResource.close)),
                      const SizedBox(
                        width: 95,
                      ),
                      CustomText(Languages.of(context)!.selectAddress,
                          style:
                              Theme.of(context).textTheme.headline5!.copyWith(
                                    color: ColorResource.color222222,
                                  )),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      top: 16.0, bottom: 27, left: 24, right: 24),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Image.asset(ImageResource.delivered),
                          const SizedBox(width: 8),
                          CustomText(Languages.of(context)!.deliverTo,
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle1!
                                  .copyWith(color: ColorResource.color787878)),
                        ],
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                          addressBottomSheet();
                        },
                        child: CustomText(bloc.address != null
                            ? Languages.of(context)!.editAddress
                            : 'Add Address',
                            style:
                                Theme.of(context).textTheme.subtitle1!.copyWith(
                                      color: ColorResource.colorF58220,
                                      decoration: TextDecoration.underline,
                                    )),
                      ),
                    ],
                  ),
                ),
                if (bloc.address != null)
                Padding(
                  padding:
                      const EdgeInsets.only(bottom: 10.0, left: 24, right: 24),
                  child: CustomText(bloc.address!.name!,
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            color: ColorResource.color222222,
                          )),
                ),
                if (bloc.address != null)
                Padding(
                  padding:
                      const EdgeInsets.only(bottom: 10.0, left: 24, right: 24),
                  child: CustomText(
                      '${bloc.address?.houseNumber}, ${bloc.address?.address1}, ${bloc.address?.address2}',
                      style: Theme.of(context).textTheme.subtitle1!.copyWith(
                            color: ColorResource.color222222,
                          )),
                ),
                if (bloc.address != null)
                Padding(
                  padding: const EdgeInsets.only(
                      top: 24.0, left: 24, right: 24, bottom: 2),
                  child: CustomButton(
                    Languages.of(context)!.continuee,
                    onTap: () {
                      // isLocationFound
                      //     ? changeLocationBottomSheet()
                      //     :
                      Navigator.pushNamed(
                          context, AppRoutes.placeOrderExpressionScreen);
                    },
                  ),
                ),
              ],
            ),
          );
        });
  }

  changeLocationBottomSheet() {
    showModalBottomSheet(
      isScrollControlled: true,
      isDismissible: false,
      context: context,
      backgroundColor: ColorResource.colorFFFFFF,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(32),
        ),
      ),
      builder: (BuildContext context) {
        return SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 13.0, left: 16),
                child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Image.asset(ImageResource.close)),
              ),
              const Center(
                child: Image(image: AssetImage(ImageResource.location)),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 14.0, left: 24, right: 24),
                child: CustomText(Languages.of(context)!.weDontDeliverHere,
                    style: Theme.of(context).textTheme.headline5!.copyWith(
                          color: ColorResource.color222222,
                        )),
              ),
              Container(
                margin:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 24),
                child: CustomText(
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                    style: Theme.of(context).textTheme.subtitle1!.copyWith(
                          color: ColorResource.color222222.withOpacity(0.5),
                        )),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 24.0, left: 24, right: 24, bottom: 24),
                child: CustomButton(
                  Languages.of(context)!.changeLocation,
                  onTap: () {
                    addressBottomSheet();
                  },
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  addressBottomSheet() async {
    await showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        backgroundColor: ColorResource.colorFFFFFF,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(32),
          ),
        ),
        context: context,
        // DraggableScrollableSheet(
        //     initialChildSize: 0.35,
        //     maxChildSize: 0.6,
        //     minChildSize: 0.35,
        builder: (BuildContext context) {
          return AddAddress(
            bloc.noAddress,
            bloc.address,
            bloc.isAddressSaved,
            (AddressModel updatedAddress) {
              bloc.isAddressSaved = true;
              setState(() {
                bloc.address = updatedAddress;
              });

              bloc.add(ExpressionPocketsInitialEvent());
            },
            bloc.findScreen,
          );
        });
  }

  // proceedBottomSheet() {
  //   showModalBottomSheet(
  //       isDismissible: true,
  //       isScrollControlled: true,
  //       backgroundColor: ColorResource.colorFFFFFF,
  //       shape: const RoundedRectangleBorder(
  //           borderRadius: BorderRadius.vertical(
  //         top: Radius.circular(32),
  //       )),
  //       context: context,
  //       builder: (BuildContext context) {
  //         return Container(
  //           width: 411,
  //           height: 56,
  //           child: Center(
  //             child: GestureDetector(
  //                 onTap: () {
  //                   selectAddressExpressionBottomSheet();
  //                 },
  //                 child: CustomText(
  //                   StringResource.proceed,
  //                   style: Theme.of(context)
  //                       .textTheme
  //                       .subtitle1!
  //                       .copyWith(color: ColorResource.colorFF781F),
  //                 )),
  //           ),
  //         );
  //       });
  // }
}
