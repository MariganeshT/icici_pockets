part of 'expression_pockets_bloc.dart';

@immutable
abstract class ExpressionPocketsState extends BaseEquatable{}

class ExpressionPocketsInitial extends ExpressionPocketsState {}

class ExpressionPocketsLoadingState extends ExpressionPocketsState {}

class ExpressionPocketsLoadedState extends ExpressionPocketsState {}



