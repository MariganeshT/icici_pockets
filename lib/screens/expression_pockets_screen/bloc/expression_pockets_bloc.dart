import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:icici/model/address_model.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:meta/meta.dart';

part 'expression_pockets_event.dart';
part 'expression_pockets_state.dart';

class ExpressionPocketsBloc
    extends Bloc<ExpressionPocketsEvent, ExpressionPocketsState> {
  ExpressionPocketsBloc() : super(ExpressionPocketsInitial());
  AddressModel? address = AddressModel(
      '632004', '2486, 19th Cross', '7th Main Rd', 'Sector-01, HSR Layout',
      name: 'Prince Anto');
  String noAddress = 'No address';
  bool isAddressSaved = false;
  String findScreen = 'expressionPocketsScreen';
  @override
  Stream<ExpressionPocketsState> mapEventToState(
    ExpressionPocketsEvent event,
  ) async* {}
}
