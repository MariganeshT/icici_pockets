part of 'expression_pockets_bloc.dart';

@immutable
abstract class ExpressionPocketsEvent extends BaseEquatable{}

class ExpressionPocketsInitialEvent extends ExpressionPocketsEvent{}

