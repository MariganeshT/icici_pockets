import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:icici/authentication/bloc/authentication_bloc.dart';
import 'package:icici/screens/logout_screen.dart/LogoutScreen.dart';
import 'package:icici/utils/app_utils.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';

import 'bloc/managenotification_bloc.dart';

class ManageNotificationScreen extends StatefulWidget {
  ManageNotificationScreen({Key? key}) : super(key: key);

  @override
  _ManageNotificationScreenState createState() =>
      _ManageNotificationScreenState();
}

class _ManageNotificationScreenState extends State<ManageNotificationScreen> {
  late ManagenotificationBloc bloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc = ManagenotificationBloc()..add(ManagenotificationInitialEvent());
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    return BlocListener<ManagenotificationBloc, ManagenotificationState>(
      bloc: bloc,
      listener: (BuildContext context, ManagenotificationState state) {
        // TODO: implement listener
      },
      child: BlocBuilder<ManagenotificationBloc, ManagenotificationState>(
        bloc: bloc,
        builder: (BuildContext context, ManagenotificationState state) {
          // if (state is SupportscreenState) {
          //     return Center(
          //       child: CircularProgressIndicator(),
          //     );
          //   }
          return SafeArea(
            top: false,
            child: Scaffold(
              backgroundColor: ColorResource.colorFAFAFA,
              body: Column(
                children: [
                  CustomAppbar(
                    backgroundColor: ColorResource.color641653,
                    titleString: StringResource.manageNotification,
                    titleSpacing: 8,
                    style: Theme.of(context)
                        .textTheme
                        .headline5!
                        .copyWith(color: ColorResource.colorFFFFFF),
                    iconEnumValues: IconEnum.back,
                    onItemSelected: (selectedItem) {
                      Navigator.pop(context);
                    },
                  ),
                  Expanded(
                      child: SingleChildScrollView(
                    padding: EdgeInsets.symmetric(horizontal: 12, vertical: 30),
                    child: Column(
                      children: _listOfNotification(),
                    ),
                  )),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  List<Widget> _listOfNotification() {
    List<Widget> widgets = [];
    bloc.notificationList.forEach((element) {
      widgets.add(
        Container(
            margin: const EdgeInsets.only(bottom: 16),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(14),
              color: ColorResource.colorFFFFFF,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.2),
                  spreadRadius: 2,
                  blurRadius: 3,
                  offset: Offset(0, 3), // changes position of shadow
                ),
               ],
             ),
         child: Padding(
           padding: EdgeInsets.symmetric(horizontal: 12),
           child: Row(
             crossAxisAlignment: CrossAxisAlignment.start,
             children: [
               Padding(
                 padding: const EdgeInsets.only(top: 22),
                 child: SizedBox(
                   width: 30,
                   child: Align(
                     alignment: Alignment.centerLeft,
                     child: Image.asset(element.leading)),
                 ),
               ),
               Flexible(
                 child: Column(
                   crossAxisAlignment: CrossAxisAlignment.start,
                   children: [
                     Row(
                       crossAxisAlignment: CrossAxisAlignment.start,
                       children: [
                         Padding(
                           padding: const EdgeInsets.only(top: 22),
                           child: CustomText(
                             element.title,
                             style: Theme.of(context)
                                     .textTheme
                                      .bodyText1,
                           ),
                         ),
                         const Spacer(),
                          Padding(
                            padding: const EdgeInsets.only(top: 14),
                            child: CupertinoSwitch(
                              value: element.switchAction,
                              activeColor: ColorResource.color641653,
                              onChanged: (value) {
                                // print("VALUE : $value");
                                setState(() {
                                  element.switchAction = !element.switchAction;
                                  if (value) {
                                    AppUtils.showToast('Activated',
                                        gravity: ToastGravity.BOTTOM);
                                  } else {
                                    AppUtils.showToast('deactivated',
                                        gravity: ToastGravity.BOTTOM);
                                  }
                                });
                              },
                            ),
                          ),
                        ],
                      ),
                      CustomText(
                        element.subTitle,
                        style: Theme.of(context).textTheme.subtitle1!.copyWith(
                            color: ColorResource.color787878, height: 1.5),
                      ),
                      const SizedBox(
                        height: 22,
                      )
                    ],
                  )),
                  // Container(
                  //    child:  Align(
                  //      alignment: Alignment.topRight,
                  //      child: CupertinoSwitch(
                  //           value: element.switchAction,
                  //           activeColor: ColorResource.color641653,
                  //           onChanged: (value){
                  //           // print("VALUE : $value");
                  //           setState(() {
                  //               element.switchAction = !element.switchAction;
                  //               if (value) {
                  //                 AppUtils.showToast('Activated',
                  //                 gravity: ToastGravity.BOTTOM);
                  //               } else{
                  //                 AppUtils.showToast('deactivated',
                  //                 gravity: ToastGravity.BOTTOM);
                  //               }
                  //             });
                  //           },
                  //                    ),
                  //    ),
                  //  ),
                ],
              ),
            )),
      );
    });
    return widgets;
  }
}
