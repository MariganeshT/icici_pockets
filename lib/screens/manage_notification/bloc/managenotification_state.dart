part of 'managenotification_bloc.dart';

@immutable
abstract class ManagenotificationState {}

class ManagenotificationInitial extends ManagenotificationState {}

class ManagenotificationLoadingState extends ManagenotificationState {}

class ManagenotificationLoadedState extends ManagenotificationState {}
