import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:icici/model/manage_notification_model.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:meta/meta.dart';

part 'managenotification_event.dart';
part 'managenotification_state.dart';

class ManagenotificationBloc extends Bloc<ManagenotificationEvent, ManagenotificationState> {
  ManagenotificationBloc() : super(ManagenotificationInitial());

  List<ManageNotificationModel> notificationList = [];

  @override
  Stream<ManagenotificationState> mapEventToState(
    ManagenotificationEvent event,
  ) async* {
    if (event is ManagenotificationEvent) {
      yield ManagenotificationLoadingState();

      notificationList.addAll([
        ManageNotificationModel(
           title: StringResource.allowPushNotification,
          subTitle: StringResource.noteForNotification,
          leading: ImageResource.allowNotification,
          ),
          ManageNotificationModel(
           title: StringResource.allowWhatsappNotification,
          subTitle: StringResource.noteForNotification,
          leading: ImageResource.allowNotification,
          ),
          ManageNotificationModel(
           title: StringResource.allowSMSNotification,
          subTitle: StringResource.noteForNotification,
          leading: ImageResource.allowNotification,
          ),
          ManageNotificationModel(
          title: StringResource.allowEmailNotification,
          subTitle: StringResource.noteForNotification,
          leading: ImageResource.allowNotification,
          ),
      ]); 

      yield ManagenotificationLoadedState();     
    }
  }
}
