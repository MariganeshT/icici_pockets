part of 'managenotification_bloc.dart';

@immutable
abstract class ManagenotificationEvent extends BaseEquatable {}

class ManagenotificationInitialEvent extends ManagenotificationEvent {}
