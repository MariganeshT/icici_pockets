import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/screens/custom_otp_screen/bloc/custom_otp_bloc.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/constants.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class CustomOtpScreen extends StatefulWidget {
  CustomOtpScreen({Key? key}) : super(key: key);

  @override
  _CustomOtpScreenState createState() => _CustomOtpScreenState();
}

class _CustomOtpScreenState extends State<CustomOtpScreen> {
  late CustomOtpBloc bloc;
  int secondsRemaining = Constants.otpWaitingTime;
  bool enableResend = false;
  Timer? timer;

  TextEditingController newTextEditingController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    bloc = BlocProvider.of<CustomOtpBloc>(context);
    timer = Timer.periodic(const Duration(seconds: 1), (_) {
      if (secondsRemaining != 0) {
        setState(() {
          secondsRemaining--;
        });
      } else {
        setState(() {
          enableResend = true;
          newTextEditingController.clear();
          secondsRemaining = Constants.otpWaitingTime;
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);

    return SafeArea(
      top: false,
      child: Scaffold(
          backgroundColor: ColorResource.colorFAFAFA,
          body: BlocListener<CustomOtpBloc, CustomOtpState>(
            bloc: bloc,
            listener: (BuildContext context, CustomOtpState state) {},
            child: BlocBuilder<CustomOtpBloc, CustomOtpState>(
              bloc: bloc,
              builder: (BuildContext context, CustomOtpState state) {
                if (state is CustomOtpLoadingScreen) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
                return Column(
                  children: <Widget>[
                    SizedBox(
                      // height: 112,
                      child: CustomAppbar(
                        backgroundColor: ColorResource.color641653,
                        titleString: Languages.of(context)!.otpVerification,
                        titleSpacing: 8,
                        style: Theme.of(context)
                            .textTheme
                            .headline5!
                            .copyWith(color: ColorResource.colorFFFFFF),
                        iconEnumValues: IconEnum.close,
                        onItemSelected: (selectedItem) {
                          Navigator.pop(context);
                        },
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 22.0, right: 22, top: 24, bottom: 68),
                              child: RichText(
                                  textAlign: TextAlign.center,
                                  text: TextSpan(children: <TextSpan>[
                                    TextSpan(
                                      text: Languages.of(context)!
                                          .otpVerificationDescription,
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle1!
                                          .copyWith(
                                            color: ColorResource.color787878,
                                          ),
                                    ),
                                    TextSpan(
                                        text: bloc.otpAddress,
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle1!
                                            .copyWith(
                                                fontWeight: FontWeight.w700,
                                                color:
                                                    ColorResource.color787878)),
                                  ])),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 20.0),
                              child: (enableResend)
                                  ? Container()
                                  : Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        CustomText(StringResource.codeExpireIn,
                                            style: Theme.of(context)
                                                .textTheme
                                                .subtitle1!
                                                .copyWith(
                                                    color: ColorResource
                                                        .color222222)),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 4),
                                          child: CustomText(
                                              StringResource.remainingSeconds(
                                                  secondsRemaining),
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .subtitle1!
                                                  .copyWith(
                                                      color: ColorResource
                                                          .color222222)),
                                        )
                                      ],
                                    ),
                            ),
                            PinCodeTextField(
                              appContext: context,
                              onChanged: (String value) {
                                // user.otp = value;
                              },
                              onCompleted: (String enteredText) {
                                // while (Navigator.canPop(context)) {
                                //   Navigator.pop(context, true);
                                // }
                                Navigator.pop(context, true);
                                // Navigator.pushNamed(
                                //     context, AppRoutes.setDigitPinScreen);
                              },
                              obscureText: true,
                              length: 6,
                              textStyle: Theme.of(context)
                                  .textTheme
                                  .headline5!
                                  .copyWith(color: ColorResource.color222222),
                              animationType: AnimationType.scale,
                              animationDuration:
                                  const Duration(milliseconds: 300),
                              keyboardType: TextInputType.number,
                              controller: newTextEditingController,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              pinTheme: PinTheme(
                                shape: PinCodeFieldShape.box,
                                borderWidth: 1,
                                borderRadius: BorderRadius.circular(8),
                                inactiveColor: ColorResource.color222222,
                                selectedColor: ColorResource.color222222,
                                activeColor: ColorResource.color222222,
                                fieldHeight: 56,
                                fieldWidth: 48,
                              ),
                            ),
                            const Spacer(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                CustomText(StringResource.codeNotReceived,
                                    style: Theme.of(context)
                                        .textTheme
                                        .subtitle1!
                                        .copyWith(
                                            color: ColorResource.color222222)),
                                Padding(
                                  padding: const EdgeInsets.only(left: 6.0),
                                  child: GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        _resendCode();
                                      });
                                    },
                                    child: CustomText(StringResource.resend,
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle1!
                                            .copyWith(
                                              color: enableResend
                                                  ? ColorResource.colorF58220
                                                  : ColorResource.color222222
                                                      .withOpacity(0.6),
                                            )),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                );
              },
            ),
          )),
    );
  }

  void _resendCode() {
    setState(() {
      secondsRemaining = Constants.otpWaitingTime;
      enableResend = false;
      newTextEditingController.clear();
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    timer!.cancel();
    super.dispose();
  }
}
