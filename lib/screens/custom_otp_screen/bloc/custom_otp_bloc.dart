import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:meta/meta.dart';

part 'custom_otp_event.dart';
part 'custom_otp_state.dart';

class CustomOtpBloc extends Bloc<CustomOtpEvent, CustomOtpState> {
  CustomOtpBloc(this.otpAddress) : super(CustomOtpInitial());

  String otpAddress = '';

  @override
  Stream<CustomOtpState> mapEventToState(
    CustomOtpEvent event,
  ) async* {
    // TODO: implement mapEventToState
  }
}
