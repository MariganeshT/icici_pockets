part of 'custom_otp_bloc.dart';

@immutable
abstract class CustomOtpEvent extends BaseEquatable {}

class CustomOtpVerificationInitialEvent extends CustomOtpEvent {}
