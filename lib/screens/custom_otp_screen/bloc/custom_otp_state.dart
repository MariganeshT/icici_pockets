part of 'custom_otp_bloc.dart';

@immutable
abstract class CustomOtpState extends BaseEquatable {}

class CustomOtpInitial extends CustomOtpState {}

class CustomOtpLoadingScreen extends CustomOtpState{}

class CustomOtpLoadedScreen extends CustomOtpState{}
