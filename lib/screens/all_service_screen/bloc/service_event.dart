part of 'service_bloc.dart';

@immutable
abstract class ServiceEvent extends BaseEquatable {}

class ServiceInitailEvent extends ServiceEvent {
  BuildContext? context;

  ServiceInitailEvent({this.context});
}
