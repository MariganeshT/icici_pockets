import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:icici/base/base_state.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/model/all_service_main_model.dart';
import 'package:icici/model/all_services_child_model.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:meta/meta.dart';

part 'service_event.dart';

class ServiceBloc extends Bloc<ServiceEvent, BaseState> {
  ServiceBloc() : super(InitialState());

  @override
  Stream<BaseState> mapEventToState(
    ServiceEvent event,
  ) async* {
    if (event is ServiceInitailEvent) {
      yield InitialState();
      List<AllServiceMainModel> allServiceModelList = [];
      allServiceModelList.addAll([
        AllServiceMainModel(
            // noOfRow: 2,
            titleName: Languages.of(event.context!)!.payment,
            ListOfIcons: [
              AllServiceChildModel(
                  name: 'Pay to Contacts',
                  icon: ImageResource.all_service_pay_to_contacts,

              ),

              AllServiceChildModel(
                  name: 'Fund Transfer',
                  icon: ImageResource.all_service_fund_transfer),
              AllServiceChildModel(
                  name: 'UPI', icon: ImageResource.all_service_upi),
              AllServiceChildModel(
                  name: 'Add Funds', icon: ImageResource.all_service_add_funds),
              AllServiceChildModel(
                  name: 'Request Money',
                  icon: ImageResource.all_service_request_money),
              AllServiceChildModel(
                  name: 'Apply for Pockets Card',
                  icon: ImageResource.all_service_apply_for_pockets_card),
              AllServiceChildModel(
                  title: 'New',
                  name: 'Scan to Pay',
                  icon: ImageResource.all_service_scan_to_pay),
            ]),
        AllServiceMainModel(
            // noOfRow: 1,
            titleName: Languages.of(event.context!)!.rechargeBillsOffers,
            ListOfIcons: [
              AllServiceChildModel(
                  name: 'Bill Pay', icon: ImageResource.all_service_bill_pay),
              AllServiceChildModel(
                  name: 'Recharge', icon: ImageResource.all_service_recharge),
              AllServiceChildModel(
                  name: 'FASTag', icon: ImageResource.all_service_fastag),
              AllServiceChildModel(
                  name: 'PayPal', icon: ImageResource.all_service_paypal),
              AllServiceChildModel(
                  name: 'Offers', icon: ImageResource.all_service_offers),
              AllServiceChildModel(
                  name: 'CashKaro Earnings',
                  icon: ImageResource.all_service_cash_karo_earning),
            ]),
        AllServiceMainModel(
            noOfRow: 1,
            titleName: Languages.of(event.context!)!.bank,
            ListOfIcons: [
              AllServiceChildModel(
                  name: 'Split Bills',
                  icon: ImageResource.all_service_split_bills),
              AllServiceChildModel(
                  name: 'Savings Account',
                  icon: ImageResource.all_service_savings_account),
              AllServiceChildModel(
                  name: 'Prepaid Card',
                  icon: ImageResource.all_service_prepaid_card),
              AllServiceChildModel(
                  name: 'Personal Loan',
                  icon: ImageResource.all_service_personal_loan),
              AllServiceChildModel(
                  name: 'Two wheeler Loan',
                  icon: ImageResource.all_service_two_wheeler_loan),
              AllServiceChildModel(
                  name: 'Wallet Protection Plan',
                  icon: ImageResource.all_service_wallet_protection_plan),
              AllServiceChildModel(
                  name: 'Card Protection Plan',
                  icon: ImageResource.all_service_card_protection_plan),
              AllServiceChildModel(
                  name: 'Mutual Funds',
                  icon: ImageResource.all_service_mutual_funds),
              AllServiceChildModel(
                  name: 'FD/RD', icon: ImageResource.all_service_fdrd),
              AllServiceChildModel(
                  name: 'iWish', icon: ImageResource.all_service_i_wish),
              AllServiceChildModel(
                  name: 'Paylater', icon: ImageResource.all_service_pay_later),
              AllServiceChildModel(
                  name: 'KYC', icon: ImageResource.all_service_kyc  ),
            ]),
      ]);
      yield SuccessState(successResponse: allServiceModelList);
    }
  }
}
