import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/base/base_state.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/model/all_service_main_model.dart';
import 'package:icici/model/all_services_child_model.dart';
import 'package:icici/screens/all_service_screen/bloc/service_bloc.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:icici/widgets/widget_utils.dart';

class AllServiceScreen extends StatefulWidget {
  const AllServiceScreen({Key? key}) : super(key: key);

  @override
  _AllServiceScreenState createState() => _AllServiceScreenState();
}

class _AllServiceScreenState extends State<AllServiceScreen> {
  late ServiceBloc bloc;
  List<AllServiceMainModel> allServiceGridModelList = [];
  List<AllServiceMainModel> allServiceModelList = [];

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<ServiceBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ServiceBloc, BaseState>(
      bloc: bloc,
      listener: (BuildContext context, BaseState state) async {
        if (state is SuccessState) {
          setState(() {
            allServiceModelList = state.successResponse;
            allServiceGridModelList = state.successResponse;
          });
        }
      },
      child: BlocBuilder<ServiceBloc, BaseState>(
        bloc: bloc,
        builder: (BuildContext context, BaseState state) {
          if (state is InitialState) {
            return const Scaffold(
              body: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
          if (state is SuccessState) {
            return SafeArea(
              top: false,
              child: Scaffold(
                backgroundColor: ColorResource.colorFAFAFA,
                body: Column(
                  children: [
                    Container(
                      color: ColorResource.color641653,
                      child: CustomAppbar(
                        titleString: Languages.of(context)!.allServices,
                        iconEnumValues: IconEnum.back,
                        showSearch: true,
                        titleSpacing: 8,
                        onItemSelected: (dynamic values) {
                          if (values is String) {
                            if (values == 'IconEnum.back') {
                              Navigator.pop(context);
                            } else {
                              List<AllServiceMainModel> tempAllServiceList = [];
                              setState(() {
                                if (values == '') {
                                  bloc.add(
                                      ServiceInitailEvent(context: context));
                                } else {
                                  tempAllServiceList.clear();
                                  allServiceModelList.forEach(
                                      (AllServiceMainModel
                                          allServiceMainModel) {
                                    bool canAdd = false;
                                    AllServiceMainModel
                                        tempAllServiceMainModel =
                                        AllServiceMainModel();
                                    tempAllServiceMainModel.ListOfIcons = [];
                                    tempAllServiceMainModel.gridCount =
                                        allServiceMainModel.gridCount;
                                    tempAllServiceMainModel.noOfRow =
                                        allServiceMainModel.noOfRow;
                                    tempAllServiceMainModel.titleName =
                                        allServiceMainModel.titleName;
                                    allServiceMainModel.ListOfIcons!.forEach(
                                        (AllServiceChildModel
                                            allServiceChildModel) {
                                      if (allServiceChildModel.name!
                                          .toLowerCase()
                                          .contains(values.toLowerCase())) {
                                        canAdd = true;
                                        tempAllServiceMainModel.ListOfIcons!
                                            .add(allServiceChildModel);
                                      }
                                    });
                                    if (canAdd) {
                                      tempAllServiceList
                                          .add(tempAllServiceMainModel);
                                    }
                                  });
                                }
                                allServiceGridModelList = tempAllServiceList;
                              });
                            }
                          } else {
                            Navigator.pop(context);
                          }
                        },
                      ),
                    ),
                    MediaQuery.removePadding(
                      context: context,
                      removeTop: true,
                      child: Expanded(
                        child: ListView.builder(
                            itemCount: allServiceGridModelList.length,
                            itemBuilder: (BuildContext context, int index) {
                              return Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 05, vertical: 5),
                                child: Card(
                                  elevation: 0,
                                  shape: RoundedRectangleBorder(
                                    side:
                                        const BorderSide(color: Colors.white70),
                                    borderRadius: BorderRadius.circular(24),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(9),
                                    child: Wrap(
                                      children: [
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 16),
                                          child: CustomText(
                                              allServiceGridModelList[index]
                                                  .titleName!,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1!
                                                  .copyWith(
                                                      color: ColorResource
                                                          .color222222,
                                                      fontWeight:
                                                          FontWeight.w700)),
                                        ),
                                        GridView.builder(
                                          itemCount: allServiceGridModelList[
                                                          index]
                                                      .noOfRow !=
                                                  null
                                              ? (allServiceGridModelList[index]
                                                      .gridCount! *
                                                  allServiceGridModelList[index]
                                                      .noOfRow!)
                                              : allServiceGridModelList[index]
                                                  .ListOfIcons!
                                                  .length,
                                          shrinkWrap: true,
                                          physics:
                                              const NeverScrollableScrollPhysics(),
                                          gridDelegate:
                                              SliverGridDelegateWithFixedCrossAxisCount(
                                            crossAxisCount:
                                                allServiceGridModelList[index]
                                                    .gridCount!,
                                          ),
                                          itemBuilder: (BuildContext context,
                                              int innerIndex) {
                                            if (allServiceGridModelList[index]
                                                    .ListOfIcons!
                                                    .length >
                                                innerIndex) {
                                              if (allServiceGridModelList[index]
                                                      .noOfRow !=
                                                  null) {
                                                if ((allServiceGridModelList[
                                                                    index]
                                                                .gridCount! *
                                                            allServiceGridModelList[
                                                                    index]
                                                                .noOfRow!) -
                                                        1 ==
                                                    innerIndex) {
                                                  return GestureDetector(
                                                    onTap: () {
                                                      moreOption(
                                                          context,
                                                          allServiceGridModelList[
                                                              index]);
                                                    },
                                                    child: Container(
                                                      alignment:
                                                          Alignment.center,
                                                      child: Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                        children: [
                                                          Container(
                                                            width: 36,
                                                            height: 36,
                                                            decoration:
                                                                BoxDecoration(
                                                              color: ColorResource
                                                                  .colorFFF3E9,
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          20),
                                                            ),
                                                            child: Center(
                                                              child: CustomText(
                                                                '+ ${(allServiceGridModelList[index].ListOfIcons!.length - (innerIndex))}',
                                                                style: Theme.of(
                                                                        context)
                                                                    .textTheme
                                                                    .subtitle2!
                                                                    .copyWith(
                                                                        color: ColorResource
                                                                            .colorFA3245,
                                                                        fontWeight:
                                                                            FontWeight.w700),
                                                                textAlign:
                                                                    TextAlign
                                                                        .center,
                                                              ),
                                                            ),
                                                          ),
                                                          CustomText(
                                                            Languages.of(
                                                                    context)!
                                                                .more,
                                                            style: Theme.of(
                                                                    context)
                                                                .textTheme
                                                                .subtitle2!
                                                                .copyWith(
                                                                    color: ColorResource
                                                                        .color222222,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400),
                                                            textAlign: TextAlign
                                                                .center,
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  );
                                                } else {
                                                  return GestureDetector(
                                                    child: WidgetUtils.gridChild(
                                                        context,
                                                        allServiceGridModelList[
                                                                    index]
                                                                .ListOfIcons![
                                                            innerIndex]),
                                                  );
                                                }
                                              } else {
                                                return GestureDetector(
                                                  child: WidgetUtils.gridChild(
                                                      context,
                                                      allServiceGridModelList[
                                                                  index]
                                                              .ListOfIcons![
                                                          innerIndex]),
                                                );
                                              }
                                            } else {
                                              return const SizedBox();
                                            }
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            }),
                      ),
                    )
                  ],
                ),
              ),
            );
          }
          return const Scaffold(
            body: Center(
              child: CircularProgressIndicator(),
            ),
          );
        },
      ),
    );
  }

  Future<dynamic> moreOption(
      BuildContext context, AllServiceMainModel allServiceMainModel) {
    return showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        enableDrag: false,
        context: context,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(32),
            topRight: Radius.circular(32),
          ),
        ),
        builder: (BuildContext bContext) {
          return Container(
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(32)),
              child: Wrap(
                children: [
                  /* ListTile(
                    selected: true,
                    leading: ,
                    title: ,
                  ),*/

                  Stack(
                    children: [
                      Container(
                        margin: const EdgeInsets.only(top: 17),
                        alignment: Alignment.topCenter,
                        child: CustomText(allServiceMainModel.titleName!,
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1!
                                .copyWith(
                                    color: ColorResource.color222222,
                                    fontWeight: FontWeight.w700)),
                      ),
                      Container(
                        height: 32,
                        width: 32,
                        margin: const EdgeInsets.only(left: 10, top: 10),
                        child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: CircleAvatar(
                            backgroundColor:
                                ColorResource.colorBFCBD7.withOpacity(0.3),
                            child: const Icon(
                              Icons.close,
                              size: 20,
                              color: ColorResource.color222222,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  Center(
                    child: SizedBox(
                      child: Column(
                        children: [
                          GridView.builder(
                            itemCount: allServiceMainModel.ListOfIcons!.length,
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              childAspectRatio: 1,
                              crossAxisSpacing: 1,
                              mainAxisSpacing: 1,
                              crossAxisCount: allServiceMainModel.gridCount!,
                            ),
                            itemBuilder: (BuildContext context, int index) {
                              return Container(
                                child: GestureDetector(
                                  child: WidgetUtils.gridChild(context,
                                      allServiceMainModel.ListOfIcons![index]),
                                ),
                              );
                            },
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ));
        });
  }
}
