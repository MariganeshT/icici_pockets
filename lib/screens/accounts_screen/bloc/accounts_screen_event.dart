part of 'accounts_screen_bloc.dart';

@immutable
abstract class AccountsScreenEvent extends BaseEquatable {}

class AccountsScreenInitialEvent extends AccountsScreenEvent {
  BuildContext? context;

  AccountsScreenInitialEvent({this.context});
}

class AccountsSetTransactionScreenEvent extends AccountsScreenEvent {}

class RequestCallBottomSheetEvent extends AccountsScreenEvent {}
