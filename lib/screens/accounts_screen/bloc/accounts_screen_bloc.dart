import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:icici/model/profile_models.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:meta/meta.dart';

import '../../../router.dart';

part 'accounts_screen_event.dart';
part 'accounts_screen_state.dart';

class AccountsScreenBloc
    extends Bloc<AccountsScreenEvent, AccountsScreenState> {
  AccountsScreenBloc() : super(AccountsScreenInitial());
  List<Profile> accounts = [];
  List<Profile> others = [];

  @override
  Stream<AccountsScreenState> mapEventToState(
    AccountsScreenEvent event,
  ) async* {
    if (event is AccountsScreenInitialEvent) {
      yield AccountsScreenLoadingState();
      accounts.clear();
      accounts.addAll([
        Profile(ImageResource.accountdetails, StringResource.accountdetails,
            trailing: ImageResource.arrow, onTap: () async {
          await Navigator.pushNamed(
              event.context!, AppRoutes.accountDetails);
        }),
        Profile(
          ImageResource.accountdetails,
          StringResource.accountdetails,
          trailing: ImageResource.arrow,
        ),
        Profile(
          ImageResource.settransactionlimit,
          StringResource.settransactionlimit,
          // onTap: () {
          //   // Navigator.pushReplacementNamed(
          //   //   context, AppRoutes.setTransactionLimitScreen
          //   this.add(AccountsSetTransactionScreenEvent());
          //   // );
          // },
          trailing: ImageResource.arrow,
        ),
        Profile(
          ImageResource.walletsource,
          StringResource.requestforwallet,
          trailing: ImageResource.arrow,
        ),
      ]);
      others.addAll([
        Profile(
          ImageResource.theme,
          StringResource.themes,
          trailing: ImageResource.arrow,
        )
      ]);
      yield AccountsScreenLoadedState();
    }
    if (event is AccountsSetTransactionScreenEvent) {
      yield AccountsSetTransactionScreenState();
    }
  }
}
