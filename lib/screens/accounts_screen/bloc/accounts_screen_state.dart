part of 'accounts_screen_bloc.dart';

@immutable
abstract class AccountsScreenState extends BaseEquatable {}

class AccountsScreenInitial extends AccountsScreenState {}

class AccountsScreenLoadingState extends AccountsScreenState {}

class AccountsScreenLoadedState extends AccountsScreenState {}

class AccountsSetTransactionScreenState extends AccountsScreenState {}

class RequestCallBottomSheetState extends AccountsScreenState {}
