import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:icici/widgets/custom_button.dart';
import 'package:icici/widgets/custom_listtile.dart';

import '../../router.dart';
import 'bloc/accounts_screen_bloc.dart';

class AccountsScreen extends StatefulWidget {
  const AccountsScreen({Key? key}) : super(key: key);

  @override
  _AccountsScreenState createState() => _AccountsScreenState();
}

class _AccountsScreenState extends State<AccountsScreen> {
  late AccountsScreenBloc bloc;
  bool isAvailableBalance = false;

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<AccountsScreenBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      child: Scaffold(
          backgroundColor: ColorResource.colorFAFAFA,
          body: BlocListener<AccountsScreenBloc, AccountsScreenState>(
              bloc: bloc,
              listener: (BuildContext context, AccountsScreenState state) {
                if (state is AccountsSetTransactionScreenState) {
                  // Navigator.pushReplacementNamed(
                  //     context, AppRoutes.setTransactionLimitScreen);
                  Navigator.pushReplacementNamed(
                      context, AppRoutes.setPinScreen);
                }
              },
              child: BlocBuilder<AccountsScreenBloc, AccountsScreenState>(
                  bloc: bloc,
                  builder: (BuildContext context, AccountsScreenState state) {
                    if (state is AccountsScreenLoadingState) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }

                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          color: ColorResource.color641653,
                          child: CustomAppbar(
                            titleString: StringResource.accounts,
                            iconEnumValues: IconEnum.back,
                            titleSpacing: 8,
                            onItemSelected: (values) {
                              Navigator.pop(context);
                            },
                          ),
                        ),
                        Expanded(
                          child: SingleChildScrollView(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding:
                                      const EdgeInsets.only(top: 30, left: 24),
                                  child: CustomText(
                                    StringResource.accounts,
                                    color: ColorResource.color222222,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                      top: 12, left: 12, right: 12, bottom: 24),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(14),
                                      boxShadow: [
                                        BoxShadow(
                                          blurRadius: 0.2,
                                          offset: Offset(0, 4),
                                          color: ColorResource.colorFFFFFF,
                                        )
                                      ]),
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Container(
                                          child: CustomListTile(
                                            Image.asset(
                                                ImageResource.accountLogo),
                                            StringResource.accountDetails,
                                            onTap: () {
                                              Navigator.pushNamed(context,
                                                  AppRoutes.accountDetails);
                                            },
                                          ),
                                        ),
                                        CustomListTile(
                                          Image.asset(
                                              ImageResource.setTransLimit),
                                          StringResource.setTransLimit,
                                          onTap: () async {
                                            await Navigator.pushNamed(
                                                context,
                                                AppRoutes
                                                    .setTransactionLimitScreen);
                                          },
                                        ),
                                        CustomListTile(
                                          Image.asset(
                                              ImageResource.walletClosureLogo),
                                          StringResource.reqForWalletClosure,
                                          onTap: () {
                                            requestCallBottomSheet(context);
                                          },
                                        )
                                      ]),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.only(top: 14, left: 24),
                                  child: CustomText(
                                    StringResource.track,
                                    color: ColorResource.color222222,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                  ),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(
                                      top: 12, left: 12, right: 12, bottom: 24),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(14),
                                      boxShadow: [
                                        const BoxShadow(
                                          blurRadius: 0.2,
                                          offset: Offset(0, 4),
                                          color: ColorResource.colorFFFFFF,
                                        )
                                      ]),
                                  child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        CustomListTile(
                                          Image.asset(
                                              ImageResource.trackService),
                                          StringResource.trackService,
                                          onTap: () async {
                                            await Navigator.pushNamed(context,
                                                AppRoutes.trackServiceRequest);
                                          },
                                        ),
                                        CustomListTile(
                                          Image.asset(
                                              ImageResource.trackPhysical),
                                          StringResource.trackPhysical,
                                          onTap: () async {
                                            await Navigator.pushNamed(
                                                context,
                                                AppRoutes
                                                    .trackPhysicalCardScreen);
                                          },
                                        ),
                                      ]),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 24),
                                  child: CustomText(
                                    StringResource.others,
                                    color: ColorResource.color222222,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                      left: 12, right: 12, top: 12),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(14),
                                      boxShadow: [
                                        BoxShadow(
                                          blurRadius: 0.2,
                                          offset: Offset(0, 4),
                                          color: ColorResource.colorFFFFFF,
                                        )
                                      ]),
                                  child: CustomListTile(
                                      Image.asset(ImageResource.themes),
                                      StringResource.themes),
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    );
                  }))),
    );
  }

  List<Widget> buildAccountsWidget() {
    List<Widget> widgets = [];
    bloc.accounts.forEach((element) {
      widgets.add(GestureDetector(
        onTap: element.onTap,
        child: ListTile(
            minLeadingWidth: 14,
            contentPadding: const EdgeInsets.only(top: 10, left: 24),
            isThreeLine: true,
            minVerticalPadding: 16,
            leading: SizedBox(
              width: 24,
              child: Image.asset(element.leadingImage),
            ),
            title: CustomText(
              element.title,
              style: Theme.of(context).textTheme.subtitle1!.copyWith(
                    color: ColorResource.color222222,
                  ),
            ),
            subtitle: element.subTitle != ''
                ? Padding(
                    padding: const EdgeInsets.only(top: 4.0),
                    child: CustomText(
                      element.subTitle,
                      style: Theme.of(context)
                          .textTheme
                          .subtitle1!
                          .copyWith(color: ColorResource.color787878),
                    ),
                  )
                : Container(),
            trailing: Padding(
              padding: const EdgeInsets.only(right: 18.0),
              child: Image.asset(element.trailing),
            )),
      ));
    });
    return widgets;
  }

  List<Widget> buildOthersWidget() {
    List<Widget> widgets = [];
    bloc.others.forEach((element) {
      widgets.add(GestureDetector(
        onTap: element.onTap,
        child: ListTile(
            minLeadingWidth: 14,
            contentPadding: const EdgeInsets.only(top: 10, left: 24),
            isThreeLine: true,
            minVerticalPadding: 16,
            leading: SizedBox(
              width: 24,
              child: Image.asset(element.leadingImage),
            ),
            title: CustomText(
              element.title,
              style: Theme.of(context).textTheme.subtitle1!.copyWith(
                    color: ColorResource.color222222,
                  ),
            ),
            subtitle: element.subTitle != ''
                ? Padding(
                    padding: const EdgeInsets.only(top: 4.0),
                    child: CustomText(
                      element.subTitle,
                      style: Theme.of(context)
                          .textTheme
                          .subtitle1!
                          .copyWith(color: ColorResource.color787878),
                    ),
                  )
                : Container(),
            trailing: Padding(
              padding: const EdgeInsets.only(right: 18.0),
              child: Image.asset(element.trailing),
            )),
      ));
    });
    return widgets;
  }

  requestCallBottomSheet(BuildContext buildContext) {
    showModalBottomSheet(
      isScrollControlled: true,
      isDismissible: false,
      context: buildContext,
      backgroundColor: ColorResource.colorFFFFFF,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(32),
        ),
      ),
      builder: (BuildContext context) {
        return Padding(
          padding: const EdgeInsets.all(24.0),
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Image.asset(ImageResource.close)),
                const Center(
                  child: Image(
                      height: 200,
                      image: AssetImage(ImageResource.walletCallReq)),
                ),
                CustomText(Languages.of(context)!.whatWentWrong,
                    style: Theme.of(context).textTheme.headline5!.copyWith(
                          color: ColorResource.color222222,
                        )),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 6),
                  child: CustomText(Languages.of(context)!.weHappy,
                      style: Theme.of(context).textTheme.subtitle1!.copyWith(
                            color: ColorResource.color222222.withOpacity(0.5),
                          )),
                ),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 6),
                  child: Wrap(
                    children: <Widget>[
                      RichText(
                          text: TextSpan(children: <TextSpan>[
                        TextSpan(
                            text: Languages.of(context)!.youCan,
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                                    color: ColorResource.color222222
                                        .withOpacity(0.5))),
                        TextSpan(
                          text: ' 86575 67777 ',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(color: ColorResource.color222222),
                          recognizer: TapGestureRecognizer()..onTap = () {},
                        ),
                        TextSpan(
                            text: Languages.of(context)!.orTap,
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                                    color: ColorResource.color222222
                                        .withOpacity(0.5))),
                        TextSpan(
                          text: ' "Call me". ',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(color: ColorResource.color222222),
                          recognizer: TapGestureRecognizer()..onTap = () {},
                        ),
                        TextSpan(
                            text: Languages.of(context)!.someone,
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                                    color: ColorResource.color222222
                                        .withOpacity(0.5))),
                      ]))
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: CustomButton(
                    Languages.of(context)!.requestForCallBack,
                    onTap: () {
                      Navigator.pop(context);
                      requestAcceptedBottomSheet(context);
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: GestureDetector(
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 45,
                      decoration: BoxDecoration(
                        color: ColorResource.colorFFFFFF,
                        border: Border.all(
                          color: ColorResource.colorFF781F,
                        ),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Center(
                        child: CustomText(
                          Languages.of(context)!.continueToDelete,
                          style:
                              Theme.of(context).textTheme.bodyText1!.copyWith(
                                    color: ColorResource.colorFF781F,
                                  ),
                        ),
                      ),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                      isAvailableBalance
                          ? balanceBottomSheet(context)
                          : Navigator.pushNamed(
                              context, AppRoutes.walletClosureScreen);
                    },
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  requestAcceptedBottomSheet(BuildContext buildContext) {
    showModalBottomSheet(
      isScrollControlled: true,
      isDismissible: false,
      context: buildContext,
      backgroundColor: ColorResource.colorFFFFFF,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(32),
        ),
      ),
      builder: (BuildContext context) {
        return Padding(
          padding: const EdgeInsets.all(24.0),
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const Center(
                  child: Image(
                      height: 200,
                      image: AssetImage(ImageResource.walletClosure)),
                ),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 6),
                  child: CustomText(Languages.of(context)!.requestAccepted,
                      style: Theme.of(context).textTheme.headline5!.copyWith(
                            color: ColorResource.color222222,
                          )),
                ),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 6),
                  child: CustomText(Languages.of(context)!.weHaveAccepted,
                      style: Theme.of(context).textTheme.subtitle1!.copyWith(
                            color: ColorResource.color222222.withOpacity(0.5),
                          )),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: CustomButton(
                    Languages.of(context)!.gotIt,
                    onTap: () {
                      Navigator.pop(context);
                    },
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  balanceBottomSheet(BuildContext buildContext) {
    showModalBottomSheet(
      isScrollControlled: true,
      isDismissible: false,
      context: buildContext,
      backgroundColor: ColorResource.colorFFFFFF,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(32),
        ),
      ),
      builder: (BuildContext context) {
        return Padding(
          padding: const EdgeInsets.all(25.0),
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const Center(
                  child: Image(
                      height: 200,
                      image: AssetImage(ImageResource.walletBalance)),
                ),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 10),
                  child: CustomText(Languages.of(context)!.weFound,
                      style: Theme.of(context).textTheme.headline5!.copyWith(
                            color: ColorResource.color222222,
                          )),
                ),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 10),
                  child: CustomText(Languages.of(context)!.weSuggest,
                      style: Theme.of(context).textTheme.subtitle1!.copyWith(
                            color: ColorResource.color222222.withOpacity(0.5),
                          )),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: CustomButton(
                    Languages.of(context)!.gotIt,
                    onTap: () async {
                      Navigator.pop(context);
                    },
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
