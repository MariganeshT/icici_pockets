import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:meta/meta.dart';

part 'accountdetailbloc_event.dart';
part 'accountdetailbloc_state.dart';

class AccountDetailBloc extends Bloc<AccountDetailEvent, AccountDetailState> {
  AccountDetailBloc() : super(AccountDetailInitial());

  bool isRequestedCard = false;

  @override
  Stream<AccountDetailState> mapEventToState(
    AccountDetailEvent event,
  ) async* {
    if (event is AccountDetailIntialEvent) {
      yield AccountDetailInitial();
      // isRequestedCard = await PreferenceHelper.getTrack();
      yield AccountDetailLoadedState();
    }
  }
}
