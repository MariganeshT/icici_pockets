part of 'accountdetailbloc_bloc.dart';

@immutable
abstract class AccountDetailState extends BaseEquatable {}

class AccountDetailInitial extends AccountDetailState {}

class AccountDetailLoadedState extends AccountDetailState {}
