part of 'accountdetailbloc_bloc.dart';

@immutable
abstract class AccountDetailEvent extends BaseEquatable {}

class AccountDetailIntialEvent extends AccountDetailEvent {}
