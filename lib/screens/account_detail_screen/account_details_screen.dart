import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/router.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';

import 'bloc/accountdetailbloc_bloc.dart';

class AccountDetailScreen extends StatefulWidget {
  const AccountDetailScreen({Key? key}) : super(key: key);

  @override
  _AccountDetailScreenState createState() => _AccountDetailScreenState();
}

class _AccountDetailScreenState extends State<AccountDetailScreen> {
  bool viewCvv = false;
  String _copy = '4333333337766';
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  late AccountDetailBloc bloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc = BlocProvider.of<AccountDetailBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: Colors.transparent));

    return BlocListener(
      listener: (context, state) {},
      bloc: bloc,
      child: BlocBuilder(
        bloc: bloc,
        builder: (context, state) {
          if (state is AccountDetailInitial) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          return Scaffold(
            body: SafeArea(
                top: false,
                child: Column(
                  children: [
                    CustomAppbar(
                      backgroundColor: ColorResource.color641653,
                      titleString: StringResource.accountDetails,
                      titleSpacing: 8,
                      style: Theme.of(context)
                          .textTheme
                          .headline5!
                          .copyWith(color: ColorResource.colorFFFFFF),
                      iconEnumValues: IconEnum.back,
                      onItemSelected: (selectedItem) {
                        Navigator.pop(context);
                      },
                      onChanged: (q) {},
                    ),
                    Expanded(
                      child: SingleChildScrollView(
                        physics: ClampingScrollPhysics(),
                        child: Container(
                          child: Stack(
                            children: [
                              Container(
                                margin: const EdgeInsets.only(
                                    left: 12, right: 12, top: 88, bottom: 15),
                                child: Card(
                                  elevation: 2,
                                  shape: RoundedRectangleBorder(
                                    side: const BorderSide(
                                      color: Colors.white70,
                                    ),
                                    borderRadius: BorderRadius.circular(14),
                                  ),
                                  child: Column(children: [
                                    Container(
                                      margin: const EdgeInsets.only(
                                          top: 250, left: 16, right: 16),
                                      child: ListTile(
                                        leading: Container(
                                            height: 24,
                                            width: 24,
                                            child: Image.asset(
                                                ImageResource.statmentLogo)),
                                        title: Transform(
                                          transform: Matrix4.translationValues(
                                              -18, 0.0, 0.0),
                                          child: CustomText(
                                            StringResource.statement,
                                            fontSize: 16,
                                            color: ColorResource.color222222,
                                          ),
                                        ),
                                        trailing: Container(
                                          child: const Icon(
                                            Icons.arrow_forward_ios,
                                            color: ColorResource.colorF58220,
                                            size: 18,
                                          ),
                                        ),
                                        onTap: () async {
                                          await Navigator.pushNamed(context,
                                              AppRoutes.statementScreen);
                                        },
                                      ),
                                    ),
                                    Container(
                                      padding: const EdgeInsets.only(
                                          left: 16, right: 16),
                                      child: ListTile(
                                        title: Transform(
                                          transform: Matrix4.translationValues(
                                              -18, 0.0, 0.0),
                                          child: CustomText(
                                            Languages.of(context)!
                                                .blockYourPhysicalCard,
                                            fontSize: 16,
                                            color: ColorResource.color222222,
                                          ),
                                        ),
                                        leading: Image.asset(
                                            ImageResource.trackPhysical),
                                        trailing: const Icon(
                                          Icons.arrow_forward_ios,
                                          color: ColorResource.colorF58220,
                                          size: 18,
                                        ),
                                        onTap: () async {
                                          await Navigator.pushNamed(context,
                                              AppRoutes.walletClosureScreen);
                                        },
                                      ),
                                    ),
                                    Visibility(
                                      visible: bloc.isRequestedCard,
                                      child: Container(
                                        padding: const EdgeInsets.only(
                                            left: 16, right: 16),
                                        child: ListTile(
                                          title: Transform(
                                            transform:
                                                Matrix4.translationValues(
                                                    -18, 0.0, 0.0),
                                            child: CustomText(
                                              StringResource.trackPhysical,
                                              fontSize: 16,
                                              color: ColorResource.color222222,
                                            ),
                                          ),
                                          leading: Image.asset(
                                              ImageResource.trackPhysical),
                                          trailing: const Icon(
                                            Icons.arrow_forward_ios,
                                            color: ColorResource.colorF58220,
                                            size: 18,
                                          ),
                                          onTap: () async {
                                            await Navigator.pushNamed(context,
                                                AppRoutes.trackShipmentScreen);
                                          },
                                        ),
                                      ),
                                    ),
                                    Visibility(
                                      visible: bloc.isRequestedCard,
                                      child: Container(
                                        padding: const EdgeInsets.only(
                                            top: 10,
                                            left: 16,
                                            right: 16,
                                            bottom: 10),
                                        child: ListTile(
                                          title: Transform(
                                            transform:
                                                Matrix4.translationValues(
                                                    -18, 0.0, 0.0),
                                            child: CustomText(
                                              StringResource.generate,
                                              fontSize: 16,
                                              color: ColorResource.color222222,
                                            ),
                                          ),
                                          leading: Image.asset(
                                              ImageResource.generate),
                                          trailing: const Icon(
                                            Icons.arrow_forward_ios,
                                            color: ColorResource.colorF58220,
                                            size: 18,
                                          ),
                                          onTap: () async {
                                            // await Navigator.pushNamed(
                                            //     context,
                                            //     AppRoutes
                                            //         .requestPhysicalCardScreen);
                                          },
                                        ),
                                      ),
                                    ),
                                    Visibility(
                                      visible: !bloc.isRequestedCard,
                                      child: GestureDetector(
                                        onTap: () {
                                          Navigator.pushNamed(
                                              context,
                                              AppRoutes
                                                  .requestPhysicalCardScreen);
                                        },
                                        child: Container(
                                          padding: const EdgeInsets.only(
                                              left: 16, right: 16, bottom: 16),
                                          child: ListTile(
                                            title: Transform(
                                              transform:
                                                  Matrix4.translationValues(
                                                      -18, 0.0, 0.0),
                                              child: CustomText(
                                                StringResource
                                                    .requestPhysicalCard,
                                                fontSize: 16,
                                                color:
                                                    ColorResource.color222222,
                                              ),
                                            ),
                                            leading: Image.asset(ImageResource
                                                .requestPhysicalcard),
                                            trailing: Icon(
                                              Icons.arrow_forward_ios,
                                              color: ColorResource.colorF58220,
                                              size: 18,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ]),
                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.only(
                                    left: 70, right: 70, top: 30),
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(24),
                                      image: const DecorationImage(
                                        image: AssetImage(
                                            ImageResource.wallet_background),
                                        fit: BoxFit.cover,
                                      ),
                                      boxShadow: [
                                        BoxShadow(
                                          blurRadius: 12.0,
                                          offset: Offset(0, 4),
                                          // spreadRadius: 1,
                                          color: ColorResource.color7090b0
                                              .withOpacity(0.2),
                                        ),
                                      ]),
                                  child: Stack(
                                    children: [
                                      Image.asset(
                                        ImageResource.dashboardScreencardbg1,
                                        fit: BoxFit.contain,
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding:
                                                const EdgeInsets.only(left: 16),
                                            child: CustomText(
                                              StringResource.pocketWallet,
                                              fontSize: 18,
                                              lineHeight: 2.5,
                                              fontWeight: FontWeight.w600,
                                              color: ColorResource.color4F0437,
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              top: 2,
                                              left: 16,
                                            ),
                                            child: GestureDetector(
                                              child: Row(
                                                children: [
                                                  CustomText(
                                                    '4333333337766',
                                                    color: ColorResource
                                                        .color4F0437,
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 8),
                                                    child: Image.asset(
                                                        ImageResource.copyNo),
                                                  ),
                                                ],
                                              ),
                                              onTap: () {
                                                _copyToClipboard();
                                              },
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                top: 32, left: 16),
                                            child: CustomText(
                                              StringResource.walletBalance,
                                              // lineHeight: 1.8,
                                              color: ColorResource.color787878,
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 16, top: 6),
                                            child: CustomText(
                                              '₹2000.00',
                                              fontSize: 24,
                                              fontWeight: FontWeight.w800,
                                              color: ColorResource.color222222,
                                            ),
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 19, right: 19),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                CustomText(
                                                  Languages.of(context)!
                                                      .validFrom,
                                                  color:
                                                      ColorResource.color787878,
                                                  fontSize: 12,
                                                ),
                                                CustomText(
                                                  Languages.of(context)!
                                                      .validThru,
                                                  color:
                                                      ColorResource.color787878,
                                                  fontSize: 12,
                                                ),
                                              ],
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 19, right: 19),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                CustomText(
                                                  Languages.of(context)!
                                                      .validToDate,
                                                  color:
                                                      ColorResource.color222222,
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                                CustomText(
                                                  Languages.of(context)!
                                                      .validFromDate,
                                                  color:
                                                      ColorResource.color222222,
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ],
                                            ),
                                          ),
                                          const SizedBox(
                                            height: 35,
                                          ),
                                          if (!viewCvv)
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Image.asset(ImageResource.cvv),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 7),
                                                  child: GestureDetector(
                                                    child: CustomText(
                                                        Languages.of(context)!
                                                            .viewCvv,
                                                        isUnderLine: true),
                                                    onTap: () {
                                                      setState(() {
                                                        if (viewCvv) {
                                                          viewCvv = false;
                                                        } else {
                                                          viewCvv = true;
                                                        }
                                                      });
                                                    },
                                                  ),
                                                ),
                                              ],
                                            ),
                                          if (viewCvv)
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 7),
                                                  child: GestureDetector(
                                                    child: Row(
                                                      children: [
                                                        CustomText(
                                                          Languages.of(context)!
                                                              .cvv,
                                                          style: Theme.of(
                                                                  context)
                                                              .textTheme
                                                              .subtitle2!
                                                              .copyWith(
                                                                  color: ColorResource
                                                                      .color787878,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400),
                                                        ),
                                                        const SizedBox(
                                                          width: 6,
                                                        ),
                                                        Container(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal: 8,
                                                                  vertical: 3),
                                                          /*color: ColorResource.colorFFEBDB,*/
                                                          decoration:
                                                              BoxDecoration(
                                                            color: ColorResource
                                                                .colorFFEBDB,
                                                            shape: BoxShape
                                                                .rectangle,
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        4),
                                                          ),
                                                          child: CustomText(
                                                            '2 3 4',
                                                            style: Theme.of(
                                                                    context)
                                                                .textTheme
                                                                .subtitle1!
                                                                .copyWith(
                                                                    color: ColorResource
                                                                        .color641653,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w800),
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                    onTap: () {
                                                      setState(() {
                                                        if (viewCvv) {
                                                          viewCvv = false;
                                                        } else {
                                                          viewCvv = true;
                                                        }
                                                      });
                                                    },
                                                  ),
                                                ),
                                              ],
                                            ),
                                          const SizedBox(
                                            height: 20,
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                )),
          );
        },
      ),
    );
  }

  Future<void> _copyToClipboard() async {
    await Clipboard.setData(ClipboardData(text: _copy));
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: CustomText(
        Languages.of(context)!.copyToClipboard,
        color: ColorResource.colorFFFFFF,
      ),
    ));
  }
}
