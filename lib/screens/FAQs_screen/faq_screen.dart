import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:icici/Model/faq_support_model.dart';
import 'package:icici/utils/app_utils.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/font.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:icici/widgets/custom_button.dart';

import 'bloc/faqs_bloc.dart';

class FaqScreen extends StatefulWidget {
  @override
  _FaqScreenState createState() => _FaqScreenState();
}

class _FaqScreenState extends State<FaqScreen> {
  late FaqsBloc bloc;
  final searchController = TextEditingController();
  bool assistantMessage = true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc = FaqsBloc()..add(FaqsInitialEvent());
  }

  onSearchTextChanged(String text) async {
    _searchResult.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    bloc.categiry_1.forEach((FaqModel faqList) {
      if (faqList.header.toLowerCase().contains(text.toLowerCase())) {
        _searchResult.add(faqList);
      }
    });

    setState(() {});
  }

  List<FaqModel> _searchResult = [];

  @override
  Widget build(BuildContext context) {
    return BlocListener<FaqsBloc, FaqsState>(
      bloc: bloc,
      listener: (BuildContext context, FaqsState state) {
        // TODO: implement listener
      },
      child: BlocBuilder<FaqsBloc, FaqsState>(
        bloc: bloc,
        builder: (BuildContext context, FaqsState state) {
          return SafeArea(
            top: false,
            child: Scaffold(
              backgroundColor: ColorResource.colorFAFAFA,
              floatingActionButton: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  assistantMessage ? initialMessage() : SizedBox(),
                  FloatingActionButton(
                    child: Image.asset(ImageResource.chat),
                    elevation: 15,
                    splashColor: ColorResource.colorFF781F,
                    foregroundColor: ColorResource.colorFF781F,
                    highlightElevation: 7,
                    onPressed: () {
                      AppUtils.showToast('Chat', gravity: ToastGravity.BOTTOM);
                    },
                  ),
                ],
              ),
              body: Column(
                children: [
                  CustomAppbar(
                    backgroundColor: ColorResource.color641653,
                    titleString: StringResource.fqa,
                    titleSpacing: 8,
                    style: Theme.of(context)
                        .textTheme
                        .headline5!
                        .copyWith(color: ColorResource.colorFFFFFF),
                    iconEnumValues: IconEnum.back,
                    showSearch: true,
                    controller: searchController,
                    onChanged: onSearchTextChanged,
                    onItemSelected: (dynamic values) {
                      if (values == 'IconEnum.back') {
                        Navigator.pop(context);
                      }
                    },
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
                      child: searchController.text.isNotEmpty
                          ? CustomScrollView(
                              slivers: [
                                SliverList(
                                  delegate: SliverChildBuilderDelegate(
                                      (BuildContext context, int index) {
                                    return _searchList(_searchResult, index);
                                  }, childCount: _searchResult.length),
                                ),
                              ],
                            )
                          : CustomScrollView(
                              slivers: [
                                SliverList(
                                  delegate: SliverChildBuilderDelegate(
                                      (BuildContext context, int index) {
                                    return _faqs(bloc.categiry_1, index);
                                  }, childCount: bloc.categiry_1.length),
                                ),
                                SliverToBoxAdapter(
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        top: 30, bottom: 25),
                                    child: chatWithUs(),
                                  ),
                                )
                              ],
                            ),
                    ),
                  ),

                  // Expanded(
                  //   child: SingleChildScrollView(
                  //     padding: const EdgeInsetsDirectional.all(12),
                  //     child: Column(
                  //       crossAxisAlignment: CrossAxisAlignment.start,
                  //       children: [
                  //         Column(children: _faqs(),),

                  //         const SizedBox(
                  //           height: 30,
                  //         ),
                  //         chatWithUs(),
                  //         const SizedBox(
                  //           height: 25,
                  //         ),
                  //       ],
                  //     ),
                  //   ),
                  //   ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  _faqs(List<FaqModel> categiry_1, int index) {
    int listcount = index;
    listcount++;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        categiry_1[index].category != null
            ? Padding(
                padding: const EdgeInsets.only(top: 25),
                child: CustomText(
                  categiry_1[index].category!,
                  style: Theme.of(context)
                      .textTheme
                      .headline5!
                      .copyWith(fontSize: 20.0, fontWeight: FontWeight.w700),
                ),
              )
            : SizedBox(),
        const SizedBox(
          height: 10,
        ),
        Container(
          margin: const EdgeInsets.only(bottom: 12),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(14),
            color: ColorResource.colorFFFFFF,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.2),
                spreadRadius: 2,
                blurRadius: 3,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
          child: Padding(
            padding: const EdgeInsets.fromLTRB(10, 3, 14, 12),
            child: Theme(
              data: ThemeData().copyWith(dividerColor: Colors.transparent),
              child: ExpansionTile(
                tilePadding: const EdgeInsetsDirectional.all(0),
                title: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomText(
                      '$listcount' + '.',
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Flexible(
                      child: CustomText(
                        categiry_1[index].header,
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    ),
                  ],
                ),
                iconColor: Colors.orange,
                collapsedIconColor: Colors.orange,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(24, 5, 28, 12),
                    child: CustomText(
                      categiry_1[index].description,
                      style: Theme.of(context).textTheme.subtitle1!.copyWith(
                          color: ColorResource.color787878, height: 1.5),
                    ),
                  )
                ],
                onExpansionChanged: (bool status) {
                  setState(() {
                    // ignore: lines_longer_than_80_chars
                    categiry_1[index].expanded = !categiry_1[index].expanded;
                  });
                },
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _searchList(List<FaqModel> categiry_1, int index) {
    int listcount = index;
    listcount++;
    return Container(
      margin: const EdgeInsets.only(top: 12),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(14),
        color: ColorResource.colorFFFFFF,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.2),
            spreadRadius: 2,
            blurRadius: 3,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(10, 3, 14, 12),
        child: Theme(
          data: ThemeData().copyWith(dividerColor: Colors.transparent),
          child: ExpansionTile(
            tilePadding: const EdgeInsetsDirectional.all(0),
            title: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomText(
                  '$listcount',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                const SizedBox(
                  width: 10,
                ),
                Flexible(
                  child: CustomText(
                    categiry_1[index].header,
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ),
              ],
            ),
            // trailing: itemData[index].expanded?
            //Icon(Icons.expand_less, size: 30,):
            //Icon(Icons.expand_more, size: 30,),
            iconColor: Colors.orange,
            collapsedIconColor: Colors.orange,
            // childrenPadding:
            // const EdgeInsetsDirectional.fromSTEB(24, 5, 28, 12),
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(24, 5, 28, 12),
                child: CustomText(
                  categiry_1[index].description,
                  style: Theme.of(context)
                      .textTheme
                      .subtitle1!
                      .copyWith(color: ColorResource.color787878, height: 1.5),
                ),
              )
            ],
            onExpansionChanged: (bool status) {
              setState(() {
                // ignore: lines_longer_than_80_chars
                categiry_1[index].expanded = !categiry_1[index].expanded;
              });
            },
          ),
        ),
      ),
    );
  }

  Widget initialMessage() {
    return Container(
      margin: EdgeInsets.fromLTRB(70, 0, 24, 6),
      decoration: BoxDecoration(
          color: ColorResource.color641653,
          borderRadius: BorderRadius.circular(14.0)),
      child: Padding(
        padding: EdgeInsetsDirectional.all(11),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Flexible(
              child: CustomText(
                "Hello, I'm Kia. I will be your virtual assistant today.",
                style: Theme.of(context).textTheme.subtitle1,
              ),
            ),
            SizedBox(
              width: 7,
            ),
            GestureDetector(
              child: Icon(
                Icons.close,
                color: ColorResource.colorFFFFFF,
                size: 19,
              ),
              onTap: () {
                setState(() {
                  assistantMessage = false;
                });
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget chatWithUs() {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          color: ColorResource.colorf5f5f7,
          borderRadius: BorderRadius.all(
            Radius.circular(16.0),
          )),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(24, 16, 16, 10),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          CustomText(StringResource.findSolution,
              style: Theme.of(context).textTheme.headline5),
          SizedBox(
            height: 6,
          ),
          CustomText(
            StringResource.faqNote,
            style: Theme.of(context)
                .textTheme
                .subtitle1!
                .copyWith(color: ColorResource.color787878, height: 1.7),
          ),
          SizedBox(
            height: 15,
          ),
          SizedBox(
            width: 135,
            child: CustomButton(
              StringResource.chatWithUs,
              fontSize: FontSize.twelve,
              textColor: ColorResource.colorFF781F,
              buttonBackgroundColor: ColorResource.colorf5f5f7,
              onTap: () async {},
            ),
          ),
        ]),
      ),
    );
  }
}
