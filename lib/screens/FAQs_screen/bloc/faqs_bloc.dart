import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:icici/Model/faq_support_model.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:meta/meta.dart';

part 'faqs_event.dart';
part 'faqs_state.dart';

class FaqsBloc extends Bloc<FaqsEvent, FaqsState> {
  FaqsBloc() : super(FaqsInitial());
    List<FaqModel> categiry_1 = [];

  @override
  Stream<FaqsState> mapEventToState(
    FaqsEvent event,
  ) async* {
    if (event is FaqsInitialEvent) {
      yield FaqsLoadedState();

      categiry_1.addAll([
        FaqModel(
          category: 'Categor_01',
      header: 'I can see varius cards on my dashboard upon login, what are those cards?',
      description:
          "Android is a mobile operating system based on a modified version of the Linux kernel and other open source software, designed primarily for touchscreen mobile devices such as smartphones and tablets. ...  ",
    ),
    FaqModel(
      header: 'where i can use this card? it is valid only on the app?',
      description:
          "Designed primarily for touchscreen mobile devices such as smartphones and tablets. ...  ",
    ),
    FaqModel(
      header: 'How do i load money into my prepaid wallet for transactions?',
      description:
          "Linux kernel and other open source software, designed primarily for touchscreen mobile devices such as smartphones and tablets. ...  ",
    ),
     FaqModel(
          category: 'Categor_02',
      header: 'I can see varius cards on my dashboard upon login, what are those cards?',
      description:
          "Android is a mobile operating system based on a modified version of the Linux kernel and other open source software, designed primarily for touchscreen mobile devices such as smartphones and tablets. ...  ",
    ),
    FaqModel(
      header: 'where i can use this card? it is valid only on the app?',
      description:
          "Designed primarily for touchscreen mobile devices such as smartphones and tablets. ...  ",
    ),
    FaqModel(
      header: 'How do i load money into my prepaid wallet for transactions?',
      description:
          "Linux kernel and other open source software, designed primarily for touchscreen mobile devices such as smartphones and tablets. ...  ",
    ),
      ]);
    }
  }
}
