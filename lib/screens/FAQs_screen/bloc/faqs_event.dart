part of 'faqs_bloc.dart';

@immutable
abstract class FaqsEvent extends BaseEquatable {}

class FaqsInitialEvent extends FaqsEvent {}
