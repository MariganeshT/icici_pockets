part of 'faqs_bloc.dart';

@immutable
abstract class FaqsState {}

class FaqsInitial extends FaqsState {}

class FaqsLoadingState extends FaqsState {}

class FaqsLoadedState extends FaqsState {}
