import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/model/track_service_request_model.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/font.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:icici/widgets/custom_button.dart';
import '../../router.dart';
import 'bloc/track_service_request_bloc.dart';

class TrackServiceRequestScreen extends StatefulWidget {
  const TrackServiceRequestScreen({Key? key}) : super(key: key);

  @override
  _TrackServiceRequestScreenState createState() =>
      _TrackServiceRequestScreenState();
}

class _TrackServiceRequestScreenState extends State<TrackServiceRequestScreen> {
  late TrackServiceRequestBloc bloc;

  bool isEmpty = false;

  TabController? _controller;
  int _selectedIndex = 0;

  @override
  void initState() {
    super.initState();
    bloc = TrackServiceRequestBloc()..add(TrackServiceRequestInitialEvent());
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    return Scaffold(
      backgroundColor: ColorResource.colorFAFAFA,
      body: BlocListener<TrackServiceRequestBloc, TrackServiceRequestState>(
        bloc: bloc,
        listener: (BuildContext context, TrackServiceRequestState state) {},
        child: BlocBuilder<TrackServiceRequestBloc, TrackServiceRequestState>(
          bloc: bloc,
          builder: (BuildContext context, TrackServiceRequestState state) {
            if (state is TrackServiceRequestLoadingState) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            return Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                CustomAppbar(
                  backgroundColor: ColorResource.color641653,
                  titleString: Languages.of(context)!.trackServiceRequest,
                  titleSpacing: 8,
                  style: Theme.of(context)
                      .textTheme
                      .headline5!
                      .copyWith(color: ColorResource.colorFFFFFF),
                  iconEnumValues: IconEnum.back,
                  onItemSelected: (selectedItem) {
                    Navigator.pop(context);
                  },
                ),
                Expanded(
                  child: DefaultTabController(
                      length: 2,
                      child: Column(children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(38, 24, 39, 0),
                          child: Container(
                            height: 46,
                            decoration: BoxDecoration(
                                color:
                                    ColorResource.colorBFCBD7.withOpacity(0.2),
                                borderRadius: const BorderRadius.all(
                                  Radius.circular(12.0),
                                )),
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  top: 4.0, bottom: 3, left: 6, right: 6),
                              child: TabBar(
                                controller: _controller,
                                labelColor: ColorResource.color222222,
                                unselectedLabelColor: ColorResource.color787878,
                                labelStyle: const TextStyle(
                                    fontSize: 14.0,
                                    fontFamily: 'SourceSansPro',
                                    fontWeight: FontWeight.w700),
                                unselectedLabelStyle: const TextStyle(
                                    fontSize: 14.0,
                                    fontFamily: 'SourceSansPro',
                                    fontWeight: FontWeight.w600),
                                indicator: BoxDecoration(
                                    borderRadius: BorderRadius.circular(12),
                                    color: ColorResource.colorFFFFFF),
                                tabs: const <Widget>[
                                  Tab(text: 'Open Request'),
                                  Tab(text: 'Closed Request'),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: TabBarView(children: <Widget>[
                            isEmpty
                                ? noOpenRequest()
                                : ListView.builder(
                                    itemCount: bloc.openRequest.length,
                                    shrinkWrap: true,
                                    itemBuilder:
                                        (BuildContext context, int index) =>
                                            buildOpenRequestWidget(
                                                bloc.openRequest[index]),
                                  ),
                            isEmpty
                                ? noCloseRequest()
                                : ListView.builder(
                                    itemCount: bloc.closedRequest.length,
                                    shrinkWrap: true,
                                    itemBuilder:
                                        (BuildContext context, int index) =>
                                            buildClosedRequestWidget(
                                                bloc.closedRequest[index]),
                                  ),
                          ]),
                        )
                      ])),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 12, right: 12, bottom: 24),
                  child: supportWidget(),
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  Widget buildClosedRequestWidget(TrackServiceRequest element) {
    return Padding(
      padding: const EdgeInsets.only(left: 12.0, right: 12, bottom: 12),
      child: Container(
        decoration: const BoxDecoration(
            color: ColorResource.colorFFFFFF,
            borderRadius: BorderRadius.all(
              Radius.circular(19.0),
            )),
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  CustomText(
                    Languages.of(context)!.serviceRequestNo,
                    style: Theme.of(context)
                        .textTheme
                        .subtitle1!
                        .copyWith(color: ColorResource.color787878),
                  ),
                  Container(
                      height: 24,
                      width: 68,
                      decoration: const BoxDecoration(
                          color: ColorResource.colorFFF3F3,
                          borderRadius: BorderRadius.all(
                            Radius.circular(19.0),
                          )),
                      child: Center(
                        child: CustomText(
                          Languages.of(context)!.closed,
                          style: Theme.of(context)
                              .textTheme
                              .subtitle1!
                              .copyWith(color: ColorResource.colorF92538),
                        ),
                      )),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0, bottom: 6),
                child: CustomText(
                  element.number,
                  style: Theme.of(context).textTheme.subtitle1!.copyWith(
                      color: ColorResource.color222222,
                      fontWeight: FontWeight.w600),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 16.0),
                child: CustomText(
                  element.date,
                  style: Theme.of(context)
                      .textTheme
                      .subtitle1!
                      .copyWith(color: ColorResource.color787878),
                ),
              ),
              const Divider(),
              Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: CustomText(
                  Languages.of(context)!.description,
                  style: Theme.of(context)
                      .textTheme
                      .subtitle1!
                      .copyWith(color: ColorResource.colorFF781F),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0, bottom: 6),
                child: CustomText(
                  element.upi,
                  style: Theme.of(context).textTheme.subtitle1!.copyWith(
                      color: ColorResource.color222222,
                      fontWeight: FontWeight.w600),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 16.0),
                child: CustomText(
                  element.details,
                  style: Theme.of(context).textTheme.subtitle1!.copyWith(
                      color: ColorResource.color222222,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget supportWidget() {
    return Container(
      decoration: const BoxDecoration(
          color: ColorResource.colorf5f5f7,
          borderRadius: BorderRadius.all(
            Radius.circular(16.0),
          )),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(12, 16, 12, 0),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          CustomText(StringResource.haveMoreQuestion,
              style: Theme.of(context).textTheme.headline5),
          const SizedBox(
            height: 6,
          ),
          CustomText(
            Languages.of(context)!.ourSupport,
            style: Theme.of(context).textTheme.subtitle1!.copyWith(
                  color: ColorResource.color787878,
                ),
          ),
          const SizedBox(
            height: 10,
          ),
          SizedBox(
            width: 135,
            child: CustomButton(
              Languages.of(context)!.gotoSupport,
              fontSize: FontSize.twelve,
              textColor: ColorResource.colorFF781F,
              buttonBackgroundColor: ColorResource.colorf5f5f7,
              onTap: () {
                Navigator.pushNamed(context, AppRoutes.supportScreen);
              },
            ),
          ),
        ]),
      ),
    );
  }

  Widget noOpenRequest() {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          const Image(image: AssetImage(ImageResource.track)),
          Padding(
            padding: const EdgeInsets.only(top: 37.0, bottom: 12),
            child: CustomText(Languages.of(context)!.noOpenRequestFound,
                style: Theme.of(context).textTheme.headline5),
          ),
          CustomText(
            'Dummy content, will be added later some time',
            style: Theme.of(context).textTheme.subtitle1!.copyWith(
                  color: ColorResource.color222222.withOpacity(0.5),
                ),
          ),
        ],
      ),
    );
  }

  Widget noCloseRequest() {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          const Image(image: AssetImage(ImageResource.track)),
          Padding(
            padding: const EdgeInsets.only(top: 37.0, bottom: 12),
            child: CustomText('No Close Request Found',
                style: Theme.of(context).textTheme.headline5),
          ),
          CustomText(
            'Dummy content, will be added later some time',
            style: Theme.of(context).textTheme.subtitle1!.copyWith(
                  color: ColorResource.color222222.withOpacity(0.5),
                ),
          ),
        ],
      ),
    );
  }

  Widget buildOpenRequestWidget(TrackServiceRequest element) {
    return Padding(
      padding: const EdgeInsets.only(left: 12.0, right: 12, bottom: 12),
      child: Container(
        decoration: const BoxDecoration(
            color: ColorResource.colorFFFFFF,
            borderRadius: BorderRadius.all(
              Radius.circular(19.0),
            )),
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  CustomText(
                    Languages.of(context)!.serviceRequestNo,
                    style: Theme.of(context)
                        .textTheme
                        .subtitle1!
                        .copyWith(color: ColorResource.color787878),
                  ),
                  Container(
                      height: 24,
                      width: 68,
                      decoration: const BoxDecoration(
                          color: ColorResource.colorFFF3F3,
                          borderRadius: BorderRadius.all(
                            Radius.circular(19.0),
                          )),
                      child: Center(
                        child: CustomText(
                          Languages.of(context)!.open,
                          style: Theme.of(context)
                              .textTheme
                              .subtitle1!
                              .copyWith(color: ColorResource.colorFF781F),
                        ),
                      )),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0, bottom: 6),
                child: CustomText(
                  element.number,
                  style: Theme.of(context).textTheme.subtitle1!.copyWith(
                      color: ColorResource.color222222,
                      fontWeight: FontWeight.w600),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 16.0),
                child: CustomText(
                  element.date,
                  style: Theme.of(context)
                      .textTheme
                      .subtitle1!
                      .copyWith(color: ColorResource.color787878),
                ),
              ),
              const Divider(),
              Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: CustomText(
                  Languages.of(context)!.description,
                  style: Theme.of(context)
                      .textTheme
                      .subtitle1!
                      .copyWith(color: ColorResource.colorFF781F),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0, bottom: 6),
                child: CustomText(
                  element.upi,
                  style: Theme.of(context).textTheme.subtitle1!.copyWith(
                      color: ColorResource.color222222,
                      fontWeight: FontWeight.w600),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 16.0),
                child: CustomText(
                  element.details,
                  style: Theme.of(context).textTheme.subtitle1!.copyWith(
                      color: ColorResource.color222222,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
