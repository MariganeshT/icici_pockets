part of 'track_service_request_bloc.dart';

@immutable
abstract class TrackServiceRequestEvent extends BaseEquatable {}

class TrackServiceRequestInitialEvent extends TrackServiceRequestEvent {}
