part of 'track_service_request_bloc.dart';

@immutable
abstract class TrackServiceRequestState extends BaseEquatable {}

class TrackServiceRequestInitial extends TrackServiceRequestState {}

class TrackServiceRequestLoadedState extends TrackServiceRequestState {}

class TrackServiceRequestLoadingState extends TrackServiceRequestState {}
