import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:icici/model/track_service_request_model.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:meta/meta.dart';

part 'track_service_request_event.dart';
part 'track_service_request_state.dart';

class TrackServiceRequestBloc
    extends Bloc<TrackServiceRequestEvent, TrackServiceRequestState> {
  TrackServiceRequestBloc() : super(TrackServiceRequestInitial());

  List<TrackServiceRequest> openRequest = [];
  List<TrackServiceRequest> closedRequest = [];

  @override
  Stream<TrackServiceRequestState> mapEventToState(
    TrackServiceRequestEvent event,
  ) async* {
    if (event is TrackServiceRequestInitialEvent) {
      yield TrackServiceRequestLoadingState();
      openRequest.addAll([
        TrackServiceRequest('PKT0001234', 'May 29 2018, 2:50 PM',
            'UPI -123818868387, ', 'Anoopjoy@paytm, ANOOPJOY'),
        TrackServiceRequest('PKT0001234', 'May 29 2018, 2:50 PM',
            'UPI -123818868387, ', 'Anoopjoy@paytm, ANOOPJOY'),
      ]);
      closedRequest.addAll([
        TrackServiceRequest('PKT0001234', 'May 29 2018, 2:50 PM',
            'UPI -123818868387, ', 'Anoopjoy@paytm, ANOOPJOY'),
        TrackServiceRequest('PKT0001234', 'May 29 2018, 2:50 PM',
            'UPI -123818868387, ', 'Anoopjoy@paytm, ANOOPJOY'),
      ]);
      yield TrackServiceRequestLoadedState();
    }
  }
}
