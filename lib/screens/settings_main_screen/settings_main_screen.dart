import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/screens/logout_screen.dart/LogoutScreen.dart';
import 'package:icici/screens/settings_main_screen/bloc/settings_main_screen_bloc.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:package_info_plus/package_info_plus.dart';

import '../../router.dart';

class SettingsMainScreen extends StatefulWidget {
  const SettingsMainScreen({Key? key}) : super(key: key);

  @override
  _SettingsMainScreenState createState() => _SettingsMainScreenState();
}

class _SettingsMainScreenState extends State<SettingsMainScreen> {
  bool status = false;
  String version = "";

  late SettingsMainScreenBloc bloc;

  @override
  void initState() {
    _initPackageInfo();
    super.initState();
    bloc = BlocProvider.of<SettingsMainScreenBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    return SafeArea(
        top: false,
        child: Scaffold(
            backgroundColor: ColorResource.colorFAFAFA,
            body: BlocListener<SettingsMainScreenBloc, SettingsMainScreenState>(
                bloc: bloc,
                listener:
                    (BuildContext context, SettingsMainScreenState state) {
                  if (state is ReferAndEarnScreenState) {
                    Navigator.pushNamed(context, AppRoutes.referAndEarn);
                  }
                  if (state is ManageNotificationState) {
                    Navigator.pushNamed(
                        context, AppRoutes.manageNotificationScreen);
                  }
                  if (state is LogoutState) {
                    logout(context);
                  }
                  if (state is NavigateFaqScreenState) {
                    Navigator.pushNamed(context, AppRoutes.faqScreen);
                  }
                  if (state is NavigateSupportScreenState) {
                    Navigator.pushNamed(context, AppRoutes.supportScreen);
                  }
                  if (state is NavigateChangePasscodeScreenState) {
                    Navigator.pushNamed(
                        context, AppRoutes.changePasscodeScreen);
                  }
                  if (state is NavigateReturnUserScreenState) {
                    Navigator.pushNamed(context, AppRoutes.returningUserScreen);
                  }

                  if (state is NavigationWebViewState) {
                    Navigator.pushNamed(context, AppRoutes.webViewScreen,
                        arguments: state.webviewUrl);
                  }
                },
                child: BlocBuilder<SettingsMainScreenBloc,
                        SettingsMainScreenState>(
                    bloc: bloc,
                    builder:
                        (BuildContext context, SettingsMainScreenState state) {
                      if (state is SettingsMainScreenLoadingState) {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                      return Column(children: <Widget>[
                        SizedBox(
                          child: CustomAppbar(
                            backgroundColor: ColorResource.color641653,
                            titleString: StringResource.settings,
                            titleSpacing: 8,
                            style: Theme.of(context)
                                .textTheme
                                .headline5!
                                .copyWith(color: ColorResource.colorFFFFFF),
                            iconEnumValues: IconEnum.back,
                            onItemSelected: (selectedItem) {
                              Navigator.pop(context);
                            },
                          ),
                        ),
                        Expanded(
                            child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 12, right: 12, top: 12),
                                child: SingleChildScrollView(
                                  child: Column(
                                      // mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            top: 24,
                                          ),
                                          child: Container(
                                            decoration: BoxDecoration(
                                              boxShadow: [
                                                const BoxShadow(
                                                  offset: Offset(0, 4),
                                                  color: Color.fromRGBO(
                                                      112, 114, 176, 0.12),
                                                  blurRadius: 12,
                                                ),
                                              ],
                                              borderRadius:
                                                  BorderRadius.circular(14),
                                              color: ColorResource.colorFFFFFF,
                                            ),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .only(left: 24),
                                                        child: CustomText(
                                                          StringResource
                                                              .overview,
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .subtitle2!
                                                                  .copyWith(
                                                                    color: ColorResource
                                                                        .color787878,
                                                                  ),
                                                        ),
                                                      ),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .only(left: 24),
                                                        child: CustomText(
                                                          StringResource
                                                              .princeanto,
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .headline5!
                                                                  .copyWith(
                                                                    color: ColorResource
                                                                        .color222222,
                                                                  ),
                                                        ),
                                                      ),
                                                    ]),
                                                // const SizedBox(
                                                //   width: 90,
                                                // ),
                                                GestureDetector(
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 24,
                                                            bottom: 24,
                                                            right: 18),
                                                    child: Container(
                                                      height: 47,
                                                      width: 122,
                                                      decoration: BoxDecoration(
                                                        color: ColorResource
                                                            .colorFFFFFF,
                                                        border: Border.all(
                                                          color: ColorResource
                                                              .colorFF781F,
                                                        ),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(8),
                                                      ),
                                                      child: Center(
                                                        child: CustomText(
                                                          StringResource
                                                              .accounts,
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .subtitle1!
                                                                  .copyWith(
                                                                    color: ColorResource
                                                                        .colorFF781F,
                                                                  ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  onTap: () {
                                                    Navigator.pushNamed(
                                                        context,
                                                        AppRoutes
                                                            .accountsScreen);
                                                  },
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 25,
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 24),
                                              child: CustomText(
                                                  StringResource.security,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyText1),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 12),
                                              child: Container(
                                                // height: 120,
                                                // width: double.infinity,
                                                decoration: BoxDecoration(
                                                  boxShadow: [
                                                    const BoxShadow(
                                                      offset: Offset(0, 4),
                                                      color: Color.fromRGBO(
                                                          112, 114, 176, 0.12),
                                                      blurRadius: 12,
                                                    ),
                                                  ],
                                                  borderRadius:
                                                      BorderRadius.circular(14),
                                                  color:
                                                      ColorResource.colorFFFFFF,
                                                ),
                                                child: Column(
                                                  children:
                                                      buildSecurityWidget(),
                                                ),
                                              ),
                                            ),
                                            const SizedBox(
                                              height: 25,
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 24),
                                              child: CustomText(
                                                  StringResource.support,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyText1),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 12),
                                              child: Container(
                                                // height: 120,
                                                width: double.infinity,
                                                decoration: BoxDecoration(
                                                  boxShadow: [
                                                    const BoxShadow(
                                                      offset: Offset(0, 4),
                                                      color: Color.fromRGBO(
                                                          112, 114, 176, 0.12),
                                                      blurRadius: 12,
                                                    ),
                                                  ],
                                                  color:
                                                      ColorResource.colorFFFFFF,
                                                  borderRadius:
                                                      BorderRadius.circular(14),
                                                ),
                                                child: Column(
                                                  children:
                                                      buildSupportWidget(),
                                                ),
                                              ),
                                            ),
                                            const SizedBox(
                                              height: 25,
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 24),
                                              child: CustomText(
                                                  StringResource.moreoptions,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyText1),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 12),
                                              child: Container(
                                                width: double.infinity,
                                                // height: 360,
                                                decoration: BoxDecoration(
                                                  boxShadow: [
                                                    const BoxShadow(
                                                      offset: Offset(0, 4),
                                                      color: Color.fromRGBO(
                                                          112, 114, 176, 0.12),
                                                      blurRadius: 12,
                                                    ),
                                                  ],
                                                  color:
                                                      ColorResource.colorFFFFFF,
                                                  borderRadius:
                                                      BorderRadius.circular(14),
                                                ),
                                                child: Column(
                                                  children:
                                                      buildMoreoptionsWidget(),
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 32, bottom: 30),
                                              child: Center(
                                                child: CustomText(
                                                  'v$version',
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .subtitle1!
                                                      .copyWith(
                                                          color: ColorResource
                                                              .color787878),
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ]),
                                )))
                      ]);
                    }))));
  }

  Future<void> _initPackageInfo() async {
    final info = await PackageInfo.fromPlatform();
    setState(() {
      version = info.version;
    });
  }

  List<Widget> buildSupportWidget() {
    List<Widget> widgets = [];
    bloc.support.forEach((element) {
      widgets.add(GestureDetector(
        onTap: element.onTap,
        child: ListTile(
            minLeadingWidth: 14,
            contentPadding: const EdgeInsets.only(top: 10, left: 24),
            isThreeLine: true,
            minVerticalPadding: 16,
            leading: SizedBox(
              width: 24,
              child: Image.asset(element.leadingImage),
            ),
            title: CustomText(
              element.title,
              style: Theme.of(context).textTheme.subtitle1!.copyWith(
                    color: ColorResource.color222222,
                  ),
            ),
            subtitle: element.subTitle != ''
                ? Padding(
                    padding: const EdgeInsets.only(top: 4.0),
                    child: CustomText(
                      element.subTitle,
                      style: Theme.of(context).textTheme.subtitle1!.copyWith(
                            color: ColorResource.color787878,
                          ),
                    ),
                  )
                : Container(),
            trailing: Padding(
              padding: const EdgeInsets.only(right: 18.0),
              child: Image.asset(element.trailing),
            )),
      ));
    });
    return widgets;
  }

  List<Widget> buildMoreoptionsWidget() {
    List<Widget> widgets = [];
    bloc.moreoptions.forEach((element) {
      widgets.add(GestureDetector(
        onTap: element.onTap,
        child: ListTile(
          minLeadingWidth: 14,
          contentPadding: const EdgeInsets.only(top: 10, left: 24),
          isThreeLine: true,
          minVerticalPadding: 16,
          leading: SizedBox(
            width: 24,
            child: Image.asset(element.leadingImage),
          ),
          title: element.isTitleColor == false
              ? CustomText(
                  element.title,
                  style: Theme.of(context).textTheme.subtitle1!.copyWith(
                        color: ColorResource.color222222,
                      ),
                )
              : CustomText(element.title,
                  style: Theme.of(context).textTheme.subtitle1!.copyWith(
                        color: ColorResource.colorf92538,
                      )),
          subtitle: element.subTitle != ''
              ? Padding(
                  padding: const EdgeInsets.only(top: 4.0),
                  child: CustomText(
                    element.subTitle,
                    style: Theme.of(context)
                        .textTheme
                        .subtitle1!
                        .copyWith(color: ColorResource.color787878),
                  ),
                )
              : Container(),
          trailing: Padding(
            padding: const EdgeInsets.only(right: 18.0),
            child: element.isTrailingIcon == true
                ? CustomText(
                    element.trailing,
                    style: Theme.of(context)
                        .textTheme
                        .subtitle1!
                        .copyWith(color: ColorResource.colorF58220),
                  )
                : Image.asset(element.trailing),
          ),
          onTap: element.onTap,
        ),
      ));
    });
    return widgets;
  }

  List<Widget> buildSecurityWidget() {
    List<Widget> widgets = [];
    bloc.security.forEach((element) {
      widgets.add(GestureDetector(
        onTap: element.onTap,
        child: ListTile(
            minLeadingWidth: 14,
            contentPadding: EdgeInsets.only(top: 10, left: 24),
            isThreeLine: true,
            minVerticalPadding: 16,
            leading: SizedBox(
              width: 24,
              child: Image.asset(element.leadingImage),
            ),
            title: CustomText(
              element.title,
              style: Theme.of(context).textTheme.subtitle1!.copyWith(
                    color: ColorResource.color222222,
                  ),
            ),
            subtitle: element.subTitle != ''
                ? Padding(
                    padding: const EdgeInsets.only(top: 4.0),
                    child: CustomText(
                      element.subTitle,
                      style: Theme.of(context)
                          .textTheme
                          .subtitle1!
                          .copyWith(color: ColorResource.color787878),
                    ),
                  )
                : Container(),
            trailing: element.isTrailingIcon == false
                ? Transform.scale(
                    scale: 0.7,
                    child: CupertinoSwitch(
                      value: bloc.isBioMetricEnabled,
                      activeColor: ColorResource.color641653,
                      onChanged: (value) {
                        bloc.add(BioAuthEvent(value));
                      },
                    ),
                  )
                //  ? CustomText(
                //    element.trailing,
                //    style: Theme.of(context)
                //    .textTheme
                //    .subtitle1!
                //    .copyWith(color: ColorResource.colorF58220),
                //  )

                : Padding(
                    padding: const EdgeInsets.only(right: 14.0),
                    child: Image.asset(element.trailing),
                  )),
      ));
    });
    return widgets;
  }

  void logout(BuildContext buildContext) {
    showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        context: buildContext,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(32.0), topRight: Radius.circular(32.0)),
        ),
        backgroundColor: ColorResource.colorFFFFFF,
        builder: (BuildContext context) {
          return LogoutScreen(bloc);
        });
  }

  // List<Widget> buildSecurityWidget() {
  //   List<Widget> widgets = [];
  //   bloc.security.forEach((element) {
  //     widgets.add(GestureDetector(
  //         onTap: element.onTap,
  //         child: Row(
  //           // mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //           children: <Widget>[
  //             Padding(
  //               padding: const EdgeInsets.only(left: 16),
  //               child: Image.asset(element.leadingImage),
  //             ),
  //             SizedBox(
  //               width: 17,
  //             ),
  //             Expanded(
  //               child: CustomText(
  //                 element.title,
  //                 style: Theme.of(context).textTheme.subtitle1!.copyWith(
  //                       color: ColorResource.color222222,
  //                     ),
  //               ),
  //             ),
  //             SizedBox(
  //               height: 50,
  //             ),
  //             element.isTrailingIcon == false
  //                 ? Transform.scale(
  //                     scale: 0.6,
  //                     child: CupertinoSwitch(
  //                       value: bloc.isBioMetricEnabled,
  //                       activeColor: ColorResource.color641653,
  //                       onChanged: (value) {
  //                         bloc.add(BioAuthEvent(value));
  //                       },
  //                     ),
  //                   )
  //                 : Padding(
  //                     padding: const EdgeInsets.only(right: 15),
  //                     child: Image.asset(element.trailing),
  //                   ),
  //           ],
  //         )));
  //   });
  //   return widgets;
  //   //       ListTile(
  //   //           minLeadingWidth: 14,
  //   //           contentPadding: EdgeInsets.only(top: 10, left: 24),
  //   //           isThreeLine: true,
  //   //           minVerticalPadding: 16,
  //   //           leading: SizedBox(
  //   //             width: 24,
  //   //             child: Image.asset(element.leadingImage),
  //   //           ),
  //   //           title: CustomText(
  //   //             element.title,
  //   //             style: Theme.of(context).textTheme.subtitle1!.copyWith(
  //   //                   color: ColorResource.color222222,
  //   //                 ),
  //   //           ),
  //   //           subtitle: element.subTitle != ''
  //   //               ? Padding(
  //   //                   padding: const EdgeInsets.only(top: 4.0),
  //   //                   child: CustomText(
  //   //                     element.subTitle,
  //   //                     style: Theme.of(context)
  //   //                         .textTheme
  //   //                         .subtitle1!
  //   //                         .copyWith(color: ColorResource.color787878),
  //   //                   ),
  //   //                 )
  //   //               : Container(),
  //   //           trailing: Padding(
  //   //             padding: const EdgeInsets.only(right: 18.0),
  //   //             child: element.isTrailingIcon == false
  //   //                 ? Transform.scale(
  //   //                     scale: 0.6,
  //   //                     child: CupertinoSwitch(
  //   //                       value: bloc.isBioMetricEnabled,
  //   //                       activeColor: ColorResource.color641653,
  //   //                       onChanged: (value) {
  //   //                         bloc.add(BioAuthEvent(value));
  //   //                       },
  //   //                     ),
  //   //                   )
  //   //                 //  ? CustomText(
  //   //                 //    element.trailing,
  //   //                 //    style: Theme.of(context)
  //   //                 //    .textTheme
  //   //                 //    .subtitle1!
  //   //                 //    .copyWith(color: ColorResource.colorF58220),
  //   //                 //  )

  //   //                 : Image.asset(element.trailing),
  //   //           )),
  //   //     ));
  //   //   });
  //   //   return widgets;
  //   // }
}
