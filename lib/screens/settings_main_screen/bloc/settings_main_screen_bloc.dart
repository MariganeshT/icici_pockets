import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:icici/Model/web_view_model.dart';
import 'package:icici/authentication/bloc/authentication_bloc.dart';
import 'package:icici/model/profile_models.dart';
import 'package:icici/screens/verify_identity_screen/verify_identity_screen.dart';
import 'package:icici/utils/app_utils.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/preference_helper.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:local_auth/local_auth.dart';
import 'package:meta/meta.dart';
import 'package:url_launcher/url_launcher.dart';

part 'settings_main_screen_event.dart';
part 'settings_main_screen_state.dart';

class SettingsMainScreenBloc
    extends Bloc<SettingsMainScreenEvent, SettingsMainScreenState> {
  AuthenticationBloc authenticationBloc;

  SettingsMainScreenBloc(this.authenticationBloc)
      : super(SettingsMainScreenInitial());
  final MyInAppBrowser browser = MyInAppBrowser();
  InAppBrowserClassOptions options = InAppBrowserClassOptions(
      crossPlatform: InAppBrowserOptions(
        hideUrlBar: true,
      ),
      inAppWebViewGroupOptions:
          InAppWebViewGroupOptions(crossPlatform: InAppWebViewOptions()));
  List<Profile> support = [];
  List<Profile> moreoptions = [];

  List<Profile> security = [];
  bool isBioMetricEnabled = false;
  String webUrl = '';

  void _launchAppLink(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      AppUtils.showToast("can't open the link");
      throw 'Could not launch $url';
    }
  }

  // _TCandUA(String url) async {
  //   if (await canLaunch(url)) {
  //     await launch(
  //       url,
  //       forceSafariVC: true,
  //       forceWebView: false,
  //       enableJavaScript: true,
  //     );
  //   } else {
  //     AppUtils.showToast("can't open the link");
  //     throw 'Could not launch $url';
  //   }
  // }

  @override
  Stream<SettingsMainScreenState> mapEventToState(
    SettingsMainScreenEvent event,
  ) async* {
    if (event is SettingsMainScreenInitialEvent) {
      yield SettingsMainScreenLoadingState();
      isBioMetricEnabled = await PreferenceHelper.getBioMetricValue();
      support.addAll([
        Profile(
          ImageResource.faq,
          StringResource.faq,
          trailing: ImageResource.arrow,
          onTap: () {
            this.add(NavigateFaqScreenEvent());
          },
        ),
        Profile(
          ImageResource.support,
          StringResource.support,
          trailing: ImageResource.arrow,
          onTap: () {
            this.add(NavigateSupportScreenEvent());
          },
        ),
      ]);
      moreoptions.addAll([
        Profile(
          ImageResource.referearn,
          StringResource.referandearn,
          trailing: ImageResource.arrow,
          onTap: () {
            this.add(ReferAndEarnScreenEvent());
          },
        ),
        Profile(
          ImageResource.rateus,
          StringResource.rateus,
          trailing: ImageResource.arrow,
          onTap: () {
            // StoreRedirect.redirect(
            //     androidAppId: "com.icicibank.pockets", iOSAppId: "1436953057");
            if (Platform.isIOS) {
              _launchAppLink(StringResource.appLinkIOS);
            } else if (Platform.isAndroid) {
              _launchAppLink(StringResource.appLinkAndroid);
            }
          },
        ),
        Profile(
          ImageResource.manage,
          StringResource.manage,
          trailing: ImageResource.arrow,
          onTap: () {
            this.add(ManageNotificationScreenEvent());
          },
        ),
        Profile(
          ImageResource.terms,
          StringResource.termsandconditions,
          trailing: ImageResource.arrow,
          onTap: () {
            WebViewModel webViewModel;
            webViewModel = WebViewModel(
                StringResource.termsAndConditionLink, 'Terms and Conditions');
            add(NavigationWebViewScreenEvent(webViewModel));
            // browser.openUrlRequest(
            //     urlRequest: URLRequest(
            //         url: Uri.parse(StringResource.termsAndConditionLink)),
            //     options: options);
            //_TCandUA(StringResource.termsAndConditionLink);
          },
        ),
        Profile(
          ImageResource.useragreement,
          StringResource.useragreement,
          trailing: ImageResource.arrow,
          onTap: () {
            WebViewModel webViewModel;
            webViewModel =
                WebViewModel('https://www.google.com/', 'User Agreement');
            add(NavigationWebViewScreenEvent(webViewModel));
            // browser.openUrlRequest(
            //     urlRequest: URLRequest(url: Uri.parse('https://flutter.dev')),
            //     options: options);
            //_TCandUA(StringResource.userAgreementLink);
          },
        ),
        Profile(ImageResource.logout, StringResource.logout,
            isTitleColor: true, isTrailingIcon: true, onTap: () {
          add(LogoutEvent());
        })
      ]);

      security.addAll([
        Profile(
          ImageResource.faceid,
          StringResource.secureface,
          // StringResource.secureface,
        ),
        Profile(ImageResource.lockpasscode, StringResource.changepasscode,
            isTrailingIcon: true, trailing: ImageResource.arrow, onTap: () {
          add(ChangePasscodeNavigationEvent());
        })
      ]);
      yield SettingsMainScreenLoadedState();
    }

    if (event is ReferAndEarnScreenEvent) {
      yield ReferAndEarnScreenState();
    }
    if (event is ManageNotificationScreenEvent) {
      yield ManageNotificationState();
    }
    if (event is LogoutEvent) {
      yield LogoutState();
    }
    if (event is NavigateSupportScreenEvent) {
      yield NavigateSupportScreenState();
    }

    if (event is ChangePasscodeNavigationEvent) {
      yield NavigateChangePasscodeScreenState();
    }

    if (event is NavigateFaqScreenEvent) {
      yield NavigateFaqScreenState();
    }
    if (event is NavigateReturnUserScreenEvent) {
      authenticationBloc.add(LoggedOut());
      // yield NavigateReturnUserScreenState();
    }

    if (event is NavigationWebViewScreenEvent) {
      yield NavigationWebViewState(event.webviewUrl);
    }

    if (event is BioAuthEvent) {
      final LocalAuthentication localAuth = LocalAuthentication();
      final List<BiometricType> availableBiometrics =
          await localAuth.getAvailableBiometrics();
      try {
        final bool didAuthenticate = await localAuth.authenticate(
          localizedReason: 'Log In to your account instantly!',
          biometricOnly: true,
        );
        if (didAuthenticate) {
          await PreferenceHelper.setBioMetricValue(event.value);
          isBioMetricEnabled = event.value;
        } else {
          // await PreferenceHelper.setBioMetricValue(false);
          // isBioMetricEnabled = false;
        }
      } on PlatformException catch (e) {
        // if (e.code == auth_error.notAvailable) {
        //   // Handle this exception here.
        // }
      }

      yield SettingsMainScreenLoadedState();
      if (event is NavigateReturnUserScreenEvent) {
        yield NavigateReturnUserScreenState();
      }
    }
  }
}
