part of 'settings_main_screen_bloc.dart';

@immutable
abstract class SettingsMainScreenState extends BaseEquatable {}

class SettingsMainScreenInitial extends SettingsMainScreenState {}

class SettingsMainScreenLoadingState extends SettingsMainScreenState {}

class SettingsMainScreenLoadedState extends SettingsMainScreenState {}

class ReferAndEarnScreenState extends SettingsMainScreenState {}

class ManageNotificationState extends SettingsMainScreenState {}

class LogoutState extends SettingsMainScreenState {}

class NavigateSupportScreenState extends SettingsMainScreenState {}

class NavigateFaqScreenState extends SettingsMainScreenState {}

class NavigateReturnUserScreenState extends SettingsMainScreenState {}

class NavigateChangePasscodeScreenState extends SettingsMainScreenState {}

class WebViewLoadingState extends SettingsMainScreenState {}

class NavigationWebViewState extends SettingsMainScreenState {
  WebViewModel webviewUrl;
  NavigationWebViewState(this.webviewUrl);
}
