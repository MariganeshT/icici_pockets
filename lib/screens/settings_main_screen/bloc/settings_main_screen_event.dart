part of 'settings_main_screen_bloc.dart';

@immutable
abstract class SettingsMainScreenEvent extends BaseEquatable {}

class SettingsMainScreenInitialEvent extends SettingsMainScreenEvent {}

class WebViewScreenInitialEvent extends SettingsMainScreenEvent {}

class ReferAndEarnScreenEvent extends SettingsMainScreenEvent {}

class ManageNotificationScreenEvent extends SettingsMainScreenEvent {}

class LogoutEvent extends SettingsMainScreenEvent {}

class NavigateSupportScreenEvent extends SettingsMainScreenEvent {}

class NavigateFaqScreenEvent extends SettingsMainScreenEvent {}

class BioAuthEvent extends SettingsMainScreenEvent {
  bool value;
  BioAuthEvent(this.value);
}

class NavigateReturnUserScreenEvent extends SettingsMainScreenEvent {}

class ChangePasscodeNavigationEvent extends SettingsMainScreenEvent {}

class NavigationWebViewScreenEvent extends SettingsMainScreenEvent {
  WebViewModel webviewUrl;
  NavigationWebViewScreenEvent(this.webviewUrl);
}
