import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/model/address_model.dart';
import 'package:icici/model/profile_address_model.dart';
import 'package:icici/router.dart';
import 'package:icici/screens/profile_screen/add_address.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:icici/widgets/custom_button.dart';

import 'bloc/regular_pockets_card_bloc.dart';

class RegularPocketsCardScreen extends StatefulWidget {
  @override
  _RegularPocketsCardScreenState createState() =>
      _RegularPocketsCardScreenState();
}

class _RegularPocketsCardScreenState extends State<RegularPocketsCardScreen> {
  late RegularPocketsCardBloc bloc;
  bool isLocationFound = true;
  AddressModel? address;
  String noAddress = 'No address';
  bool isAddressSaved = false;
  bool _isAlwaysShown = true;

  int currentPage = 0;
  CarouselControllerImpl _carouselController = CarouselControllerImpl();

  void onPageChange(int index, CarouselPageChangedReason changeReason) {
    setState(() {
      currentPage = index;
    });
  }

  @override
  void initState() {
    super.initState();
    bloc = RegularPocketsCardBloc()..add(RegularPocketsCardInitialEvent());
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: Colors.transparent));
    return Scaffold(
        backgroundColor: ColorResource.color641653,
        body: BlocListener<RegularPocketsCardBloc, RegularPocketsCardState>(
          bloc: bloc,
          listener: (BuildContext context, RegularPocketsCardState state) {
            if (state is RegularPocketsCardInitial) {
              selectAddressBottomSheet();
            }
          },
          child: BlocBuilder<RegularPocketsCardBloc, RegularPocketsCardState>(
            bloc: bloc,
            builder: (BuildContext context, RegularPocketsCardState state) {
              return Container(
                color: ColorResource.color641653,
                child: MediaQuery.removePadding(
                  context: context,
                  removeRight: true,
                  removeBottom: true,
                  removeLeft: true,
                  child: Column(children: <Widget>[
                    CustomAppbar(
                      titleString:
                          Languages.of(context)!.requestForPhysicalCard,
                      iconEnumValues: IconEnum.back,
                      titleSpacing: 8,
                      onItemSelected: (values) async {
                        Navigator.pop(context);
                      },
                    ),
                    Expanded(
                      child: Column(children: <Widget>[
                        Expanded(
                          child: CarouselSlider(
                            carouselController: _carouselController,
                            options: CarouselOptions(
                                onPageChanged: onPageChange,
                                disableCenter: true,
                                pageSnapping: false,
                                viewportFraction: 0.70,
                                enableInfiniteScroll: false),
                            items: [
                              Image.asset(ImageResource.regular),
                              Image.asset(ImageResource.expression),
                            ],
                          ),
                        ),
                        const SizedBox(height: 10),
                        Expanded(
                            child: SizedBox(
                          width: MediaQuery.of(context).size.width,
                          height: 400,
                          child: Container(
                            decoration: const BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(14),
                                    topRight: Radius.circular(14)),
                                color: ColorResource.colorFFFFFF),
                            child: currentPage == 0
                                ? regularPocketsCardWidget()
                                : expressionPocketsCardWidget(),
                          ),
                        ))
                      ]),
                    ),
                  ]),
                ),
              );
            },
          ),
        ));
  }

  Widget regularPocketsCardWidget() {
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 12.0),
            child: CustomText(Languages.of(context)!.regularPocketsCard,
                style: Theme.of(context).textTheme.headline5!.copyWith(
                    color: ColorResource.color222222,
                    fontWeight: FontWeight.w700)),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const SizedBox(
                  height: 18,
                  child: Center(
                    child: Icon(
                      Icons.circle_sharp,
                      size: 10,
                    ),
                  ),
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: CustomText(
                      // Languages.of(context)!.weHappy,
                      'Use the card for all your shopping requirement at stores.',
                      style: Theme.of(context).textTheme.subtitle1!.copyWith(
                            color: ColorResource.color222222.withOpacity(0.5),
                          )),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: <Widget>[
                const Icon(
                  Icons.circle_sharp,
                  size: 10,
                ),
                const SizedBox(width: 10),
                CustomText(
                    // Languages.of(context)!.weHappy,
                    'Fees: ₹100.00+GST.',
                    style: Theme.of(context).textTheme.subtitle1!.copyWith(
                          color: ColorResource.color222222.withOpacity(0.5),
                        )),
              ],
            ),
          ),
          const Spacer(),
          Flexible(
            child: CustomButton(
              Languages.of(context)!.applyNow,
              onTap: () {
                // Navigator.pop(context);
                selectAddressBottomSheet();
              },
            ),
          )
        ],
      ),
    );
  }

  Widget expressionPocketsCardWidget() {
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: Column(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            // mainmkAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 16.0),
                child: CustomText(Languages.of(context)!.expressionPocketsCard,
                    style: Theme.of(context).textTheme.headline5!.copyWith(
                        color: ColorResource.color222222,
                        fontWeight: FontWeight.w700)),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    const SizedBox(
                      height: 18,
                      child: Center(
                        child: Icon(
                          Icons.circle_sharp,
                          size: 10,
                        ),
                      ),
                    ),
                    const SizedBox(width: 10),
                    Expanded(
                      child: CustomText(
                          // Languages.of(context)!.weHappy,
                          'Personalize your card design choose from a gallery of 100+ awesome designs.',
                          style: Theme.of(context)
                              .textTheme
                              .subtitle1!
                              .copyWith(
                                color:
                                    ColorResource.color222222.withOpacity(0.5),
                              )),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    const SizedBox(
                      height: 18,
                      child: Center(
                        child: Icon(
                          Icons.circle_sharp,
                          size: 10,
                        ),
                      ),
                    ),
                    const SizedBox(width: 10),
                    Expanded(
                      child: CustomText(
                          // Languages.of(context)!.weHappy,
                          'Get some great offers and discounts using this card.',
                          style: Theme.of(context)
                              .textTheme
                              .subtitle1!
                              .copyWith(
                                color:
                                    ColorResource.color222222.withOpacity(0.5),
                              )),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    const Icon(
                      Icons.circle_sharp,
                      size: 10,
                    ),
                    const SizedBox(width: 10),
                    CustomText(
                        // Languages.of(context)!.weHappy,
                        'Fees: ₹₹250.00+GST.',
                        style: Theme.of(context).textTheme.subtitle1!.copyWith(
                              color: ColorResource.color222222.withOpacity(0.5),
                            )),
                  ],
                ),
              ),
            ],
          ),
          const Spacer(),
          Flexible(
            child: CustomButton(
              Languages.of(context)!.applyNow,
              onTap: () {
                Navigator.pushNamed(context, AppRoutes.expressionPocketsScreen);
              },
            ),
          )
        ],
      ),
    );
  }

  selectAddressBottomSheet() {
    showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        context: context,
        backgroundColor: ColorResource.colorFFFFFF,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(32),
          ),
        ),
        builder: (BuildContext context) {
          return SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding:
                      const EdgeInsets.only(top: 16.0, bottom: 24, left: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Image.asset(ImageResource.close)),
                      CustomText(Languages.of(context)!.selectAddress,
                          style:
                              Theme.of(context).textTheme.headline5!.copyWith(
                                    color: ColorResource.color222222,
                                  )),
                      SizedBox()
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      top: 16.0, bottom: 27, left: 24, right: 24),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Image.asset(ImageResource.delivered),
                          const SizedBox(width: 8),
                          CustomText(Languages.of(context)!.deliverTo,
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle1!
                                  .copyWith(color: ColorResource.color787878)),
                        ],
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                          addressBottomSheet();
                        },
                        child: CustomText(
                            bloc.address != null
                                ? Languages.of(context)!.editAddress
                                : 'Add Address',
                            style:
                                Theme.of(context).textTheme.subtitle1!.copyWith(
                                      color: ColorResource.colorF58220,
                                      decoration: TextDecoration.underline,
                                    )),
                      ),
                    ],
                  ),
                ),
                if (bloc.address != null)
                  Padding(
                    padding: const EdgeInsets.only(
                        bottom: 10.0, left: 24, right: 24),
                    child: CustomText(bloc.address!.name!,
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              color: ColorResource.color222222,
                            )),
                  ),
                if (bloc.address != null)
                  Padding(
                    padding: const EdgeInsets.only(
                        bottom: 10.0, left: 24, right: 24),
                    child: CustomText(
                        '${bloc.address?.houseNumber}, ${bloc.address?.address1}, ${bloc.address?.address2}',
                        style: Theme.of(context).textTheme.subtitle1!.copyWith(
                              color: ColorResource.color222222,
                            )),
                  ),
                if (bloc.address != null)
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 24.0, left: 24, right: 24, bottom: 24),
                    child: CustomButton(
                      Languages.of(context)!.continuee,
                      onTap: () {
                        Navigator.pop(context);
                        isLocationFound
                            ? changeLocationBottomSheet()
                            : Navigator.pushNamed(
                                context, AppRoutes.placeOrderScreen);
                      },
                    ),
                  ),
              ],
            ),
          );
        });
  }

  changeLocationBottomSheet() {
    showModalBottomSheet(
      isScrollControlled: true,
      isDismissible: false,
      context: context,
      backgroundColor: ColorResource.colorFFFFFF,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(32),
        ),
      ),
      builder: (BuildContext context) {
        return SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 13.0, left: 16),
                child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Image.asset(ImageResource.close)),
              ),
              const Center(
                child: Image(
                    height: 200, image: AssetImage(ImageResource.location)),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 14.0, left: 24, right: 24),
                child: CustomText(Languages.of(context)!.weDontDeliverHere,
                    style: Theme.of(context).textTheme.headline5!.copyWith(
                          color: ColorResource.color222222,
                        )),
              ),
              Container(
                margin:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 24),
                child: CustomText(
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                    style: Theme.of(context).textTheme.subtitle1!.copyWith(
                          color: ColorResource.color222222.withOpacity(0.5),
                        )),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 24.0, left: 24, right: 24, bottom: 24),
                child: CustomButton(
                  Languages.of(context)!.changeLocation,
                  onTap: () {
                    Navigator.pop(context);
                    addressBottomSheet();
                  },
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  addressBottomSheet() async {
    await showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        backgroundColor: ColorResource.colorFFFFFF,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(32),
          ),
        ),
        context: context,
        builder: (BuildContext context) {
          return AddAddress(
            bloc.noAddress,
            bloc.address,
            bloc.isAddressSaved,
            (AddressModel updatedAddress) {
              bloc.isAddressSaved = true;
              setState(() {
                bloc.address = updatedAddress;
              });

              bloc.add(RegularPocketsCardInitialEvent());
            },
            bloc.findScreen,
          );
        });
  }
}
