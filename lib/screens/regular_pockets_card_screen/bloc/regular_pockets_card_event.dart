part of 'regular_pockets_card_bloc.dart';

@immutable
abstract class RegularPocketsCardEvent extends BaseEquatable {}

class RegularPocketsCardInitialEvent extends RegularPocketsCardEvent {}
