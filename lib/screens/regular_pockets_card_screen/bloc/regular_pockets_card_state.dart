part of 'regular_pockets_card_bloc.dart';

@immutable
abstract class RegularPocketsCardState extends BaseEquatable {}

class RegularPocketsCardInitial extends RegularPocketsCardState {}
