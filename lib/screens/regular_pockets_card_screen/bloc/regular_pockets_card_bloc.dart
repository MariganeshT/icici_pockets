import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:icici/model/address_model.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:meta/meta.dart';

part 'regular_pockets_card_event.dart';

part 'regular_pockets_card_state.dart';

class RegularPocketsCardBloc
    extends Bloc<RegularPocketsCardEvent, RegularPocketsCardState> {
  RegularPocketsCardBloc() : super(RegularPocketsCardInitial());
  AddressModel? address = AddressModel(
      '632004', '2486, 19th Cross', '7th Main Rd', 'Sector-01, HSR Layout',
      name: 'Prince Anto');
  String noAddress = 'No address';
  bool isAddressSaved = false;
  String findScreen = 'regularPocketsCardScreen';

  @override
  Stream<RegularPocketsCardState> mapEventToState(
    RegularPocketsCardEvent event,
  ) async* {
    if (event is RegularPocketsCardInitialEvent) {
      yield RegularPocketsCardInitial();
    }
  }
}
