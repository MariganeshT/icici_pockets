import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:meta/meta.dart';

part 'otp_verification_event.dart';
part 'otp_verification_state.dart';

class OtpVerificationBloc
    extends Bloc<OtpVerificationEvent, OtpVerificationState> {
  OtpVerificationBloc() : super(OtpVerificationInitial());

  @override
  Stream<OtpVerificationState> mapEventToState(
    OtpVerificationEvent event,
  ) async* {
    if (event is NavigatePersonalEvent) {
      yield NavigatePersonalState();
    }
    if (event is NavigateTermsEvent) {
      yield NavigateTermsState();
    }
  }
}
