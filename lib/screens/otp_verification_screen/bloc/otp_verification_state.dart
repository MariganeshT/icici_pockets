part of 'otp_verification_bloc.dart';

@immutable
abstract class OtpVerificationState extends BaseEquatable {}

class OtpVerificationInitial extends OtpVerificationState {}

class NavigatePersonalState extends OtpVerificationState {}

class NavigateTermsState extends OtpVerificationState {}
