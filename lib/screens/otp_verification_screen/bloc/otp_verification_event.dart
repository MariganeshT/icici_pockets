part of 'otp_verification_bloc.dart';

abstract class OtpVerificationEvent extends BaseEquatable {}

class OtpVerificationInitialEvent extends OtpVerificationEvent {}

class NavigatePersonalEvent extends OtpVerificationInitialEvent {}

class NavigateTermsEvent extends OtpVerificationInitialEvent {}
