import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/router.dart';
import 'package:icici/utils/app_utils.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/constants.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import 'bloc/otp_verification_bloc.dart';

class OtpVerificationScreen extends StatefulWidget {
  final String? navigateScreen;
  bool isForgotMpinFlow;
  bool isTerms;

  OtpVerificationScreen(
      {this.isForgotMpinFlow = false,
      this.isTerms = false,
      this.navigateScreen});

  @override
  _OtpVerificationScreenState createState() => _OtpVerificationScreenState();
}

class _OtpVerificationScreenState extends State<OtpVerificationScreen> {
  int secondsRemaining = Constants.otpWaitingTime;
  bool enableResend = false;
  Timer? timer;
  int attemptCount = 0;
  bool contactSupport = false;
  bool errorValue = false;
  bool isContact = false;

  TextEditingController newTextEditingController = TextEditingController();

  late OtpVerificationBloc bloc;

  @override
  initState() {
    super.initState();
    bloc = BlocProvider.of<OtpVerificationBloc>(context);
    timer = Timer.periodic(const Duration(seconds: 1), (_) {
      if (secondsRemaining != 0) {
        setState(() {
          secondsRemaining--;
        });
      } else {
        setState(() {
          enableResend = true;
          newTextEditingController.clear();
          secondsRemaining = Constants.otpWaitingTime;
        });
      }
    });
  }

  void _resendCode() {
    setState(() {
      secondsRemaining = Constants.otpWaitingTime;
      enableResend = false;
      newTextEditingController.clear();
    });
  }

  @override
  dispose() {
    timer!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      child: Scaffold(
          backgroundColor: ColorResource.colorFAFAFA,
          body: BlocListener<OtpVerificationBloc, OtpVerificationState>(
              bloc: bloc,
              listener: (BuildContext context, OtpVerificationState state) {},
              child: BlocBuilder<OtpVerificationBloc, OtpVerificationState>(
                  bloc: bloc,
                  builder: (BuildContext context, OtpVerificationState state) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Stack(
                          children: <Widget>[
                            Container(
                              width: double.infinity,
                              child: Image.asset(
                                ImageResource.verifyIdentity,
                                // fit: BoxFit.contain,
                              ),
                              decoration: BoxDecoration(
                                color: ColorResource.color641653,
                                borderRadius: BorderRadius.vertical(
                                    bottom: Radius.elliptical(
                                        MediaQuery.of(context).size.width,
                                        31.0)),
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                CustomAppbar(
                                  titleString: '',
                                  iconEnumValues: widget.isForgotMpinFlow
                                      ? IconEnum.empty
                                      : IconEnum.close,
                                  isAuthentication: true,
                                  onItemSelected: (selectedItem) {
                                    Navigator.pop(context, 'fromOTPScreen');
                                  },
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      bottom: 10, left: 12),
                                  child: CustomText(
                                    StringResource.codeVerification,
                                    style:
                                        Theme.of(context).textTheme.headline6,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      bottom: 1, left: 12),
                                  child: CustomText(
                                      StringResource.pleaseEnterTheCode,
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle1!
                                          .copyWith(
                                              color:
                                                  ColorResource.colorFFFFFF)),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 12),
                                  child: RichText(
                                      text: TextSpan(children: <TextSpan>[
                                    TextSpan(
                                        text: StringResource.sent,
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle1),
                                    TextSpan(
                                        text: ' +91 958 268 1800',
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle1)
                                  ])),
                                ),
                              ],
                            ),
                          ],
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 20.0),
                                  child: (enableResend)
                                      ? Container()
                                      : Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            CustomText(
                                                StringResource.codeExpireIn,
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .subtitle1!
                                                    .copyWith(
                                                        color: ColorResource
                                                            .color222222)),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 4),
                                              child: CustomText(
                                                  StringResource
                                                      .remainingSeconds(
                                                          secondsRemaining),
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .subtitle1!
                                                      .copyWith(
                                                          color: ColorResource
                                                              .color222222)),
                                            )
                                          ],
                                        ),
                                ),
                                PinCodeTextField(
                                  enablePinAutofill: false,
                                  appContext: context,
                                  onChanged: (String value) {
                                    // user.otp = value;
                                    print(value);
                                  },
                                  validator: (String? value) {},
                                  onCompleted: (String value) {
                                    AppUtils.hideKeyBoard(context);
                                    if (newTextEditingController.text.length ==
                                        6) {
                                      if (newTextEditingController.text ==
                                          '111111') {
                                        // Navigator.pop(context);
                                        if (widget.navigateScreen ==
                                            'personalScreen') {
                                          bloc.add(
                                            NavigatePersonalEvent(),
                                          );
                                        } else if (widget.navigateScreen ==
                                            'termsScreen') {
                                          bloc.add(NavigateTermsEvent());
                                        }
                                        if (widget.isTerms ||
                                            widget.isForgotMpinFlow) {
                                          Navigator.pushReplacementNamed(
                                              context, AppRoutes.setPinScreen,
                                              arguments: widget.isTerms);
                                        } else {
                                          Navigator.pushReplacementNamed(
                                              context,
                                              AppRoutes.termsConditionsScreen);
                                        }
                                      } else {
                                        setState(() {
                                          errorValue = true;
                                          newTextEditingController.clear();
                                          if (attemptCount == 3) {
                                            AppUtils.showErrorToast(
                                                'Your number has been blocked for 6 hours contact support.');
                                            isContact = true;
                                          } else {
                                            AppUtils.showToast(
                                                '\n${3 - attemptCount} attempts remaining\n');
                                          }
                                          if (attemptCount != 3) {
                                            attemptCount++;
                                          }
                                        });
                                      }
                                    }
                                  },
                                  length: 6,
                                  textStyle: Theme.of(context)
                                      .textTheme
                                      .headline5!
                                      .copyWith(
                                          color: ColorResource.color222222),
                                  animationType: AnimationType.scale,
                                  animationDuration:
                                      const Duration(milliseconds: 300),
                                  keyboardType: TextInputType.number,
                                  controller: newTextEditingController,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  obscureText: true,
                                  pinTheme: PinTheme(
                                      shape: PinCodeFieldShape.box,
                                      borderWidth: 1,
                                      borderRadius: BorderRadius.circular(8),
                                      inactiveColor: errorValue
                                          ? ColorResource.colorF92538
                                          : ColorResource.color222222,
                                      selectedColor: errorValue
                                          ? ColorResource.colorF92538
                                          : ColorResource.color222222,
                                      activeColor: errorValue
                                          ? ColorResource.colorF92538
                                          : ColorResource.color222222,
                                      fieldHeight: 56,
                                      fieldWidth: 48,
                                      errorBorderColor:
                                          ColorResource.colorF92538),
                                ),
                                errorValue
                                    ? Padding(
                                        padding:
                                            const EdgeInsets.only(top: 24.0),
                                        child: Center(
                                          child: CustomText(
                                              StringResource.incorrectOTP,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .subtitle1!
                                                  .copyWith(
                                                      color: ColorResource
                                                          .colorF92538)),
                                        ),
                                      )
                                    : const SizedBox(),
                                const Spacer(),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    CustomText(StringResource.codeNotReceived,
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle1!
                                            .copyWith(
                                                color:
                                                    ColorResource.color222222)),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 6.0),
                                      child: GestureDetector(
                                        onTap: () {
                                          isContact
                                              ? Navigator.popAndPushNamed(
                                                  context,
                                                  AppRoutes.supportScreen)
                                              : setState(() {
                                                  if (enableResend)
                                                    _resendCode();
                                                });
                                        },
                                        child: CustomText(
                                            isContact
                                                ? StringResource.contactSupport
                                                : StringResource.resend,
                                            style: Theme.of(context)
                                                .textTheme
                                                .subtitle1!
                                                .copyWith(
                                                  color: (enableResend ||
                                                          isContact)
                                                      ? ColorResource
                                                          .colorF58220
                                                      : ColorResource
                                                          .color222222
                                                          .withOpacity(0.6),
                                                )),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    );
                  }))),
    );
  }
}

class OTPVerificationModel {
  final String navigateScreen;
  bool isForgotMpinFlow;
  bool isTerms;

  OTPVerificationModel(
      {this.isForgotMpinFlow = false,
      this.isTerms = false,
      this.navigateScreen = ''});
}
