import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_contact/contacts.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:icici/router.dart';
import 'package:icici/utils/app_utils.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/font.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:icici/widgets/custom_button.dart';
import 'package:icici/widgets/custom_dialog.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:url_launcher/url_launcher.dart';

import 'bloc/referandearn_bloc.dart';

class ReferAndEarn extends StatefulWidget {
  ReferAndEarn({Key? key}) : super(key: key);

  @override
  _ReferAndEarnState createState() => _ReferAndEarnState();
}

class _ReferAndEarnState extends State<ReferAndEarn> {
  late ReferandearnBloc bloc;
  final searchController = TextEditingController();
  late List<Contact> listContacts;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc = ReferandearnBloc()..add(ReferandearnInitialEvent());
    listContacts = [];
    _getPermission();
  }

  onSearchTextChanged(String text) async {
    _searchResult.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    setState(() {
      listContacts.forEach((Contact allcontacts) {
        // print("${allcontacts.displayName}");
        if (('${allcontacts.displayName}').contains(text)) {
          _searchResult.add(allcontacts);
        }
      });
    });
    setState(() {});
  }

  List<Contact> _searchResult = [];

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: Colors.transparent));
    return BlocListener<ReferandearnBloc, ReferandearnState>(
      bloc: bloc,
      listener: (BuildContext context, ReferandearnState state) {
        if (state is MyReferalState) {
          Navigator.pushNamed(context, AppRoutes.myReferal);
        }
        if (state is SearchContactState) {
          searchController.clear();
          _searchContactList();
        }
      },
      child: BlocBuilder<ReferandearnBloc, ReferandearnState>(
        bloc: bloc,
        builder: (BuildContext context, ReferandearnState state) {
          //  if (state is ReferandearnLoadingState) {
          //   return const Center(
          //     child: CircularProgressIndicator(),
          //   );
          // }
          return Scaffold(
            body: SafeArea(
              top: false,
              bottom: false,
              child: Stack(
                children: [
                  Column(
                    children: [
                      Expanded(
                          flex: 3,
                          child: Container(
                            // color: ColorResource.color641653,
                            decoration: const BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                    ImageResource.dashboard_background),
                                fit: BoxFit.cover,
                              ),
                            ),
                          )),
                      Expanded(
                          flex: 1,
                          child: Container(
                            color: ColorResource.colorFAFAFA,
                          )),
                    ],
                  ),
                  Column(
                    children: [
                      CustomAppbar(
                        titleString: 'Refer & Earn',
                        titleSpacing: 8,
                        style: Theme.of(context)
                            .textTheme
                            .headline5!
                            .copyWith(color: ColorResource.colorFFFFFF),
                        iconEnumValues: IconEnum.back,
                        showTextButton: true,
                        textButtonString: StringResource.myreferals,
                        onItemSelected: (values) {
                          if (values == 'textButtonTriggering') {
                            bloc.add(NavigateMyReferalEvent());
                          } else {
                            Navigator.pop(context);
                          }
                        },
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Image.asset(ImageResource.invitefriend),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 55, vertical: 25),
                        child: CustomText(StringResource.inviteYourFriend,
                            textAlign: TextAlign.center,
                            style: Theme.of(context)
                                .textTheme
                                .headline5!
                                .copyWith(
                                    color: ColorResource.colorFFFFFF,
                                    fontSize: 20,
                                    height: 1.5,
                                    fontWeight: FontWeight.w900)),
                      )
                    ],
                  ),
                  DraggableScrollableSheet(
                    minChildSize: 0.45,
                    maxChildSize: 0.89,
                    builder: (BuildContext context,
                        ScrollController scrollController) {
                      return Container(
                        decoration: BoxDecoration(
                          color: ColorResource.colorFFFFFF,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(32.0),
                              topRight: Radius.circular(32.0)),
                        ),
                        child: CustomScrollView(
                          controller: scrollController,
                          slivers: [
                            SliverToBoxAdapter(
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 22, vertical: 16),
                                child: Column(
                                  children: [
                                    CustomText(
                                      StringResource.yourReferalCode,
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle2!,
                                    ),
                                    const SizedBox(
                                      height: 14,
                                    ),
                                    DottedBorder(
                                      borderType: BorderType.RRect,
                                      radius: Radius.circular(8),
                                      color: ColorResource.colorFF781F,
                                      child: Container(
                                        padding: EdgeInsets.symmetric(
                                          horizontal: 14,
                                          vertical: 8,
                                        ),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          border: Border.all(
                                              style: BorderStyle.none,
                                              color: ColorResource.colorFF781F),
                                        ),
                                        child: CustomText(
                                          'POC0246788',
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline5!
                                              .copyWith(
                                                  fontWeight: FontWeight.w700),
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 25,
                                    ),
                                    CustomButton(
                                      StringResource.shareViaWhatsapp,
                                      isLeading: true,
                                      trailingWidget: Image.asset(
                                        ImageResource.whatsapp,
                                      ),
                                      onTap: () async {
                                        const url =
                                            "https://wa.me/?text=Pockets";
                                        await launch(url);
                                      },
                                    ),
                                    const SizedBox(
                                      height: 24,
                                    ),
                                    Wrap(
                                      // runSpacing: 80,
                                      spacing: 60,
                                      children: _shareOptionWidget(),
                                    ),
                                    const SizedBox(
                                      height: 40,
                                    ),
                                    Row(
                                      children: [
                                        CustomText(
                                          StringResource
                                              .contactsNotYetOnPocktes,
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline5,
                                        ),
                                        Spacer(),
                                        GestureDetector(
                                          child: Icon(
                                            Icons.search,
                                            size: 32,
                                          ),
                                          onTap: () {
                                            bloc.add(SearchContactEvent());
                                          },
                                        ),
                                      ],
                                    ),
                                    // const SizedBox(height: 15,),
                                    // Expanded(
                                    //   child: _contactList(scrollController),
                                    //   ),
                                    // _contactList(),
                                  ],
                                ),
                              ),
                            ),
                            (listContacts.length > 0)
                                ? SliverList(
                                    delegate: SliverChildBuilderDelegate(
                                    (BuildContext context, int index) {
                                      return Padding(
                                        padding: const EdgeInsets.only(
                                            left: 10, right: 14),
                                        child: _contactList(
                                            listContacts[index], index),
                                      );
                                    },
                                    childCount: listContacts.length,
                                  ))
                                : SliverToBoxAdapter(
                                    child: Center(
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          const SizedBox(
                                            height: 8,
                                          ),
                                          CircularProgressIndicator(),
                                          const SizedBox(
                                            height: 15,
                                          ),
                                          Text("Fetching Contacts..")
                                        ],
                                      ),
                                    ),
                                  ),
                          ],
                        ),
                      );
                    },
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  List<Widget> _shareOptionWidget() {
    List<Widget> widgets = [];
    bloc.shareOption.forEach((element) {
      widgets.add(GestureDetector(
        child: Column(
          children: [
            Image.asset(element.image),
            const SizedBox(
              height: 10,
            ),
            CustomText(
              element.text,
              style: Theme.of(context)
                  .textTheme
                  .subtitle2!
                  .copyWith(color: ColorResource.colore222222),
            ),
          ],
        ),
        onTap: element.onTap,
      ));
    });
    return widgets;
  }

  Widget _contactList(Contact contact, int index) {
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 14),
      child: ListTile(
        leading: Container(
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
          ),
          child: (contact.avatar != null)
              ? CircleAvatar(
                  maxRadius: 22,
                  backgroundColor: ColorResource.colorFF781F,
                  backgroundImage: MemoryImage(contact.avatar!),
                )
              : CircleAvatar(
                  backgroundColor: ColorResource.colorFF781F,
                  child: CustomText(
                    contact.initials(),
                    style: Theme.of(context).textTheme.headline5!.copyWith(
                        color: ColorResource.colorFFFFFF, fontSize: 16),
                  )),
          // child:  CircleAvatar(
          //     maxRadius: 22,
          //     backgroundImage:
          //    contactlist.image.isNotEmpty ?
          //     NetworkImage(
          //       contactlist.image
          //     ) : null,
          //     backgroundColor: ColorResource.colorFF781F,
          //     child: contactlist.image.isEmpty ?
          //      CustomText(
          //        getInitials(contactlist.name).toString(),
          //       style: Theme.of(context)
          //         .textTheme
          //         .headline5!.copyWith(
          //           color: ColorResource.colorFFFFFF, fontSize: 16),
          // ) : null,
          //   ),
        ),
        title: CustomText(
          contact.displayName ?? '',
          style: Theme.of(context).textTheme.bodyText1,
        ),
        subtitle: CustomText(
          (contact.phones.length > 0)
              ? contact.phones
                  .get(0)
                  .toString()
                  .substring(5, contact.phones.get(0).toString().length - 1)
              : '',
          style: Theme.of(context)
              .textTheme
              .subtitle1!
              .copyWith(color: ColorResource.color787878, height: 1.5),
        ),
        trailing: GestureDetector(
          child: CustomText(
            StringResource.invite,
            style: Theme.of(context)
                .textTheme
                .subtitle1!
                .copyWith(color: ColorResource.colorFF781F),
          ),
          // onTap: bloc.contactList[index].onTap,
          onTap: () {
            AppUtils.showToast('Invited', gravity: ToastGravity.BOTTOM);
          },
        ),
      ),
    );
  }

  void _searchContactList() async {
    await showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        backgroundColor: ColorResource.colorFFFFFF,
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
            return Column(
              children: [
                Container(
                  alignment: Alignment.bottomCenter,
                  width: MediaQuery.of(context).size.width,
                  height: 110,
                  color: ColorResource.color641653,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: AppBar(
                          elevation: 0,
                          title: Container(
                            height: 50,
                            margin:
                                const EdgeInsets.only(left: 5.0, right: 5.0),
                            padding: const EdgeInsets.only(left: 5.0),
                            alignment: Alignment.centerLeft,
                            decoration: const BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5.0)),
                              // color: Colors.white,
                              // border: Border.all(color: Colors.white, width: 2.0)
                            ),
                            child: TextFormField(
                              autofocus: true,
                              controller: searchController,
                              cursorColor: Colors.white,
                              decoration: InputDecoration(
                                hintMaxLines: 1,
                                hintText: StringResource.searchContactsHint,
                                border: InputBorder.none,
                                fillColor: Colors.grey,
                                suffixIcon: GestureDetector(
                                  child: const Icon(Icons.clear,
                                      size: 22, color: Colors.grey),
                                  onTap: () {
                                    setState(() {
                                      searchController.clear();
                                      onSearchTextChanged('');
                                    });
                                  },
                                ),
                                hintStyle: Theme.of(context)
                                    .textTheme
                                    .bodyText1!
                                    .copyWith(
                                        color: ColorResource.color787878,
                                        fontFamily: Font.sourceSansProRegular
                                            .toString(),
                                        fontWeight: FontWeight.w400),
                              ),
                              keyboardType: TextInputType.text,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1!
                                  .copyWith(
                                      color: Colors.white,
                                      fontSize: 16,
                                      fontFamily:
                                          Font.sourceSansProRegular.toString(),
                                      fontWeight: FontWeight.w600),
                              onFieldSubmitted: (String stringValues) {
                                FocusScope.of(context)
                                    .requestFocus(FocusNode());
                              },
                              onChanged: (String value) {
                                setState(() {
                                  onSearchTextChanged(value);
                                });
                              },
                            ),
                          ),
                          leading: GestureDetector(
                            child: Icon(
                              Icons.arrow_back,
                              size: 26,
                            ),
                            onTap: () {
                              Navigator.pop(context);
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: CustomScrollView(
                    slivers: [
                      searchController.text.isEmpty
                          ? SliverList(
                              delegate: SliverChildBuilderDelegate(
                              (BuildContext context, int index) {
                                return Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10, right: 14),
                                  child:
                                      _contactList(listContacts[index], index),
                                );
                              },
                              childCount: listContacts.length,
                            ))
                          : SliverList(
                              delegate: SliverChildBuilderDelegate(
                              (BuildContext context, int index) {
                                return Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10, right: 14),
                                  child:
                                      _contactList(_searchResult[index], index),
                                );
                              },
                              childCount: _searchResult.length,
                            ))
                    ],
                  ),
                )
              ],
            );
          });
        });
  }

  Future<void> _makePhoneCall(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  readContacts() async {
    await Contacts.streamContacts().forEach((contact) {
      print('Name list ${contact.displayName}');
      setState(() {
        listContacts.add(contact);
      });
    });
  }

  Future<void> _getPermission() async {
    final PermissionStatus permission = await Permission.contacts.request();
    if (permission == PermissionStatus.granted) {
      readContacts();
    } else {
      await DialogUtils.showDialog(
        buildContext: context,
        //Sample title like: Languages.of(buildContext)!.smsSendDescription,
        title: '"Pockets" wants to access your Contacts',
        //Sample Description like: Languages.of(buildContext)!.smsSendDescriptionNote,
        description: 'To Invite your Contacts, Allow Contacts Access in App Settings',
        okBtnText: 'Allow',
        cancelBtnText: 'Deny',
        okBtnFunction: (String value) async {
          await openAppSettings();
        },
      );
    }
  }
}
