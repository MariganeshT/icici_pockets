import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/services.dart';
import 'package:icici/model/refer&earn_model.dart';
import 'package:icici/utils/app_utils.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:meta/meta.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';

part 'referandearn_event.dart';
part 'referandearn_state.dart';

class ReferandearnBloc extends Bloc<ReferandearnEvent, ReferandearnState> {
  ReferandearnBloc() : super(ReferandearnInitial());

  List<ShareOptions> shareOption = [];
  List<ContactListModel> contactList = [];

  Future<void> _makeAction(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  String referCode = 'Pockets Referal Code: POC0376788';

  @override
  Stream<ReferandearnState> mapEventToState(
    ReferandearnEvent event,
  ) async* {
    if (event is ReferandearnInitialEvent) {
      yield ReferandearnLoadingState();

      shareOption.addAll([
        ShareOptions(ImageResource.sendmail2, StringResource.email, onTap: () {
          _makeAction('mailto:pockets@icicibank.com');
        }),
        ShareOptions(ImageResource.copy2, StringResource.copyShare, onTap: () {
          AppUtils.showToast('Referal Code Copied');
          Clipboard.setData(
              const ClipboardData(text: 'Pockets Referal Code: POC0376788'));
          Share.share(referCode, subject: referCode);
        }),
        ShareOptions(ImageResource.more, StringResource.more, onTap: () {
          Share.share('Pockets Referal Code: POC0376788');
          AppUtils.showToast('more');
        }),
      ]);

      contactList.addAll([
        ContactListModel(image: '', name: 'New King', mobileNo: '987653210'),
        ContactListModel(
            image: 'https://i.stack.imgur.com/0VpX0.png',
            name: 'New User',
            mobileNo: '987653210'),
        ContactListModel(
            image: 'https://i.stack.imgur.com/0VpX0.png',
            name: 'New User',
            mobileNo: '987653210'),
        ContactListModel(image: '', name: 'New User', mobileNo: '987653210'),
        ContactListModel(
            image: 'https://i.stack.imgur.com/0VpX0.png',
            name: 'New User',
            mobileNo: '808656544'),
        ContactListModel(
            image: 'https://i.stack.imgur.com/0VpX0.png',
            name: 'New User',
            mobileNo: '987653210'),
        ContactListModel(image: '', name: 'New User', mobileNo: '987653210'),
        ContactListModel(
            image: 'https://i.stack.imgur.com/0VpX0.png',
            name: 'New User',
            mobileNo: '987653210'),
        ContactListModel(
            image: 'https://i.stack.imgur.com/0VpX0.png',
            name: 'New User',
            mobileNo: '987653210'),
        ContactListModel(
            image: 'https://i.stack.imgur.com/0VpX0.png',
            name: 'New User',
            mobileNo: '987653210'),
        ContactListModel(image: '', name: 'New User', mobileNo: '987653210'),
        ContactListModel(
            image: 'https://i.stack.imgur.com/0VpX0.png',
            name: 'New User',
            mobileNo: '987653210'),
      ]);

      yield ReferandearnLoadedState();
    }

    if (event is NavigateMyReferalEvent) {
      yield MyReferalState();
    }
    if (event is SearchContactEvent) {
      yield SearchContactState();
    }
  }
}
