part of 'referandearn_bloc.dart';

@immutable
abstract class ReferandearnEvent extends BaseEquatable {}

class ReferandearnInitialEvent extends ReferandearnEvent {}

class SearchContactEvent extends ReferandearnEvent {}

class NavigateMyReferalEvent extends ReferandearnEvent {}