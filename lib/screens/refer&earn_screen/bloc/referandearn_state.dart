part of 'referandearn_bloc.dart';

@immutable
abstract class ReferandearnState {}

class ReferandearnInitial extends ReferandearnState {}

class ReferandearnLoadingState extends ReferandearnState {}

class ReferandearnLoadedState extends ReferandearnState {}

class SearchContactState extends ReferandearnState {}

class MyReferalState extends ReferandearnState {}