import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/model/address_model.dart';
import 'package:icici/screens/place_order_screen/bloc/place_order_bloc.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/preference_helper.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:icici/widgets/custom_button.dart';

import '../../router.dart';

class PlaceOrderScreen extends StatefulWidget {
  AddressModel address;

  PlaceOrderScreen(this.address);

  @override
  _PlaceOrderScreenState createState() => _PlaceOrderScreenState();
}

class _PlaceOrderScreenState extends State<PlaceOrderScreen> {
  bool isAddFund = false;
  PlaceOrderBloc? bloc;

  String noAddress = 'No address';
  bool isAddressSaved = false;

  @override
  void initState() {
    super.initState();
    bloc = PlaceOrderBloc()..add(PlaceOrderInitialEvent());
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: Colors.transparent));
    return Scaffold(
        backgroundColor: ColorResource.color641653,
        body: BlocListener<PlaceOrderBloc, PlaceOrderState>(
          bloc: bloc,
          listener: (BuildContext context, PlaceOrderState state) {},
          child: BlocBuilder<PlaceOrderBloc, PlaceOrderState>(
            bloc: bloc,
            builder: (BuildContext context, PlaceOrderState state) {
              return Container(
                color: ColorResource.color641653,
                child: MediaQuery.removePadding(
                    context: context,
                    removeRight: true,
                    removeBottom: true,
                    removeLeft: true,
                    child: Column(children: [
                      CustomAppbar(
                        titleString:
                            Languages.of(context)!.requestForPhysicalCard,
                        iconEnumValues: IconEnum.back,
                        titleSpacing: 8,
                        onItemSelected: (values) async {
                          Navigator.pop(context);
                        },
                      ),
                      Expanded(
                        child: Column(children: <Widget>[
                          Expanded(
                              flex: 6,
                              child: Image.asset(ImageResource.regular)),
                          Expanded(
                            flex: 8,
                            child: SizedBox(
                              width: MediaQuery.of(context).size.width,
                              child: Container(
                                  decoration: const BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(14),
                                          topRight: Radius.circular(14)),
                                      color: ColorResource.colorFFFFFF),
                                  child: placeOrderWidget()),
                            ),
                          ),
                        ]),
                      ),
                    ])),
              );
            },
          ),
        ));
  }

  placeOrderWidget() {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 10.0),
              child: Row(
                children: <Widget>[
                  Image.asset(ImageResource.delivered),
                  const SizedBox(width: 8),
                  CustomText(Languages.of(context)!.deliverTo,
                      style: Theme.of(context)
                          .textTheme
                          .subtitle1!
                          .copyWith(color: ColorResource.color787878)),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: CustomText('Prince Anto',
                  style: Theme.of(context).textTheme.headline5!.copyWith(
                        color: ColorResource.color222222,
                      )),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 15.0),
              child: CustomText(
                  // '${widget.address.houseNumber},${widget.address.address1},${widget.address.address2}${(widget.address.address2 != null && widget.address.address2!.isNotEmpty) ? ',' : ''}${widget.address.pincode}',
                  'Sunchee Scedar, 2486, 19th, Cross, 7th Main Rd,Sector-01, HSR Layout',
                  style: Theme.of(context).textTheme.subtitle1!.copyWith(
                        color: ColorResource.color222222,
                      )),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: CustomText(Languages.of(context)!.billDetails,
                  style: Theme.of(context).textTheme.headline5!.copyWith(
                        color: ColorResource.color222222,
                      )),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                CustomText(Languages.of(context)!.expressionPocketsCard,
                    style: Theme.of(context).textTheme.subtitle1!.copyWith(
                          color: ColorResource.color222222,
                        )),
                CustomText('₹250.00',
                    style: Theme.of(context).textTheme.subtitle1!.copyWith(
                        color: ColorResource.color222222,
                        fontWeight: FontWeight.w700)),
              ],
            ),
            SizedBox(
              height: 5,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                CustomText(Languages.of(context)!.gst,
                    style: Theme.of(context).textTheme.subtitle1!.copyWith(
                          color: ColorResource.color222222,
                        )),
                CustomText('₹49.00',
                    style: Theme.of(context).textTheme.subtitle1!.copyWith(
                        color: ColorResource.color222222,
                        fontWeight: FontWeight.w700)),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 20.0,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  CustomText(Languages.of(context)!.toPay,
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          color: ColorResource.color222222,
                          fontWeight: FontWeight.w700)),
                  CustomText('₹299.00',
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          color: ColorResource.color222222,
                          fontWeight: FontWeight.w800)),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20.0, right: 10),
              child: CustomButton(
                Languages.of(context)!.placeOrder,
                onTap: () async {
                  await PreferenceHelper.setTrack(true);
                  isAddFund
                      ? addFundsBottomSheet()
                      : Navigator.pushNamed(
                          context, AppRoutes.orderStatusScreen);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  addFundsBottomSheet() {
    showModalBottomSheet(
      isScrollControlled: true,
      isDismissible: false,
      context: context,
      backgroundColor: ColorResource.colorFFFFFF,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(32),
        ),
      ),
      builder: (BuildContext context) {
        return SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 13.0, left: 16),
                child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Image.asset(ImageResource.close)),
              ),
              const Center(
                child: Image(image: AssetImage(ImageResource.addFunds)),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 14.0, left: 24, right: 24),
                child: CustomText(Languages.of(context)!.addFund,
                    style: Theme.of(context).textTheme.headline5!.copyWith(
                          color: ColorResource.color222222,
                        )),
              ),
              Container(
                margin:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 24),
                child: Wrap(
                  children: <Widget>[
                    RichText(
                        text: TextSpan(children: <TextSpan>[
                      TextSpan(
                          // text: Languages.of(context)!.youCan,
                          text:
                              'The balance required to apply for a physical card is',
                          style: Theme.of(context)
                              .textTheme
                              .subtitle1!
                              .copyWith(
                                  color: ColorResource.color222222
                                      .withOpacity(0.5))),
                      TextSpan(
                        text: ' `100 + GST.',
                        style: Theme.of(context).textTheme.subtitle1!.copyWith(
                            color: ColorResource.color222222.withOpacity(0.5)),
                      ),
                      TextSpan(
                          // text: Languages.of(context)!.orTap,
                          text:
                              ' Please add funds to apply for your Pockets card.',
                          style: Theme.of(context)
                              .textTheme
                              .subtitle1!
                              .copyWith(
                                  color: ColorResource.color222222
                                      .withOpacity(0.5))),
                    ]))
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 24.0, left: 24, right: 24, bottom: 24),
                child: CustomButton(
                  Languages.of(context)!.addFunds,
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
