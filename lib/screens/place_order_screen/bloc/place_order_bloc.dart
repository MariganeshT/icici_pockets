import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:meta/meta.dart';

part 'place_order_event.dart';
part 'place_order_state.dart';

class PlaceOrderBloc extends Bloc<PlaceOrderEvent, PlaceOrderState> {
  PlaceOrderBloc() : super(PlaceOrderInitial());

  @override
  Stream<PlaceOrderState> mapEventToState(
    PlaceOrderEvent event,
  ) async* {
    // TODO: implement mapEventToState
  }
}
