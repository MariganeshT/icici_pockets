part of 'place_order_bloc.dart';

@immutable
abstract class PlaceOrderEvent extends BaseEquatable {}

class PlaceOrderInitialEvent extends PlaceOrderEvent {}
