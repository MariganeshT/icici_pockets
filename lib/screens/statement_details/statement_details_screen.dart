import 'dart:collection';

import 'package:flowder/flowder.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:icici/base/base_state.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/languages/app_locale_constant.dart';
import 'package:icici/model/statement_type_model.dart';
import 'package:icici/screens/statement_details/statement_details_bloc.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:icici/widgets/custom_button.dart';
import 'package:icici/widgets/download_statement_widget.dart';
import 'package:icici/widgets/widget_utils.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';

class StatementDetailsScreen extends StatefulWidget {
  const StatementDetailsScreen({Key? key}) : super(key: key);

  @override
  _StatementDetailsScreenState createState() => _StatementDetailsScreenState();
}

class _StatementDetailsScreenState extends State<StatementDetailsScreen> {
  Locale? _locale;
  late StatementDetailsBloc bloc;
  List<StatementTypeModel> statementTypeModelList = [];
  TextEditingController startDateController = TextEditingController();
  TextEditingController endDateController = TextEditingController();
  bool bottomButtonVisible = false;
  DateTime? startDate;
  DateTime? endDate;
  int? selectedIndex;
  bool isDownload = false;

  late DownloaderUtils options;
  late DownloaderCore core;
  final String path='';

  @override
  void didChangeDependencies() {
    getLocale().then((Locale locale) {
      setState(() {
        _locale = locale;
      });
    });
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<StatementDetailsBloc>(context);
    statementTypeModelList = bloc.statementTypeModelList;
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: Colors.transparent));
    return BlocListener(
        bloc: bloc,
        listener: (BuildContext context, BaseState state) {},
        child: BlocBuilder(
            bloc: bloc,
            builder: (BuildContext context, BaseState state) {
              if (state is LoadingState) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
              return SafeArea(
                top: false,
                child: Scaffold(
                    backgroundColor: ColorResource.colorFAFAFA,
                    bottomSheet: bottomButtonVisible
                        ? Container(
                            margin: const EdgeInsets.only(
                                bottom: 20, left: 12, right: 12),
                            child: CustomButton(
                              isDownload
                                  ? Languages.of(context)!.downloadStatement
                                  : Languages.of(context)!.viewStatement,
                              onTap: () {
                                if (!isDownload) {
                                  Map<String, dynamic> values = HashMap();
                                  values['index'] = selectedIndex!;
                                  values['rangeType'] = bloc
                                      .statementTypeModelList[selectedIndex!]
                                      .name;
                                  values['from'] = 'statementDetailsScreen';
                                  if (selectedIndex == 3) {
                                    String? errorMsg;
                                    if (startDate == null && endDate == null) {
                                      errorMsg = Languages.of(context)!
                                          .startAndEndDateRequired;
                                    } else if (endDate == null) {
                                      errorMsg = Languages.of(context)!
                                          .endDateRequired;
                                    } else if (startDate == null) {
                                      errorMsg = Languages.of(context)!
                                          .startDateRequired;
                                    } else {
                                      values['startDate'] = startDate!;
                                      values['endDate'] = endDate!;
                                      Navigator.pop(context, values);
                                    }
                                    if (errorMsg != null) {
                                      Fluttertoast.showToast(
                                          msg: errorMsg,
                                          toastLength: Toast.LENGTH_SHORT,
                                          gravity: ToastGravity.BOTTOM,
                                          backgroundColor:
                                              ColorResource.color020e36,
                                          textColor: Colors.white,
                                          fontSize: 12.0);
                                    }
                                  } else {
                                    Navigator.pop(context, values);
                                  }
                                } else {
                                  _showDownloadBottomSheet(context);
                                }
                              },
                            ),
                          )
                        : null,
                    body: Column(children: [
                      Expanded(
                          flex: 0,
                          child: Container(
                            color: ColorResource.color641653,
                            child: CustomAppbar(
                              titleString:
                                  Languages.of(context)!.detailedStatement,
                              iconEnumValues: IconEnum.back,
                              onItemSelected: (values) {
                                Navigator.pop(context, 'backNavigation');
                              },
                            ),
                          )),
                      MediaQuery.removePadding(
                        removeLeft: true,
                        removeTop: true,
                        context: context,
                        child: Container(
                            margin: EdgeInsets.all(12),
                            child: SingleChildScrollView(
                                child: Column(children: [
                              Column(children: [
                                Expanded(
                                    flex: 0,
                                    child: Container(
                                        child: Column(children: [
                                      Container(
                                        margin:
                                            EdgeInsets.only(left: 12, top: 5),
                                        alignment: Alignment.topLeft,
                                        child: CustomText(
                                          Languages.of(context)!
                                              .statementDuration,
                                          style: Theme.of(context)
                                              .textTheme
                                              .subtitle2!
                                              .copyWith(
                                                  color:
                                                      ColorResource.color787878,
                                                  fontWeight: FontWeight.w600),
                                        ),
                                      ),
                                      ListView.builder(
                                          itemCount: bloc
                                              .statementTypeModelList.length,
                                          shrinkWrap: true,
                                          primary: true,
                                          physics:
                                              NeverScrollableScrollPhysics(),
                                          itemBuilder: (BuildContext context,
                                              int index) {
                                            return Card(
                                                elevation: 2,
                                                margin:
                                                    EdgeInsets.only(top: 16),
                                                color:
                                                    ColorResource.colorFFFFFF,
                                                shape: RoundedRectangleBorder(
                                                  side: BorderSide(
                                                      color: Colors.white70,
                                                      width: 1),
                                                  borderRadius:
                                                      BorderRadius.circular(14),
                                                ),
                                                child: Container(
                                                    alignment: Alignment.center,
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            vertical: 10),
                                                    child: Column(children: [
                                                      Container(
                                                        child: ListTile(
                                                          trailing:
                                                              GestureDetector(
                                                            child: Container(
                                                              child: bloc
                                                                      .statementTypeModelList[
                                                                          index]
                                                                      .isSelected
                                                                  ? Padding(
                                                                      padding:
                                                                          EdgeInsets.all(
                                                                              5),
                                                                      child:
                                                                          Container(
                                                                        child:
                                                                            Image(
                                                                          image:
                                                                              AssetImage(ImageResource.radio_selected),
                                                                          height:
                                                                              24,
                                                                          width:
                                                                              24,
                                                                        ),
                                                                      ),
                                                                    )
                                                                  : Padding(
                                                                      padding:
                                                                          const EdgeInsets.all(
                                                                              5),
                                                                      child:
                                                                          Icon(
                                                                        Icons
                                                                            .radio_button_off_outlined,
                                                                        color: ColorResource
                                                                            .color222222
                                                                            .withOpacity(0.3),
                                                                        size:
                                                                            24,
                                                                      ),
                                                                    ),
                                                            ),
                                                            onTap: () {
                                                              setState(() {
                                                                selectedIndex =
                                                                    index;
                                                                bottomButtonVisible =
                                                                    true;
                                                                bloc.statementTypeModelList
                                                                    .forEach(
                                                                        (element) {
                                                                  element.isSelected =
                                                                      false;
                                                                });
                                                                bloc
                                                                    .statementTypeModelList[
                                                                        index]
                                                                    .isSelected = bloc
                                                                            .statementTypeModelList[index]
                                                                            .isSelected ==
                                                                        true
                                                                    ? false
                                                                    : true;

                                                                startDateController
                                                                    .text = '';
                                                                endDateController
                                                                    .text = '';
                                                                isDownload = bloc
                                                                    .statementTypeModelList[
                                                                        index]
                                                                    .isDownload;
                                                              });
                                                            },
                                                          ),
                                                          title: CustomText(
                                                            bloc
                                                                .statementTypeModelList[
                                                                    index]
                                                                .name,
                                                            style: Theme.of(
                                                                    context)
                                                                .textTheme
                                                                .subtitle1!
                                                                .copyWith(
                                                                    color: ColorResource
                                                                        .color222222,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w600),
                                                          ),
                                                        ),
                                                      ),
                                                      Container(
                                                          margin: EdgeInsets
                                                              .symmetric(
                                                                  horizontal:
                                                                      15),
                                                          child: Visibility(
                                                              visible: bloc
                                                                      .statementTypeModelList[
                                                                          index]
                                                                      .isSelected &&
                                                                  bloc
                                                                      .statementTypeModelList[
                                                                          index]
                                                                      .isDateRange,
                                                              child: Row(
                                                                  children: [
                                                                    Flexible(
                                                                      child:
                                                                          GestureDetector(
                                                                        onTap:
                                                                            () {
                                                                          _selectDate(
                                                                              context,
                                                                              true,
                                                                              startDateController);
                                                                        },
                                                                        child: WidgetUtils().dateTextField(
                                                                            context,
                                                                            Languages.of(context)!.startDate,
                                                                            startDateController),
                                                                      ),
                                                                    ),
                                                                    SizedBox(
                                                                      width: 10,
                                                                    ),
                                                                    Container(
                                                                        child: Flexible(
                                                                            child: GestureDetector(
                                                                      onTap:
                                                                          () {
                                                                        _selectDate(
                                                                            context,
                                                                            false,
                                                                            endDateController);
                                                                      },
                                                                      child: WidgetUtils().dateTextField(
                                                                          context,
                                                                          Languages.of(context)!
                                                                              .endDate,
                                                                          endDateController),
                                                                    )))
                                                                  ])))
                                                    ])));
                                          })
                                    ])))
                              ])
                            ]))),
                      )
                    ])),
              );
            }));
  }

  Future<void> _selectDate(BuildContext buildContext, bool isStartDate,
      TextEditingController controller) async {
    DateTime? pickedDate;

    if (isStartDate) {
      pickedDate = await showDatePicker(
          context: buildContext,
          initialDate: endDate != null ? endDate! : DateTime.now(),
          firstDate: DateTime(1994, 1, 5),
          lastDate: endDate != null ? endDate! : DateTime.now(),
          builder:
              // ignore: always_specify_types
              (context, child) {
            return Theme(
              data: Theme.of(context).copyWith(
                textTheme: TextTheme(
                  subtitle1: TextStyle(
                      fontSize: 10.0), // <-- here you can do your font smaller
                  headline1: TextStyle(fontSize: 8.0),
                ),
                colorScheme: const ColorScheme.light(
                  primary: ColorResource
                      // ignore: lines_longer_than_80_chars
                      .color641653, // header background color

                  // ignore: avoid_redundant_argument_values
                  onPrimary: ColorResource
                      // ignore: lines_longer_than_80_chars
                      .colorFFFFFF, // header text color

                  // ignore: avoid_redundant_argument_values
                  onSurface: Colors.black,
                  // body text color
                ),
                textButtonTheme: TextButtonThemeData(
                  style: TextButton.styleFrom(
                    primary: ColorResource
                        // ignore: lines_longer_than_80_chars
                        .color641653, // button text color
                  ),
                ),
              ),
              child: child!,
            );
          });
    } else {
      pickedDate = await showDatePicker(
          context: buildContext,
          initialDate: DateTime.now(),
          firstDate: startDate != null ? startDate! : DateTime(1994, 1, 5),
          lastDate: DateTime.now(),
          builder:
              // ignore: always_specify_types
              (context, child) {
            return Theme(
              data: Theme.of(context).copyWith(
                textTheme: TextTheme(
                  subtitle1: TextStyle(
                      fontSize: 10.0), // <-- here you can do your font smaller
                  headline1: TextStyle(fontSize: 7.0),
                ),
                colorScheme: const ColorScheme.light(
                  primary: ColorResource
                      // ignore: lines_longer_than_80_chars
                      .color641653, // header background color

                  // ignore: avoid_redundant_argument_values
                  onPrimary: ColorResource
                      // ignore: lines_longer_than_80_chars
                      .colorFFFFFF, // header text color

                  // ignore: avoid_redundant_argument_values
                  onSurface: Colors.black,
                  // body text color
                ),
                textButtonTheme: TextButtonThemeData(
                  style: TextButton.styleFrom(
                    primary: ColorResource
                        // ignore: lines_longer_than_80_chars
                        .color641653, // button text color
                  ),
                ),
              ),
              child: child!,
            );
          });
    }
    final DateFormat outputFormat = DateFormat('dd/MM/yyyy');
    var outputDate;
    setState(() {
      if (pickedDate != null) {
        if (isStartDate) {
          startDate = pickedDate;
          outputDate = outputFormat.format(startDate!);
        } else {
          endDate = pickedDate;
          outputDate = outputFormat.format(endDate!);
        }
      }
      if (outputDate != null) {
        controller.text = outputDate;
      }
    });
  }

  _showDownloadBottomSheet(BuildContext buildContext) {
    showModalBottomSheet(
      context: buildContext,
      elevation: 20,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(24.0), topRight: Radius.circular(24.0)),
      ),
      builder: (BuildContext context) {
        return Container(
          height: 150,
          margin: EdgeInsets.only(top: 10),
          padding: EdgeInsets.symmetric(horizontal: 10),
          width: double.infinity,
          alignment: Alignment.center,
          child: Column(
            children: [
              GestureDetector(
                child: ListTile(
                  trailing: Padding(
                    padding: EdgeInsets.all(5),
                    child: Container(
                      child: Image(
                        image: AssetImage(ImageResource.arrow),
                        height: 18,
                        width: 18,
                      ),
                    ),
                  ),
                  leading: Padding(
                    padding: EdgeInsets.all(5),
                    child: Container(
                      child: Image(
                        image: AssetImage(ImageResource.file),
                        height: 24,
                        width: 24,
                      ),
                    ),
                  ),
                  title: Transform(
                    transform: Matrix4.translationValues(-12, 0.0, 0.0),
                    child: CustomText(
                      Languages.of(context)!.downloadPDF,
                      style: Theme.of(context).textTheme.subtitle1!.copyWith(
                          color: ColorResource.color222222,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
                onTap: () async {
                  _requestPermissions();
                  Navigator.pop(context);
                  await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              const DownloadStatementPdf()));

                  // options = DownloaderUtils(
                  //   progressCallback: (current, total) {
                  //     final progress = (current / total) * 100;
                  //     print('Downloading: $progress');
                  //   },
                  //   file: File('$path/Icici.pdf'),
                  //   progress: ProgressImplementation(),
                  //   onDone: () => print('COMPLETE'),
                  //   deleteOnCancel: true,
                  //
                  // );
                  // core = await Flowder.download(
                  //     'https://www.icicibank.com/managed-assets/docs/form-center/cc_auto.pdf',
                  //     options);
                },
              ),
              GestureDetector(
                child: ListTile(
                  trailing: Padding(
                    padding: EdgeInsets.all(5),
                    child: Container(
                      child: Image(
                        image: AssetImage(ImageResource.arrow),
                        height: 18,
                        width: 18,
                      ),
                    ),
                  ),
                  leading: Padding(
                    padding: EdgeInsets.all(5),
                    child: Container(
                      child: Image(
                        image: AssetImage(ImageResource.mail),
                        height: 24,
                        width: 24,
                      ),
                    ),
                  ),
                  title: Transform(
                    transform: Matrix4.translationValues(-12, 0.0, 0.0),
                    child: CustomText(
                      Languages.of(context)!.downloadStatementOverEmail,
                      style: Theme.of(context).textTheme.subtitle1!.copyWith(
                          color: ColorResource.color222222,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
                onTap: () {
                  Navigator.pop(context);
                  _showSentSuccessBottomSheet(context);
                },
              ),
            ],
          ),
        );
      },
    );
  }

  _showSentSuccessBottomSheet(BuildContext buildContext) {
    showModalBottomSheet(
      context: buildContext,
      isScrollControlled: true,
      backgroundColor: ColorResource.colorFFFFFF,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(24.0),
          topRight: Radius.circular(24.0),
        ),
      ),
      builder: (BuildContext context) {
        return Container(
          padding: EdgeInsets.symmetric(vertical: 25, horizontal: 25),
          // alignment: Alignment.center,
          constraints: const BoxConstraints(
            minHeight: 150,
            maxHeight: 420,
          ),
          child: Column(
            children: [
              Container(
                child: const Image(
                    image: AssetImage(ImageResource.statement_sent_background)),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                child: CustomText(Languages.of(context)!.statementSentMessage,
                    style: Theme.of(context).textTheme.headline5!.copyWith(
                        color: ColorResource.color222222,
                        fontWeight: FontWeight.w600)),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                child: CustomText(Languages.of(context)!.statementSentNote,
                    style: Theme.of(context).textTheme.subtitle2!.copyWith(
                        color: ColorResource.color222222.withOpacity(0.4),
                        fontWeight: FontWeight.w500)),
              ),
              CustomButton(
                Languages.of(context)!.done,
                onTap: () async {
                  int count = 0;
                  Navigator.popUntil(context, (route) {
                    return count++ == 3;
                  });
                },
              )
            ],
          ),
        );
      },
    );
  }

  Future<bool> _requestPermissions() async {
    var status = await Permission.storage.status;

    if (status != PermissionStatus.granted) {
      await Permission.storage.request();
    }
    return status == PermissionStatus.granted;
  }
}
