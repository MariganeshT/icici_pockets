import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:icici/base/base_state.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/model/statement_type_model.dart';
import 'package:icici/screens/statement_details/statement_details_event.dart';

class StatementDetailsBloc extends Bloc<StatementDetailsEvent, BaseState> {
  StatementDetailsBloc() : super(InitialState());

  List<StatementTypeModel> statementTypeModelList = [];

  @override
  Stream<BaseState> mapEventToState(
    StatementDetailsEvent event,
  ) async* {
    if (event is StatementDetailsInitialEvent) {
      yield InitialState();
      statementTypeModelList.addAll([
        StatementTypeModel(
            Languages.of(event.context!)!.lastMonth, false, false, false),
        StatementTypeModel(
            Languages.of(event.context!)!.lastThreeMonth, false, false, false),
        StatementTypeModel(
            Languages.of(event.context!)!.lastSixMonth, false, false, true),
        StatementTypeModel(Languages.of(event.context!)!.selectCustomDateRange,
            false, true, true),
      ]);

      yield SuccessState();
    }
  }
}
