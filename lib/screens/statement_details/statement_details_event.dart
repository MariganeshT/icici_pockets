import 'package:flutter/cupertino.dart';
import 'package:icici/utils/base_equatable.dart';

@immutable
abstract class StatementDetailsEvent extends BaseEquatable {}

class StatementDetailsInitialEvent extends StatementDetailsEvent {
  BuildContext? context;

  StatementDetailsInitialEvent({this.context});
}
