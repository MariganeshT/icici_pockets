import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/screens/landing_page/bloc/landing_bloc.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/font.dart';
import 'package:icici/utils/preference_helper.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_button.dart';

import '../../router.dart';

class LandingScreen extends StatefulWidget {
  LandingScreen({Key? key}) : super(key: key);

  @override
  _LandingScreenState createState() => _LandingScreenState();
}

class _LandingScreenState extends State<LandingScreen> {
  late LandingBloc bloc;
  int _current = 0;

  PageController controller = PageController();
  var currentPageValue = 0.0;

  List<Color> backGroundColors = [
    ColorResource.colorfdf3e6,
    ColorResource.coloreffee5,
    ColorResource.color5765f2
  ];

  @override
  void initState() {
    bloc = BlocProvider.of<LandingBloc>(context);
    controller.addListener(() {
      setState(() {
        _current = controller.page!.toInt();
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: bloc,
      listener: (BuildContext context, LandingState state) {},
      child: BlocBuilder(
        bloc: bloc,
        builder: (BuildContext context, LandingState state) {
          if (state is LandingInitial) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }

          return Scaffold(
            body: Stack(
              children: [
                PageView.builder(
                  itemCount: bloc.banners.length,
                  physics: const ClampingScrollPhysics(),
                  controller: controller,
                  itemBuilder: (BuildContext context, int position) {
                    if (position == 0) {
                      return landing1Widget();
                    }
                    if (position == 1) {
                      return landing2Widget();
                    }
                    if (position == 2) {
                      return landing3Widget();
                    }
                    return Container();
                  },
                ),
                Column(
                  children: [
                    Spacer(),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 28.0, vertical: 48),
                      child: Row(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            // ignore: unnecessary_this
                            children: this.bloc.banners.map((Widget url) {
                              // ignore: unnecessary_this
                              final int index = this.bloc.banners.indexOf(url);

                              return AnimatedContainer(
                                duration: Duration(milliseconds: 150),
                                width: 8.0,
                                height: 8.0,
                                margin: const EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 5.0),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: _current == index
                                      ? ColorResource.colorFF781F
                                      : ColorResource.colorFF781F
                                          .withOpacity(0.4),
                                ),
                              );
                            }).toList(),
                          ),
                          const Spacer(),
                          SizedBox(
                              width: 131,
                              child: CustomButton(
                                StringResource.getStarted,
                                onTap: () async {
                                  final bool value = await PreferenceHelper
                                      .getBioMetricValue();

                                  print(value);

                                  // // bloc.add(LandingPageBioAuthEvent());
                                  while (Navigator.canPop(context)) {
                                    Navigator.pop(context);
                                  }

                                  await Navigator.pushReplacementNamed(
                                      context, AppRoutes.welcomeScreen);
                                },
                              ))
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  Widget landing1Widget() {
    return Container(
      color: bloc.backgroundColors[0],
      padding: const EdgeInsets.only(top: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Spacer(),
              Padding(
                padding: const EdgeInsets.only(
                  top: 7.0,
                  right: 32,
                ),
                child: GestureDetector(
                  onTap: () {
                    while (Navigator.canPop(context)) {
                      Navigator.pop(context);
                    }

                    Navigator.pushReplacementNamed(
                        context, AppRoutes.welcomeScreen);
                  },
                  child: CustomText(
                    StringResource.skip,
                    style: Theme.of(context)
                        .textTheme
                        .subtitle1!
                        .copyWith(color: ColorResource.color222222),
                  ),
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 35.0, left: 28, right: 28),
            child: CustomText(
              bloc.titles[0],
              style: Theme.of(context).textTheme.headline6!.copyWith(
                  color: ColorResource.color222222,
                  fontWeight: FontWeights.bold),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
                top: 12.0, left: 28, right: 28, bottom: 27),
            child: CustomText(
              bloc.descriptions[0],
              style: Theme.of(context)
                  .textTheme
                  .subtitle1!
                  .copyWith(color: ColorResource.color222222.withOpacity(0.7)),
            ),
          ),
          Expanded(
              child: AspectRatio(
            aspectRatio: 1.33,
            child: Container(child: bloc.banners[0]),
          )),
          const SizedBox(
            height: 129,
          )
        ],
      ),
    );
  }

  Widget landing2Widget() {
    return Container(
      color: bloc.backgroundColors[1],
      padding: const EdgeInsets.only(top: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Spacer(),
              Padding(
                padding: const EdgeInsets.only(
                  top: 7.0,
                  right: 32,
                ),
                child: GestureDetector(
                  onTap: () {
                    while (Navigator.canPop(context)) {
                      Navigator.pop(context);
                    }

                    Navigator.pushReplacementNamed(
                        context, AppRoutes.welcomeScreen);
                  },
                  child: CustomText(
                    StringResource.skip,
                    style: Theme.of(context)
                        .textTheme
                        .subtitle1!
                        .copyWith(color: ColorResource.color222222),
                  ),
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 35.0, left: 28, right: 28),
            child: CustomText(
              bloc.titles[1],
              style: Theme.of(context).textTheme.headline6!.copyWith(
                  color: ColorResource.color222222,
                  fontWeight: FontWeights.bold),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 12.0, left: 28, right: 28),
            child: CustomText(
              bloc.descriptions[1],
              style: Theme.of(context)
                  .textTheme
                  .subtitle1!
                  .copyWith(color: ColorResource.color222222.withOpacity(0.7)),
            ),
          ),
          Expanded(
              child: AspectRatio(
                  aspectRatio: 1.33, child: Container(child: bloc.banners[1]))),
          const SizedBox(
            height: 129,
          )
        ],
      ),
    );
  }

  Widget landing3Widget() {
    return Container(
      color: bloc.backgroundColors[2],
      padding: const EdgeInsets.only(top: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Spacer(),
              Padding(
                padding: const EdgeInsets.only(
                  top: 7.0,
                  right: 32,
                ),
                child: CustomText(
                  '',
                  fontSize: 16,
                  color: ColorResource.color1A1151,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 35.0, left: 28, right: 28),
            child: CustomText(
              bloc.titles[2],
              style: Theme.of(context).textTheme.headline6!.copyWith(
                  color: ColorResource.color222222,
                  fontWeight: FontWeights.bold),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 12.0, left: 28, right: 28),
            child: CustomText(
              bloc.descriptions[2],
              style: Theme.of(context)
                  .textTheme
                  .subtitle1!
                  .copyWith(color: ColorResource.color222222.withOpacity(0.9)),
            ),
          ),
          Expanded(
              child: AspectRatio(
            aspectRatio: 1.33,
            child: Container(child: bloc.banners[2]),
          )),
          const SizedBox(
            height: 129,
          )
        ],
      ),
    );
  }
}
