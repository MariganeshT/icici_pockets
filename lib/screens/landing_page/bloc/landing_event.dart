part of 'landing_bloc.dart';

@immutable
abstract class LandingEvent extends BaseEquatable {}

class LandingInitialEvent extends LandingEvent {}

class LandingPageBioAuthEvent extends LandingEvent{}

