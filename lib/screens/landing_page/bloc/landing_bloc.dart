import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:local_auth/error_codes.dart' as auth_error;
import 'package:local_auth/local_auth.dart';
import 'package:meta/meta.dart';

part 'landing_event.dart';
part 'landing_state.dart';

class LandingBloc extends Bloc<LandingEvent, LandingState> {
  LandingBloc() : super(LandingInitial());
  late String version;
  List<Widget> banners = [];
  List<Color> backgroundColors = [];
  List<String> titles = [];
  List<String> descriptions = [];

  @override
  Stream<LandingState> mapEventToState(
    LandingEvent event,
  ) async* {
    if (event is LandingInitialEvent) {
      banners = [
        Image.asset(
          ImageResource.landing1,

          // fit: BoxFit.fill,
          // fit: BoxFit.fitHeight,
        ),
        Image.asset(
          ImageResource.landing2,
          // fit: BoxFit.fill,

          // fit: BoxFit.fitHeight,
        ),
        Image.asset(
          ImageResource.landing3,
          // fit: BoxFit.fill,

          // fit: BoxFit.fitHeight,
        ),
      ];

      backgroundColors = [
        ColorResource.colorfdf3e6,
        ColorResource.coloreffee5,
        ColorResource.colorEFEFEF
      ];

      titles = [
        StringResource.landing1Title,
        StringResource.landing2Title,
        StringResource.landing3Title,
      ];

      descriptions = [
        StringResource.landing1Description,
        StringResource.landing2Description,
        StringResource.landing3Description,
      ];
      yield LandingLoadedState();
    }

    if (event is LandingPageBioAuthEvent) {
      final LocalAuthentication localAuth = LocalAuthentication();
      final List<BiometricType> availableBiometrics =
          await localAuth.getAvailableBiometrics();
      try {
        final bool didAuthenticate = await localAuth.authenticate(
          localizedReason: 'Log In to your account instantly!',
          biometricOnly: true,
        );
        if (didAuthenticate) {
          // ignore: avoid_print
          print('Authenticated');
        } else {
          // ignore: avoid_print
          print('Unauthenticated');
        }
      } on PlatformException catch (e) {
        if (e.code == auth_error.notAvailable) {
          // Handle this exception here.
        }
      }
    }
  }
}
