part of 'landing_bloc.dart';

@immutable
abstract class LandingState {}

class LandingInitial extends LandingState {}

class LandingLoadedState extends LandingState {}

class LandingLoadingState extends LandingState {}

