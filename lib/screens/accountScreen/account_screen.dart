// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:icici/router.dart';
// import 'package:icici/screens/accountScreen/bloc/accountscreen_bloc.dart';
// import 'package:icici/utils/color_resource.dart';
// import 'package:icici/utils/image_resource.dart';
// import 'package:icici/utils/string_resource.dart';
// import 'package:icici/widgets/customText.dart';
// import 'package:icici/widgets/custom_listtile.dart';

// class AccountScreen extends StatefulWidget {
//   const AccountScreen({Key? key}) : super(key: key);

//   @override
//   _AccountScreenState createState() => _AccountScreenState();
// }

// class _AccountScreenState extends State<AccountScreen> {
//   late AccountscreenBloc accountscreenBloc;

//   @override
//   void initState() {
//     accountscreenBloc = BlocProvider.of<AccountscreenBloc>(context);
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     SystemChrome.setSystemUIOverlayStyle(
//         const SystemUiOverlayStyle(statusBarColor: Colors.transparent));
//     return Scaffold(
//         appBar: AppBar(
//           centerTitle: false,
//           title: CustomText(
//             StringResource.accounts,
//             fontSize: 24,
//             fontWeight: FontWeight.w600,
//             color: ColorResource.colorFFFFFF,
//           ),
//           leading: Icon(
//             Icons.arrow_back,
//             color: ColorResource.colorFFFFFF,
//             size: 24,
//           ),
//         ),
//         body: BlocListener<AccountscreenBloc, AccountscreenState>(
//             bloc: accountscreenBloc,
//             listener: (BuildContext context, AccountscreenState state) {
//               // if (state is ProfileBottomSheetState) {
//               //   addressBottomSheet();
//               // }
//             },
//             child: BlocBuilder<AccountscreenBloc, AccountscreenState>(
//                 bloc: accountscreenBloc,
//                 builder: (BuildContext context, AccountscreenState state) {
//                   if (state is AccountscreenLoadingState) {
//                     return Center(
//                       child: CircularProgressIndicator(),
//                     );
//                   }
//                   return Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       Padding(
//                         padding: const EdgeInsets.only(top: 30, left: 24),
//                         child: CustomText(
//                           StringResource.accounts,
//                           color: ColorResource.color222222,
//                           fontWeight: FontWeight.bold,
//                           fontSize: 20,
//                         ),
//                       ),
//                       Container(
//                         margin: EdgeInsets.only(
//                             top: 12, left: 12, right: 12, bottom: 24),
//                         decoration: BoxDecoration(
//                             borderRadius: BorderRadius.circular(14),
//                             boxShadow: [
//                               BoxShadow(
//                                 blurRadius: 0.2,
//                                 offset: Offset(0, 4),
//                                 color: ColorResource.colorFFFFFF,
//                               )
//                             ]),
//                         child: Column(
//                             crossAxisAlignment: CrossAxisAlignment.center,
//                             mainAxisAlignment: MainAxisAlignment.center,
//                             children: [
//                               Container(
//                                 child: CustomListTile(
//                                   Image.asset(ImageResource.accountLogo),
//                                   StringResource.accountDetails,
//                                   onTap: () async {
//                                     await Navigator.pushNamed(
//                                         context, AppRoutes.accountDetails);
//                                   },
//                                 ),
//                               ),
//                               CustomListTile(
//                                 Image.asset(ImageResource.setTransLimit),
//                                 StringResource.setTransLimit,
//                               ),
//                               CustomListTile(
//                                   Image.asset(ImageResource.walletClosureLogo),
//                                   StringResource.reqForWalletClosure)
//                             ]),
//                       ),
//                       Padding(
//                         padding: const EdgeInsets.only(left: 24),
//                         child: CustomText(
//                           StringResource.others,
//                           color: ColorResource.color222222,
//                           fontWeight: FontWeight.bold,
//                           fontSize: 20,
//                         ),
//                       ),
//                       Container(
//                         margin: EdgeInsets.only(left: 12, right: 12, top: 12),
//                         decoration: BoxDecoration(
//                             borderRadius: BorderRadius.circular(14),
//                             boxShadow: [
//                               BoxShadow(
//                                 blurRadius: 0.2,
//                                 offset: Offset(0, 4),
//                                 color: ColorResource.colorFFFFFF,
//                               )
//                             ]),
//                         child: CustomListTile(Image.asset(ImageResource.themes),
//                             StringResource.themes,

//                         ),
//                       )
//                     ],
//                   );
//                 })));
//   }
// }
