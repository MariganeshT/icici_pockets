import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:icici/widgets/custom_button.dart';

import 'bloc/myreferal_bloc.dart';

class MyReferalScreen extends StatefulWidget {
  MyReferalScreen({Key? key}) : super(key: key);

  @override
  _MyReferalScreenState createState() => _MyReferalScreenState();
}

class _MyReferalScreenState extends State<MyReferalScreen> {
  late MyreferalBloc bloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc = MyreferalBloc()..add(MyreferalInitialEvent());
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    return BlocListener<MyreferalBloc, MyreferalState>(
      bloc: bloc,
      listener: (BuildContext context, MyreferalState state) {},
      child: BlocBuilder<MyreferalBloc, MyreferalState>(
        bloc: bloc,
        builder: (BuildContext context, MyreferalState state) {
          if (state is MyreferalLoadingState) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          return Scaffold(
            bottomNavigationBar: Container(
              padding: EdgeInsets.symmetric(horizontal: 24, vertical: 40),
              child: CustomButton(
                StringResource.myRewards,
                onTap: () {
                  Fluttertoast.showToast(
                      msg: 'Coming Soon',
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.CENTER,
                      backgroundColor: ColorResource.colorFF781F,
                      textColor: Colors.white,
                      fontSize: 12.0);
                },
              ),
            ),
            body: SafeArea(
              top: false,
              child: Column(
                children: [
                  CustomAppbar(
                    backgroundColor: ColorResource.color641653,
                    titleString: StringResource.myreferals,
                    titleSpacing: 8,
                    textButtonString: '',
                    style: Theme.of(context)
                        .textTheme
                        .headline5!
                        .copyWith(color: ColorResource.colorFFFFFF),
                    iconEnumValues: IconEnum.back,
                    onItemSelected: (selectedItem) {
                      Navigator.pop(context);
                    },
                  ),
                  bloc.invitedList.isEmpty
                      ? _noReferalFound()
                      : Expanded(child: _invitedList()),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _noReferalFound() {
    return Expanded(
        child: Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 35),
      child: Column(
        children: [
          Container(
              height: 280,
              width: 280,
              child: Image.asset(ImageResource.noReferalFound)),
          const SizedBox(
            height: 30,
          ),
          CustomText(
            StringResource.noReferralsFound,
            style: Theme.of(context).textTheme.headline5,
          ),
          const SizedBox(
            height: 12,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: CustomText(
              StringResource.noteMyReferal,
              textAlign: TextAlign.center,
              style:
                  Theme.of(context).textTheme.subtitle2!.copyWith(fontSize: 14),
            ),
          ),
        ],
      ),
    ));
  }

  Widget _invitedList() {
    return ListView.builder(
      padding: EdgeInsets.symmetric(vertical: 20),
      dragStartBehavior: DragStartBehavior.down,
      itemCount: bloc.invitedList.length,
      itemBuilder: (BuildContext context, int index) {
        return Padding(
          padding: const EdgeInsets.only(left: 10, right: 14),
          child: ListTile(
            leading: Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
              ),
              child: CircleAvatar(
                maxRadius: 22,
                backgroundImage: NetworkImage(bloc.invitedList[index].image),
                backgroundColor: ColorResource.colorFF781F,
              ),
            ),
            title: CustomText(
              bloc.invitedList[index].name,
              style: Theme.of(context).textTheme.bodyText1,
            ),
            subtitle: CustomText(
              bloc.invitedList[index].mobileNo,
              style: Theme.of(context)
                  .textTheme
                  .subtitle1!
                  .copyWith(color: ColorResource.color787878, height: 1.5),
            ),
            trailing: bloc.invitedList[index].inviteResponse
                ? CustomText(
                    StringResource.accepted,
                    style: Theme.of(context).textTheme.subtitle1!.copyWith(
                        color: ColorResource.color11AB04,
                        fontWeight: FontWeight.w700),
                  )
                : CustomText(
                    StringResource.invited,
                    style: Theme.of(context).textTheme.subtitle1!.copyWith(
                        color: ColorResource.color787878,
                        fontWeight: FontWeight.w600),
                  ),
          ),
        );
      },
    );
  }
}
