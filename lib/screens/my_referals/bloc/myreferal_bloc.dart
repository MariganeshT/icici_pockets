import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:icici/model/refer&earn_model.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:meta/meta.dart';

part 'myreferal_event.dart';
part 'myreferal_state.dart';

class MyreferalBloc extends Bloc<MyreferalEvent, MyreferalState> {
  MyreferalBloc() : super(MyreferalInitial());

  List<MyReferalModel> invitedList = [];

  @override
  Stream<MyreferalState> mapEventToState(
    MyreferalEvent event,
  ) async* {
    if (event is MyreferalInitialEvent) {
      yield MyreferalLoadingState();

      invitedList.addAll([
        // MyReferalModel('https://i.stack.imgur.com/0VpX0.png', 'New User', '987653210', true),
        // MyReferalModel('https://i.stack.imgur.com/0VpX0.png', 'New User', '987653210', false),
      ]);

      yield MyreferalLoadedState();
    }
  }
}
