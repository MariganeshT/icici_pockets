part of 'myreferal_bloc.dart';

@immutable
abstract class MyreferalEvent extends BaseEquatable {}

class MyreferalInitialEvent extends MyreferalEvent {}