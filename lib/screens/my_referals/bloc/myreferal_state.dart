part of 'myreferal_bloc.dart';

@immutable
abstract class MyreferalState {}

class MyreferalInitial extends MyreferalState {}

class MyreferalLoadingState extends MyreferalState {}

class MyreferalLoadedState extends MyreferalState {}