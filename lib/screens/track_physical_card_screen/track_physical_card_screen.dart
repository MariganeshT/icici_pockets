import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:icici/widgets/custom_button.dart';

import '../../router.dart';
import 'bloc/track_physical_card_bloc.dart';

class TrackPhysicalCardScreen extends StatefulWidget {
  const TrackPhysicalCardScreen({Key? key}) : super(key: key);

  @override
  _TrackPhysicalCardScreenState createState() =>
      _TrackPhysicalCardScreenState();
}

class _TrackPhysicalCardScreenState extends State<TrackPhysicalCardScreen> {
  TrackPhysicalCardBloc? bloc;

  @override
  void initState() {
    super.initState();
    bloc = TrackPhysicalCardBloc()..add(TrackPhysicalCardInitialEvent());
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    return SafeArea(
      top: false,
      child: Scaffold(
          backgroundColor: ColorResource.colorFAFAFA,
          body: BlocListener<TrackPhysicalCardBloc, TrackPhysicalCardState>(
            bloc: bloc,
            listener: (BuildContext context, TrackPhysicalCardState state) {},
            child: BlocBuilder<TrackPhysicalCardBloc, TrackPhysicalCardState>(
              bloc: bloc,
              builder: (BuildContext context, TrackPhysicalCardState state) {
                return Column(
                  children: <Widget>[
                    CustomAppbar(
                      backgroundColor: ColorResource.color641653,
                      titleString: Languages.of(context)!.trackPhysicalCard,
                      titleSpacing: 8,
                      style: Theme.of(context)
                          .textTheme
                          .headline5!
                          .copyWith(color: ColorResource.colorFFFFFF),
                      iconEnumValues: IconEnum.back,
                      onItemSelected: (selectedItem) {
                        Navigator.pop(context);
                      },
                    ),
                    Expanded(
                        child: SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 24, 0, 0),
                        child: Column(
                          children: <Widget>[
                            const Center(
                              child: Image(
                                  height: 250,
                                  image: AssetImage(ImageResource.trackCard)),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 37.0, bottom: 12),
                              child: CustomText(
                                  Languages.of(context)!
                                      .nothingyouorderedPhysicalCard,
                                  style: Theme.of(context).textTheme.headline5),
                            ),
                            CustomText(
                              'Dummy content, will be added later some time',
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle1!
                                  .copyWith(
                                    color: ColorResource.color222222
                                        .withOpacity(0.5),
                                  ),
                            ),
                          ],
                        ),
                      ),
                    )),
                  ],
                );
              },
            ),
          ),
          bottomSheet: Padding(
            padding: const EdgeInsets.all(24),
            child: CustomButton(
              Languages.of(context)!.requestPhysicalCard,
              onTap: () {
                Navigator.pushReplacementNamed(
                    context, AppRoutes.requestPhysicalCardScreen);
              },
            ),
          )),
    );
  }
}
