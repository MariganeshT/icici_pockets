part of 'track_physical_card_bloc.dart';

@immutable
abstract class TrackPhysicalCardState extends BaseEquatable {}

class TrackPhysicalCardInitial extends TrackPhysicalCardState {}
