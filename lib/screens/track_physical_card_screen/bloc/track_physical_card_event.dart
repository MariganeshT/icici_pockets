part of 'track_physical_card_bloc.dart';

@immutable
class TrackPhysicalCardEvent extends BaseEquatable {}

class TrackPhysicalCardInitialEvent extends TrackPhysicalCardEvent {}
