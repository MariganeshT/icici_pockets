import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:meta/meta.dart';

part 'track_physical_card_event.dart';
part 'track_physical_card_state.dart';

class TrackPhysicalCardBloc
    extends Bloc<TrackPhysicalCardEvent, TrackPhysicalCardState> {
  TrackPhysicalCardBloc() : super(TrackPhysicalCardInitial());

  @override
  Stream<TrackPhysicalCardState> mapEventToState(
    TrackPhysicalCardEvent event,
  ) async* {}
}
