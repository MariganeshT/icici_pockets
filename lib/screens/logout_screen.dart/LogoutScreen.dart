import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:icici/router.dart';
import 'package:icici/screens/settings_main_screen/bloc/settings_main_screen_bloc.dart';
import 'package:icici/utils/app_utils.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_button.dart';

class LogoutScreen extends StatefulWidget {
  SettingsMainScreenBloc bloc;
  LogoutScreen(this.bloc, {Key? key}) : super(key: key);

  @override
  _LogoutScreenState createState() => _LogoutScreenState();
}

class _LogoutScreenState extends State<LogoutScreen> {
  @override
  Widget build(BuildContext context) {
    return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
            return WillPopScope(
              onWillPop: () async => false,
              child: Container(
                padding: MediaQuery.of(context).viewInsets,
                child: Container(
                  decoration: const BoxDecoration(
                    color: ColorResource.colorFFFFFF,
                    borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(32.0),
                    topRight: Radius.circular(32.0)),
                    ),
                  
                  child: Stack(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 14, top: 12),
                        child: GestureDetector(
                          onTap: (){
                            Navigator.pop(context);
                          },
                          child: Image.asset(
                              ImageResource.close)
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(25, 5, 25, 30),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            
                              Center(
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 10),
                                  child: Container(
                                    height: 200,
                                      width: 200,
                                      child: Image.asset(ImageResource.logoutImage2)),
                                )),
                                const SizedBox(height: 5,),
                              CustomText(StringResource.areYouSure,
                                        style: Theme.of(context)
                                        .textTheme.headline5!
                                        .copyWith(fontSize: 24)),
                              const SizedBox(height: 15,),
                              CustomText(
                                StringResource.noteLogout,
                              textAlign: TextAlign.start,
                                        style: 
                                        Theme.of(context).textTheme
                                        .subtitle1!.copyWith(
                                          color: ColorResource.color787878,
                                           fontWeight: FontWeight.w400)),
                              const SizedBox(height: 40,),
                             CustomButton(
                               StringResource.logOut,
                               onTap: (){
                                 AppUtils.showToast('Loged out');
                                 widget.bloc.add(
                                   NavigateReturnUserScreenEvent());
                                
                               },
                               ),
                              
                          ],
                        ),
                      ),
                    ],
                  ),),),
            );
              }
            );
  }


}