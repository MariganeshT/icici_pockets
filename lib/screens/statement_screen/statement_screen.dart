import 'dart:io';
import 'dart:isolate';
import 'dart:ui';

import 'package:downloads_path_provider_28/downloads_path_provider_28.dart';
import 'package:flowder/flowder.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:icici/base/base_state.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/languages/app_locale_constant.dart';
import 'package:icici/model/profile_model.dart';
import 'package:icici/router.dart';
import 'package:icici/screens/statement_screen/statement_screen_bloc.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:icici/widgets/custom_button.dart';
import 'package:icici/widgets/custom_dialog.dart';
import 'package:icici/widgets/download_statement_widget.dart';
import 'package:icici/widgets/widget_utils.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

class StatementScreen extends StatefulWidget {
  const StatementScreen({Key? key}) : super(key: key);

  @override
  _StatementScreenState createState() => _StatementScreenState();
}

class _StatementScreenState extends State<StatementScreen> {
  Locale? _locale;
  late StatementScreenBloc bloc;
  bool showFilter = false;
  bool showDownload = false;
  String? rangeType;
  List<String> filterString = [];
  late DownloaderUtils options;
  late DownloaderCore core;
   late final String path;
  String appBarTitleName = '';

  ReceivePort _port = ReceivePort();

  @override
  void didChangeDependencies() {
    getLocale().then((Locale locale) {
      setState(() {
        _locale = locale;
      });
    });
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<StatementScreenBloc>(context);
    initPlatformState();
    IsolateNameServer.registerPortWithName(
        _port.sendPort, 'downloader_send_port');
    _port.listen((dynamic data) {
      String id = data[0];
      DownloadTaskStatus status = data[1];

      int progress = data[2];
      if (status.toString() == "DownloadTaskStatus(3)" &&
          progress == 100 &&
          id != null) {
        String query = "SELECT * FROM task WHERE task_id='" + id + "'";
        var tasks = FlutterDownloader.loadTasksWithRawQuery(query: query);
        //if the task exists, open it
        if (tasks != null) FlutterDownloader.open(taskId: id);
      }
    });

    FlutterDownloader.registerCallback(downloadCallback);
  }

  @override
  void dispose() {
    IsolateNameServer.removePortNameMapping('downloader_send_port');
    super.dispose();
  }

  static void downloadCallback(String id, DownloadTaskStatus status,
      int progress) {
    final SendPort? send =
    IsolateNameServer.lookupPortByName('downloader_send_port');
    send!.send([id, status, progress]);
  }

  @override
  Widget build(BuildContext context) {
    appBarTitleName = Languages.of(context)!.statement;
    SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: Colors.transparent));
    return BlocListener(
      bloc: bloc,
      listener: (BuildContext context, BaseState state) {},
      child: BlocBuilder(
        bloc: bloc,
        builder: (BuildContext context, BaseState state) {
          if (state is LoadingState) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          return SafeArea(
            top: false,
            child: Scaffold(
              body: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    color: ColorResource.color641653,
                    child: CustomAppbar(
                      titleString: appBarTitleName,
                      iconEnumValues: IconEnum.back,
                      showDownload: showDownload,
                      showFilter: showFilter,
                      onItemSelected: (values) {
                        if (values == 'filter') {
                          _showFilterBottomSheet(context, bloc);
                        } else if (values == 'download') {
                          _showDownloadBottomSheet(context);
                        } else {
                          Navigator.pop(context);
                        }
                      },
                    ),
                  ),
                  if (rangeType != null)
                    Container(
                      child: CustomText(
                        rangeType!,
                        style: Theme
                            .of(context)
                            .textTheme
                            .subtitle2!
                            .copyWith(
                            color: ColorResource.color787878,
                            fontWeight: FontWeight.w600),
                      ),
                      margin: const EdgeInsets.only(top: 16, left: 12),
                    ),
                  if (filterString.isNotEmpty)
                    SizedBox(
                      height: 45,
                      child: Container(
                        margin: const EdgeInsets.only(left: 10.0, top: 5.0),
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: filterString.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Card(
                              color: ColorResource.colorFFFFFF,
                              shape: const RoundedRectangleBorder(
                                side: BorderSide(
                                    color: ColorResource.colorF1F1FD,
                                    width: 1.0),
                                borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                              ),
                              child: Container(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 4.0, vertical: 8.0),
                                margin:
                                const EdgeInsets.symmetric(horizontal: 5.0),
                                alignment: Alignment.center,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    CustomText(
                                      filterString[index].toString(),
                                      style: Theme
                                          .of(context)
                                          .textTheme
                                          .subtitle1!
                                          .copyWith(
                                          color: ColorResource.color787878,
                                          fontWeight: FontWeight.w600),
                                      textAlign: TextAlign.center,
                                    ),
                                    const SizedBox(
                                      width: 8,
                                    ),
                                    GestureDetector(
                                      child: const Icon(
                                        Icons.close,
                                        color: ColorResource.color787878,
                                        size: 14,
                                      ),
                                      onTap: () {
                                        setState(() {
                                          bloc.filterList.forEach(
                                                  (List<ProfileModel>
                                              profileModelElement) {
                                                profileModelElement.forEach(
                                                        (ProfileModel element) {
                                                      if (filterString[index] ==
                                                          element.name) {
                                                        element.isSelected =
                                                        false;
                                                        profileModelElement[0]
                                                            .isSelected = false;
                                                      }
                                                    });
                                              });
                                          filterString
                                              .remove(filterString[index]);
                                        });
                                      },
                                    )
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                  Flexible(
                    flex: 2,
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          MediaQuery.removePadding(
                            context: context,
                            removeBottom: true,
                            removeTop: true,
                            child: Container(
                              margin: const EdgeInsets.only(bottom: 80),
                              child: Card(
                                  shape: RoundedRectangleBorder(
                                    side:
                                    const BorderSide(color: Colors.white70),
                                    borderRadius: BorderRadius.circular(14),
                                  ),
                                  margin: const EdgeInsets.all(12),
                                  elevation: 2,
                                  child: Column(children: [
                                    ListView.builder(
                                        itemCount:
                                        bloc.statementModelList.length,
                                        shrinkWrap: true,
                                        physics:
                                        const NeverScrollableScrollPhysics(),
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return GestureDetector(
                                            onTap: () {
                                              Navigator.pushNamed(
                                                  context,
                                                  AppRoutes
                                                      .paymentStatusScreen);
                                            },
                                            child: WidgetUtils()
                                                .statementListWidget(
                                                bloc.statementModelList[
                                                index],
                                                context),
                                          );
                                        }),
                                  ])),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              bottomSheet: Padding(
                padding:
                const EdgeInsets.only(left: 12.0, right: 12, bottom: 12),
                child: SizedBox(
                  // margin: EdgeInsets.only(bottom: 20),
                  child: CustomButton(
                    Languages.of(context)!.viewFullHistory,
                    onTap: () async {
                      setState(() {
                        rangeType = null;
                      });
                      final dynamic result = await Navigator.pushNamed(
                          context, AppRoutes.statementDetailsScreen);
                      setState(() {
                        if (result == 'backNavigation') {
                          showDownload = false;
                          showFilter = false;
                          filterString.clear();
                          bloc.filterList.forEach((List<ProfileModel> element) {
                            element.forEach((ProfileModel element) {
                              element.isSelected = false;
                            });
                          });
                        } else {
                          if (result != null) {
                            int value = result!['index'];
                            String from = result!['from'];
                            rangeType = result!['rangeType'];
                            if (from == 'statementDetailsScreen') {
                              showDownload = true;
                              showFilter = true;
                            }
                            //if values == 3 //Date range statement
                            if (value == 3) {
                              DateTime startDate = result!['startDate'];
                              DateTime endDate = result!['endDate'];
                            }
                          }
                        }
                      });
                    },
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  _showDownloadBottomSheet(BuildContext buildContext) {
    showModalBottomSheet(
      context: buildContext,
      elevation: 20,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(24.0), topRight: Radius.circular(24.0)),
      ),
      builder: (BuildContext context) {
        return Container(
          height: 150,
          margin: EdgeInsets.only(top: 10),
          padding: EdgeInsets.symmetric(horizontal: 10),
          width: double.infinity,
          alignment: Alignment.center,
          child: Column(
            children: [
              GestureDetector(
                child: ListTile(
                  trailing: Padding(
                    padding: EdgeInsets.all(5),
                    child: Container(
                      child: Image(
                        image: AssetImage(ImageResource.arrow),
                        height: 18,
                        width: 18,
                      ),
                    ),
                  ),
                  leading: Padding(
                    padding: EdgeInsets.all(5),
                    child: Container(
                      child: Image(
                        image: AssetImage(ImageResource.file),
                        height: 24,
                        width: 24,
                      ),
                    ),
                  ),
                  title: Transform(
                    transform: Matrix4.translationValues(-12, 0.0, 0.0),
                    child: CustomText(
                      Languages.of(context)!.downloadPDF,
                      style: Theme
                          .of(context)
                          .textTheme
                          .subtitle1!
                          .copyWith(
                          color: ColorResource.color222222,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
                onTap: () async {

                  final PermissionStatus status =
                  await Permission.storage.request();
                  if (status == PermissionStatus.granted) {
                    if (Platform.isIOS) {
                      String localPath =
                          path + Platform.pathSeparator + 'Download';
                      final savedDir = Directory(localPath);
                      bool hasExisted = await savedDir.exists();
                      if (!hasExisted) {
                        savedDir.create();
                      }
                      String? _taskid = await FlutterDownloader.enqueue(
                        url:
                        "https://www.icicibank.com/managed-assets/docs/form-center/cc_auto.pdf",
                        savedDir: localPath,
                        showNotification:
                        true,
                        // show download progress in status bar (for Android)
                        openFileFromNotification:
                        true, // click on notification to open downloaded file (for Android)
                      );
                    } else {

                      // await FlutterDownloader.open(taskId: _taskid!);
                      options = DownloaderUtils(
                        progressCallback: (current, total) {
                          final progress = (current / total) * 100;
                          print('Downloading: $progress');
                        },
                        file: File('$path/Icici.pdf'),

                        progress: ProgressImplementation(),
                        onDone: () => print('COMPLETE'),
                        deleteOnCancel: true,
                      );
                      core = await Flowder.download(
                          'https://www.icicibank.com/managed-assets/docs/form-center/cc_auto.pdf',
                          options);
                      // File(path).create(recursive: true);
                      print(options.file);
                      print(path);
                      String localPath =
                          path + Platform.pathSeparator + 'Download';
                      final savedDir = Directory(localPath);
                      bool hasExisted = await savedDir.exists();
                      if (!hasExisted) {
                        savedDir.create();
                      }
                      String? _taskid = await FlutterDownloader.enqueue(
                        url:
                        "https://www.icicibank.com/managed-assets/docs/form-center/cc_auto.pdf",
                        savedDir: localPath,
                        showNotification:
                        true,
                        // show download progress in status bar (for Android)
                        openFileFromNotification:
                        true, // click on notification to open downloaded file (for Android)
                      );
                    }
                    Navigator.pop(context);


                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) =>
                                DownloadStatementPdf()));

                  }
                },
              ),
              GestureDetector(
                child: ListTile(
                  trailing: Padding(
                    padding: EdgeInsets.all(5),
                    child: Container(
                      child: Image(
                        image: AssetImage(ImageResource.arrow),
                        height: 18,
                        width: 18,
                      ),
                    ),
                  ),
                  leading: Padding(
                    padding: EdgeInsets.all(5),
                    child: Container(
                      child: Image(
                        image: AssetImage(ImageResource.mail),
                        height: 24,
                        width: 24,
                      ),
                    ),
                  ),
                  title: Transform(
                    transform: Matrix4.translationValues(-12, 0.0, 0.0),
                    child: CustomText(
                      Languages.of(context)!.downloadStatementOverEmail,
                      style: Theme
                          .of(context)
                          .textTheme
                          .subtitle1!
                          .copyWith(
                          color: ColorResource.color222222,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
                onTap: () {
                  Navigator.pop(context);
                  _showSentSuccessBottomSheet(context);
                },
              ),
            ],
          ),
        );
      },
    );
  }

  _showFilterBottomSheet(BuildContext buildContext, StatementScreenBloc bloc) {
    bool buttonShow = false;
    buttonShow = false;
    bloc.filterList.forEach((element) {
      element.forEach((element) {
        if (element.isSelected) {
          buttonShow = true;
        }
      });
    });
    showModalBottomSheet(
      context: buildContext,
      isScrollControlled: true,
      // elevation: 20,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(24.0), topRight: Radius.circular(24.0)),
      ),
      builder: (BuildContext context) {
        return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
              return Wrap(
                children: [
                  Container(
                    alignment: Alignment.center,
                    child: Column(
                      children: [
                        CustomText(
                          Languages.of(context)!.filter,
                          style: Theme
                              .of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(
                              color: ColorResource.color222222,
                              fontWeight: FontWeight.w600),
                        ),
                        SingleChildScrollView(
                          child: Container(
                              child: Column(
                                children: [
                                  ListView.builder(
                                      itemCount: bloc.filterList.length,
                                      shrinkWrap: true,
                                      primary: true,
                                      physics: ScrollPhysics(),
                                      itemBuilder: (BuildContext context,
                                          int index) {
                                        return Container(
                                            alignment: Alignment.center,
                                            padding:
                                            EdgeInsets.symmetric(
                                                horizontal: 10),
                                            child: Column(children: [
                                              ListView.builder(
                                                  itemCount:
                                                  bloc.filterList[index].length,
                                                  shrinkWrap: true,
                                                  primary: true,
                                                  physics:
                                                  NeverScrollableScrollPhysics(),
                                                  itemBuilder: (
                                                      BuildContext context,
                                                      int innerListIndex) {
                                                    return Container(
                                                      padding: EdgeInsets.all(
                                                          5),
                                                      child: Row(
                                                        children: [
                                                          Padding(
                                                            padding: EdgeInsets
                                                                .all(5),
                                                            child: Container(
                                                              height: 24,
                                                              width: 24,
                                                              child: Checkbox(
                                                                shape:
                                                                const RoundedRectangleBorder(
                                                                  borderRadius:
                                                                  BorderRadius
                                                                      .all(
                                                                      Radius
                                                                          .circular(
                                                                          4.0)),
                                                                ),
                                                                checkColor:
                                                                Colors.white,
                                                                activeColor:
                                                                ColorResource
                                                                    .color10CB00,
                                                                value: bloc
                                                                    .filterList[index]
                                                                [innerListIndex]
                                                                    .isSelected,
                                                                onChanged:
                                                                    (
                                                                    bool? value) {
                                                                  setState(() {
                                                                    bloc
                                                                        .filterList[
                                                                    index][
                                                                    innerListIndex]
                                                                        .isSelected =
                                                                    value!;
                                                                    if (innerListIndex ==
                                                                        0 &&
                                                                        bloc
                                                                            .filterList[
                                                                        index]
                                                                        [0]
                                                                            .isSelected) {
                                                                      bloc
                                                                          .filterList[
                                                                      index]
                                                                          .forEach(
                                                                              (
                                                                              ProfileModel
                                                                              element) {
                                                                            element
                                                                                .isSelected =
                                                                            true;
                                                                          });
                                                                    }
                                                                    if (innerListIndex ==
                                                                        0 &&
                                                                        !bloc
                                                                            .filterList[
                                                                        index]
                                                                        [0]
                                                                            .isSelected) {
                                                                      bloc
                                                                          .filterList[
                                                                      index]
                                                                          .forEach(
                                                                              (
                                                                              element) {
                                                                            element
                                                                                .isSelected =
                                                                            false;
                                                                          });
                                                                    } else {
                                                                      bool
                                                                      isAllSelected =
                                                                      true;
                                                                      bloc
                                                                          .filterList[
                                                                      index]
                                                                          .asMap()
                                                                          .forEach((
                                                                          int
                                                                          iteration,
                                                                          ProfileModel
                                                                          element) {
                                                                        if (iteration !=
                                                                            0) {
                                                                          if (!element
                                                                              .isSelected) {
                                                                            isAllSelected =
                                                                            false;
                                                                          }
                                                                        }
                                                                      });
                                                                      if (isAllSelected) {
                                                                        bloc
                                                                            .filterList[
                                                                        index]
                                                                        [0]
                                                                            .isSelected =
                                                                        true;
                                                                      } else {
                                                                        bloc
                                                                            .filterList[
                                                                        index]
                                                                        [0]
                                                                            .isSelected =
                                                                        false;
                                                                      }
                                                                    }

                                                                    //To hide and show the bottom button
                                                                    buttonShow =
                                                                    false;
                                                                    bloc
                                                                        .filterList
                                                                        .forEach(
                                                                            (
                                                                            element) {
                                                                          element
                                                                              .forEach(
                                                                                  (
                                                                                  element) {
                                                                                if (element
                                                                                    .isSelected) {
                                                                                  buttonShow =
                                                                                  true;
                                                                                }
                                                                              });
                                                                        });
                                                                  });
                                                                },
                                                              ),
                                                            ),
                                                          ),
                                                          CustomText(
                                                            bloc
                                                                .filterList[index]
                                                            [innerListIndex]
                                                                .name,
                                                            style: innerListIndex ==
                                                                0
                                                                ? Theme
                                                                .of(context)
                                                                .textTheme
                                                                .subtitle1!
                                                                .copyWith(
                                                                color: ColorResource
                                                                    .color222222,
                                                                fontWeight:
                                                                FontWeight
                                                                    .w700)
                                                                : Theme
                                                                .of(context)
                                                                .textTheme
                                                                .subtitle2!
                                                                .copyWith(
                                                                color: ColorResource
                                                                    .color222222,
                                                                fontWeight:
                                                                FontWeight
                                                                    .w400),
                                                          )
                                                        ],
                                                      ),
                                                    );
                                                  }),
                                              if (bloc.filterList.length - 1 !=
                                                  index)
                                                Container(
                                                  color: ColorResource
                                                      .color787878,
                                                  height: 0.15,
                                                )
                                            ]));
                                      }),
                                  Visibility(
                                    visible: buttonShow,
                                    child: Container(
                                      margin: EdgeInsets.only(
                                          bottom: 50, left: 20, right: 20),
                                      child: CustomButton(
                                        Languages.of(context)!.showResults,
                                        onTap: () {
                                          Navigator.pop(context);
                                          filterStringSetState();
                                        },
                                      ),
                                    ),
                                  )
                                ],
                              )),
                        )
                      ],
                    ),
                  ),
                ],
              );
            });
      },
    );
  }

  void filterStringSetState() {
    setState(() {
      filterString.clear();
      bloc.filterList.forEach((List<ProfileModel> elementList) {
        elementList.forEach((ProfileModel element) {
          if (element.isSelected) {
            if (!element.name.contains('All')) {
              filterString.add(element.name);
            }
          }
        });
      });
    });
  }

  _showSentSuccessBottomSheet(BuildContext buildContext) {
    showModalBottomSheet(
      context: buildContext,
      isScrollControlled: true,
      backgroundColor: ColorResource.colorFFFFFF,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(24.0),
          topRight: Radius.circular(24.0),
        ),
      ),
      builder: (BuildContext context) {
        return Container(
          padding: EdgeInsets.symmetric(vertical: 25, horizontal: 25),
          // alignment: Alignment.center,
          constraints: const BoxConstraints(
            minHeight: 150,
            maxHeight: 420,
          ),
          child: Column(
            children: [
              Container(
                child: const Image(
                    image: AssetImage(ImageResource.statement_sent_background)),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                child: CustomText(Languages.of(context)!.statementSentMessage,
                    style: Theme
                        .of(context)
                        .textTheme
                        .headline5!
                        .copyWith(
                        color: ColorResource.color222222,
                        fontWeight: FontWeight.w600)),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                child: CustomText(Languages.of(context)!.statementSentNote,
                    style: Theme
                        .of(context)
                        .textTheme
                        .subtitle2!
                        .copyWith(
                        color: ColorResource.color222222.withOpacity(0.4),
                        fontWeight: FontWeight.w500)),
              ),
              CustomButton(
                Languages.of(context)!.done,
                onTap: () async {
                  await Navigator.pushNamed(context, AppRoutes.accountDetails);
                  Navigator.pop(buildContext);
                },
              )
            ],
          ),
        );
      },
    );
  }
//path to download
  Future<void> initPlatformState() async {
    _setPath();
    if (!mounted) return;
  }

  void _setPath() async {
    if (Platform.isAndroid) {



          Directory? tempDir = await _getDownloadDirectory();
        String tempPath = tempDir.path;
        path = tempPath;
      } else {
      if (Platform.isIOS) {
        Directory documents = await getApplicationDocumentsDirectory();
        String filePath = documents.path;
        path = filePath;
      }
    }
  }

  // Future<void> permissionRequest(BuildContext buildContext) async {
  //   final PermissionStatus permission = await Permission.storage.request();
  //   if (permission == PermissionStatus.granted) {
  //     if (Platform.isAndroid) {
  //       final tempDir = await DownloadsPathProvider.downloadsDirectory;
  //       final String tempPath = tempDir!.path;
  //       path = tempPath;
  //     }
  //     Directory tempDir = await getTemporaryDirectory();
  //     String tempPath = tempDir.path;
  //
  //         final Directory appDocDir = await getApplicationDocumentsDirectory();
  //         path = appDocDir.path;
  //
  //         Navigator.pop(context);
  //
  //         await Navigator.push(
  //             context,
  //             MaterialPageRoute(
  //                 builder: (BuildContext context) => const DownloadStatementPdf()));
  //
  //         options = DownloaderUtils(
  //           progressCallback: (current, total) {
  //             final progress = (current / total) * 100;
  //             print('Downloading: $progress');
  //           },
  //           file: File('$path/Icici.pdf'),
  //           progress: ProgressImplementation(),
  //           onDone: () => print('COMPLETE'),
  //           deleteOnCancel: true,
  //         );
  //         core = await Flowder.download(
  //             'https://www.icicibank.com/managed-assets/docs/form-center/cc_auto.pdf',
  //             options);
  //         print(options.file);
  //       } else {
  //         await DialogUtils.showDialog(
  //           buildContext: context,
  //           title: Languages.of(buildContext)!.storagePermissionTitle,
  //           description:Languages.of(buildContext)!.storagePermissionDescriptionForDownload,
  //           okBtnText: Languages.of(buildContext)!.allow,
  //           cancelBtnText: Languages.of(buildContext)!.deny,
  //           okBtnFunction: (String value) async {
  //             await openAppSettings();
  //           },
  //         );
  //       }
  //   }

    // Future<String> getFilePath() async {
    //   Directory appDocumentsDirectory =
    //   await getApplicationDocumentsDirectory(); // 1
    //   String appDocumentsPath = appDocumentsDirectory.path; // 2
    //   String filePath = '$appDocumentsPath/demoTextFile.txt'; // 3
    //
    //   return filePath;
    // }

    Future<bool> _requestPermissions() async {
      final PermissionStatus status = await Permission.storage.request();
      bool statusResult = false;
      if (status == PermissionStatus.granted) {
        statusResult = true;


      } else {
        await openAppSettings();
      }
      return statusResult;
    }

    Future<Directory> _getDownloadDirectory() async {
      if (Platform.isAndroid) {
        // return await DownloadsPathProvider.downloadsDirectory as Directory;
        return await getTemporaryDirectory().then((value) => value);
      }

      Future<bool> _requestPermissions() async {
        var status = await Permission.storage.status;

        if (status != PermissionStatus.granted) {
          await Permission.storage.request();
        }
        return status == PermissionStatus.granted;
      }

      // in this example we are using only Android and iOS so I can assume
      // that you are not trying it for other platforms and the if statement
      // for iOS is unnecessary

      // iOS directory visible to user

      return await getApplicationDocumentsDirectory();
    }
  // Future<void> _showNotification() async {
  //   final android = AndroidNotificationDetails(
  //       'channel id', 'channel name', 'channel description',
  //       priority: Priority.high, importance: Importance.max);
  //   final iOS = IOSNotificationDetails();
  //   final platform = NotificationDetails(android: android, iOS: iOS);
  //
  //   final isSuccess = true;
  //
  //   await flutterLocalNotificationsPlugin.show(
  //     0, // notification id
  //     isSuccess ? 'Success' : 'Failure',
  //     isSuccess
  //         ? 'File has been downloaded successfully!'
  //         : 'There was an error while downloading the file.',
  //     platform,
  //   );
  // }


  Future selectNotification(String? payload) async {
      //Handle notification tapped logic here
    }


  }