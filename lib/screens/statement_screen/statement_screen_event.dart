part of 'statement_screen_bloc.dart';

@immutable
abstract class StatementScreenEvent extends BaseEquatable {}

class StatementInitialEvent extends StatementScreenEvent {
  BuildContext? context;

  StatementInitialEvent({this.context});
}
