import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:icici/base/base_state.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/model/profile_model.dart';
import 'package:icici/model/statement_model.dart';
import 'package:icici/utils/base_equatable.dart';

part 'statement_screen_event.dart';

class StatementScreenBloc extends Bloc<StatementScreenEvent, BaseState> {
  StatementScreenBloc() : super(InitialState());

  List<StatementModel> statementModelList = [];
  List<List<ProfileModel>> filterList = [];
  List<ProfileModel> filterPaid = [];
  List<ProfileModel> filterUPIStatus = [];
  List<ProfileModel> filterUPIPaymentType = [];
  // List<ProfileModel> filterUPICategory = [];

  @override
  Stream<BaseState> mapEventToState(
    StatementScreenEvent event,
  ) async* {
    if (event is StatementInitialEvent) {
      yield LoadingState();
      filterPaid.addAll([
        ProfileModel(
            Languages.of(event.context!)!.allPaidAndReceived, 0, false),
        ProfileModel(Languages.of(event.context!)!.credit, 1, false),
        ProfileModel(Languages.of(event.context!)!.debit, 2, false)
      ]);
      filterUPIStatus.addAll([
        ProfileModel(
            Languages.of(event.context!)!.allUPITransactionStaus, 0, false),
        ProfileModel(Languages.of(event.context!)!.success, 1, false),
        ProfileModel(Languages.of(event.context!)!.failure, 2, false),
        ProfileModel(Languages.of(event.context!)!.timeout, 3, false)
      ]);
      filterUPIPaymentType.addAll([
        ProfileModel(Languages.of(event.context!)!.allPaymentsTypes, 0, false),
        ProfileModel(Languages.of(event.context!)!.transfers, 1, false),
        ProfileModel(Languages.of(event.context!)!.upi, 2, false),
        ProfileModel(Languages.of(event.context!)!.billPayment, 3, false),
        ProfileModel(Languages.of(event.context!)!.onlinePayment, 4, false)
      ]);
      // filterUPICategory.addAll([
      //   ProfileModel(Languages.of(event.context!)!.category, 0, false),
      //   ProfileModel(Languages.of(event.context!)!.lifeStyle, 1, false),
      //   ProfileModel(Languages.of(event.context!)!.homeExpenses, 2, false),
      //   ProfileModel(Languages.of(event.context!)!.utilities, 3, false),
      //   ProfileModel(Languages.of(event.context!)!.outgoingTransfers, 4, false)
      // ]);
      filterList.addAll([filterPaid, filterUPIStatus, filterUPIPaymentType]);
      statementModelList.addAll([
        StatementModel(null, 'Modi', 'Oct 01 2021, 2:50 PM', 100000.03, 2),
        StatementModel(
            'https://m.economictimes.com/thumb/msid-77240865,width-1200,height-900,resizemode-4,imgsize-286123/singapore-pm.jpg',
            'Lee Hsien Loong',
            'May 05 2021, 2:50 PM',
            10000.40,
            1),
        StatementModel('https://static.dw.com/image/19392675_303.jpg',
            'David Cameron', 'Sep 05 2021, 2:50 PM', 2980.00, 1),
        StatementModel(
            'https://upload.wikimedia.org/wikipedia/commons/f/f8/Cyril_Ramaphosa_-_President_of_South_Africa_-_2018_%28cropped%29.jpg',
            'Cyril Ramaphosa',
            'Apr 03 2021, 2:50 PM',
            20.00,
            2),
        StatementModel(
            'https://pbs.twimg.com/profile_images/1414358967371935744/yAadRgF-_400x400.jpg',
            'Boris Johnson',
            'May 05 2021, 2:50 PM',
            20.44,
            2),
        StatementModel(null, 'Xi Jinping', '2011-09-30 12:10:13', 10.00, 1),
        StatementModel(
            'https://pbs.twimg.com/profile_images/936639677121081344/_e5l_DEG.jpg',
            'Theresa may',
            'May 01 2021, 2:50 PM',
            10020.00,
            1),
        StatementModel(
            'https://upload.wikimedia.org/wikipedia/commons/thumb/0/0a/Scott_Morrison_2014.jpg/220px-Scott_Morrison_2014.jpg',
            'Scott Morrison',
            'Feb 08 2021, 2:50 PM',
            10020.00,
            2),
        StatementModel('https://images.indianexpress.com/2020/09/SUGA.jpg',
            'Yoshihide Suga', 'Jan 24 2021, 2:50 PM', 120.00, 1),
        StatementModel(
            'https://upload.wikimedia.org/wikipedia/commons/6/6d/Shinz%C5%8D_Abe_Official.jpg',
            'Shinzo Abe',
            'May 05 2021, 2:50 PM',
            10000.44,
            2),
        StatementModel(null, 'Joe Biden', 'May 05 2021, 2:50 PMs', 10.88, 1),
      ]);

      yield SuccessState();
    }
  }
}
