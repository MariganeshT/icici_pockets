part of 'terms_conditions_bloc.dart';

@immutable
class TermsConditionsEvent extends BaseEquatable {}

class TermsConditionsInitialEvent extends TermsConditionsEvent {}
