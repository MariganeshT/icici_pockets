part of 'terms_conditions_bloc.dart';

@immutable
class TermsConditionsState extends BaseEquatable {}

class TermsConditionsInitial extends TermsConditionsState {}
