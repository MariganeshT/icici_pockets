import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:meta/meta.dart';

part 'terms_conditions_event.dart';
part 'terms_conditions_state.dart';

class TermsConditionsBloc
    extends Bloc<TermsConditionsEvent, TermsConditionsState> {
  TermsConditionsBloc() : super(TermsConditionsInitial());

  @override
  Stream<TermsConditionsState> mapEventToState(
    TermsConditionsEvent event,
  ) async* {}
}
