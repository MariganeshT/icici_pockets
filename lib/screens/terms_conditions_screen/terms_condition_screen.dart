import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/utils/app_utils.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/font.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_button.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../../router.dart';
import 'bloc/terms_conditions_bloc.dart';

class TermsConditionScreen extends StatefulWidget {
  //final MyInAppBrowser browser = MyInAppBrowser();

  @override
  _TermsConditionScreenState createState() => _TermsConditionScreenState();
}

class _TermsConditionScreenState extends State<TermsConditionScreen> {
  // InAppBrowserClassOptions options = InAppBrowserClassOptions(
  //     crossPlatform: InAppBrowserOptions(hideUrlBar: true),
  //     inAppWebViewGroupOptions:
  //         InAppWebViewGroupOptions(crossPlatform: InAppWebViewOptions()));

  bool accept = false;

  late TermsConditionsBloc bloc;

  //InAppWebViewController? webView;
  String url = "";
  double progress = 0;
  bool isLoading = true;

  @override
  void initState() {
    super.initState();
    bloc = TermsConditionsBloc()..add(TermsConditionsInitialEvent());
  }

  _TCandUA(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        forceSafariVC: true,
        forceWebView: true,
        enableJavaScript: true,
      );
    } else {
      AppUtils.showToast("can't open the link");
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        top: false,
        child: Scaffold(
            backgroundColor: ColorResource.colorFAFAFA,
            body: BlocListener<TermsConditionsBloc, TermsConditionsState>(
              bloc: bloc,
              listener: (context, state) {},
              child: BlocBuilder<TermsConditionsBloc, TermsConditionsState>(
                bloc: bloc,
                builder: (BuildContext context, TermsConditionsState state) {
                  return Padding(
                    padding: const EdgeInsets.only(
                        top: 40, left: 16.0, right: 16, bottom: 8),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            CustomText(
                              Languages.of(context)!.termsAndConditions,
                              style: Theme.of(context)
                                  .textTheme
                                  .headline5!
                                  .copyWith(
                                      color: ColorResource.color222222,
                                      fontWeight: FontWeights.bold),
                            ),
                            const Image(
                                height: 35,
                                image:
                                    AssetImage(ImageResource.termsConditions)),
                          ],
                        ),
                        Container(
                            padding: EdgeInsets.all(6.0),
                            child: progress < 1.0
                                ? LinearProgressIndicator(value: progress)
                                : Container()),
                        Expanded(
                          child: Stack(
                            children: [
                              Container(
                                margin: const EdgeInsets.all(3.0),
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.white)),
                                child: WebView(
                                  initialUrl:
                                      StringResource.termsAndConditionLink,
                                  onWebViewCreated:
                                      (WebViewController webViewController) {
                                    Completer<WebViewController>()
                                        .complete(webViewController);
                                  },
                                  onPageFinished: (finish) {
                                    setState(() {
                                      isLoading = false;
                                    });
                                  },
                                ),

                                // child: InAppWebView(
                                //   initialUrlRequest: URLRequest(
                                //       url: Uri.parse(
                                //           'https://www.icicibank.com/Personal-Banking/bank-wallet/pockets/pockets.html')),
                                //   initialOptions: InAppWebViewGroupOptions(
                                //       crossPlatform: InAppWebViewOptions(),
                                //       ios: IOSInAppWebViewOptions(),
                                //       android: AndroidInAppWebViewOptions(
                                //           useHybridComposition: true)),
                                //   onWebViewCreated:
                                //       (InAppWebViewController controller) {
                                //     webView = controller;
                                //   },
                                //   onLoadStart: (InAppWebViewController controller,
                                //       Uri? url) {
                                //     setState(() {
                                //       this.url = url?.toString() ?? '';
                                //     });
                                //   },
                                //   onLoadStop: (InAppWebViewController controller,
                                //       Uri? url) async {
                                //     setState(() {
                                //       this.url = url?.toString() ?? '';
                                //     });
                                //   },
                                //   onProgressChanged:
                                //       (InAppWebViewController controller,
                                //           int progress) {
                                //     setState(() {
                                //       this.progress = progress / 100;
                                //     });
                                //   },
                                // ),
                              ),
                              isLoading
                                  ? Center(
                                      child: CircularProgressIndicator(
                                        color: ColorResource.color222222,
                                      ),
                                    )
                                  : Stack()
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
            bottomNavigationBar:
                Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              Container(
                margin: const EdgeInsets.only(
                    top: 20, left: 24, right: 24, bottom: 10),
                child: Row(children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        accept = !accept;
                      });
                    },
                    child: Icon(
                      accept ? Icons.check_box_outlined : Icons.crop_square,
                      color: accept
                          ? ColorResource.color203718
                          : ColorResource.color787878,
                    ),
                  ),
                  const SizedBox(width: 10),
                  Expanded(
                    child: CustomText(
                      Languages.of(context)!.iHave,
                      style: Theme.of(context).textTheme.subtitle2!.copyWith(
                            color: ColorResource.color222222,
                          ),
                    ),
                  ),
                ]),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 8.0, left: 24, right: 24, bottom: 20),
                child: CustomButton(Languages.of(context)!.proceed, onTap: () {
                  if (accept) {
                    Navigator.pushReplacementNamed(
                        context, AppRoutes.personalDetailsScreen);
                  } else {
                    AppUtils.showErrorToast(StringResource.select);
                  }
                }),
              )
            ])));
  }
}

// class MyInAppBrowser extends InAppBrowser {
//   @override
//   // ignore: always_specify_types
//   Future onBrowserCreated() async {
//     // ignore: avoid_print
//     print('Browser Created!');
//   }

//   @override
//   // ignore: always_specify_types
//   Future onLoadStart(Uri? url) async {
//     // ignore: avoid_print
//     print('Started $url');
//   }

//   @override
//   // ignore: always_specify_types
//   Future onLoadStop(Uri? url) async {
//     // ignore: avoid_print
//     print('Stopped $url');
//   }

//   @override
//   void onLoadError(Uri? url, int code, String message) {
//     // ignore: avoid_print
//     print("Can't load $url.. Error: $message");
//   }

//   @override
//   void onProgressChanged(int progress) {
//     // ignore: avoid_print
//     print('Progress: $progress');
//   }

//   @override
//   void onExit() {
//     // ignore: avoid_print
//     print('Browser closed!');
//   }
// }
