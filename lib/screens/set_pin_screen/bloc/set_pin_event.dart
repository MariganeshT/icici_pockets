part of 'set_pin_bloc.dart';

@immutable
abstract class SetPinEvent extends BaseEquatable {}

class SetPinInitialEvent extends SetPinEvent {}

class LandingPageBioAuthEvent extends SetPinEvent{}
