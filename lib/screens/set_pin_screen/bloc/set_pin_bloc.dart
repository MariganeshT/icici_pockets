import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/services.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:local_auth/local_auth.dart';
import 'package:meta/meta.dart';

part 'set_pin_event.dart';
part 'set_pin_state.dart';

class SetPinBloc extends Bloc<SetPinEvent, SetPinState> {
  SetPinBloc() : super(SetPinInitial());

  @override
  Stream<SetPinState> mapEventToState(
    SetPinEvent event,
  ) async* {
    if (event is LandingPageBioAuthEvent) {
      final LocalAuthentication localAuth = LocalAuthentication();
      final List<BiometricType> availableBiometrics =
          await localAuth.getAvailableBiometrics();
      try {
        final bool didAuthenticate = await localAuth.authenticate(
          localizedReason: 'Log In to your account instantly!',
          biometricOnly: true,
        );
        if (didAuthenticate) {
          // ignore: avoid_print
          print('Authenticated');
        } else {
          // ignore: avoid_print
          print('Unauthenticated');
        }
      } on PlatformException catch (e) {
        // if (e.code == auth_error.notAvailable) {
        //   // Handle this exception here.
        // }
      }
    }
  }
}
