import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/font.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/preference_helper.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/boimetric_authentication_alertbox.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:icici/widgets/custom_button.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import '../../router.dart';

class SetPinScreen extends StatefulWidget {
  bool isForgotMpinFlow;
  bool isTerms;

  // ignore: sort_constructors_first
  SetPinScreen({this.isForgotMpinFlow = false, this.isTerms = false});

  @override
  _SetPinScreenState createState() => _SetPinScreenState();
}

class _SetPinScreenState extends State<SetPinScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController passcode = TextEditingController();
  final TextEditingController confirmPasscode = TextEditingController();
  bool errorValue = false;

  _showToast(String text) {
    Fluttertoast.showToast(
        msg: text,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 14.0);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        key: _scaffoldKey,
        backgroundColor: ColorResource.colorFAFAFA,
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  width: double.infinity,
                  child: Image.asset(
                    ImageResource.setpin,
                    fit: BoxFit.contain,
                  ),
                  decoration: BoxDecoration(
                    color: ColorResource.color641653,
                    borderRadius: BorderRadius.vertical(
                      bottom: Radius.elliptical(
                        MediaQuery.of(context).size.width,
                        35.0,
                      ),
                    ),
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 12, top: 8),
                      child: CustomAppbar(
                        titleString: StringResource.set4digitPasscode,
                        indicatorIndex: 3,
                        isAuthentication: true,
                      ),
                    ),
                    SizedBox(
                      width: 278,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 12),
                        child: CustomText(
                          StringResource.noteSetPasscode,
                          style: Theme.of(context)
                              .textTheme
                              .subtitle1!
                              .copyWith(color: ColorResource.colorFFFFFF),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(
              height: 30.0,
            ),
            Center(
              child: Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    CustomText(
                      StringResource.enterPasscode,
                      style: Theme.of(context)
                          .textTheme
                          .subtitle1!
                          .copyWith(color: ColorResource.color222222),
                    ),
                    const SizedBox(
                      height: 8.0,
                    ),
                    Center(
                      child: PinCodeTextField(
                        // validator: (String? value) {
                        //   _showToast(StringResource.pleaseSetPasscode);
                        // },
                        appContext: context,
                        controller: passcode,
                        onChanged: (String value) {},
                        onCompleted: (String value) {},
                        length: 4,
                        obscureText: true,
                        textStyle: const TextStyle(
                            fontSize: FontSize.fourteen,
                            color: ColorResource.color222222),
                        animationType: AnimationType.scale,
                        animationDuration: const Duration(milliseconds: 300),
                        keyboardType: TextInputType.number,
                        mainAxisAlignment: MainAxisAlignment.center,
                        pinTheme: PinTheme(
                            fieldOuterPadding: const EdgeInsets.all(8),
                            shape: PinCodeFieldShape.box,
                            borderRadius: BorderRadius.circular(8),
                            borderWidth: 1,
                            inactiveColor: ColorResource.color222222,
                            selectedColor: ColorResource.color222222,
                            activeColor: ColorResource.color222222,
                            fieldHeight: 56,
                            fieldWidth: 48,
                            errorBorderColor: ColorResource.colorF92538),
                      ),
                    ),
                    const SizedBox(
                      height: 30.0,
                    ),
                    CustomText(
                      StringResource.confirmPasscode,
                      style: Theme.of(context)
                          .textTheme
                          .subtitle1!
                          .copyWith(color: ColorResource.color222222),
                    ),
                    const SizedBox(
                      height: 8.0,
                    ),
                    PinCodeTextField(
                      // validator: (String? value) {
                      //   _showToast(StringResource.pleaseSetPasscode);
                      // },
                      appContext: context,
                      controller: confirmPasscode,
                      onChanged: (String value) {},
                      onCompleted: (String value) {},
                      length: 4,
                      obscureText: true,
                      textStyle: const TextStyle(
                          fontSize: FontSize.fourteen,
                          color: ColorResource.color222222),
                      animationType: AnimationType.scale,
                      animationDuration: const Duration(milliseconds: 300),
                      keyboardType: TextInputType.number,
                      mainAxisAlignment: MainAxisAlignment.center,
                      pinTheme: PinTheme(
                        fieldOuterPadding: const EdgeInsets.all(8),
                        shape: PinCodeFieldShape.box,
                        borderRadius: BorderRadius.circular(8),
                        borderWidth: 1,
                        inactiveColor: errorValue
                            ? ColorResource.colorF92538
                            : ColorResource.color222222,
                        selectedColor: errorValue
                            ? ColorResource.colorF92538
                            : ColorResource.color222222,
                        activeColor: errorValue
                            ? ColorResource.colorF92538
                            : ColorResource.color222222,
                        errorBorderColor: ColorResource.colorF92538,
                        fieldHeight: 56,
                        fieldWidth: 48,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Flexible(
              child: Container(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: CustomButton(
                    StringResource.buttonTextContinue,
                    onTap: () async {
                      if (passcode.text.isNotEmpty &&
                          confirmPasscode.text.isNotEmpty) {
                        if (passcode.text == confirmPasscode.text) {
                          passcode.clear();
                          confirmPasscode.clear();
                          PreferenceHelper.setMPINStatus(true);
                          final bool bioMetricValue =
                              await PreferenceHelper.getBioMetricValue();
                          if (bioMetricValue) {
                            pageNavigation();
                          } else {
                            await showDialog(
                                context: context,
                                barrierDismissible: false,
                                builder: (BuildContext context) {
                                  return FingerprintAlertDialogue(() async {
                                    PreferenceHelper.setBioMetricValue(true);
                                    pageNavigation();
                                  }, () {
                                    PreferenceHelper.setBioMetricValue(false);
                                    pageNavigation();
                                  });
                                });
                          }
                        } else {
                          _showToast(StringResource.passwordNotMatch);
                          setState(() {
                            errorValue = true;
                          });
                        }
                      } else {
                        _showToast(StringResource.pleaseSetPasscode);
                      }
                    },
                  ),
                ),
              ),
            ),
          ],
        ));
  }

  void pageNavigation() {
    while (Navigator.canPop(context)) {
      Navigator.pop(context);
    }
    if (widget.isForgotMpinFlow) {
      while (Navigator.canPop(context)) {
        Navigator.pop(context);
      }

      Navigator.pushReplacementNamed(context, AppRoutes.dashboardScreen);
    } else {
      Navigator.pushReplacementNamed(context, AppRoutes.walletSuccessScreen);
    }
  }
}
