import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/screens/confirm_passcode_screen/bloc/confirmpasscode_bloc.dart';
import 'package:icici/screens/confirm_passcode_screen/confirm_success.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:icici/widgets/custom_button.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class ConformPasscodeScreen extends StatefulWidget {
  ConformPasscodeScreen({Key? key}) : super(key: key);

  @override
  _ConformPasscodeScreenState createState() => _ConformPasscodeScreenState();
}

class _ConformPasscodeScreenState extends State<ConformPasscodeScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final TextEditingController passcodeController = TextEditingController();
  final TextEditingController confirmPasscodeController =
      TextEditingController();
  late ConfirmpasscodeBloc bloc;
  // FocusNode foucusNodeFirst = FocusNode();
  FocusNode focusNodeSecond = FocusNode();
  bool error = false;
  bool buttonVisible = false;
  FocusNode conformPasscodeFocusNode = FocusNode();

  @override
  void initState() {
    bloc = ConfirmpasscodeBloc()..add(ConfirmpasscodeInitialEvent());
    // foucusNodeFirst.unfocus();
    // foucusNodeFirst.requestFocus();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      key: _scaffoldKey,
      backgroundColor: ColorResource.colorFAFAFA,
      body: BlocListener<ConfirmpasscodeBloc, ConfirmpasscodeState>(
        bloc: bloc,
        listener: (context, state) {},
        child: BlocBuilder<ConfirmpasscodeBloc, ConfirmpasscodeState>(
            bloc: bloc,
            builder: (context, state) {
              if (state is ConfirmpasscodeLoadingState) {
                return Scaffold(
                  body: Center(
                    child: CircularProgressIndicator(),
                  ),
                );
              } else if (state is ConfirmpasscodeLoadedState) {
                return Column(
                  children: [
                    SizedBox(
                      child: CustomAppbar(
                        backgroundColor: ColorResource.color641653,
                        titleString: Languages.of(context)!.changePasscode,
                        titleSpacing: 8,
                        style: Theme.of(context)
                            .textTheme
                            .headline5!
                            .copyWith(color: ColorResource.colorFFFFFF),
                        iconEnumValues: IconEnum.back,
                        onItemSelected: (selectedItem) {
                          Navigator.pop(context);
                          Navigator.pop(context);
                        },
                      ),
                    ),
                    Column(
                      children: <Widget>[
                        SizedBox(height: 45),
                        CustomText(
                          Languages.of(context)!.enterNewPasscode,
                          style: Theme.of(context)
                              .textTheme
                              .subtitle1!
                              .copyWith(
                                  fontWeight: FontWeight.w400,
                                  color: ColorResource.color222222),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 45.0),
                          child: PinCodeTextField(
                            appContext: context,
                            onChanged: (String value) {},
                            // focusNode: foucusNodeFirst,
                            autoFocus: true,
                            onCompleted: (String enteredText) {
                              focusNodeSecond.requestFocus();
                              // if (widget.navigateScreen == 'WalletBottomSheet') {
                              //   // bloc.add(
                              //   //   NavigateWalletClosureBottomsheetEvent(),
                              //   // );
                              // } else if (widget.navigateScreen ==
                              //     'TransactionSuccessBottomSheet') {
                              //   // bloc.add(NavigateTransactionLimitSuccessEvent());
                              // }
                            },
                            obscureText: true,
                            length: 4,
                            textStyle: Theme.of(context)
                                .textTheme
                                .headline5!
                                .copyWith(color: ColorResource.color222222),
                            animationType: AnimationType.scale,
                            animationDuration:
                                const Duration(milliseconds: 300),
                            keyboardType: TextInputType.number,
                            controller: passcodeController,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            pinTheme: PinTheme(
                              shape: PinCodeFieldShape.box,
                              borderWidth: 1,
                              borderRadius: BorderRadius.circular(8),
                              inactiveColor: ColorResource.color222222,
                              selectedColor: ColorResource.color222222,
                              activeColor: ColorResource.color222222,
                              fieldHeight: 60.89,
                              fieldWidth: 48,
                            ),
                          ),
                        ),
                        SizedBox(height: 21),
                        CustomText(
                          Languages.of(context)!.confirmPasscode,
                          style: Theme.of(context)
                              .textTheme
                              .subtitle1!
                              .copyWith(
                                  fontWeight: FontWeight.w400,
                                  color: ColorResource.color222222),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding:
                              const EdgeInsets.only(left: 45.0, right: 45.0),
                          child: PinCodeTextField(
                            appContext: context,
                            onChanged: (String value) {},
                            autoFocus: true,
                            focusNode: focusNodeSecond,
                            onCompleted: (String enteredText) {
                              // if (widget.navigateScreen == 'WalletBottomSheet') {
                              //   // bloc.add(
                              //   //   NavigateWalletClosureBottomsheetEvent(),
                              //   // );
                              // } else if (widget.navigateScreen ==
                              //     'TransactionSuccessBottomSheet') {
                              //   // bloc.add(NavigateTransactionLimitSuccessEvent());
                              // }
                              if (passcodeController.text.isNotEmpty &&
                                  confirmPasscodeController.text.isNotEmpty) {
                                if (passcodeController.text ==
                                    confirmPasscodeController.text) {
                                  setState(() {
                                    error = false;
                                    buttonVisible = true;
                                  });
                                } else {
                                  setState(() {
                                    error = true;
                                    buttonVisible = false;
                                  });
                                }
                              }
                            },
                            obscureText: true,
                            length: 4,
                            textStyle: Theme.of(context)
                                .textTheme
                                .headline5!
                                .copyWith(color: ColorResource.color222222),
                            animationType: AnimationType.scale,
                            animationDuration:
                                const Duration(milliseconds: 300),
                            keyboardType: TextInputType.number,
                            controller: confirmPasscodeController,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            pinTheme: PinTheme(
                              shape: PinCodeFieldShape.box,
                              borderWidth: 1,
                              borderRadius: BorderRadius.circular(8),
                              inactiveColor: error
                                  ? ColorResource.colorF92538
                                  : ColorResource.color222222,
                              selectedColor: error
                                  ? ColorResource.colorF92538
                                  : ColorResource.color222222,
                              activeColor: error
                                  ? ColorResource.colorF92538
                                  : ColorResource.color222222,
                              fieldHeight: 60.89,
                              fieldWidth: 48,
                            ),
                          ),
                        ),
                        SizedBox(height: 10),
                        error
                            ? CustomText(
                                Languages.of(context)!.notMatchedText,
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle2!
                                    .copyWith(
                                        fontWeight: FontWeight.w400,
                                        color: Colors.red),
                              )
                            : SizedBox(),
                        buttonVisible
                            ? Container(
                                alignment: Alignment.bottomCenter,
                                child: Padding(
                                  padding: EdgeInsets.only(
                                      top: MediaQuery.of(context).size.height -
                                          450,
                                      left: 24,
                                      right: 24),
                                  child: CustomButton(
                                    StringResource.buttonSetPasscode,
                                    onTap: () {
                                      changePasscodeBottomSheet();
                                    },
                                  ),
                                ),
                              )
                            : SizedBox(),
                      ],
                    )
                  ],
                );
              }
              return Scaffold(
                body: CircularProgressIndicator(),
              );
            }),
      ),
    );
  }

  void changePasscodeBottomSheet() {
    showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        context: context,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(32.0), topRight: Radius.circular(32.0)),
        ),
        backgroundColor: ColorResource.colorFFFFFF,
        builder: (BuildContext context) {
          return const ConfirmPasscodeSuccess();
        });
  }
}
