import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'confirmpasscode_event.dart';
part 'confirmpasscode_state.dart';

class ConfirmpasscodeBloc
    extends Bloc<ConfirmpasscodeEvent, ConfirmpasscodeState> {
  ConfirmpasscodeBloc() : super(ConfirmpasscodeInitial());

  @override
  Stream<ConfirmpasscodeState> mapEventToState(
    ConfirmpasscodeEvent event,
  ) async* {
    if (event is ConfirmpasscodeInitialEvent) {
      yield ConfirmpasscodeLoadedState();
    }
  }
}
