part of 'confirmpasscode_bloc.dart';

@immutable
abstract class ConfirmpasscodeState {}

class ConfirmpasscodeInitial extends ConfirmpasscodeState {}

class ConfirmpasscodeLoadingState extends ConfirmpasscodeState {}

class ConfirmpasscodeLoadedState extends ConfirmpasscodeState {}

class ConfirmpasscodeFailureState extends ConfirmpasscodeState {}
