part of 'confirmpasscode_bloc.dart';

@immutable
abstract class ConfirmpasscodeEvent {}

class ConfirmpasscodeInitialEvent extends ConfirmpasscodeEvent {}
