import 'package:flutter/material.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_button.dart';

class ConfirmPasscodeSuccess extends StatefulWidget {
  const ConfirmPasscodeSuccess({Key? key}) : super(key: key);

  @override
  _ConfirmPasscodeSuccessState createState() => _ConfirmPasscodeSuccessState();
}

class _ConfirmPasscodeSuccessState extends State<ConfirmPasscodeSuccess> {
  @override
  Widget build(BuildContext context) {
    return StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) {
      return WillPopScope(
        onWillPop: () async => false,
        child: Container(
          padding: MediaQuery.of(context).viewInsets,
          child: Container(
            decoration: const BoxDecoration(
              color: ColorResource.colorFFFFFF,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(32.0),
                  topRight: Radius.circular(32.0)),
            ),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(24, 12, 24, 30),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(child: Image.asset(ImageResource.change_passcode)),
                  const SizedBox(
                    height: 15,
                  ),
                  CustomText(
                    Languages.of(context)!.passcodeChangedSuccessfully,
                    style: Theme.of(context)
                        .textTheme
                        .headline5!
                        .copyWith(fontWeight: FontWeight.w400),
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  CustomText(
                    Languages.of(context)!.passcodeChangedSuccessfullySubText,
                    style: Theme.of(context).textTheme.subtitle1!.copyWith(
                        color: Color.fromRGBO(34, 34, 34, 0.5),
                        fontWeight: FontWeight.w400),
                  ),
                  const SizedBox(
                    height: 40,
                  ),
                  CustomButton(
                    StringResource.continueText,
                    onTap: () {
                      Navigator.pop(context);
                      if (Navigator.canPop(context)) {
                        Navigator.pop(context);
                        Navigator.pop(context);
                      }
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    });
  }
}
