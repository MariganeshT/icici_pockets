import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:icici/Model/faq_support_model.dart';
import 'package:icici/utils/app_utils.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:meta/meta.dart';
import 'package:url_launcher/url_launcher.dart';

part 'supportscreen_event.dart';
part 'supportscreen_state.dart';

class SupportscreenBloc extends Bloc<SupportscreenEvent, SupportscreenState> {
  SupportscreenBloc() : super(SupportscreenInitial());

  List<Support> support = [];

   Future<void> _makeAction(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Stream<SupportscreenState> mapEventToState(
    SupportscreenEvent event,
  ) async* {
    if (event is SupportscreenInitialEvent) {
      yield SupportscreenLoadingState();

      support.addAll([
        Support(ImageResource.sendmail, StringResource.email,
            subTitle: 'pockets@icicibank.com',
            onTap: () {
              print('object');
              _makeAction('mailto:pockets@icicibank.com');
            },),
        Support(ImageResource.callus, StringResource.callUs,
            subTitle: '98765 43210',
            onTap: () {
              _makeAction('tel:9876543210');
            },
            subTitle2: '8am - 5pm IST',
            isTitleColor: true,
            ),
        Support(
          ImageResource.livesupport,
          StringResource.liveSupport,
          subTitle: 'Live Support Chat',
          onTap: () {
            AppUtils.showToast('Chat');
          },
        ),
        
      ]);

      yield SupportscreenLoadedState();
    }
    if (event is NavigateFaqEvent) {
      yield NavigateFaqScreenState();
    }
  }
}
