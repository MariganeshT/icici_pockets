part of 'supportscreen_bloc.dart';

@immutable
abstract class SupportscreenState {}

class SupportscreenInitial extends SupportscreenState {}

class SupportscreenLoadingState extends SupportscreenState {}

class SupportscreenLoadedState extends SupportscreenState {}

class NavigateFaqScreenState extends SupportscreenState {}
