part of 'supportscreen_bloc.dart';

@immutable
abstract class SupportscreenEvent extends BaseEquatable {}

class SupportscreenInitialEvent extends SupportscreenEvent {}

class NavigateFaqEvent extends SupportscreenEvent {}
