import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/router.dart';
import 'package:icici/screens/FAQs_screen/faq_screen.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/font.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:icici/widgets/custom_button.dart';

import 'bloc/supportscreen_bloc.dart';

class SupportScreen extends StatefulWidget {
  SupportScreen({Key? key}) : super(key: key);

  @override
  _SupportScreenState createState() => _SupportScreenState();
}

class _SupportScreenState extends State<SupportScreen> {
  late SupportscreenBloc bloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc = SupportscreenBloc()..add(SupportscreenInitialEvent());
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    return SafeArea(
      top: false,
      child: Scaffold(
        backgroundColor: ColorResource.colorFAFAFA,
        body: BlocListener<SupportscreenBloc, SupportscreenState>(
          bloc: bloc,
          listener: (BuildContext context, SupportscreenState state) {
            if (state is NavigateFaqScreenState) {
              Navigator.pushNamed(context, AppRoutes.faqScreen);
            }
          },
          child: BlocBuilder<SupportscreenBloc, SupportscreenState>(
            bloc: bloc,
            builder: (BuildContext context, SupportscreenState state) {
              // if (state is SupportscreenState) {
              //     return Center(
              //       child: CircularProgressIndicator(),
              //     );
              //   }
              return Column(
                children: [
                  CustomAppbar(
                    backgroundColor: ColorResource.color641653,
                    titleString: StringResource.support,
                    titleSpacing: 8,
                    style: Theme.of(context)
                        .textTheme
                        .headline5!
                        .copyWith(color: ColorResource.colorFFFFFF),
                    iconEnumValues: IconEnum.back,
                    onItemSelected: (selectedItem) {
                      Navigator.pop(context);
                    },
                  ),
                  Expanded(
                      child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(12, 16, 12, 16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 5, bottom: 12),
                            child: CustomText(StringResource.weAreToHelp,
                                style: Theme.of(context).textTheme.headline5),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 16),
                            decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.2),
                                    spreadRadius: 2,
                                    blurRadius: 3,
                                    offset: Offset(
                                        0, 3), // changes position of shadow
                                  ),
                                ],
                                color: ColorResource.colorFFFFFF,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(14.0),
                                )),
                            child: Column(children: buildSupportWidget()),
                          ),
                        ],
                      ),
                    ),
                  )),
                  Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: faqWidget(),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  )
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  List<Widget> buildSupportWidget() {
    List<Widget> widgets = [];
    bloc.support.forEach((element) {
      widgets.add(ListTile(
        minLeadingWidth: 14,
        onTap: element.onTap,
        leading: Image.asset(element.leadingImage),
        title: CustomText(
          element.title,
          style: Theme.of(context)
              .textTheme
              .subtitle1!
              .copyWith(color: ColorResource.color787878, fontSize: 14),
        ),
        subtitle: Padding(
          padding: const EdgeInsets.only(top: 4.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomText(
                element.subTitle,
                style: element.isTitleColor
                    ? Theme.of(context)
                        .textTheme
                        .bodyText1!
                        .copyWith(color: ColorResource.colore222222)
                    : Theme.of(context)
                        .textTheme
                        .bodyText1!
                        .copyWith(color: ColorResource.colorF58220),
              ),
              SizedBox(
                height: 4,
              ),
              element.subTitle2 != ''
                  ? CustomText(
                      element.subTitle2,
                      style: Theme.of(context)
                          .textTheme
                          .subtitle2!
                          .copyWith(color: ColorResource.color787878),
                    )
                  : SizedBox(),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ));
    });
    return widgets;
  }

  Widget faqWidget() {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          color: ColorResource.colorf5f5f7,
          borderRadius: BorderRadius.all(
            Radius.circular(16.0),
          )),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(24, 16, 130, 10),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          CustomText(StringResource.haveMoreQuestion,
              style: Theme.of(context).textTheme.headline5),
          SizedBox(
            height: 6,
          ),
          CustomText(
            StringResource.visitFAQ,
            style: Theme.of(context)
                .textTheme
                .subtitle1!
                .copyWith(color: ColorResource.color787878, height: 1.7),
          ),
          SizedBox(
            height: 15,
          ),
          SizedBox(
            width: 135,
            child: CustomButton(
              StringResource.goToFaq,
              fontSize: FontSize.twelve,
              textColor: ColorResource.colorFF781F,
              buttonBackgroundColor: ColorResource.colorf5f5f7,
              onTap: () {
                bloc.add(NavigateFaqEvent());
              },
            ),
          ),
        ]),
      ),
    );
  }
}
