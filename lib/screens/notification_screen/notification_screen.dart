import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/base/base_state.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/languages/app_locale_constant.dart';
import 'package:icici/screens/notification_screen/notification_bloc.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/constants.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:icici/widgets/widget_utils.dart';
import 'package:intl/intl.dart';

class NotificationScreen extends StatefulWidget {
  const NotificationScreen({Key? key}) : super(key: key);

  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  Locale? _locale;
  late NotificationBloc bloc;
  String dateValues = '';

  @override
  void didChangeDependencies() {
    getLocale().then((Locale locale) {
      setState(() {
        _locale = locale;
      });
    });
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<NotificationBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: bloc,
      listener: (BuildContext context, BaseState state) {
        if (state is SuccessState) {
          // bloc.notificationList.groupBy((NotificationModel m) => m.dateTime);
        }
      },
      child: BlocBuilder(
        bloc: bloc,
        builder: (BuildContext context, BaseState state) {
          if (state is LoadingState) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          return SafeArea(
            top: false,
            child: Scaffold(
              body: Column(
                children: [
                  Container(
                    color: ColorResource.color641653,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 16),
                      child: CustomAppbar(
                        titleString: Languages.of(context)!.notifications,
                        iconEnumValues: IconEnum.back,
                        titleSpacing: 8,
                        clear: bloc.notificationList.isNotEmpty
                            ? Languages.of(context)!.clearAll
                            : null,
                        onItemSelected: (values) {
                          if (values == Constants.clearAll) {
                            setState(() {
                              bloc.notificationList.clear();
                            });
                          } else {
                            Navigator.pop(context);
                          }
                        },
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          MediaQuery.removePadding(
                            context: context,
                            child: bloc.notificationList.isNotEmpty
                                ? Card(
                                    shape: RoundedRectangleBorder(
                                      side: BorderSide(color: Colors.white70),
                                      borderRadius: BorderRadius.circular(14),
                                    ),
                                    margin: EdgeInsets.all(12.0),
                                    elevation: 2,
                                    child: Column(
                                      children: [
                                        Expanded(
                                          flex: 0,
                                          child: Container(
                                            child: Column(
                                              children: [
                                                ListView.separated(
                                                  reverse: true,
                                                  itemCount: bloc
                                                      .notificationList.length,
                                                  shrinkWrap: true,
                                                  padding: const EdgeInsets.all(
                                                      12.0),
                                                  physics:
                                                      const NeverScrollableScrollPhysics(),
                                                  itemBuilder:
                                                      (BuildContext context,
                                                          int index) {
                                                    final DateFormat
                                                        outputFormat =
                                                        DateFormat(
                                                            'dd/MM/yyyy');
                                                    DateTime dateTime =
                                                        DateTime.parse(bloc
                                                            .notificationList[
                                                                index]
                                                            .dateTime!
                                                            .toString());
                                                    Widget returnWidget =
                                                        Container();
                                                    if (dateValues !=
                                                        outputFormat
                                                            .format(dateTime)) {
                                                      dateValues = outputFormat
                                                          .format(dateTime);
                                                      returnWidget = Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          CustomText(
                                                            dateTime.isToday()
                                                                ? Languages.of(
                                                                        context)!
                                                                    .today
                                                                : dateTime
                                                                        .isYesterday()
                                                                    ? Languages.of(
                                                                            context)!
                                                                        .yesterday
                                                                    : DateFormat(
                                                                            "dd/MM/yyyy")
                                                                        .format(
                                                                            dateTime),
                                                            style: Theme.of(
                                                                    context)
                                                                .textTheme
                                                                .subtitle2!
                                                                .copyWith(
                                                                    color: ColorResource
                                                                        .color787878,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w600),
                                                          ),
                                                          WidgetUtils()
                                                              .notificationListWidget(
                                                                  bloc.notificationList[
                                                                      index],
                                                                  context)
                                                        ],
                                                      );
                                                    } else {
                                                      returnWidget = WidgetUtils()
                                                          .notificationListWidget(
                                                              bloc.notificationList[
                                                                  index],
                                                              context);
                                                    }
                                                    return returnWidget;
                                                  },
                                                  separatorBuilder:
                                                      (BuildContext context,
                                                          int index) {
                                                    return const Divider(
                                                      height: 10,
                                                    );
                                                  },
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                : Container(
                                    child: Column(
                                      children: [
                                        SizedBox(
                                          height: 40,
                                        ),
                                        Container(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 60),
                                          height: 300,
                                          width:
                                              MediaQuery.of(context).size.width,
                                          child: const Image(
                                            image: AssetImage(ImageResource
                                                .notification_empty),
                                            fit: BoxFit.fill,
                                          ),
                                        ),
                                        Container(
                                          margin: const EdgeInsets.only(
                                              left: 20,right: 20,top: 35),
                                          child: CustomText(
                                              Languages.of(context)!
                                                  .notificationEmpty,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline5!
                                                  .copyWith(
                                                      color: ColorResource
                                                          .color222222,
                                                      fontWeight:
                                                          FontWeight.w600)),
                                        ),
                                        Container(
                                          margin: const EdgeInsets.only(
                                              left: 20,right: 20,top: 12),
                                          child: CustomText(
                                              Languages.of(context)!
                                                  .notificationEmptyDescription,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText2!
                                                  .copyWith(
                                                      color: ColorResource
                                                          .color222222
                                                          .withOpacity(0.4),
                                                      fontWeight:
                                                          FontWeight.w500)),
                                        ),
                                      ],
                                    ),
                                  ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}

extension DateHelpers on DateTime {
  bool isToday() {
    final DateTime now = DateTime.now();
    return now.day == this.day &&
        now.month == this.month &&
        now.year == this.year;
  }

  bool isYesterday() {
    final DateTime yesterday = DateTime.now().subtract(const Duration(days: 1));
    return yesterday.day == this.day &&
        yesterday.month == this.month &&
        yesterday.year == this.year;
  }
}
