import 'package:flutter/cupertino.dart';
import 'package:icici/utils/base_equatable.dart';

@immutable
abstract class NotificationEvent extends BaseEquatable {}

class NotificationInitialEvent extends NotificationEvent {
  BuildContext? context;

  NotificationInitialEvent({this.context});
}
