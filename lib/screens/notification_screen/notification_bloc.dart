import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:icici/base/base_state.dart';
import 'package:icici/model/notification_model.dart';
import 'package:icici/screens/notification_screen/notification_event.dart';

class NotificationBloc extends Bloc<NotificationEvent, BaseState> {
  NotificationBloc() : super(InitialState());

  List<NotificationModel> notificationList = [];

  @override
  Stream<BaseState> mapEventToState(
    NotificationEvent event,
  ) async* {
    if (event is NotificationInitialEvent) {
      yield LoadingState();
      notificationList.addAll([
        NotificationModel(
            imageAddress:
                'https://m.economictimes.com/thumb/msid-77240865,width-1200,height-900,resizemode-4,imgsize-286123/singapore-pm.jpg',
            name: 'Lee Hsien Loong',
            message:
                'Set up Face ID to access Pockets without having to type your Passcode.',
            dateTime: '2001-04-29 13:20:13'),
        NotificationModel(
            imageAddress: '',
            message:
                'Set up Face ID to access Pockets without having to type your Passcode.',
            name: 'Modi',
            dateTime: '2001-04-29 13:20:13'),
        NotificationModel(
            imageAddress: 'https://static.dw.com/image/19392675_303.jpg',
            name: 'David Cameron',
            message:
                'Set up Face ID to access Pockets without having to type your Passcode.',
            dateTime: '2001-04-29 13:20:13'),
        NotificationModel(
            imageAddress:
                'https://upload.wikimedia.org/wikipedia/commons/f/f8/Cyril_Ramaphosa_-_President_of_South_Africa_-_2018_%28cropped%29.jpg',
            name: 'Cyril Ramaphosa',
            dateTime: '2001-04-29 13:20:13',
            message: 'Set up Face ID to access Pockets without having to type your Passcode.'),
        NotificationModel(
            dateTime: '2019-04-11 12:40:23',
            imageAddress:
                'https://pbs.twimg.com/profile_images/1414358967371935744/yAadRgF-_400x400.jpg',
            name: 'Boris Johnson',
            message: 'Notification'),
        NotificationModel(
            name: 'Joe Biden',
            dateTime: '2013-01-30 10:10:13',
            message: 'Set up Face ID to access Pockets without having to type your Passcode.'),
        NotificationModel(
            dateTime: '2021-09-06 12:40:23',
            imageAddress:
                'https://pbs.twimg.com/profile_images/1414358967371935744/yAadRgF-_400x400.jpg',
            name: 'Boris Johnson',
            message: 'Notification'),
        NotificationModel(
            name: 'Joe Biden',
            dateTime: '2021-09-06 12:40:23',
            message: "Message"),
        NotificationModel(
            dateTime: '2021-09-06 12:40:23',
            imageAddress:
                'https://pbs.twimg.com/profile_images/1414358967371935744/yAadRgF-_400x400.jpg',
            name: 'Boris Johnson',
            message: 'Notification'),
        NotificationModel(
            name: 'Joe Biden',
            dateTime: '2021-09-06 12:40:23',
            message: 'Set up Face ID to access Pockets without having to type your Passcode.'),
        NotificationModel(
            dateTime: '2021-09-06 12:40:23',
            imageAddress:
                'https://pbs.twimg.com/profile_images/1414358967371935744/yAadRgF-_400x400.jpg',
            name: 'Boris Johnson',
            message: 'Notification'),
        NotificationModel(
            dateTime: '2021-09-05 12:40:23',
            imageAddress:
                'https://pbs.twimg.com/profile_images/1414358967371935744/yAadRgF-_400x400.jpg',
            name: 'Boris Johnson',
            message: 'Notification'),
        NotificationModel(
            name: 'Joe Biden',
            dateTime: '2021-09-05 12:40:23',
            message: 'Message'),
        NotificationModel(
            dateTime: '2021-09-05 12:40:23',
            imageAddress:
                'https://pbs.twimg.com/profile_images/1414358967371935744/yAadRgF-_400x400.jpg',
            name: 'Boris Johnson',
            message:
                'Set up Face ID to access Pockets without having to type your Passcode.'),
        NotificationModel(
            name: 'Joe Biden',
            dateTime: '2021-09-05 12:40:23',
            message: 'Message'),
        NotificationModel(
            dateTime: '2021-09-05 12:40:23',
            imageAddress:
                'https://pbs.twimg.com/profile_images/1414358967371935744/yAadRgF-_400x400.jpg',
            name: 'Boris Johnson',
            message:
                'Set up Face ID to access Pockets without having to type your Passcode.'),
        NotificationModel(
            dateTime: '2021-09-04 12:40:23',
            imageAddress:
                'https://pbs.twimg.com/profile_images/1414358967371935744/yAadRgF-_400x400.jpg',
            name: 'Boris Johnson',
            message:
                'Set up Face ID to access Pockets without having to type your Passcode.'),
        NotificationModel(
            name: 'Joe Biden',
            dateTime: '2021-09-04 12:40:23',
            message: 'Set up Face ID to access Pockets without having to type your Passcode.'),
        NotificationModel(
            dateTime: '2021-09-04 12:40:23',
            imageAddress:
                'https://pbs.twimg.com/profile_images/1414358967371935744/yAadRgF-_400x400.jpg',
            name: 'Boris Johnson',
            message: 'Notification'),
        NotificationModel(
            name: 'Joe Biden',
            dateTime: '2021-09-04 12:40:23',
            message: 'Set up Face ID to access Pockets without having to type your Passcode.'),
        NotificationModel(
            dateTime: '2021-09-04 12:40:23',
            imageAddress:
                'https://pbs.twimg.com/profile_images/1414358967371935744/yAadRgF-_400x400.jpg',
            name: 'Boris Johnson',
            message:
                'Set up Face ID to access Pockets without having to type your Passcode.'),
        NotificationModel(
            dateTime: '2021-09-03 12:40:23',
            imageAddress:
                'https://pbs.twimg.com/profile_images/1414358967371935744/yAadRgF-_400x400.jpg',
            name: 'Boris Johnson',
            message: 'Notification'),
        NotificationModel(
            name: 'Joe Biden',
            dateTime: '2021-09-03 12:40:23',
            message: 'Set up Face ID to access Pockets without having to type your Passcode.'),
        NotificationModel(
            dateTime: '2021-09-03 12:40:23',
            imageAddress:
                'https://pbs.twimg.com/profile_images/1414358967371935744/yAadRgF-_400x400.jpg',
            name: 'Boris Johnson',
            message:
                'Set up Face ID to access Pockets without having to type your Passcode.'),
        NotificationModel(
            name: 'Joe Biden',
            dateTime: '2021-09-03 12:40:23',
            message: 'Message'),
        NotificationModel(
            dateTime: '2021-09-03 12:40:23',
            imageAddress:
                'https://pbs.twimg.com/profile_images/1414358967371935744/yAadRgF-_400x400.jpg',
            name: 'Boris Johnson',
            message: 'Notification'),
        NotificationModel(
            name: 'Joe Biden',
            dateTime: '2021-09-02 12:40:23',
            message: 'Message'),
        NotificationModel(
            dateTime: '2021-09-02 12:40:23',
            imageAddress:
                'https://pbs.twimg.com/profile_images/1414358967371935744/yAadRgF-_400x400.jpg',
            name: 'Boris Johnson',
            message:
                "Set up Face ID to access Pockets without having to type your Passcode."),
        NotificationModel(
            name: 'Joe Biden',
            dateTime: '2021-09-01 12:40:23',
            message: "Message"),
        NotificationModel(
            dateTime: '2021-09-01 12:40:23',
            imageAddress:
                'https://pbs.twimg.com/profile_images/1414358967371935744/yAadRgF-_400x400.jpg',
            name: 'Boris Johnson',
            message:
                'Set up Face ID to access Pockets without having to type your Passcode.'),
        NotificationModel(
            name: 'Joe Biden',
            dateTime: '2021-08-31 12:40:23',
            message: 'Message'),
        NotificationModel(
            dateTime: '2021-08-31 12:40:23',
            imageAddress:
                'https://pbs.twimg.com/profile_images/1414358967371935744/yAadRgF-_400x400.jpg',
            name: 'Boris Johnson',
            message: 'Notification'),
        NotificationModel(
            name: 'Joe Biden',
            dateTime: '2021-08-30 12:40:23',
            message: 'Message'),
        NotificationModel(
            dateTime: '2021-08-30 12:40:23',
            imageAddress:
                'https://pbs.twimg.com/profile_images/1414358967371935744/yAadRgF-_400x400.jpg',
            name: 'Boris Johnson',
            message: 'Notification'),
        NotificationModel(
            name: 'Joe Biden',
            dateTime: '2021-08-30 12:40:23',
            message: 'Message'),
        NotificationModel(
            dateTime: '2021-08-30 12:40:23',
            imageAddress:
                'https://pbs.twimg.com/profile_images/1414358967371935744/yAadRgF-_400x400.jpg',
            name: 'Boris Johnson',
            message:
                'Set up Face ID to access Pockets without having to type your Passcode.'),
        NotificationModel(
            dateTime: '2021-08-29 12:40:23',
            imageAddress:
                'https://pbs.twimg.com/profile_images/1414358967371935744/yAadRgF-_400x400.jpg',
            name: 'Boris Johnson',
            message:
                'Set up Face ID to access Pockets without having to type your Passcode.'),
        NotificationModel(
            dateTime: '2021-08-28 12:40:23',
            imageAddress:
                'https://pbs.twimg.com/profile_images/1414358967371935744/yAadRgF-_400x400.jpg',
            name: 'Boris Johnson',
            message: 'Notification'),
        NotificationModel(
            dateTime: '2021-08-27 12:40:23',
            imageAddress:
                'https://pbs.twimg.com/profile_images/1414358967371935744/yAadRgF-_400x400.jpg',
            name: 'Boris Johnson',
            message:
                'Set up Face ID to access Pockets without having to type your Passcode.'),
        NotificationModel(
            dateTime: '2021-08-26 12:40:23',
            imageAddress:
                'https://pbs.twimg.com/profile_images/1414358967371935744/yAadRgF-_400x400.jpg',
            name: 'Boris Johnson',
            message:
                'Set up Face ID to access Pockets without having to type your Passcode.'),
        NotificationModel(
            dateTime: '2021-08-25 12:40:23',
            imageAddress:
                'https://pbs.twimg.com/profile_images/1414358967371935744/yAadRgF-_400x400.jpg',
            name: 'Boris Johnson',
            message:
                'Set up Face ID to access Pockets without having to type your Passcode.'),
        NotificationModel(
            dateTime: '2021-09-07 11:40:23',
            imageAddress:
                'https://pbs.twimg.com/profile_images/1414358967371935744/yAadRgF-_400x400.jpg',
            name: 'Boris Johnson',
            message:
                'Set up Face ID to access Pockets without having to type your Passcode.'),
      ]);
      notificationList.sort((NotificationModel a, NotificationModel b) =>
          a.dateTime!.compareTo(b.dateTime!));
      yield SuccessState(successResponse: notificationList);
    }
  }
}
