import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:meta/meta.dart';

part 'place_expression_event.dart';
part 'place_expression_state.dart';

class PlaceExpressionBloc extends Bloc<PlaceExpressionEvent, PlaceExpressionState> {
  PlaceExpressionBloc() : super(PlaceExpressionInitial());

  @override
  Stream<PlaceExpressionState> mapEventToState(
    PlaceExpressionEvent event,
  ) async* {
    // TODO: implement mapEventToState
  }
}
