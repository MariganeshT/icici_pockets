part of 'place_expression_bloc.dart';

@immutable
abstract class PlaceExpressionEvent extends BaseEquatable{}

class PlaceExpressionInitialEvent extends PlaceExpressionEvent {} 
