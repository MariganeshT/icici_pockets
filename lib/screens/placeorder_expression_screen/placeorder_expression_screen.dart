import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:icici/widgets/custom_button.dart';

import '../../router.dart';
import 'bloc/place_expression_bloc.dart';

class PlaceExpression extends StatefulWidget {
  const PlaceExpression({ Key? key }) : super(key: key);

  @override
  _PlaceExpressionState createState() => _PlaceExpressionState();
}

class _PlaceExpressionState extends State<PlaceExpression> {
  bool isAddFundStatus = false;
  PlaceExpressionBloc? bloc;

  @override
  void initState() {
    super.initState();
    bloc = PlaceExpressionBloc()..add(PlaceExpressionInitialEvent());
  }
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: Colors.transparent));
    return Scaffold(
        backgroundColor: ColorResource.color641653,
        body: BlocListener<PlaceExpressionBloc, PlaceExpressionState>(
          bloc: bloc,
          listener: (BuildContext context, PlaceExpressionState state) {},
          child: BlocBuilder<PlaceExpressionBloc, PlaceExpressionState>(
            bloc: bloc,
            builder: (BuildContext context, PlaceExpressionState state) {
              return Container(
                color: ColorResource.color641653,
                child: MediaQuery.removePadding(
                    context: context,
                    removeRight: true,
                    removeBottom: true,
                    removeLeft: true,
                    child: Column(children: [
                      CustomAppbar(
                        titleString:
                            StringResource.expressioncard,
                        iconEnumValues: IconEnum.back,
                        titleSpacing: 8,
                        onItemSelected: (values) async {
                          Navigator.pop(context);
                        },
                      ),
                      
                      Expanded(
                        child: Column(children: <Widget>[
                          Expanded(
                              flex: 6,
                              child: Image.asset(ImageResource.expressiontheme)),
                          Expanded(
                            flex: 8,
                            child: SizedBox(
                              width: MediaQuery.of(context).size.width,
                              child: Card(
                                  shape: const RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(14),
                                      topRight: Radius.circular(14),
                                    ),
                                  ),
                                  child: placeExpressionOrderWidget()),
                            ),
                          ),
                        ]),
                      ),
                  
                    ])));
            },
          ),
        ));
  }

  placeExpressionOrderWidget() {
    return Padding(
      padding: const EdgeInsets.all(24.0),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 27.0),
              child: Row(
                children: <Widget>[
                  Image.asset(ImageResource.delivered),
                  const SizedBox(width: 8),
                  CustomText(Languages.of(context)!.deliverTo,
                      style: Theme.of(context)
                          .textTheme
                          .subtitle1!
                          .copyWith(color: ColorResource.color787878)),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 10.0),
              child: CustomText('Prince Anto',
                  style: Theme.of(context).textTheme.headline5!.copyWith(
                        color: ColorResource.color222222,
                      )),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 24.0),
              child: CustomText(
                  'Sunchee Scedar, 2486, 19th, Cross, 7th Main Rd,Sector-01, HSR Layout',
                  style: Theme.of(context).textTheme.subtitle1!.copyWith(
                        color: ColorResource.color222222,
                      )),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 10.0),
              child: CustomText(Languages.of(context)!.billDetails,
                  style: Theme.of(context).textTheme.headline5!.copyWith(
                        color: ColorResource.color222222,
                      )),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                CustomText(Languages.of(context)!.expressionPocketsCard,
                    style: Theme.of(context).textTheme.subtitle1!.copyWith(
                          color: ColorResource.color222222,
                        )),
                CustomText('₹250.00',
                    style: Theme.of(context).textTheme.subtitle1!.copyWith(
                        color: ColorResource.color222222,
                        fontWeight: FontWeight.w700)),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  CustomText(Languages.of(context)!.gst,
                      style: Theme.of(context).textTheme.subtitle1!.copyWith(
                            color: ColorResource.color222222,
                          )),
                  CustomText('₹49.00',
                      style: Theme.of(context).textTheme.subtitle1!.copyWith(
                          color: ColorResource.color222222,
                          fontWeight: FontWeight.w700)),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 33.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  CustomText(Languages.of(context)!.toPay,
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          color: ColorResource.color222222,
                          fontWeight: FontWeight.w700)),
                  CustomText('₹299.00',
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          color: ColorResource.color222222,
                          fontWeight: FontWeight.w800)),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 24.0),
              child: CustomButton(
                Languages.of(context)!.placeOrder,
                onTap: () {
                  isAddFundStatus
                      ? addFundsStatusBottomSheet()
                      : Navigator.pushNamed(
                          context, AppRoutes.orderSatusExpressionScreen);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  addFundsStatusBottomSheet() {
    showModalBottomSheet(
      isScrollControlled: true,
      isDismissible: false,
      context: context,
      backgroundColor: ColorResource.colorFFFFFF,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(32),
        ),
      ),
      builder: (BuildContext context) {
        return SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 13.0, left: 16),
                child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Image.asset(ImageResource.close)),
              ),
              const Center(
                child: Image(image: AssetImage(ImageResource.addFunds)),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 14.0, left: 24, right: 24),
                child: CustomText(Languages.of(context)!.addFund,
                    style: Theme.of(context).textTheme.headline5!.copyWith(
                          color: ColorResource.color222222,
                        )),
              ),
              Container(
                margin:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 24),
                child: Wrap(
                  children: <Widget>[
                    RichText(
                        text: TextSpan(children: <TextSpan>[
                      TextSpan(
                          // text: Languages.of(context)!.youCan,
                          text:
                              'The balance required to apply for a physical card is',
                          style: Theme.of(context)
                              .textTheme
                              .subtitle1!
                              .copyWith(
                                  color: ColorResource.color222222
                                      .withOpacity(0.5))),
                      TextSpan(
                        text: ' `100 + GST.',
                        style: Theme.of(context).textTheme.subtitle1!.copyWith(
                            color: ColorResource.color222222.withOpacity(0.5)),
                      ),
                      TextSpan(
                          // text: Languages.of(context)!.orTap,
                          text:
                              ' Please add funds to apply for your Pockets card.',
                          style: Theme.of(context)
                              .textTheme
                              .subtitle1!
                              .copyWith(
                                  color: ColorResource.color222222
                                      .withOpacity(0.5))),
                    ]))
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 24.0, left: 24, right: 24, bottom: 24),
                child: CustomButton(
                  Languages.of(context)!.addFunds,
                  onTap: () {
                    Navigator.pop(context);
                    // addressBottomSheet();
                  },
                ),
              ),
            ],
          ),
        );
      },
    );
  }

}