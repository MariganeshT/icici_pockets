import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_button.dart';

import '../../router.dart';
import 'bloc/welcome_home_bloc.dart';

class WelcomeHomeScreen extends StatefulWidget {
  const WelcomeHomeScreen({Key? key}) : super(key: key);

  @override
  _WelcomeHomeScreenState createState() => _WelcomeHomeScreenState();
}

class _WelcomeHomeScreenState extends State<WelcomeHomeScreen> {
  late WelcomeHomeBloc bloc;
  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<WelcomeHomeBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Padding(
        padding:
            const EdgeInsets.only(left: 23, right: 25, bottom: 10, top: 10),
        child: CustomButton(
          Languages.of(context)!.setUpPasscode,
          onTap: () {
            Navigator.pushReplacementNamed(context, AppRoutes.setPinScreen);
          },
        ),
      ),
      backgroundColor: ColorResource.colorFAFAFA,
      body: BlocListener<WelcomeHomeBloc, WelcomeHomeState>(
        listener: (context, state) {},
        child: BlocBuilder<WelcomeHomeBloc, WelcomeHomeState>(
          builder: (context, state) {
            if (state is WelcomeHomeLoadingState) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            return SafeArea(
              top: false,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Stack(
                    children: [
                      AspectRatio(
                        aspectRatio: 1.07,
                        // width: double.infinity,
                        child: Image.asset(
                          ImageResource.welcomehome,
                          fit: BoxFit.fill,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 60, left: 20),
                        child: Column(
                          // mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text.rich(TextSpan(children: <InlineSpan>[
                              TextSpan(
                                  text:
                                      Languages.of(context)!.welcomeHomeScreen,
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline6!
                                      .copyWith(
                                          fontSize: 36,
                                          color: ColorResource.colorFF781F)),
                              const WidgetSpan(
                                child: SizedBox(
                                  width: 14,
                                ),
                              ),
                              TextSpan(
                                  text: Languages.of(context)!.welcomeHomeText,
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline6!
                                      .copyWith(
                                          fontSize: 36,
                                          color: ColorResource.colorFFFFFF))
                            ])),
                            SizedBox(height: 14),
                            SizedBox(
                              width: 256,
                              child: CustomText(
                                Languages.of(context)!.welcomeExisting,
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1!
                                    .copyWith(
                                      // fontWeight: FontWeight.w600,
                                      fontSize: 18,
                                      fontStyle: FontStyle.normal,
                                      color: ColorResource.colorFFFFFF
                                          .withOpacity(0.8),
                                    ),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 65, right: 65),
                      child: AspectRatio(
                          aspectRatio: 1.07,
                          child: Image.asset(
                            ImageResource.welcomenew,
                          )),
                    ),
                  ),
                  // SizedBox(
                  //   height: 20,
                  // ),
                  // Container(
                  //   // alignment: Alignment.center,
                  //   height: 270,
                  //   width: 280,
                  //   child: Image.asset(
                  //     ImageResource.welcomenew,
                  //     alignment: Alignment.center,
                  //   ),
                  // )
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
