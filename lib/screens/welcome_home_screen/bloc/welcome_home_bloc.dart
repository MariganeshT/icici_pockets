import 'package:bloc/bloc.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:meta/meta.dart';

part 'welcome_home_event.dart';
part 'welcome_home_state.dart';

class WelcomeHomeBloc extends Bloc<WelcomeHomeEvent, WelcomeHomeState> {
  WelcomeHomeBloc() : super(WelcomeHomeInitial()) {
    on<WelcomeHomeEvent>((event, emit) {
      // TODO: implement event handler
    });
  }
}
