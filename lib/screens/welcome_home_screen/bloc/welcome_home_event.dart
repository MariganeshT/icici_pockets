part of 'welcome_home_bloc.dart';

@immutable
abstract class WelcomeHomeEvent extends BaseEquatable {}

class WelcomeHomeInitialEvent extends WelcomeHomeEvent {}
