part of 'welcome_home_bloc.dart';

@immutable
abstract class WelcomeHomeState {}

class WelcomeHomeInitial extends WelcomeHomeState {}

class WelcomeHomeLoadingState extends WelcomeHomeState {}

class WelcomeHomeLoadedState extends WelcomeHomeState {}
