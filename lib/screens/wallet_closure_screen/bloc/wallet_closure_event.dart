part of 'wallet_closure_bloc.dart';

@immutable
abstract class WalletClosureEvent extends BaseEquatable {}

class WalletClosureInitialEvent extends WalletClosureEvent {}
