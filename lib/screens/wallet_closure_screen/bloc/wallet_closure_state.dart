part of 'wallet_closure_bloc.dart';

@immutable
abstract class WalletClosureState extends BaseEquatable {}

class WalletClosureInitial extends WalletClosureState {}

class WalletClosureLoadingState extends WalletClosureState {}

class WalletClosureSuccessState extends WalletClosureState {}
