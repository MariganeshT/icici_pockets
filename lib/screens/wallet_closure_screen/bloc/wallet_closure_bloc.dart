import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:icici/model/wallet_models.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:meta/meta.dart';

part 'wallet_closure_event.dart';
part 'wallet_closure_state.dart';

class WalletClosureBloc extends Bloc<WalletClosureEvent, WalletClosureState> {
  WalletClosureBloc() : super(WalletClosureInitial());

  List<WalletModel> walletModelList = [];

  @override
  Stream<WalletClosureState> mapEventToState(
    WalletClosureEvent event,
  ) async* {
    walletModelList.addAll([
      WalletModel('Reason 01', false, false),
      WalletModel('Reason 02', false, false),
      WalletModel('Reason 03', false, false),
      WalletModel('Reason 04', false, false),
      WalletModel('Other', false, true),
    ]);

    yield WalletClosureSuccessState();
  }
}
