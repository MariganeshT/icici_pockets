import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/router.dart';
import 'package:icici/utils/app_utils.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_button.dart';

class RequestAcceptBottomSheet extends StatefulWidget {
  const RequestAcceptBottomSheet({Key? key}) : super(key: key);

  @override
  _RequestAcceptBottomSheetState createState() =>
      _RequestAcceptBottomSheetState();
}

class _RequestAcceptBottomSheetState extends State<RequestAcceptBottomSheet> {
  String _copy = 'PKT0001234';
  @override
  Widget build(BuildContext context) {
    return StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) {
      return WillPopScope(
        onWillPop: () async => false,
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const Center(
                  child: Image(
                      height: 200,
                      image: AssetImage(ImageResource.walletClosure)),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 18.0),
                  child: CustomText(Languages.of(context)!.requestAccepted,
                      style: Theme.of(context).textTheme.headline5!.copyWith(
                            color: ColorResource.color222222,
                          )),
                ),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 10),
                  child: Wrap(
                    children: <Widget>[
                      RichText(
                          text: TextSpan(children: <TextSpan>[
                        TextSpan(
                            text: Languages.of(context)!.yourRequest,
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                                    color: ColorResource.color222222
                                        .withOpacity(0.5))),
                        TextSpan(
                          text: ' 86575 67777 ',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(color: ColorResource.color222222),
                          recognizer: TapGestureRecognizer()..onTap = () {},
                        ),
                        TextSpan(
                            text: Languages.of(context)!.toDeactivate,
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                                    color: ColorResource.color222222
                                        .withOpacity(0.5))),
                      ]))
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 10),
                  child: Row(
                    children: <Widget>[
                      CustomText(Languages.of(context)!.referenceNo,
                          style:
                              Theme.of(context).textTheme.subtitle1!.copyWith(
                                    color: ColorResource.color222222,
                                  )),
                      CustomText(_copy,
                          style:
                              Theme.of(context).textTheme.headline5!.copyWith(
                                    color: ColorResource.color222222,
                                  )),
                      const SizedBox(
                        width: 5,
                      ),
                      GestureDetector(
                          onTap: () async {
                            await Clipboard.setData(ClipboardData(text: _copy));
                            AppUtils.showToast('Copied');
                          },
                          child: Image.asset(ImageResource.copy1)),
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 10),
                  child: CustomText(Languages.of(context)!.pleaseNote,
                      style: Theme.of(context).textTheme.subtitle1!.copyWith(
                            color: ColorResource.color222222.withOpacity(0.5),
                          )),
                ),
                CustomButton(
                  Languages.of(context)!.done,
                  onTap: () {
                    int count = 0;
                    Navigator.popUntil(context, (route) {
                      return count++ == 3;
                    });
                  },
                )
              ],
            ),
          ),
        ),
      );
    });
  }
}
