import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/model/wallet_models.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:icici/widgets/custom_button.dart';
import 'package:icici/widgets/custom_textfield.dart';

import '../../router.dart';
import 'bloc/wallet_closure_bloc.dart';

class WalletClosureScreen extends StatefulWidget {
  @override
  _WalletClosureScreenState createState() => _WalletClosureScreenState();
}

class _WalletClosureScreenState extends State<WalletClosureScreen> {
  WalletClosureBloc? bloc;
  TextEditingController otherController = TextEditingController();
  bool bottomButtonVisible = false;
  int? selectedIndex;
  final GlobalKey<FormState> _form = GlobalKey<FormState>();

  FocusNode? otherFocus;
  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<WalletClosureBloc>(context);

    otherFocus = FocusNode();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: Colors.transparent));

    return BlocListener<WalletClosureBloc, WalletClosureState>(
      bloc: bloc,
      listener: (BuildContext context, WalletClosureState state) {},
      child: BlocBuilder<WalletClosureBloc, WalletClosureState>(
        bloc: bloc,
        builder: (BuildContext context, WalletClosureState state) {
          if (state is WalletClosureLoadingState) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }

          return SafeArea(
            top: false,
            child: Scaffold(
                resizeToAvoidBottomInset: true,
                backgroundColor: ColorResource.colorFAFAFA,
                bottomNavigationBar: bottomButtonVisible
                    ? Container(
                        margin: const EdgeInsets.only(
                            top: 24, bottom: 40, left: 24, right: 24),
                        child: CustomButton(
                          Languages.of(context)!.closeWallet,
                          onTap: () {
                            Navigator.pushNamed(
                                context, AppRoutes.setMpinScreen,
                                arguments: 'WalletBottomSheet');
                          },
                        ),
                      )
                    : null,
                body: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    CustomAppbar(
                      backgroundColor: ColorResource.color641653,
                      titleString:
                          Languages.of(context)!.reasonforWalletClosure,
                      titleSpacing: 8,
                      style: Theme.of(context)
                          .textTheme
                          .headline5!
                          .copyWith(color: ColorResource.colorFFFFFF),
                      iconEnumValues: IconEnum.back,
                      onItemSelected: (selectedItem) {
                        Navigator.pop(context);
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 18.0, top: 16),
                      child: CustomText(
                        Languages.of(context)!.pleaseTellUs,
                        style: Theme.of(context).textTheme.subtitle1!.copyWith(
                              color: ColorResource.color787878,
                            ),
                      ),
                    ),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 8, left: 12, right: 12, bottom: 8),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              ListView.builder(
                                  padding: const EdgeInsets.all(0),
                                  itemCount: bloc!.walletModelList.length,
                                  shrinkWrap: true,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Card(
                                        elevation: 2,
                                        margin: const EdgeInsets.only(top: 16),
                                        color: ColorResource.colorFFFFFF,
                                        shape: RoundedRectangleBorder(
                                          side: const BorderSide(
                                              color: Colors.white70),
                                          borderRadius:
                                              BorderRadius.circular(14),
                                        ),
                                        child: Container(
                                            alignment: Alignment.center,
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 10),
                                            child: Column(children: <Widget>[
                                              ListTile(
                                                trailing: GestureDetector(
                                                  child: Container(
                                                    child: bloc!
                                                            .walletModelList[
                                                                index]
                                                            .isSelected
                                                        ? const Padding(
                                                            padding:
                                                                EdgeInsets.all(
                                                                    5),
                                                            child: Image(
                                                              image: AssetImage(
                                                                  ImageResource
                                                                      .radio_selected),
                                                              height: 24,
                                                              width: 24,
                                                            ),
                                                          )
                                                        : Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                    .all(5),
                                                            child: Icon(
                                                              Icons
                                                                  .radio_button_off_outlined,
                                                              color: ColorResource
                                                                  .color222222
                                                                  .withOpacity(
                                                                      0.3),
                                                              size: 24,
                                                            ),
                                                          ),
                                                  ),
                                                  onTap: () {
                                                    setState(() {
                                                      selectedIndex = index;
                                                      bottomButtonVisible =
                                                          true;
                                                      bloc!.walletModelList
                                                          .forEach((WalletModel
                                                              element) {
                                                        element.isSelected =
                                                            false;
                                                      });
                                                      bloc!
                                                          .walletModelList[
                                                              index]
                                                          .isSelected = bloc!
                                                                  .walletModelList[
                                                                      index]
                                                                  .isSelected ==
                                                              true
                                                          ? false
                                                          : true;
                                                    });
                                                  },
                                                ),
                                                title: CustomText(
                                                  bloc!.walletModelList[index]
                                                      .name,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .subtitle1!
                                                      .copyWith(
                                                        color: ColorResource
                                                            .color222222,
                                                      ),
                                                ),
                                              ),
                                              Container(
                                                  margin: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 15),
                                                  child: Visibility(
                                                      visible: bloc!
                                                              .walletModelList[
                                                                  index]
                                                              .isSelected &&
                                                          bloc!
                                                              .walletModelList[
                                                                  index]
                                                              .isOthers,
                                                      child: Form(
                                                        key: _form,
                                                        child: CustomTextField(
                                                          Languages.of(context)!
                                                              .reason,
                                                          otherController,
                                                          focusNode: otherFocus,
                                                          keyBoardType:
                                                              TextInputType
                                                                  .text,
                                                          onEditing: () {
                                                            // _saveForm();
                                                          },
                                                          validatorCallBack:
                                                              (bool values) {},
                                                        ),
                                                      ))),
                                            ])));
                                  }),
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                )),
          );
        },
      ),
    );
  }
}
