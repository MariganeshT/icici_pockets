import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/base/base_state.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/languages/app_locale_constant.dart';
import 'package:icici/screens/track_shipment_screen/track_shipment_bloc.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:intl/intl.dart';

class TrackShipmentScreen extends StatefulWidget {
  const TrackShipmentScreen({Key? key}) : super(key: key);

  @override
  _TrackShipmentScreenState createState() => _TrackShipmentScreenState();
}

class _TrackShipmentScreenState extends State<TrackShipmentScreen> {
  Locale? _locale;
  late TrackShipmentBloc bloc;
  String dateValues = '';

  // final DateFormat outputFormat = DateFormat.yMMMd();

  @override
  void didChangeDependencies() {
    getLocale().then((Locale locale) {
      setState(() {
        _locale = locale;
      });
    });
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<TrackShipmentBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: bloc,
      listener: (BuildContext context, BaseState state) {
        if (state is SuccessState) {
          // bloc.notificationList.groupBy((NotificationModel m) => m.dateTime);
        }
      },
      child: BlocBuilder(
        bloc: bloc,
        builder: (BuildContext context, BaseState state) {
          if (state is LoadingState || state is InitialState) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          return SafeArea(
            top: false,
            child: Scaffold(
              body: Stack(
                children: [
                  SizedBox(
                    child: Column(
                      children: [
                        Expanded(
                          flex: 2,
                          child: Container(
                            decoration: const BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                    ImageResource.trackship_background),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Container(
                            color: ColorResource.colorE5E5E5,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    children: [
                      Flexible(
                        child: Column(
                          children: [
                            CustomAppbar(
                              titleString: Languages.of(context)!.trackShipment,
                              iconEnumValues: IconEnum.back,
                              titleSpacing: 8,
                              onItemSelected: (values) {
                                Navigator.pop(context);
                              },
                            ),
                            Expanded(
                              child: SizedBox(
                                child: Column(
                                  children: [
                                    const SizedBox(
                                      height: 16,
                                    ),
                                    CustomText(
                                        Languages.of(context)!.orderDetails,
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle1!
                                            .copyWith(
                                                color:
                                                    ColorResource.colorFF781F,
                                                fontWeight: FontWeight.w400)),
                                    Container(
                                      margin: const EdgeInsets.only(top: 08),
                                      child: CustomText('''
${Languages.of(context)!.dateOfOrder} ${DateFormat.yMMMd().format(DateTime.parse(bloc.shipmentModel.orderedDate.toString()))}''',
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1!
                                              .copyWith(
                                                  color:
                                                      ColorResource.colorFFFFFF,
                                                  fontWeight: FontWeight.w700)),
                                    ),
                                    Container(
                                      margin: const EdgeInsets.only(top: 08),
                                      child: CustomText('''
${Languages.of(context)!.amount} ₹${bloc.shipmentModel.amount}''',
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1!
                                              .copyWith(
                                                  color:
                                                      ColorResource.colorFFFFFF,
                                                  fontWeight: FontWeight.w700)),
                                    ),
                                    Container(
                                      margin: const EdgeInsets.only(top: 32),
                                      child: CustomText(
                                          Languages.of(context)!
                                              .estimatedDelivery,
                                          style: Theme.of(context)
                                              .textTheme
                                              .subtitle1!
                                              .copyWith(
                                                  color:
                                                      ColorResource.colorFF781F,
                                                  fontWeight: FontWeight.w400)),
                                    ),
                                    Container(
                                      margin: const EdgeInsets.only(top: 08),
                                      child: CustomText('5 Jun - 6 Jun',
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1!
                                              .copyWith(
                                                  color:
                                                      ColorResource.colorFFFFFF,
                                                  fontWeight: FontWeight.w700)),
                                    ),
                                    Flexible(
                                      child: Container(
                                        alignment: Alignment.topLeft,
                                        margin: const EdgeInsets.symmetric(
                                            vertical: 32, horizontal: 12),
                                        child: Card(
                                          elevation: 0,
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(14),
                                          ),
                                          shadowColor: ColorResource.color090B0,
                                          // Chan
                                          child: Column(
                                            children: [
                                              Flexible(
                                                child: ListView.builder(
                                                    physics: ScrollPhysics(),
                                                    itemCount: bloc
                                                        .shipmentModel
                                                        .shipmentTrack!
                                                        .length,
                                                    itemBuilder:
                                                        (BuildContext con,
                                                            int ind) {
                                                      if (ind != 0) {
                                                        return Column(
                                                          children: [
                                                            Container(
                                                              padding:
                                                                  const EdgeInsets
                                                                      .only(
                                                                left: 31,
                                                                right: 31,
                                                              ),
                                                              child: Column(
                                                                  children: [
                                                                    Row(
                                                                      children: [
                                                                        Column(
                                                                          children:
                                                                              List.generate(
                                                                            5,
                                                                            (int ii) =>
                                                                                Padding(
                                                                              padding: const EdgeInsets.only(
                                                                                  left: 25,
                                                                                  right: 10,
                                                                                  top: 2,
                                                                                  bottom: 2),
                                                                              child:
                                                                                  Container(
                                                                                height:
                                                                                    3,
                                                                                width:
                                                                                    2,
                                                                                color: bloc.shipmentModel.shipmentTrack![ind].isActive!
                                                                                    ? ColorResource.colorFF781F
                                                                                    : ColorResource.color787878,
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                    Container(
                                                                      child: Row(
                                                                          children: [
                                                                            const SizedBox(
                                                                              width:
                                                                                  5,
                                                                            ),
                                                                            Card(
                                                                              child:
                                                                                  Container(
                                                                                child:
                                                                                    Image(
                                                                                  image: AssetImage(bloc.shipmentModel.shipmentTrack![ind].isActive! ? ImageResource.shipment_tracking_activate : ImageResource.shipment_tracking_step_icon),
                                                                                  fit: BoxFit.fill,
                                                                                  width: 16,
                                                                                  height: 16,
                                                                                ),
                                                                                margin:
                                                                                    const EdgeInsets.all(8),
                                                                              ),
                                                                              elevation:
                                                                                  2,
                                                                              shape:
                                                                                  RoundedRectangleBorder(
                                                                                borderRadius:
                                                                                    BorderRadius.circular(8),
                                                                              ),
                                                                            ),
                                                                            const SizedBox(
                                                                              width:
                                                                                  16,
                                                                            ),
                                                                            CustomText(
                                                                              bloc
                                                                                  .shipmentModel
                                                                                  .shipmentTrack![ind]
                                                                                  .description!,
                                                                              style: Theme.of(context)
                                                                                  .textTheme
                                                                                  .subtitle1!
                                                                                  .copyWith(color: bloc.shipmentModel.shipmentTrack![ind].isActive! ? ColorResource.color222222 : ColorResource.color787878, fontWeight: bloc.shipmentModel.shipmentTrack![ind].isActive! ? FontWeight.w700 : FontWeight.w600),
                                                                            )
                                                                          ]),
                                                                    ),

                                                                  ]),
                                                            ),
                                                            (ind ==
                                                                bloc.shipmentModel.shipmentTrack!.length -
                                                                    1)
                                                                ? Container(
                                                              width: MediaQuery.of(context)
                                                                  .size
                                                                  .width,
                                                              child:
                                                              Card(
                                                                elevation:
                                                                0,
                                                                margin:
                                                                EdgeInsets.zero,
                                                                color: ColorResource
                                                                    .color1ABFCBD7
                                                                    .withOpacity(0.10),
                                                                shape:
                                                                RoundedRectangleBorder(
                                                                  borderRadius:
                                                                  BorderRadius.circular(16),
                                                                ),
                                                                child:
                                                                Container(
                                                                  margin:
                                                                  const EdgeInsets.symmetric(vertical: 16, horizontal: 25),
                                                                  child:
                                                                  Column(
                                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                                    children: [
                                                                      CustomText(Languages.of(context)!.deliveryAddress, style: Theme.of(context).textTheme.subtitle1!.copyWith(color: ColorResource.color787878, fontWeight: FontWeight.w400)),
                                                                      const SizedBox(
                                                                        height: 10,
                                                                      ),
                                                                      CustomText(bloc.shipmentModel.name!, style: Theme.of(context).textTheme.bodyText1!.copyWith(color: ColorResource.color222222, fontWeight: FontWeight.w700)),
                                                                      const SizedBox(
                                                                        height: 10,
                                                                      ),
                                                                      CustomText(bloc.shipmentModel.deliveryAddress!, style: Theme.of(context).textTheme.subtitle1!.copyWith(color: ColorResource.color222222, fontWeight: FontWeight.w400)),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ),
                                                              margin: EdgeInsets.only(
                                                                  top:
                                                                  20),
                                                            )
                                                                : Container()
                                                          ],
                                                        );
                                                      } else {
                                                        return Container(
                                                          padding:
                                                          const EdgeInsets
                                                              .only(
                                                            left: 31,
                                                            right: 31,
                                                          ),
                                                          child: Row(children: [
                                                            const SizedBox(
                                                              width: 5,
                                                            ),
                                                            Card(
                                                              child: Container(
                                                                child:
                                                                    const Image(
                                                                  image: AssetImage(
                                                                      ImageResource
                                                                          .shipment_tracking_activate),
                                                                  fit: BoxFit
                                                                      .contain,
                                                                  width: 16,
                                                                  height: 16,
                                                                ),
                                                                margin:
                                                                    const EdgeInsets
                                                                        .all(8),
                                                                width: 16,
                                                                height: 16,
                                                              ),
                                                              elevation: 2,
                                                              shape:
                                                                  RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            8),
                                                              ),
                                                            ),
                                                            const SizedBox(
                                                              width: 16,
                                                            ),
                                                            CustomText(
                                                                bloc
                                                                    .shipmentModel
                                                                    .shipmentTrack![
                                                                        0]
                                                                    .description!,
                                                                style: Theme.of(
                                                                        context)
                                                                    .textTheme
                                                                    .subtitle1!
                                                                    .copyWith(
                                                                        color: ColorResource
                                                                            .color222222,
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w700))
                                                          ]),
                                                        );
                                                      }
                                                    }),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}

extension DateHelpers on DateTime {
  bool isToday() {
    final DateTime now = DateTime.now();
    return now.day == this.day &&
        now.month == this.month &&
        now.year == this.year;
  }

  bool isYesterday() {
    final DateTime yesterday = DateTime.now().subtract(const Duration(days: 1));
    return yesterday.day == this.day &&
        yesterday.month == this.month &&
        yesterday.year == this.year;
  }
}
