import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:icici/base/base_state.dart';
import 'package:icici/model/shipment_model.dart';
import 'package:icici/model/shipment_tracking_step_model.dart';
import 'package:icici/screens/track_shipment_screen/track_shipment_event.dart';

class TrackShipmentBloc extends Bloc<TrackShipmentEvent, BaseState> {
  TrackShipmentBloc() : super(InitialState());

  ShipmentModel shipmentModel = ShipmentModel();

  @override
  Stream<BaseState> mapEventToState(
    TrackShipmentEvent event,
  ) async* {
    if (event is TrackShipmentInitialEvent) {
      yield LoadingState();
      shipmentModel.orderedDate = '2021-09-02 12:40:23';
      shipmentModel.estimatedDeliveryDate = '2021-10-01 12:40:23';
      shipmentModel.name = 'Prince Anto';
      shipmentModel.amount = 2500.00;
      shipmentModel.deliveryAddress =
          '2486, Sunchee Scedar, 17th Cross, 7th main, Sector-01, HSR Layout- 560102';
      shipmentModel.shipmentTrack = [
        ShipmentTrackingStepModel(
            description: 'Ordered Thrusday, 02 Sep', isActive: true),
        ShipmentTrackingStepModel(
            description: 'Shipped Friday, 07 Sep', isActive: true),
        ShipmentTrackingStepModel(
            description: 'Out for delivery', isActive: false),
        ShipmentTrackingStepModel(
            description: 'Arriving Wednesday', isActive: false),
      ];
      yield SuccessState();
    }
  }
}
