import 'package:flutter/cupertino.dart';
import 'package:icici/utils/base_equatable.dart';

@immutable
abstract class TrackShipmentEvent extends BaseEquatable {}

class TrackShipmentInitialEvent extends TrackShipmentEvent {
  BuildContext? context;

  TrackShipmentInitialEvent({this.context});
}
