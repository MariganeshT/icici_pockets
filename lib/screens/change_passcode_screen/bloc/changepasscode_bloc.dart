import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'changepasscode_event.dart';
part 'changepasscode_state.dart';

class ChangepasscodeBloc
    extends Bloc<ChangepasscodeEvent, ChangepasscodeState> {
  ChangepasscodeBloc() : super(ChangepasscodeInitial());

  @override
  Stream<ChangepasscodeState> mapEventToState(
    ChangepasscodeEvent event,
  ) async* {
    if (event is ChangepasscodeInitialEvent) {
      yield ChangepasscodeLoadedState();
    }
  }
}
