part of 'changepasscode_bloc.dart';

@immutable
abstract class ChangepasscodeEvent {}

class ChangepasscodeInitialEvent extends ChangepasscodeEvent {}
