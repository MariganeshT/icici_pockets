part of 'changepasscode_bloc.dart';

@immutable
abstract class ChangepasscodeState {}

class ChangepasscodeInitial extends ChangepasscodeState {}

class ChangepasscodeLoadingState extends ChangepasscodeState {}

class ChangepasscodeLoadedState extends ChangepasscodeState {}

class ChangepasscodeFailureState extends ChangepasscodeState {}
