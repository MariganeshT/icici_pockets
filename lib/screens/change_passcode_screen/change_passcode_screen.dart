import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:icici/languages/app_languages.dart';
import 'package:icici/screens/change_passcode_screen/bloc/changepasscode_bloc.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/widgets/customText.dart';
import 'package:icici/widgets/custom_appbar.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import '../../router.dart';

class ChangePasscodeScreen extends StatefulWidget {
  ChangePasscodeScreen({Key? key}) : super(key: key);

  @override
  _ChangePasscodeScreenState createState() => _ChangePasscodeScreenState();
}

class _ChangePasscodeScreenState extends State<ChangePasscodeScreen> {
  TextEditingController changePasscodeController = TextEditingController();
  late ChangepasscodeBloc bloc;
  FocusNode focusNode = FocusNode();

  @override
  void initState() {
    bloc = ChangepasscodeBloc()..add(ChangepasscodeInitialEvent());
    focusNode.requestFocus();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorResource.colorFAFAFA,
      body: BlocListener<ChangepasscodeBloc, ChangepasscodeState>(
        listener: (context, state) {},
        child: BlocBuilder<ChangepasscodeBloc, ChangepasscodeState>(
            bloc: bloc,
            builder: (context, state) {
              if (state is ChangepasscodeLoadingState) {
                return Scaffold(
                  body: Center(
                    child: CircularProgressIndicator(),
                  ),
                );
              } else if (state is ChangepasscodeLoadedState) {
                return Column(
                  children: [
                    SizedBox(
                      child: CustomAppbar(
                        backgroundColor: ColorResource.color641653,
                        titleString: Languages.of(context)!.changePasscode,
                        titleSpacing: 8,
                        style: Theme.of(context)
                            .textTheme
                            .headline5!
                            .copyWith(color: ColorResource.colorFFFFFF),
                        iconEnumValues: IconEnum.back,
                        onItemSelected: (selectedItem) {
                          Navigator.pop(context);
                        },
                      ),
                    ),
                    Column(
                      children: <Widget>[
                        const SizedBox(height: 45),
                        CustomText(
                          Languages.of(context)!.enterCurrentPasscode,
                          style: Theme.of(context)
                              .textTheme
                              .subtitle1!
                              .copyWith(
                                  fontWeight: FontWeight.w400,
                                  color: ColorResource.color222222),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 45.0),
                          child: PinCodeTextField(
                            enablePinAutofill: false,
                            appContext: context,
                            onChanged: (String value) {},
                            focusNode: focusNode,
                            onCompleted: (String enteredText) {
                              Navigator.pushNamed(
                                  context, AppRoutes.confirmPasscodeScreen);
                            },
                            obscureText: true,
                            length: 4,
                            textStyle: Theme.of(context)
                                .textTheme
                                .headline5!
                                .copyWith(color: ColorResource.color222222),
                            animationType: AnimationType.scale,
                            autoDismissKeyboard: false,
                            animationDuration:
                                const Duration(milliseconds: 300),
                            keyboardType: TextInputType.number,
                            controller: changePasscodeController,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            pinTheme: PinTheme(
                              shape: PinCodeFieldShape.box,
                              borderWidth: 1,
                              borderRadius: BorderRadius.circular(8),
                              inactiveColor: ColorResource.color222222,
                              selectedColor: ColorResource.color222222,
                              activeColor: ColorResource.color222222,
                              fieldHeight: 60.89,
                              fieldWidth: 48,
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                );
              }
              return Scaffold(
                body: CircularProgressIndicator(),
              );
            }),
      ),
    );
  }
}
