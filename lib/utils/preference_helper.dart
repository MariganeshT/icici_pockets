import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:icici/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreferenceHelper {
  static const storage = FlutterSecureStorage();

  // Get Biometric authentication

  // static Future<String?> getBioMetricValue() {
  //   return storage.read(key: Constants.bioMetricLogin);
  // }

  // Get Mpin sta

  // static Future<String?> getMPINStatus() {
  //   return storage.read(key: Constants.mpin);
  // }

  static Future<bool> getBioMetricValue() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(Constants.bioMetricLogin) ?? false;
  }

  static setBioMetricValue(bool status) async {
    // obtain shared preferences
    final prefs = await SharedPreferences.getInstance();

    // set value
    await prefs.setBool(Constants.bioMetricLogin, status);
  }

  static Future<bool> getTrack() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(Constants.getTrack) ?? false;
  }

  static setTrack(bool status) async {
    // obtain shared preferences
    final prefs = await SharedPreferences.getInstance();

    // set value
    await prefs.setBool(Constants.getTrack, status);
  }

  static Future<bool> getMPINStatus() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(Constants.mpin) ?? false;
  }

  static setMPINStatus(bool status) async {
    // obtain shared preferences
    final prefs = await SharedPreferences.getInstance();

    // set value
    await prefs.setBool(Constants.mpin, status);
  }

  static setPreferenceValue(String keyPair, dynamic value) async {
    final prefs = await SharedPreferences.getInstance();
    if (value is bool) {
      await prefs.setBool(Constants.mpin, value);
    }
    if (value is int) {
      await prefs.setInt(Constants.mpin, value);
    }
    if (value is double) {
      await prefs.setDouble(Constants.mpin, value);
    }
    if (value is String) {
      await prefs.setString(Constants.mpin, value);
    }
  }

  static setPreference(String keyPair, dynamic value) async {
    final prefs = await SharedPreferences.getInstance();
    if (value is bool) {
      await prefs.setBool(keyPair, value);
    }
    if (value is int) {
      await prefs.setInt(keyPair, value);
    }
    if (value is double) {
      await prefs.setDouble(keyPair, value);
    }
    if (value is String) {
      await prefs.setString(keyPair, value);
    }
  }

  static Future<dynamic> getPreference(String keyPair) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.get(keyPair);
  }
// static const String keyPair = "IMAGE_KEY";

// dynamic getPreferenceValue(String keyPair) async {
//   final prefs = await SharedPreferences.getInstance();
//   return prefs.get(keyPair);
// }

// static Future<String> getStringPreferenceValue(String keyPair) async {
//   final prefs = await SharedPreferences.getInstance();
//   return prefs.getString(keyPair) ?? '';
// }

// // Set Biometric authentication

// static void setBioMetricValue(String value) {
//   storage.write(key: Constants.bioMetricLogin, value: value);
// }

// // Set MPIN status

// static void setMPINStatus(String value) {
//   storage.write(key: Constants.mpin, value: value);
// }
}
