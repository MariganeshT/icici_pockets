import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:icici/utils/color_resource.dart';
import 'package:icici/utils/custom_radio_view.dart';
import 'package:icici/utils/font.dart';

class CustomRadioEditTextField<T> extends StatefulWidget {
  CustomRadioEditTextField(
    this.editFieldController,
    this.focusNode, {
    this.absoluteZeroSpacing = false,
    required this.buttonIcons,
    required this.buttonLables,
    this.buttonTextStyle = const ButtonTextStyle(),
    required this.buttonValues,
    this.defaultSelected,
    double padding = 3,
    this.selectedBorderColor,
    this.selectedColor,
    double spacing = 0.0,
    this.style = const TextStyle(color: Colors.black),
    this.unSelectedBorderColor,
    this.unSelectedColor,
    this.wrapAlignment = WrapAlignment.start,
  })  : assert(buttonLables.length == buttonValues.length, '''
Button values list and button lables list should have same number of eliments '''),
        assert(buttonValues.toSet().length == buttonValues.length,
            'Multiple buttons with same value cannot exist') {
    if (absoluteZeroSpacing) {
      this.padding = 0;
      this.spacing = 0;
    } else {
      this.padding = padding;
      this.spacing = spacing;
    }
  }

  final bool absoluteZeroSpacing;
  final ButtonTextStyle buttonTextStyle;
  final Color? selectedColor;
  final Color? selectedBorderColor;
  final Color? unSelectedColor;
  final Color? unSelectedBorderColor;

  late final double padding;
  late final double spacing;
  final List<FocusNode> focusNode;
  final List<String> buttonLables;
  final List<T> buttonIcons;
  final List<T> buttonValues;
  final List<TextEditingController> editFieldController;
  final TextStyle style;
  final T? defaultSelected;
  final WrapAlignment wrapAlignment;

  _CustomRadioEditTextFieldState createState() =>
      _CustomRadioEditTextFieldState();
}

class _CustomRadioEditTextFieldState extends State<CustomRadioEditTextField> {
  String? _currentSelectedLabel;

  @override
  void initState() {
    super.initState();
    if (widget.defaultSelected != null) {
      if (widget.buttonValues.contains(widget.defaultSelected)) {
        final int index = widget.buttonValues.indexOf(widget.defaultSelected);
        _currentSelectedLabel = widget.buttonLables[index];
      } else {
        throw Exception('Default Value not found in button value list');
      }
    }
  }

  List<Widget> _buildButtonsRow(BuildContext context) {
    return widget.buttonValues.map((e) {
      int index = widget.buttonValues.indexOf(e);
      final TextEditingController textEditingController =
          TextEditingController();
      _currentSelectedLabel == widget.buttonLables[index]
          ? true
          : widget.editFieldController[index].clear();

      return GestureDetector(
        onTap: () {
          setState(() {
            _currentSelectedLabel = widget.buttonLables[index];
            widget.editFieldController.forEach((TextEditingController element) {
              if (element != widget.editFieldController[index]) {
                widget.focusNode[index].unfocus();
              } else {
                widget.editFieldController[index].notifyListeners();
                widget.focusNode[index].requestFocus(FocusNode());
              }
            });
          });
        },
        child: Center(
          child: Container(
            padding:
                const EdgeInsets.symmetric(horizontal: 5.0, vertical: 10.0),
            alignment: Alignment.centerLeft,
            height: 80,
            child: TextFormField(
              autofocus: true,
              textInputAction: TextInputAction.done,
              onFieldSubmitted: (String value) {
                widget.editFieldController[index].removeListener(() {
                  widget.focusNode[index].unfocus();
                });
              },
              controller: widget.editFieldController[index],
              onChanged: (String text) {
                setState(() {});
              },
              focusNode: widget.focusNode[index],
              enabled: _currentSelectedLabel == widget.buttonLables[index]
                  ? true
                  : false,
              textAlignVertical: TextAlignVertical.center,
              textAlign: TextAlign.left,
              decoration: InputDecoration(
                contentPadding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                labelText: widget.buttonLables[index],
                labelStyle: Theme.of(context).textTheme.subtitle1!.copyWith(
                      color: _currentSelectedLabel == widget.buttonLables[index]
                          ? ColorResource.colorF58220
                          : ColorResource.color222222,
                    ),
                filled: true,
                fillColor: ColorResource.colorFFFFFF,
                prefix: _currentSelectedLabel == widget.buttonLables[index]
                    // widget.focusNode[index].hasFocus
                    ? const Text.rich(
                        TextSpan(children: <InlineSpan>[
                          WidgetSpan(
                            child: Padding(
                              child: Icon(
                                Icons.radio_button_checked_rounded,
                                color: ColorResource.color203718,
                                size: 24,
                              ),
                              padding: EdgeInsets.only(right: 10.0),
                            ),
                          ),
                        ]),
                      )
                    : null,
                prefixIcon: _currentSelectedLabel != widget.buttonLables[index]
                    ? Padding(
                        padding: const EdgeInsets.all(10),
                        child: Icon(Icons.radio_button_off,
                            color: ColorResource.color222222.withOpacity(0.5)),
                      )
                    : null,
                border: const OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(12.0)),
                  borderSide:
                      BorderSide(color: ColorResource.colorF58220, width: 1),
                ),
                focusedBorder: const OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  borderSide:
                      BorderSide(color: ColorResource.colorF58220, width: 1),
                ),
                enabledBorder: const OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  borderSide:
                      BorderSide(color: ColorResource.colorF58220, width: 1),
                ),
                disabledBorder: const OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  borderSide:
                      BorderSide(color: ColorResource.colorefefef, width: 1),
                ),
              ),
              style: widget.style,
            ),
          ),
        ),
      );
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    return _buildRadioButtons(context);
  }

  _buildRadioButtons(BuildContext context) {
    return Center(
      child: CustomListViewSpacing(
        spacing: widget.spacing,
        scrollDirection: Axis.vertical,
        children: _buildButtonsRow(context),
      ),
    );
  }
}

class CustomListViewSpacing extends StatelessWidget {
  final List<Widget> children;
  final double spacing;
  final Axis scrollDirection;

  const CustomListViewSpacing(
      {required this.children,
      this.spacing = 0.0,
      this.scrollDirection = Axis.horizontal});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorResource.colorFAFAFA,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              child: ListView(
                scrollDirection: scrollDirection,
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                primary: false,
                children: children
                    .map((Widget widget) => Container(
                          padding: EdgeInsets.all(spacing),
                          child: widget,
                        ))
                    .toList(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
