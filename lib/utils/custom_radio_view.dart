// import 'package:flutter/material.dart';
// import 'package:icici/utils/font.dart';

// class CustomRadioView<T> extends StatefulWidget {
//   CustomRadioView({
//     required this.buttonLables,
//     required this.buttonValues,
//     required this.buttonIcons,
//     this.buttonTextStyle = const ButtonTextStyle(),
//     this.autoWidth = false,
//     this.radioButtonValue,
//     required this.unSelectedColor,
//     this.unSelectedBorderColor,
//     double padding = 0,
//     double spacing = 0.0,
//     required this.selectedColor,
//     this.selectedBorderColor,
//     this.height = 52,
//     this.width = 100,
//     this.enableButtonWrap = false,
//     this.horizontal = false,
//     this.enableShape = false,
//     this.elevation = 10,
//     this.shapeRadius = 50,
//     this.radius = 20,
//     this.defaultSelected,
//     this.customShape,
//     this.absoluteZeroSpacing = false,
//     this.margin,
//     this.wrapAlignment = WrapAlignment.start,
//   })  : assert(buttonLables.length == buttonValues.length,
//             "Button values list and button lables list should have same number of eliments "),
//         assert(unSelectedColor != null, "Unselected color cannot be null"),
//         assert(buttonValues.toSet().length == buttonValues.length,
//             "Multiple buttons with same value cannot exist"),
//         assert(selectedColor != null, "Selected color cannot be null") {
//     if (absoluteZeroSpacing) {
//       this.padding = 0;
//       this.spacing = 0;
//     } else {
//       this.padding = padding;
//       this.spacing = spacing;
//     }
//   }

//   ///Orientation of the Button Group
//   final bool horizontal;

//   ///Values of button
//   final List<T> buttonValues;

//   ///Values of icons
//   final List<T> buttonIcons;

//   ///This option will make sure that there is no spacing in between buttons
//   final bool absoluteZeroSpacing;

//   ///Margins around card
//   final EdgeInsetsGeometry? margin;

//   ///Default value is 35
//   final double height;
//   late final double padding;

//   ///Spacing between buttons
//   late final double spacing;

//   ///Default selected value
//   final T? defaultSelected;

//   ///Only applied when in vertical mode
//   ///This will use minimum space required
//   ///If enables it will ignore [width] field
//   final bool autoWidth;

//   ///Use this if you want to keep width of all the buttons same
//   final double width;

//   final List<String> buttonLables;

//   ///Styling class for label
//   final ButtonTextStyle buttonTextStyle;

//   final void Function(T)? radioButtonValue;

//   ///Unselected Color of the button
//   final Color unSelectedColor;

//   ///Selected Color of button
//   final Color selectedColor;

//   ///Unselected Color of the button border
//   final Color? unSelectedBorderColor;

//   ///Selected Color of button border
//   final Color? selectedBorderColor;

//   /// A custom Shape can be applied (will work only if [enableShape] is true)
//   final ShapeBorder? customShape;

//   ///alignment for button when [enableButtonWrap] is true
//   final WrapAlignment wrapAlignment;

//   /// This will enable button wrap (will work only if orientation is vertical)
//   final bool enableButtonWrap;

//   ///if true button will have rounded corners
//   ///If you want custom shape you can use [customShape] property
//   final bool enableShape;
//   final double elevation;

//   /// Radius for non-shape radio button
//   final double radius;

//   /// Radius for shape radio button
//   final double shapeRadius;

//   _CustomRadioViewState createState() => _CustomRadioViewState();
// }

// class _CustomRadioViewState extends State<CustomRadioView> {
//   String? _currentSelectedLabel;

//   @override
//   void initState() {
//     super.initState();
//     if (widget.defaultSelected != null) {
//       if (widget.buttonValues.contains(widget.defaultSelected)) {
//         int index = widget.buttonValues.indexOf(widget.defaultSelected);
//         _currentSelectedLabel = widget.buttonLables[index];
//       } else
//         throw Exception("Default Value not found in button value list");
//     }
//   }

//   List<Widget> _buildButtonsColumn() {
//     return widget.buttonValues.map((e) {
//       int index = widget.buttonValues.indexOf(e);
//       return Padding(
//         padding: EdgeInsets.all(widget.padding),
//         child: Card(
//           margin: widget.margin ??
//               EdgeInsets.all(widget.absoluteZeroSpacing ? 0 : 4),
//           color: _currentSelectedLabel == widget.buttonLables[index]
//               ? widget.selectedColor
//               : widget.unSelectedColor,
//           elevation: widget.elevation,
//           child: Container(
//             height: widget.height,
//             child: MaterialButton(
//               onPressed: () {
//                 widget.radioButtonValue!(e);
//                 setState(() {
//                   _currentSelectedLabel = widget.buttonLables[index];
//                 });
//               },
//               child: Center(
//                 child: Text(
//                   widget.buttonLables[index],
//                   textAlign: TextAlign.center,
//                   overflow: TextOverflow.ellipsis,
//                   maxLines: 1,
//                   style: widget.buttonTextStyle.textStyle.copyWith(
//                     color: _currentSelectedLabel == widget.buttonLables[index]
//                         ? widget.buttonTextStyle.selectedColor
//                         : widget.buttonTextStyle.unSelectedColor,
//                   ),
//                 ),
//               ),
//             ),
//           ),
//         ),
//       );
//     }).toList();
//   }

//   List<Widget> _buildButtonsRow() {
//     return widget.buttonValues.map((e) {
//       int index = widget.buttonValues.indexOf(e);
//       return GestureDetector(
//         onTap: () {
//           widget.radioButtonValue!(e);
//           setState(() {
//             _currentSelectedLabel = widget.buttonLables[index];
//           });
//         },
//         child: Column(
//           children: [
//             Container(
//               // height: 15,
//               width: widget.autoWidth ? null : widget.width,
//               child: Text(
//                 widget.buttonLables[index],
//                 textAlign: TextAlign.center,
//                 overflow: TextOverflow.ellipsis,
//                 maxLines: 1,
//                 style: widget.buttonTextStyle.textStyle.copyWith(
//                   color: _currentSelectedLabel == widget.buttonLables[index]
//                       ? widget.buttonTextStyle.selectedColor
//                       : widget.buttonTextStyle.unSelectedColor,
//                 ),
//               ),
//             ),
//             Container(
//               margin: EdgeInsets.only(top: 10),
//               padding: EdgeInsets.all(15.0),
//               decoration: BoxDecoration(
//                   color: _currentSelectedLabel == widget.buttonLables[index]
//                       ? widget.buttonTextStyle.backgroundColor
//                       : Colors.transparent,
//                   border: Border.all(
//                     color: _currentSelectedLabel == widget.buttonLables[index]
//                         ? Colors.transparent
//                         : widget.buttonTextStyle.unSelectedColor,
//                   ),
//                   borderRadius: BorderRadius.all(Radius.circular(40))),
//               child: widget.buttonIcons[index],
//             ),
//           ],
//         ),
//       );
//     }).toList();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return _buildRadioButtons();
//   }

//   _buildRadioButtons() {
//     if (widget.horizontal)
//       return Container(
//         height: widget.height * (widget.buttonLables.length * 1.5) +
//             widget.padding * 2 * widget.buttonLables.length,
//         child: Center(
//           child: CustomListViewSpacing(
//             spacing: widget.spacing,
//             scrollDirection: Axis.vertical,
//             children: _buildButtonsColumn(),
//           ),
//         ),
//       );
//     if (!widget.horizontal && widget.enableButtonWrap)
//       return Container(
//         child: Center(
//           child: Wrap(
//             spacing: widget.spacing,
//             direction: Axis.horizontal,
//             alignment: widget.wrapAlignment,
//             children: _buildButtonsRow(),
//           ),
//         ),
//       );
//     if (!widget.horizontal && !widget.enableButtonWrap)
//       return Container(
//         height: (widget.height + widget.padding) * 2,
//         child: Center(
//           child: CustomListViewSpacing(
//             spacing: widget.spacing,
//             scrollDirection: Axis.horizontal,
//             children: _buildButtonsRow(),
//           ),
//         ),
//       );
//   }
// }

import 'package:flutter/material.dart';

class ButtonTextStyle {
  final Color selectedColor;
  final Color unSelectedColor;
  final Color backgroundColor;
  final TextStyle textStyle;

  const ButtonTextStyle({
    this.selectedColor = Colors.white,
    this.unSelectedColor = Colors.white,
    this.backgroundColor = Colors.transparent,
    this.textStyle = const TextStyle(
      fontFamily: 'Mulish-Regular',
      fontWeight: FontWeight.bold,
      fontStyle: FontStyle.normal,
      fontSize: 14,
    ),
  });
}

// class SelectionModel {
//   final String lableName;
//   final IconData lableIcon;

//   const SelectionModel({
//     this.lableName = "Gender",
//     this.lableIcon = Icons.male,
//   });
// }

// class CustomListViewSpacing extends StatelessWidget {
//   final List<Widget> children;
//   final double spacing;
//   final Axis scrollDirection;

//   CustomListViewSpacing(
//       {required this.children,
//       this.spacing = 0.0,
//       this.scrollDirection = Axis.vertical});

//   Widget build(BuildContext context) {
//     return ListView(
//       shrinkWrap: true,
//       scrollDirection: scrollDirection,
//       children: children
//           .map((c) => Container(
//                 padding: EdgeInsets.all(spacing),
//                 child: c,
//               ))
//           .toList(),
//     );
//   }
// }
