class StringResource {
  static const String emailAddress = 'Email address';
  static const String fullName = 'Full Name';
  static const String asPerPanCard = 'As per PAN Card';
  static const String codeVerification = 'OTP Verification';
  static const String pleaseEnterTheCode = 'Please enter the OTP we have';
  static const String sent = 'sent you to';
  static const String codeExpireIn = 'OTP expire in';
  static const String codeNotReceived = 'OTP not received?';
  static const String resend = 'Resend';
  static const String contactSupport = 'Contact Support';
  static const String verifyYourIdentity = 'Verify Your Identity';
  static const String toVerifyYourIdentity =
      // ignore: lines_longer_than_80_chars
      'To verify your identity we need only one of the following document details';
  static const String aadharNumber = 'Aadhar Card';
  static const String passport = 'Passport';
  static const String voterID = 'Voter ID';
  static const String drivingLicense = 'Driving License';
  static const String nRegaJobCard = 'NREGA Job Card';
  static const String iAccept = 'I accept the';
  static const String terms = ' Terms & Conditions';
  static const String and = ' and';
  static const String privacy = ' Privacy.';

  static const String done = 'Done';
  static const String verifyMobileNumber = 'Verify Mobile Number';
  static const String verifyPersonalDetails = 'Verify Personal Details';
  static const String setupPasscode = 'Set up Passcode';
  static const String step = 'Step';
  static const String incorrectOTP = 'Incorrect OTP';

  static const String landing1Title = 'Experience UPI on Wallet';
  static const String landing1Description =
      'Pay at merchant sites, scan to pay, generate collect & send requests, transfer money & much more directly from your Pockets wallet.';
  static const String landing2Title = 'Shop Anywhere';
  static const String landing2Description =
      'With instant Pockets VISA card, you can shop online at any Indian website or get a Pockets physical card to shop at stores.';
  static const String landing3Title = 'Pockets Rewards';
  static const String landing3Description =
      'Instant cashbacks on Bill Pay, Recharge & other transactions.';

  static String remainingSeconds(int time) {
    return '$time seconds';
  }

  static const String generate = 'Generate New PIN for Pockets Card';
  static const String dateOfBirth = 'Date of Birth';
  static const String male = 'Male';
  static const String female = 'Female';
  static const String transGender = 'Transgender';
  static const String personalDetailsReq = 'Personal details requirement';
  static const String personalDetailsDes =
      'Before moving ahead, we need few important things to setup your account';
  static const String referralCode = 'Referral Code (Optional)';
  static const String personalDetails = 'Personal Details';
  static const String continueText = 'Continue';
  static const String set4digitPasscode = 'Set 4 digit passcode';
  static const String enterPasscode = 'Enter Passcode';
  static const String confirmPasscode = 'Confirm Passcode';
  static const String noteSetPasscode =
      'This passcode will be used every time you log in to the application.';
  static const String buttonTextContinue = 'Continue';
  static const String noteWalletSuccess =
      'Your Pockets Wallet is activated & offers you the following benefits -';
  static const String walletUPIid = 'Wallet UPI ID';
  static const String walletLimit = 'Wallet Limit';
  static const String basedOnKYC = '*Based on KYC';
  static const String virtualVISAcard = 'Virtual VISA Card';
  static const String goToDashboard = 'Go to Dashboard';
  static const String skip = 'Skip';
  static const String getStarted = 'Get Started';
  static const String select = 'Please select Terms & Conditions';
  static const String empty = 'Please select at least one Identity';
  static const String searchHint = 'Search';
  static const String passwordNotMatch = 'Passcode does not match';
  static const String pleaseSetPasscode = 'Please enter passcode';
  static const String genderErrorMsg = 'Select Gender';
  static const String verify = 'Verify';
  static const String update = 'Update';
  static const String ok = 'Ok';
  static const String dontAllow = 'Don’t Allow';
  static const String loginInstantly = 'Log In to your account instantly!';
  static const String doYouWantAllowFaceid =
      'Do you want to allow “Pockets” to use Face ID?';
  static const String forgotyourPinCode = 'Forgot your PIN Code?';
  static const String settings = 'Settings';
  static const String overview = 'Overview';
  static const String princeanto = 'Prince Anto';
  static const String profile = 'Profile';
  static const String security = 'Security';
  static const String accounts = 'Accounts';
  static const String support = 'Support';
  static const String moreoptions = 'More Options';
  static const String accountdetails = 'Account Details';
  static const String settransactionlimit = 'Set Transaction Limit';
  static const String requestforwallet = 'Request for Wallet closure';
  static const String myProfile = 'My Profile';
  static const String themes = 'Themes';
  static const String mobileNumber = 'Mobile Number';
  static const String emailId = 'Email ID';
  static const String address = 'Address';
  static const String kyc = 'KYC';
  static const String pocketsCard = 'Pockets Card';
  static const String upiId = 'UPI ID';
  static const String myQrCode = 'My QR code';
  static const String kycDocuments = 'KYC Document';
  static const String addAddress = 'Add Address';
  static const String completeKyc = 'Complete KYC';
  static const String applyForPhysicalCard = 'Apply for Physical card';
  static const String displayQrCode = 'Display QR code';
  static const String others = 'Others';
  static const String faq = 'FAQ';
  static const String referandearn = 'Refer & Earn';
  static const String rateus = 'Rate us';
  static const String manage = 'Manage Notifications';
  static const String termsandconditions = 'Terms & Conditions';
  static const String useragreement = 'User Agreement';
  static const String logout = 'Logout';
  static const String secureface = 'Secure with Face ID';
  static const String setupfaceid =
      'Set up Face ID to access Pockets without having to type your Passcode.';
  static const String changepasscode = 'Change Passcode';
  static const String setaccess = 'Set up passcode to access pockets';
  static const String save = 'Save';
  static const String pincode = 'Pincode';
  static const String house = 'House or Flat No';
  static const String address1 = 'Address 1';
  static const String address2 = 'Address Line 2 ( Optional)';
  static const String pocketWallet = 'Pockets Wallet';
  static const String walletBalance = 'Wallet Balance';
  static const String addFunds = 'Add Funds';
  static const String linkYourIciciaccount = 'Link your Bank Account';
  static const String linkIciciQuotes =
      'Duis aute irure dolor in reprehenderit in voluptate.';
  static const String linkAccount = 'Link Account';
  static const String linkYourOtheraccount = 'Link your other Bank Account';
  static const String refferal = 'Referral';
  static const String applyPhysicalCard = 'Apply for physical card';
  static const String applyNow = 'Apply Now';
  static const String validFrom = 'VALID FROM';
  static const String validTo = 'VALID TO';
  static const String edit = 'Edit';
  static const String personaldetailsDeshint =
      'Kindly enter all the mandatory personal details.';
  static const String weAreToHelp = "We're here to help";
  static const String email = 'Email';
  static const String callUs = 'Call Us';
  static const String liveSupport = 'Live Support';
  static const String haveMoreQuestion = 'Have more questions?';
  static const String visitFAQ = 'Visit our frequently asked questions page.';
  static const String goToFaq = 'Go to FAQs';
  static const String fqa = 'FAQs';
  static const String findSolution = "Can't find a solution?";
  static const String faqNote =
      'Get in touch with us our team is on standby to help.';
  static const String chatWithUs = 'Chat with us';
  static const String share = 'Share';
  static const String qrCode = 'QR Code';
  static const String qrDescription =
      'Show your Pockets QR Code to complete payment.';
  static const String qrNote =
      'Note: Generated QR code will expire in 2 minutes.';
  static const String appLinkAndroid =
      'https://play.google.com/store/apps/details?id=com.icicibank.pockets&referrer=utm_source%3Dicici-bank-site%26utm_medium%3Dpockets-launcher-page%26utm_campaign%3DGet-Pockets';
  static const String appLinkIOS =
      'https://apps.apple.com/in/app/pockets-by-icici-bank/id893982636';
  static const String manageNotification = 'Manage Notifications';
  static const String allowPushNotification = 'Allow Push Notifications';
  static const String allowWhatsappNotification =
      'Allow WhatsApp Notifications';
  static const String allowSMSNotification = 'Allow SMS Notifications';
  static const String allowEmailNotification = 'Allow Email Notifications';
  static const String noteForNotification =
      'We need to send you notifications for various cashback offers. new updates and payment status.';
  static const String areYouSure = 'Are you sure?';
  static const String noteLogout =
      'Are you sure want to log out of Pockets app. You need to sign in again to access to this app.';
  static const String logOut = 'Logout';

  static const String trendingServices = 'Trending Services';
  static const String seeAllServices = 'See All Services';
  static const String inviteYourFriend =
      'Invite your friends and get Bonus Points!!!';
  static const String yourReferalCode = 'Your Referral Code';
  static const String shareViaWhatsapp = 'Share via Whatsapp';
  static const String copyShare = 'Copy & Share';
  static const String more = 'More';
  static const String contactsNotYetOnPocktes = 'Contacts not yet on Pockets';
  static const String invite = 'Invite';
  static const String searchContactsHint = 'Search by number or name';
  static const String myreferals = 'My Referrals';
  static const String invited = 'Invited';
  static const String accepted = 'Accepted';
  static const String noReferralsFound = 'No Referrals Found';
  static const String noteMyReferal =
      'Dummy content, will be added later some time';
  static const String myRewards = 'My Rewards';
  static const String saveToPhotos = 'Save to photos';
  static const String photoSaved = 'Photo saved';
  static const String viewprofile = 'View Profile';
  static const String dailyLimit = 'New Daily Limit';
  static const String monthlyLimit = 'New Monthly limit';
  static const String confirm = 'Confirm';
  static const String pleaseEnterMPIN = 'Please Enter MPIN';
  static const String fourDigitPin =
      'MPIN is a four digit PIN that is asked while doing login in Pockets';
  static const String mpin = 'MPIN';
  static const String dailyLimitSuccess = 'Daily Limit Change successfully!';
  static const String increaseLimit =
      'You have increased your daily limit from ₹10,000.00 to ₹15,000.00.';
  static const String accountDetails = 'Account Details';
  static const String statement = 'Statement';
  static const String requestPhysicalCard = 'Request Physical Card';
  static const String setTransLimit = 'Set Transaction Limit';
  static const String reqForWalletClosure = 'Request for Wallet closure';
  static const String billPayRecharges = 'Bill Pay & Recharges';
  static const String fundtransferswitch =
      'Switch off, if you do not want to use Pockets for fund transfer';
  static const String billpayandrecharges =
      'Switch off, if you do not want to use Pockets for Bill pay & recharges';
  static const String daily = 'Daily Limit:';
  static const String monthly = 'Monthly Limit:';
  static const String editLimit = 'Edit Limit';
  static const String tenthousand = '₹10000.00';
  static const String fundtransfer = 'Fund Transfer';
  static const String pointofsale = 'Point of sale (POS)';
  static const String onlinetransactions = 'Online Transactions';
  static const String changeLimit = 'Change Limit';
  static const String termsAndConditionLink =
      'https://www.icicibank.com/Personal-Banking/bank-wallet/pockets/pockets.html';
  static const String userAgreementLink =
      'https://www.icicibank.com/Personal-Banking/bank-wallet/pockets/pockets.html';
  static const String dailyexceed =
      '*Daily limit cannot be exceed more than ₹10000.00.';
  static const String monthlyexceed =
      '*Monthly limit cannot be exceed more than ₹10000.00.';
  static const String track = 'Track';
  static const String trackService = 'Track Service Request';
  static const String trackPhysical = 'Track Physical Card';
  static const String changePasscode = 'Change Passcode';
  static const String enterCurrentPasscode = 'Enter Current Passcode';
  static const String notMatchedText = 'Not matched, Please enter correct PIN';
  static const String enterNewPasscode = 'Enter New Passcode';
  static const String buttonSetPasscode = 'Set Passcode';
  static const String setPin = 'Set PIN';
  static const String passcodeChangedSuccessfully =
      'Passcode changed successfully';
  static const String
      loremIpsumDolorSitAmetConsecteturAdipiscingElitIncididunt =
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit incididunt.';
  static const String requestphysical = 'Request for Physical Card';
  static const String expressioncard = 'Expression Pocket Card';
  static const String regularcard = 'Regular Pockets Card';
  static const String choosetheme = 'Choose theme';
  static const String selecttheme = 'Select your Card Theme';
  static const String alltheme = 'All Theme';
  static const String selectthemes = 'Select Theme';
  static const String proceed = 'Proceed';
}
