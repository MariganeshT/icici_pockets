import 'package:icici/login_setup_model.dart';
import 'package:icici/utils/image_resource.dart';
import 'package:icici/utils/string_resource.dart';

class Constants {
  static const String tenant = 'RBLPENCILTON';
  static const int otpWaitingTime = 60;
  static const String airtel = 'Airtel';
  static const String jio = 'Jio';
  static const String bsnl = 'BSNL';
  static const String vi = 'Vi';
  static const String vodafone = 'Vodafone';
  static List<LoginSetupModel> verificationList = [
    LoginSetupModel(ImageResource.callPNG, StringResource.verifyMobileNumber,
        '${StringResource.step} 01'),
    LoginSetupModel(ImageResource.profilePNG,
        StringResource.verifyPersonalDetails, '${StringResource.step} 02'),
    LoginSetupModel(ImageResource.passwordPNG, StringResource.setupPasscode,
        '${StringResource.step} 03')
  ];
  static const String bioMetricLogin = 'BioMetricLogin';

  static const String mpin = 'mpin';

  static const String home = 'home';
  static const String bag = 'bag';
  static const String profileImageShared = 'profileImageShared';
  static const String clearAll = 'clearAll';
  static const String getTrack = 'getTrack';
  static const String getGenerate = 'getTrack';
  static const String kycDontAskMeAgain = 'kycDontAskMeAgain';

  static List<int> keyBoardNumberList() {
    final List<int> keyBoardNumber = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
    keyBoardNumber.shuffle();
    keyBoardNumber.insert(9, 0);
    keyBoardNumber.insert(11, 0);
    return keyBoardNumber;
  }
}
