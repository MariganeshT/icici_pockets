class ImageResource {
  static const String landing1 = 'assets/landing1.png';

  static const String landing2 = 'assets/landing2.png';

  static const String landing3 = 'assets/landing3.png';

  static const String walletSuccess = 'assets/wallet_success.png';

  static const String walletDone = 'assets/wallet_done.png';
  static const String callPNG = 'assets/png/call.png';
  static const String passwordPNG = 'assets/png/password.png';
  static const String welcomeImagePNG = 'assets/png/welcome_image.png';

  static const String airtelPNG = 'assets/png/airtel.png';
  static const String jioPNG = 'assets/png/jio.png';
  static const String bsnlPNG = 'assets/png/bsnl.png';
  static const String viPNG = 'assets/png/vi.png';
  static const String profilePNG = 'assets/png/profile.png';
  static const String darkBlueBackgroundPNG =
      'assets/png/dark_blue_background.png';
  static const String setpin = 'assets/png/set_pin.png';

  static const String walletUPI = 'assets/wallet_upi.png';
  static const String walletLimit = 'assets/wallet_limit.png';
  static const String visaCard = 'assets/visa_card.png';
  static const String walletBg = 'assets/wallet_bg.png';
  static const String walletBgDot = 'assets/wallet_bg_dot.png';
  static const String walletBgGradient = 'assets/wallet_bg_gradient.png';

  static const String verifyIdentity = 'assets/png/verify_identity.png';
  static const String generate = 'assets/png/generate.png';

  static const String mobileVerificationLoading =
      'assets/png/mobile_verification_loading.png';

  static const String maleIcon = 'assets/Frame 1.png';
  static const String femaleIcon = 'assets/Frame 2.png';
  static const String transGenderIcon = 'assets/Frame 3.png';
  static const String personLDetailBottomsheet = 'assets/bottomsheet.png';
  static const String personalDetailBackImage = 'assets/personaldetailsbg.png';
  static const String pocketRewardBottomSheet = 'assets/pocketrewardbottomsheet.png';
  static const String returninguser = 'assets/png/returning_user.png';
  static const String facelock = 'assets/png/face_lock.png';
  static const String wallet = 'assets/png/Wallet.png';
  static const String arrow = 'assets/png/arrow.png';
  static const String theme = 'assets/png/theme.png';
  static const String kycDocument = 'assets/png/kyc_document.png';
  static const String qrCode = 'assets/png/qr_code.png';
  static const String pocketsCard = 'assets/png/pockets_card.png';
  static const String upi = 'assets/png/upi.png';
  static const String kyc = 'assets/png/kyc.png';
  static const String address = 'assets/png/address.png';
  static const String emailId = 'assets/png/email_id.png';
  static const String mobileNumber = 'assets/png/mobile_number.png';
  static const String accountdetails = 'assets/png/accountdetails.png';
  static const String settransactionlimit =
      'assets/png/settransactionlimit.png';
  static const String walletsource = 'assets/png/walletsource.png';
  static const String faq = 'assets/png/faq.png';
  static const String support = 'assets/png/support.png';
  static const String referearn = 'assets/png/referearn.png';
  static const String rateus = 'assets/png/rateus.png';
  static const String manage = 'assets/png/manage.png';
  static const String terms = 'assets/png/termsconditions.png';
  static const String useragreement = 'assets/png/useragreement.png';
  static const String logout = 'assets/png/logout.png';
  static const String faceid = 'assets/png/faceid.png';
  static const String lock = 'assets/png/passcode';

  static const String profile = 'assets/png/profileimage.png';
  static const String lockpasscode = 'assets/png/lockpasscode.png';
  static const String close = 'assets/png/close.png';
  static const String personalDetailBackImage1 = 'assets/Group 33767.png';
  static const String dashboardScreencardbg1 = 'assets/carddesign1.png';
  static const String dashboardScreencardbg2 = 'assets/carddesignn2.png';
  static const String dashboardScreencardbg3 = 'assets/carddesign3.png';
  static const String dashboardScreencardbg4 = 'assets/carddesignn4.png';
  static const String dashboardVisaImage = 'assets/visa.png';
  static const String dashboardAppbariconssearch = 'assets/Frame-3.png';
  static const String dashboardAppbariconsscan = 'assets/Frame-2.png';
  static const String dashboardAppbariconsnotifications = 'assets/Frame.png';
  static const String servicesIcon1 = 'assets/Folder.png';
  static const String servicesIcon2 = 'assets/Folder1.png';
  static const String servicesIcon3 = 'assets/png/Wallet.png';
  static const String servicesIcon4 = 'assets/Folder2.png';
  static const String servicesIcon5 = 'assets/Frame-4.png';
  static const String servicesIcon6 = 'assets/Folder3.png';
  static const String servicesIcon7 = 'assets/Frame-5.png';
  static const String servicesIcon8 = 'assets/EditSquare.png';
  static const String servicesIcon9 = 'assets/Stroke1.png';
  static const String supportBackground = 'assets/lounge.png';
  static const String supportImage = 'assets/supportBackground.png';
  static const String home = 'assets/png/home.png';
  static const String bag = 'assets/png/bag.png';
  static const String scan = 'assets/png/scan.png';
  static const String callus = 'assets/callus.png';
  static const String sendmail = 'assets/sendmail.png';
  static const String livesupport = 'assets/livesupport.png';
  static const String chat = 'assets/chat.png';
  static const String logoutImage2 = 'assets/logout_image.png';
  static const String allowNotification = 'assets/notification.png';
  static const String share2 = 'assets/share2.png';
  static const String camera = 'assets/png/camera.png';
  static const String camera1 = 'assets/png/camera1.png';
  static const String gallery = 'assets/png/gallery.png';
  static const String suppor_background = 'assets/png/suppor_background.png';
  static const String support_image = 'assets/chatimage.png';
  static const String offer_image = 'assets/png/offer_image.png';
  static const String dashboard_background =
      'assets/png/dashboard_background.png';
  static const String wallet_background = 'assets/png/wallet_background.png';
  static const String invitefriend = 'assets/invitefriend.png';
  static const String whatsapp = 'assets/whatsapp.png';
  static const String sendmail2 = 'assets/sendmail2.png';
  static const String copy1 = 'assets/png/copy.png';
  static const String copy2 = 'assets/copy2.png';
  static const String more = 'assets/more.png';
  static const String noReferalFound = 'assets/myreferal.png';
  static const String radio_selected = 'assets/png/radio_selected.png';
  static const String date_icon = 'assets/png/date_icon.png';
  static const String filter = 'assets/png/filter.png';
  static const String download_icon = 'assets/png/download_icon.png';
  static const String mail = 'assets/png/mail.png';
  static const String file = 'assets/png/file.png';
  static const String un_selected_icon = 'assets/png/un_selected_icon.png';
  static const String selected_icon = 'assets/png/selected_icon.png';
  static const String statement_sent_background = 'assets/statementsent.png';
  static const String statmentLogo = 'assets/statementlogo.png';
  static const String requestPhysicalcard = 'assets/requestdetaillogo.png';
  static const String accountLogo = 'assets/accountlogo.png';
  static const String setTransLimit = 'assets/settransactionlimit.png';
  static const String walletClosureLogo = 'assets/walletclosurelogo.png';
  static const String themes = 'assets/themeslogo.png';
  static const String share = 'assets/sharef.png';
  static const String cancel = 'assets/cancelarrow.png';
  static const String pay_to_contact = 'assets/pay_to_contact.png';
  static const String notification_empty = 'assets/png/notification_empty.png';
  static const String settings_icon = 'assets/png/settings_icon.png';
  static const String trackship_background =
      'assets/png/trackship_background.png';
  static const String shipment_tracking_step_icon =
      'assets/png/shipment_tracking_step_icon.png';
  static const String shipment_tracking_activate =
      'assets/png/shipment_tracking_activate.png';
  static const String billpay = 'assets/png/billpay.png';
  static const String fundtransfer = 'assets/png/fundtransfer.png';
  static const String pointofsale = 'assets/png/pointofsale.png';
  static const String onlinetransaction = 'assets/png/onlinetransaction.png';
  static const String success_image = 'assets/png/success_image.png';

  static const String settings = 'assets/settings.png';
  static const String verifiedEmail = 'assets/verifiedEmail.png';
  static const String request = 'assets/png/request.png';
  static const String walletClosure = 'assets/png/wallet_closure.png';
  static const String walletBalance = 'assets/png/wallet_balance.png';
  static const String walletCallReq = 'assets/png/wallet_call_req.png';

  static const String all_service_pay_to_contacts =
      'assets/png/all_service_pay_to_contact.png';
  static const String all_service_fund_transfer =
      'assets/png/all_service_fund_transfer.png';
  static const String all_service_upi = 'assets/png/all_service_upi.png';
  static const String all_service_add_funds =
      'assets/png/all_service_add_funds.png';
  static const String all_service_request_money =
      'assets/png/all_service_request_money.png';
  static const String all_service_apply_for_pockets_card =
      'assets/png/all_service_apply_for_pockets_card.png';
  static const String all_service_scan_to_pay =
      'assets/png/all_service_scan_to_pay.png';
  static const String all_service_bill_pay =
      'assets/png/all_service_bill_pay.png';
  static const String all_service_recharge =
      'assets/png/all_service_recharge.png';
  static const String all_service_fastag = 'assets/fasttagicon.png';
  static const String all_service_paypal = 'assets/png/all_service_paypal.png';
  static const String all_service_offers = 'assets/png/all_service_offers.png';
  static const String all_service_cash_karo_earning =
      'assets/png/all_service_cash_karo_earning.png';
  static const String all_service_split_bills =
      'assets/png/all_service_split_bills.png';
  static const String all_service_savings_account =
      'assets/png/all_service_savings_account.png';
  static const String all_service_prepaid_card =
      'assets/png/all_service_prepaid_card.png';
  static const String all_service_personal_loan =
      'assets/png/all_service_personal_loan.png';
  static const String all_service_two_wheeler_loan =
      'assets/png/all_service_two_wheeler_loan.png';
  static const String all_service_wallet_protection_plan =
      'assets/png/all_service_wallet_protection_plan.png';
  static const String all_service_card_protection_plan =
      'assets/png/all_service_card_protection_plan.png';
  static const String all_service_mutual_funds =
      'assets/png/all_service_mutual_funds.png';
  static const String all_service_fdrd = 'assets/png/all_service_fdrd.png';
  static const String all_service_i_wish = 'assets/png/all_service_i_wish.png';
  static const String all_service_pay_later =
      'assets/png/all_service_pay_later.png';
  static const String all_service_kyc = 'assets/png/all_service_kyc.png';
  static const String trackPhysical = 'assets/png/track_physical.png';
  static const String trackService = 'assets/png/track_service.png';
  static const String track = 'assets/png/track.png';
  static const String trackCard = 'assets/png/track_card.png';
  static const String location = 'assets/png/location.png';
  static const String addFunds = 'assets/png/add_funds.png';
  static const String delivered = 'assets/png/delivered.png';
  static const String copyOrder = 'assets/png/copy.png';
  static const String expression = 'assets/png/expression.png';
  static const String regular = 'assets/png/regular.png';
  static const String termsConditions = 'assets/png/terms_conditions.png';
  static const String placeholder_image = 'assets/placeholderimage.png';
  static const String search = 'assets/search.png';
  static const String unavailble = 'assets/unavailable.png';

  static const String change_passcode = 'assets/change_passcode.png';
  static const String downArrow = 'assets/downarrow.png';
  static const String cardRectangle = 'assets/cardrectangle.png';
  static const String cvv = 'assets/cvv.png';
  static const String regularexpression = 'assets/png/regular_expression.png';
  static const String dropdown = 'assets/png/dropdown.png';
  static const String tick = 'assets/png/tick.png';
  static const String choosethemeone = 'assets/png/choosetheme_one.png';
  static const String choosethemetwo = 'assets/png/choosetheme_two.png';
  static const String expressiontheme = 'assets/png/expression_theme.png';
  static const String sharee = 'assets/share.png';

  static const String copyNo = 'assets/copyno.png';
  static const String kyc_dialog_image = 'assets/png/kyc_dialog_image.png';
  static const String welcomehome = 'assets/png/welcome_home.png';
  static const String welcomenew = 'assets/png/welcome_new.png';
}
