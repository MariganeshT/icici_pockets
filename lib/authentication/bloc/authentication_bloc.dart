import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:icici/utils/base_equatable.dart';
import 'package:icici/utils/preference_helper.dart';
import 'package:meta/meta.dart';

part 'authentication_event.dart';
part 'authentication_state.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  AuthenticationBloc() : super(AuthenticationUnInitialized());

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    if (event is AppStarted) {
      await Future.delayed(const Duration(seconds: 5));

      bool mpinStatus = await PreferenceHelper.getMPINStatus();
      if (mpinStatus) {
        yield AuthenticationAuthenticated();
      } else {
        yield AuthenticationUnAuthenticated();
      }

      // bool isSafeDevice = await SafeDevice.isSafeDevice;
      // print('the devide safety device is $isSafeDevice');
      // if (isSafeDevice) {
      //   await Future.delayed(const Duration(seconds: 5));
      //   yield AuthenticationUnAuthenticated();
      // } else {
      //   yield SplashScreenState();
      // }
    }

    if (event is LoggedOut) {
      yield AuthenticationAuthenticated();
    }
  }
}
