part of 'authentication_bloc.dart';

@immutable
abstract class AuthenticationEvent extends BaseEquatable {}

class AppStarted extends AuthenticationEvent {}

class LoggedIn extends AuthenticationEvent {}

class LoggedOut extends AuthenticationEvent {}

class SessionLogout extends AuthenticationEvent {}
