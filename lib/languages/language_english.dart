import 'package:icici/languages/app_languages.dart';

class LanguageEn extends Languages {
  @override
  String get appName => 'ICICI Pockets';

  @override
  String get labelWelcome => 'You have pushed the button this many times:';

  @override
  String get labelSelectLanguage => 'Select Language';

  @override
  String get showPopUp => 'Show popup';

  @override
  String get setLightTheme => 'Set Light Theme';

  @override
  String get setDarkTheme => 'Set Dark Theme';

  @override
  String get noteWalletSuccess =>
      'Your Pockets Wallet is activated & offers you the following benefits -';

  @override
  String get walletUPIid => 'Wallet UPI ID';

  @override
  String get walletLimit => 'Wallet Limit';

  @override
  String get virtualVISAcard => 'Virtual VISA Card';

  @override
  String get congratulations => 'Congratulations !!!';

  @override
  String get goToDashboard => 'Go to Dashboard';

  String get welcomeString => 'Welcome to';

  @override
  String get pockets => 'Pockets';

  @override
  String get welcomeDescriptionString =>
      '3 step journey to start\nyour neo-banking\nexperience:';

  @override
  String get verifyMobileNumber => 'Verify Mobile Number';

  @override
  String get verifyPersonalDetails => 'Verify Personal Details';

  @override
  String get setupPasscode => 'Set up Passcode';

  @override
  String get verifyYourMobileNumber => 'Verify your mobile number';

  @override
  String get noteWelcomeScreen =>
      '*Regular SMS charges will apply for verification.';

  @override
  String get moSimCardErrorMessage =>
      'Sorry, We can\'t go ahead, ICICI need atlease one sim in mobile';

  @override
  String get selectYourSim => 'Please select your SIM';

  @override
  String get sim => 'SIM';

  @override
  String get verify => 'Verify';

  @override
  String get noteWelocomeScreen =>
      '*Regular SMS charges will apply for verification.';

  @override
  String get exisitingUserRegisterMessage =>
      '*For ICICI existing customer, please select the number registered with your bank.';

  @override
  String get numberVerificationString => 'Number verification in the process';

  @override
  String get numberVerificationDescriptionString =>
      'We’re verifying your number. Have patience with us, don’t close the app.';

  String get asPerPanCard => 'As per PAN card';

  @override
  String get personalDetails => 'Personal Details';

  @override
  String get dateOfBirth => 'Date of Birth';

  @override
  String get male => 'Male';

  @override
  String get female => 'Female';

  @override
  String get transGender => 'Transgender';

  @override
  String get personalDetailsReq => 'Personal details requirement';

  @override
  // ignore: lines_longer_than_80_chars
  String get personalDetailsDes =>
      'Before moving ahead, we need few important things to setup your account';

  @override
  String get continueText => 'Continue';

  @override
  String get ok => 'Ok';

  @override
  String get dontAllow => 'Don\'t Allow';

  @override
  String get smsSendDescription =>
      'Do you want to allow “Pockets” to send a SMS?';

  @override
  String get smsSendDescriptionNote =>
      '\nSend SMS to verify the users mobile number';

  @override
  String get mobileNumber => 'Mobile Number';

  String get goodToSeeYouBack => 'Good to see you back!';

  @override
  String get home => 'Home';

  @override
  String get rewards => 'Rewards';

  @override
  String get addAddress => 'Add Address';

  @override
  String get updateAddress => 'Update Address';

  @override
  String get verifyEmail => 'Verify Email';

  @override
  String get updateEmail => 'Update Email';

  @override
  String get otpVerification => 'OTP verification';

  @override
  String get camera => 'Camera';

  @override
  String get gallery => 'Gallery';

  @override
  String get otpVerificationDescription =>
      'Please enter the OTP we have sent you to your Email Id ';

  @override
  String get whatWentWrong => 'What went wrong???';

  @override
  String get weHappy =>
      'We\'re happy to help you at any cost. Please allow us to understand your concern.';

  @override
  String get youCan => 'You can call us on';

  @override
  String get orTap => 'or tap the button';

  @override
  String get someone => 'Someone from Pockets team will assist you.';

  @override
  String get requestForCallBack => 'Request for call back';

  @override
  String get continueToDelete => 'Continue to Delete';

  @override
  String get requestAccepted => 'Request Accepted';

  @override
  String get weHaveAccepted =>
      'We have accepted your request. You can expect Call from us within 2 hours.';

  @override
  String get gotIt => 'Got it';

  @override
  String get pleaseTellUs => 'Please tell us why you’re leaving';

  @override
  String get reasonForWalletClosure => 'Reason for Wallet closure';

  @override
  String get closeWallet => 'Close Wallet';

  @override
  String get reason => 'Reason';

  @override
  String get yourRequest =>
      'Your request for Pockets wallet closure has been registered and will be closed in 2 working days. Meanwhile,You can call us on';

  @override
  String get toDeactivate => 'to deactivate request for wallet closure.';

  @override
  String get referenceNo => 'Reference No: ';

  @override
  String get pleaseNote =>
      'Note: Please note down reference no. for wallet closure tracking. Contact customer care for any further concerns.';

  @override
  String get weFound => 'We found you have enough balance in wallet';

  @override
  String get weSuggest =>
      'We suggest you to transfer or use the balance, then only you can request for closure.';

  String get seeAllServices => 'See All Services';

  @override
  String get needHelp => 'Need Help';

  @override
  String get reasonforWalletClosure => 'Reason for Wallet closure';

  @override
  String get supportingHours => '24x7 Help & Support';

  @override
  String get liveSupport => 'Live Support';

  @override
  String get complete => 'Complete';

  @override
  String get trendingService => 'Trending Services';

  @override
  String get KYCRequired => 'KYC Required';

  @override
  String get statement => 'Statement';

  @override
  String get viewFullHistory => 'View Full History';

  @override
  String get detailedStatement => 'Detailed Statement';

  @override
  String get lastMonth => 'Last Month';

  @override
  String get lastThreeMonth => 'Last 3 Months';

  @override
  String get lastSixMonth => 'Last 6 Months';

  @override
  String get selectCustomDateRange => 'Select Custom Date Range';

  @override
  String get statementDuration => 'For which period you need a statement?';

  @override
  String get startDate => 'Start Date';

  @override
  String get endDate => 'End Date';

  @override
  String get downloadStatement => 'Download Statement';

  @override
  String get downloadPDF => 'Download PDF';

  @override
  String get downloadStatementOverEmail => 'Download Statement over email';

  @override
  String get filter => 'Filters';

  @override
  String get allPaidAndReceived => 'All paid/received';

  @override
  String get credit => 'Credit';

  @override
  String get debit => 'Debit';

  @override
  String get allUPITransactionStaus => 'All UPI transaction status';

  @override
  String get failure => 'Failure';

  @override
  String get success => 'Success';

  @override
  String get timeout => 'Timeout';

  @override
  String get allPaymentsTypes => 'All payments types';

  @override
  String get transfers => 'Transfers';

  @override
  String get upi => 'UPI';

  @override
  String get showResults => 'Show Results';

  @override
  String get billPayment => 'Bill Payments';

  @override
  String get onlinePayment => 'Online Payment';

  @override
  String get startDateRequired => 'Start date is required';

  @override
  String get endDateRequired => 'End date is required';

  @override
  String get startAndEndDateRequired => 'Start & End date is required';

  @override
  String get statementSentMessage =>
      'Statement sent to your registered email address';

  @override
  String get statementSentNote =>
      'You will receive an email with your statement for the requested period.';

  @override
  String get done => 'Done';

  @override
  String get allServices => 'All Services';

  @override
  String get notifications => 'Notifications';

  @override
  String get clearAll => 'Clear all';

  @override
  String get today => 'Today';

  @override
  String get yesterday => 'Yesterday';

  @override
  String get notificationEmpty => 'No Notification Found';

  @override
  String get notificationEmptyDescription =>
      'Dummy content, will be added latersome time';

  @override
  String get trackShipment => 'Track Shipments';

  @override
  String get orderDetails => 'Order Details:';

  @override
  String get dateOfOrder => 'Date of order:';

  @override
  String get amount => 'Amount Paid:';

  @override
  String get estimatedDelivery => 'Estimated Delivery:';

  @override
  String get deliveryAddress => 'Delivery Address:';

  @override
  String get payment => 'Payments';

  @override
  String get rechargeBillsOffers => 'Recharge, Bills & Offers';

  @override
  String get bank => 'Bank';

  @override
  String get trackServiceRequest => 'Track Service Request';

  @override
  String get openRequest => 'Open Request';

  @override
  String get closedRequest => 'Closed Request';

  @override
  String get serviceRequestNo => 'Service Request No.';

  @override
  String get open => 'Open';

  @override
  String get description => 'Description:';

  @override
  String get haveMoreQuestions => 'Have more questions?';

  @override
  String get ourSupport => 'Our support staff will try to resolve your query.';

  @override
  String get gotoSupport => 'Go to Support';

  @override
  String get noOpenRequestFound => 'No Open Request Found';

  @override
  String get trackPhysicalCard => 'Track Physical card';

  @override
  String get nothingyouorderedPhysicalCard =>
      'Nothing you ordered Physical card';

  @override
  String get requestPhysicalCard => 'Request Physical Card';

  @override
  String get amountPaid => 'Amount Paid';

  @override
  String get closed => 'Closed';

  @override
  String get upiid => 'UPI ID';

  @override
  String get checkAvailability => 'Check Availability';

  @override
  String get alternateSuggestedUpiid => 'Alternate Suggested UPI ID';

  @override
  String get notAvailable => 'Not Available';

  @override
  String get available => 'Available';

  @override
  String get pocketStatement => 'Pocket Statement';

  @override
  String get accounts => 'Accounts';

  @override
  String get others => 'Others';

  @override
  String get setTransactionLimit => 'Set Transaction Limit';

  @override
  String get reqForWalletClosure => 'Request for wallet Closure';

  @override
  String get authenticateAccount => 'Authenticate Account';

  @override
  String get selectBankAccount => 'Select Bank Account';

  @override
  String get selectDebitCard => 'Select Debit Card';

  @override
  String get gridNumber => 'Grid Number';

  @override
  String get set4DigitPIN => 'Set 4 digit PIN';

  @override
  String get enter4DigitPIN => 'Enter 4 digit New PIN';

  @override
  String get pinCreatedSuccessfully => 'PIN created successfully';

  @override
  String get reEnter4DigitPIN => 'Re-Enter 4 digit New PIN';

  @override
  String get termsAndConditions => 'Terms & conditions';

  @override
  String get proceed => 'Proceed';

  @override
  String get iHave =>
      'I have gone through the safe bank tips and agree with the Terms & Conditions';

  @override
  String get thisPasscode =>
      'This passcode will be used every time you log in to the application.';

  @override
  String get gridHint =>
      'Your ICICI Bank Debit card has grid on its reverse. Enter numbers from the grid as printed against the alphabets.';

  @override
  String get gridWrongHint => 'Wrong Grid Number, Please enter correct Grid';

  @override
  String get paymentStatus => 'Payment Status';

  @override
  String get sentTo => 'Sent To';

  @override
  String get sentAmountId => '9958084563@okicici';

  @override
  String get sentDate => 'May 29 2018, 2:50 PM';

  @override
  String get transactionId => 'Transaction ID';

  @override
  String get transRefNo => '#100138080791';

  @override
  String get transferVia => 'Transfer Via';

  @override
  String get vpa => 'VPA';

  @override
  String get edit => 'Edit';

  @override
  String get category => 'Category';

  @override
  String get outgoingTransfer => 'Outgoing Transfer';

  @override
  String get remark => 'Remark';

  @override
  String get sentFrom => 'Sent From';

  @override
  String get na => 'NA';

  @override
  String get raiseDispute => 'Raise Dispute';

  @override
  String get validFrom => 'Valid From';

  @override
  String get validThru => 'Valid Thru';

  @override
  String get validToDate => '20 May 2021';

  @override
  String get validFromDate => 'Apr 2026';

  @override
  String get viewCvv => 'View CVV';

  @override
  String get cvv => 'Cvv';

  String get enterCurrentPasscode => 'Enter CurrentPasscode';

  @override
  String get enterNewPasscode => 'Enter New Passcode';

  @override
  String get confirmPasscode => 'Confirm Passcode';

  @override
  String get passcodeChangedSuccessfully => 'Passcode changed successfully';

  @override
  String get passcodeChangedSuccessfullySubText =>
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit incididunt.';

  @override
  String get setPasscode => 'Set Passcode';

  @override
  String get notMatchedText => 'Not matched, Please enter correct PIN';

  @override
  String get changePasscode => 'Change Passcode';

  String get weDontDeliverHere => 'We don’t deliver here yet';

  @override
  String get regularPocketsCard => 'Regular Pockets Card';

  @override
  String get selectAddress => 'Select Address';

  @override
  String get deliverTo => 'Deliver to';

  @override
  String get applyNow => 'Apply Now';

  @override
  String get continuee => 'Continue';

  @override
  String get changeLocation => 'Change Location';

  @override
  String get save => 'Save';

  @override
  String get addFund => 'Add Fund';

  @override
  String get addFunds => 'Add Funds';

  @override
  String get editAddress => 'Edit Address';

  @override
  String get billDetails => 'Bill Details:';

  @override
  String get toPay => 'To Pay';

  @override
  String get expressionPocketsCard => 'Expression Pockets Card';

  @override
  String get gst => 'GST';

  @override
  String get placeOrder => 'Place Order';

  @override
  String get thankYou => 'Thank you for placing Expression Pockets Card.';

  @override
  String get returntoAccountDetails => 'Retun to Account Details';

  @override
  String get requestForPhysicalCard => 'Request for Physical Card';

  @override
  String get orderStatus => 'Order Status';

  @override
  String get paidAmount => '₹2000.00';

  @override
  String get copyToClipboard => 'Copied to clipboard';

  @override
  String get pocketsRewards => 'Pockets Rewards';

  @override
  String get pocketsRewardsDes =>
      'Thank you for creating an account with Pockets.';
  @override
  String get pocketsRewardsDes1 =>
      'No promocode required. Pay bills, Recharge mobile, Buy FASTag; use @pockets UPI ID to send money or pay.';
  @override
  String get availNow => 'Avail Now';

  @override
  String get termsAndConditionsApply => '*Terms and Conditions apply ';

  @override
  String get expressionPocketCard => 'Expression Pocket card';

  @override
  String get blockYourPhysicalCard => 'Block your Physical Card';

  @override
  String get more => 'More..';

  @override
  String get completeKYC => 'Complete KYC';

  @override
  String get kycComplete => 'KYC InComplete';

  @override
  String get dontShowMeAgain => 'Don’t Show me again';

  @override
  String get viewStatement => 'View Statement';

  @override
  String get kycCompleteDescription => '''
Your wallet is minimum KYC compliant, please complete full KYC to enjoy uninterrupted services.''';

  @override
  String get welcomeHomeScreen => 'Welcome';

  @override
  String get welcomeHomeText => 'Home!';

  @override
  String get welcomeExisting =>
      'Your’re already an existing Pockets account holder.';

  @override
  String get setUpPasscode => 'Set up Passcode';
  @override
  String get skip => 'Skip';
  @override
  String get expressionCard => 'Expression Pocket Card';
  @override
  String get chooseTheme => 'Choose theme';
  @override
  String get selectTheme => 'Select Theme';

  @override
  String get storagePermissionDescriptionForDownload =>
      'If you deny this permission, downalod feature of Pockets App will not working';

  @override
  String get storagePermissionTitle => 'Requesting Storage Permission';

  @override
  String get allow => 'Allow';

  @override
  String get deny => 'Deny';
  @override
  String get lifeStyle => 'Lifestyle';
  @override
  String get outgoingTransfers => 'Outgoing Transfer';
  @override
  String get utilities => 'Utilities';
  @override
  String get homeExpenses => 'Home Expenses';
}
