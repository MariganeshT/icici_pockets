import 'package:icici/languages/app_languages.dart';
/*

// class LanguageHi extends Languages {
//   @override
//   String get appName => "आईसीआईसीआई पॉकेट्स";

//   @override
//   String get labelWelcome => "आपने कई बार यह बटन दबाया हैे:";

//   @override
//   String get labelSelectLanguage => "भाषा का चयन करें";

//   @override
//   String get showPopUp => "पॉपअप दिखाएं";

//   @override
//   String get setLightTheme => "लाइट थीम सेट करें";

  @override
  String get setDarkTheme => 'डार्क थीम सेट करें';

  @override
  String get asPerPanCard => 'एस्परपैनकार्ड';
  @override
  String get personalDetails => 'व्यक्तिगत विवरण';

  @override
  String get dateOfBirth => 'जन्म की तारीख';

  @override
  String get male => 'पुरुष';

  @override
  String get female => 'महिला';

  @override
  String get transGender => 'ट्रांसजेंडर';

  @override
  String get personalDetailsReq => 'व्यक्तिगत विवरण की आवश्यकता';

  @override
  // ignore: lines_longer_than_80_chars
  String get personalDetailsDes =>
      'आगे बढ़ने से पहले, हमें आपका खाता सेटअप करने के लिए कुछ महत्वपूर्ण चीज़ों की आवश्यकता है';

  @override
  String get continueText => 'जारी रखें';

  @override
  String get verify => 'सत्यापित करें';
}
*/
