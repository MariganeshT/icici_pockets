package com.example.icici

/*
import android.annotation.SuppressLint
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import android.os.Build.VERSION
import android.os.Build.VERSION_CODES
import android.telephony.SubscriptionInfo
import android.telephony.SubscriptionManager
import android.telephony.TelephonyManager
import androidx.annotation.RequiresApi
import io.flutter.embedding.android.FlutterFragmentActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant


class MainActivity : FlutterFragmentActivity() {
    private val CHANNEL = "icici.flutter.methodChannel"
    private val SMSCHANNEL = "sendSms"
    private val callResult: MethodChannel.Result? = null

    @SuppressLint("NewApi")
    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine)
        MethodChannel(
            flutterEngine.dartExecutor.binaryMessenger,
            CHANNEL
        ).setMethodCallHandler { call, result ->
            when (call.method) {
                "activeSubscriptionInfoList" -> {
                    val results = try {
    getSIMModules();
} catch (e: Exception) {
    Toast.makeText(
            applicationContext, "SIM's not available",
            Toast.LENGTH_SHORT
    ).show()
}
                    result.success(results)
                }
                "SMS" -> {
                    call.argument<String>("selectedSimSlotName")?.let { name ->
                        call.argument<Int>("selectedSimSlotNumber")?.let {
                            SmsUtils().sendSMS(
                                applicationContext, "+919585313659", name, it
                            )
                        }
                    }
                }
                else -> {
                    result.notImplemented()
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    @RequiresApi(VERSION_CODES.LOLLIPOP_MR1)
    private fun getSIMModules(): ArrayList<String> {
        val localSubscriptionManager =
            getSystemService(TELEPHONY_SUBSCRIPTION_SERVICE) as SubscriptionManager
        val values = ArrayList<String>()
        val _sb: List<SubscriptionInfo> =
            SubscriptionManager.from(applicationContext).activeSubscriptionInfoList
        if (localSubscriptionManager.activeSubscriptionInfoCount > 1) {
            for (element in _sb) {
                values.add(element.displayName.toString())
            }
        } else {
            val tManager = baseContext
                .getSystemService(TELEPHONY_SERVICE) as TelephonyManager
            values.add(tManager.networkOperatorName)
        }


        return values
    }

    private fun getBatteryLevel(): Int {
        val batteryLevel: Int
        if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
            val batteryManager = getSystemService(Context.BATTERY_SERVICE) as BatteryManager
            batteryLevel = batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)
        } else {
            val intent = ContextWrapper(applicationContext).registerReceiver(
                null,
                IntentFilter(Intent.ACTION_BATTERY_CHANGED)
            )
            batteryLevel =
                intent!!.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) * 100 / intent.getIntExtra(
                    BatteryManager.EXTRA_SCALE,
                    -1
                )
        }
        return batteryLevel
    }
}
*/


import android.annotation.SuppressLint
import android.os.Build.VERSION_CODES
import android.telephony.SubscriptionInfo
import android.telephony.SubscriptionManager
import android.telephony.TelephonyManager
import android.widget.Toast
import androidx.annotation.RequiresApi
import io.flutter.embedding.android.FlutterFragmentActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant

class MainActivity : FlutterFragmentActivity() {
    private val CHANNEL = "icici.flutter.methodChannel"

    @RequiresApi(VERSION_CODES.M)
    @SuppressLint("ServiceCast", "HardwareIds")
    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine)
        MethodChannel(
                flutterEngine.dartExecutor.binaryMessenger,
                CHANNEL
        ).setMethodCallHandler { call, result ->
            when (call.method) {
                "activeSubscriptionInfoList" -> {
                    val results = getSIMModules()
                    result.success(results)
                }
                "SMS" -> {
                    call.argument<String>("selectedSimSlotName")?.let { name ->
                        call.argument<Int>("selectedSimSlotNumber")?.let {
                            result.success(SmsUtils().sendSMS(
                                    applicationContext, "+919585313659", name, it
                            ))
                        }
                    }
                }
                else -> {
                    result.notImplemented()
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    @RequiresApi(VERSION_CODES.LOLLIPOP_MR1)
    private fun getSIMModules(): ArrayList<String> {
        val values = ArrayList<String>()
        try {
            val localSubscriptionManager =
                    getSystemService(TELEPHONY_SUBSCRIPTION_SERVICE) as SubscriptionManager

            val _sb: List<SubscriptionInfo> =
                    SubscriptionManager.from(applicationContext).activeSubscriptionInfoList
            if (localSubscriptionManager.activeSubscriptionInfoCount > 1) {
                for (element in _sb) {
                    values.add(element.displayName.toString())
                }
            } else {
                val tManager = baseContext
                        .getSystemService(TELEPHONY_SERVICE) as TelephonyManager
                values.add(tManager.networkOperatorName)
            }
        } catch (e: Exception) {
            Toast.makeText(
                    applicationContext, "SIM's not available",
                    Toast.LENGTH_SHORT
            ).show()
        }
        return values
    }
}
